<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\ProgramResource;
use Illuminate\Http\Request;
use App\Galuz;

class Program extends Model
{
   	protected $table = 'programs';
   	public $primaryKey  = 'id_program';

    protected $guarded = [];

    public static $columns = [
            0 => 'id_program',
            1 => 'name_program',
            2 => 'created_at',
        ];

    static public function getListForDatatable(Request $request)
    {
    	$where = [];
        $search = $request->input('search.value');
        if ($request->input('galuz_id')){ $where = [['galuzes_program', 'like', '%'.Galuz::find($request->input('galuz_id'))->name_galuz.'%']];}
        $totalData = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->where($where)
            ->count();

        $totalFiltered = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id_program', '=', (int)$search, 'or'],
                            ['name_program', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->where($where)
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id_program', '=', (int)$search, 'or'],
                            ['name_program', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->where($where)
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => ProgramResource::collection($models),
        ];

        return $json_data;
    }


    public function getGaluzesList()
    {
    	$result = '';
        foreach (json_decode($this->galuzes_program) as $galuz_program){
            $result .= '<span class="galuz-current-program" data-id-galuz="'. $galuz_program->id_galuz .'">'. $galuz_program->name_galuz .'</span><br>';
        }
    	return $result;
    }


    public function getTree()
    {
        $result = '';
        foreach (json_decode( $this->content_program ) as $line){
            $result .= '<div class="element line">';
            $result .= '<div class="card-header header-elements-inline bg-grey-300">';
            $result .= '<div class="card-title">' . $line->name_line . '</div>';
            $result .= '<div class="header-elements"><div class="list-icons">';
            $result .= '<a class="list-icons-item element-toggle" data-action="toggle"><i class="icon-arrow-down5"></i></a>';
            $result .= '<a class="list-icons-item modal_value" data-action="delete"><i class="icon-pencil7"></i></a>';
            $result .= '<a class="list-icons-item modal_value" data-action="delete"><i class="icon-googleplus5"></i></a>';
            $result .= '<a class="list-icons-item modal_value" data-action="delete"><i class="icon-gallery"></i></a>';
            $result .= '<a class="list-icons-item element-remove" data-action="delete"><i class="icon-cross3"></i></a>';
            $result .= '</div></div>';
            $result .= '</div>';
            $result .= '<div class="element-container problems">';
                foreach ($line->problems as $problem){
                    $result .= '<div class="element problem">';
                    $result .= '<div class="card-header header-elements-inline bg-green-300">';
                    $result .= '<div class="card-title">' . $problem->name_problem . '</div>';
                    $result .= '<div class="header-elements"><div class="list-icons">';
                    $result .= '<a class="list-icons-item element-toggle" data-action="toggle"><i class="icon-arrow-down5"></i></a>';
                    $result .= '<a class="list-icons-item modal_value" data-action="delete"><i class="icon-pencil7"></i></a>';
                    $result .= '<a class="list-icons-item element-remove" data-action="delete"><i class="icon-cross3"></i></a>';
                    $result .= '</div></div>';
                    $result .= '</div>';
                    $result .= '<div class="element-container opises">';
                        foreach ($problem->opises as $opis){
                            $result .= '<div class="element opis">';
                            $result .= '<div class="card-header header-elements-inline bg-orange-300">';
                            $result .= '<div class="card-title">' . $opis->name_opis . '</div>';
                            $result .= '<div class="header-elements"><div class="list-icons">';
                            $result .= '<a class="list-icons-item element-toggle" data-action="toggle"><i class="icon-arrow-down5"></i></a>';
                            $result .= '<a class="list-icons-item modal_value" data-action="delete"><i class="icon-pencil7"></i></a>';
                            $result .= '<a class="list-icons-item element-remove" data-action="delete"><i class="icon-cross3"></i></a>';
                            $result .= '</div></div>';
                            $result .= '</div>';
                            $result .= '<div class="element-container">';
                                    /*$result .= '<div class="element kors">';
                                    $result .= '<div class="card-header header-elements-inline bg-gray">';
                                    $result .= '<table class="table table-striped text-dark w-100">';
                                    $result .= '<tr>';
                                    $result .= '<th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Код&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>';
                                    $result .= '<th>Очікуваний результат</th>';
                                    $result .= '<th>Інструмент</th>';
                                    $result .= '<th></th>';
                                    $result .= '</tr>';
                                    foreach ($opis->kors as $kor){
                                        $result .= '<tr>';
                                        $result .= '<th>'.$kor->kod.'</th>';
                                        $result .= '<th>'.$kor->name.'</th>';
                                        $result .= '<th>'.$kor->tool.'</th>';
                                        $result .= '<th><button class="btn btn-danger remove-item">X</button></th>';
                                        $result .= '</tr>';
                                    }
                                    $result .= '</table>';
                                    $result .= '</div>';
                                    $result .= '</div>';
                                    $result .= '<div class="empty-container"></div>';*/
                            $result .= '</div>';
                            $result .= '</div>';
                        }
                    $result .= '</div>';
                    $result .= '</div>';
                }
            $result .=  '</div>';
            $result .= '</div>';
        }
        return $result;
    }
}