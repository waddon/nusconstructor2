<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\NoteResource;
use Illuminate\Http\Request;
use Hexa;

class Note extends Model
{
    protected $table = 'notes';
    protected $guarded = [];
    public $primaryKey  = 'id_note';
    public $timestamps = FALSE;

    public function cikl()
    {
        return $this->belongsTo('App\Cikl','id_cikl','id_cikl');
    }


    public static $columns = [
            0 => 'id_note',
            1 => 'text_note',
        ];

    static public function getListForDatatable(Request $request)
    {
        $search = $request->input('search.value');
        $totalData = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();

        $totalFiltered = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id_note', '=', (int)$search, 'or'],
                            ['text_note', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id_note', '=', (int)$search, 'or'],
                            ['text_note', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => NoteResource::collection($models),
        ];

        return $json_data;
    }


    public function getExcerpt()
    {
        $string = strip_tags($this->text_note);
        $excerpt = mb_substr($string, 0, mb_strpos(Hexa::mb_wordwrap($string, 300), "\n"));
        return strlen($string) == strlen($excerpt) ? $string : $excerpt . '...';
    }
}
