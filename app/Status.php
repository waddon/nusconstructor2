<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\StatusResource;
use Illuminate\Http\Request;

class Status extends Model
{
    protected $table = 'statuses';
    protected $guarded = [];
    public $primaryKey  = 'id_status';    
    public $timestamps = FALSE;


    public static $columns = [
            0 => 'id_status',
            1 => 'name_status',
        ];

    static public function getListForDatatable(Request $request)
    {
        $search = $request->input('search.value');
        $totalData = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();

        $totalFiltered = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id_status', '=', (int)$search, 'or'],
                            ['name_status', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id_status', '=', (int)$search, 'or'],
                            ['name_status', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => StatusResource::collection($models),
        ];

        return $json_data;
    }

}
