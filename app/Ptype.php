<?php

namespace App;
use App\Http\Resources\PtypeResource;
use Illuminate\Http\Request;

use Illuminate\Database\Eloquent\Model;

class Ptype extends Model
{
    protected $table = 'ptypes';
    protected $guarded = [];
    public $primaryKey  = 'id_ptype';    
    public $timestamps = FALSE;

    public static $columns = [
            0 => 'id_ptype',
            1 => 'name_ptype',
        ];

    static public function getListForDatatable(Request $request)
    {
        $search = $request->input('search.value');
        $totalData = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();

        $totalFiltered = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id_ptype', '=', (int)$search, 'or'],
                            ['name_ptype', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id_ptype', '=', (int)$search, 'or'],
                            ['name_ptype', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => PtypeResource::collection($models),
        ];

        return $json_data;
    }


}
