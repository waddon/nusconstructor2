<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;

use Spatie\Permission\Traits\HasRoles;

use App\Models\Cperiod;

class User extends Authenticatable
{
    // protected $connection = 'mysql';
    use HasRoles;
    use SoftDeletes;
    use Notifiable;

    protected $table = 'table_users';
    protected $guarded = [];

    public static $columns = [
            0 => 'id',
            1 => 'name',
            2 => 'email',
            3 => 'roles',
            4 => 'created_at',
        ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'data' => 'object',
    ];

    public function schools()
    {
        return $this->belongsToMany('App\School');
    }

    public function details()
    {
        return $this->hasMany('App\Detail');
    }

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public function serts()
    {
        return $this->hasMany('App\Order')->where('show','=',true);
    }

    public function teams()
    {
        // return $this->belongsToMany('App\Models\Team');
        return $this->belongsToMany('App\Models\Team')->withPivot(['teamrole_id','meta'])->using('App\Models\CustomPivot')->orderBy('team_user.teamrole_id');
    }

    public function newprograms()
    {
        return $this->belongsToMany('App\Models\Newprogram');
    }

    public function tloads()
    {
        return $this->morphMany('App\Models\Tload', 'loadable');
    }


    static public function getListForDatatable(Request $request)
    {
        $role_id = $request->role_id ? $request->role_id : 0;
        $search = $request->input('search.value');
        $totalData = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();

        $totalFiltered = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or'],
                            ['second_name', 'like', "%{$search}%", 'or'],
                            ['patronymic', 'like', "%{$search}%", 'or'],
                            ['email', 'like', "%{$search}%", 'or'],
                        ]);
                })
            ->when( $role_id,  function($query) use ($role_id) {
                    return $query->whereHas('roles', function($q) use ($role_id){
                        $q->where('id', '=', $role_id);
                    });
                })
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or'],
                            ['second_name', 'like', "%{$search}%", 'or'],
                            ['patronymic', 'like', "%{$search}%", 'or'],
                            ['email', 'like', "%{$search}%", 'or'],
                        ]);
                })
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( $role_id,  function($query) use ($role_id) {
                    return $query->whereHas('roles', function($q) use ($role_id){
                        $q->where('id', '=', $role_id);
                    });
                })
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => UserResource::collection($models),
        ];

        return $json_data;
    }

    public function getAvatarPath()
    {
        return $this->thumb != '' ? '/storage/' . $this->thumb : '/img/anonymous.png';
    }

    public function combineName()
    {
        return $this->name . ' ' . $this->second_name;
    }


    # возвращает список активных периодов планирования
    public function getActivePlaningPeriods()
    {
        # позже добавить условие наличия шаблонов учебной нагрузки
        return Cperiod::has('loadtemplates')->get();
    }


    # Возвращает список валидных навантажень
    public function getValidTloads($cperiod_id)
    {
        $tloads = $this->tloads()->where('cperiod_id','=',$cperiod_id)->get();
        foreach ($tloads as $key => $tload) {
            if (!$tload->validate()) unset($tloads[$key]);
        }
        return $tloads;
    }

}
