<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\ClassResource;
use Illuminate\Http\Request;

class SchoolClass extends Model
{
    protected $table = 'classes';
    protected $guarded = [];
    public $primaryKey  = 'id_class';
    public $timestamps = FALSE;

    public function cikl()
    {
        return $this->belongsTo('App\Cikl','id_cikl','id_cikl');
    }


    public static $columns = [
            0 => 'id_class',
            1 => 'name_class',
        ];

    static public function getListForDatatable(Request $request)
    {
        $search = $request->input('search.value');
        $totalData = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();

        $totalFiltered = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id_class', '=', (int)$search, 'or'],
                            ['name_class', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id_class', '=', (int)$search, 'or'],
                            ['name_class', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => ClassResource::collection($models),
        ];

        return $json_data;
    }

}