<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\RivenResource;
use Illuminate\Http\Request;

class Riven extends Model
{
    protected $table = 'rivens';
    protected $guarded = [];
    public $primaryKey  = 'id_riven';
    public $timestamps = FALSE;

    public function cikls()
    {
        return $this->hasMany('App\Cikl', 'id_riven', 'id_riven');
    }

    public static $columns = [
            0 => 'id_riven',
            1 => 'name_riven',
        ];

    static public function getListForDatatable(Request $request)
    {
        $search = $request->input('search.value');
        $totalData = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();

        $totalFiltered = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id_riven', '=', (int)$search, 'or'],
                            ['name_riven', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id_riven', '=', (int)$search, 'or'],
                            ['name_riven', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => RivenResource::collection($models),
        ];

        return $json_data;
    }

}
