<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\LayoutResource;
use Illuminate\Http\Request;

use App\Classes\Socket\Pusher;

class Layout extends Model
{

    /*
     * Close all connections
     **/
    public function closeAllConnections($attr)
    {
        $data = [
            'topic_id' => 'layout-' . $this->id,
            // 'data' => 'someData: ' . rand(1,100),
            'command' => ( isset($attr['command']) && $attr['command'] ) ? $attr['command'] : '---',
            'owner' => ( isset($attr['owner']) && $attr['owner'] ) ? $attr['owner'] : '---',
        ];
        Pusher::sentDataToServer($data);
        return true;
    }

}