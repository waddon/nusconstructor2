<?php

namespace App\Http\Livewire\User;

use Livewire\Component;
use App\Models\Team;

class Teams extends Component
{
    protected $listeners = [
        'execAction' => 'execAction',
        'refresh' => '$refresh',
    ];

    public $teamId = 0;
    public $temp;


    public function execAction($attr)
    {
        $targetModelAction = isset($attr['action']) ? $attr['action'] : 'undefined';
        $value = isset($attr['value']) ? $attr['value'] : 'undefined';

        switch ($targetModelAction) {
            case 'destroy': $this->destroy();break;
            case 'leave': $this->leave();break;
            case 'store': $this->store($value);break;
        }
    }


    public function destroy()
    {
        $team = Team::find($this->teamId);
        if($team && $team->isLeader(auth()->user()->id)) {
            $team->delete();
        }
    }


    public function store($value)
    {
        $team = Team::find($this->teamId);
        if($team && $team->isLeader(auth()->user()->id)) {
            $team->update(['name' => $value]);
        } else {
            $team = Team::create([
                'name' => $value,
                'ttype_id' => 2,
            ]);
            $users[auth()->user()->id] = ['teamrole_id' => 1, 'meta'=> (object)[]];
            $team->users()->sync($users);
        }
    }


    public function leave()
    {
        auth()->user()->teams()->detach($this->teamId);
    }


    public function render()
    {
        return view('livewire.user.teams');
    }
}
