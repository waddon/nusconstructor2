<?php

namespace App\Http\Livewire\Common;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Team;
use App\Models\Cycle;
use App\Models\Branch;
use App\Models\Newprogram;

class ProgramList extends Component
{

    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    protected $listeners = [
        'execAction' => 'execAction',
        'refresh' => '$refresh',
    ];
    private $possibleTabs = [
            'active' => [
                    'title' => 'MAIN.TAB.DEVELOPING',
                    'icon' => 'icon-hammer-wrench',
                    'buttons' => ['edit', 'delete'],
                ],
            'approving' => [
                    'title' => 'MAIN.TAB.APPROVING',
                    'icon' => 'icon-library2',
                    'buttons' => ['edit', 'delete'],
                ],
            'published' => [
                    'title' => 'MAIN.TAB.PUBLISHED',
                    'icon' => 'icon-checkmark',
                    'buttons' => ['edit', 'delete'],
                ],
            'deleted' => [
                    'title' => 'MAIN.TAB.TRASH',
                    'icon' => 'icon-trash',
                    'buttons' => ['restore'],
                ],
        ];

    public $filterData;
    public $modelId;
    public $tabs = [];

    public $countVariants = [2, 5, 10, 25, 50, 100];
    public $perPage = 10;
    public $activeTab = null;
    public $filterCycle = 0;
    public $filterBranch = 0;
    public $search;

    public function mount($filterData)
    {
        $this->filterData = $filterData;
        if(isset($filterData['tabs']) && is_array($filterData['tabs'])) {
            foreach ($filterData['tabs'] as $tabName) {
                if(isset($this->possibleTabs[$tabName])) {
                    $this->tabs[] = [
                        'name' => $tabName,
                        'title' => __($this->possibleTabs[$tabName]['title']),
                        'icon' => $this->possibleTabs[$tabName]['icon'],
                        'buttons' => $this->possibleTabs[$tabName]['buttons'],
                    ];
                }
                $this->activeTab = $this->activeTab ?? $tabName;
            }
        }
    }


    public function updatingActiveTab()
    {
        $this->resetPage();
    }


    public function updatingPerPage()
    {
        $this->resetPage();
    }


    public function execAction($attr)
    {
        $targetModelAction = isset($attr['action']) ? $attr['action'] : 'undefined';
        $value = isset($attr['value']) ? $attr['value'] : 'undefined';

        switch ($targetModelAction) {
            case 'delete': $this->delete();break;
            case 'restore': $this->restore();break;
            case 'destroy': $this->destroy();break;
            case 'clone': $this->clone();break;
        }
    }


    public function delete()
    {
        $model = Newprogram::find($this->modelId);
        if ($model) $model->delete();
    }


    public function restore()
    {
        $model = Newprogram::withTrashed()->find($this->modelId);
        if ($model) $model->restore();
    }


    public function destroy()
    {
        $model = Newprogram::withTrashed()->find($this->modelId);
        if ($model) $model->forceDelete();
    }


    public function clone()
    {
        $model = Newprogram::find($this->modelId);
        if ($model) $model->clone();
    }


    public function render()
    {
        $search = $this->search;
        $filterCycle = $this->filterCycle;
        $filterBranch = $this->filterBranch;
        $models = Newprogram::query()
            ->owner($this->filterData['owner'], $this->filterData['id'] ?? 0)
            ->type($this->filterData['type'])
            ->tab($this->activeTab)
            ->when($search, function($query) use ($search){
                return $query->where('name', 'like', '%' . $search . '%');
            })
            ->when($filterCycle, function($query) use ($filterCycle){
                return $query->where('cycle_id', '=', $filterCycle);
            })
            ->when( $filterBranch,  function($query) use ($filterBranch) {
                    return $query->whereHas('branches', function($q) use ($filterBranch){
                        $q->where('id', '=', $filterBranch);
                    });
                })
            ->paginate($this->perPage);
        return view('livewire.common.program-list',[
            'models' => $models,
            'cycles' => Cycle::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-CYCLES'),0),
            'cycles2' => Cycle::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.SELECT-CYCLE'),0),
            'branches' => Branch::get(),
            'branches2' => Branch::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-BRANCHES'),0),
        ]);
    }
}
