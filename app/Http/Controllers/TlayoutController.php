<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreLayoutRequest;
use App\Http\Controllers\Controller;

use App\Models\Team;
use App\Models\Layout;
use App\Models\Cycle;
use App\Models\Period;
use App\Models\Branch;
use App\Models\Line;

class TlayoutController extends Controller {

    public function __construct()
    {
        $this->middleware('accessLayout', ['only' => ['edit', 'update', 'delete', 'restore', 'destroy','catch']]);
        $this->middleware('canEditLayout', ['only' => ['edit', 'update', 'delete', 'restore', 'destroy','catch']]);
    }


    public function index(Request $request, $team_id)
    {
        $team = Team::findOrFail($team_id);
        return view('team.tlayouts.index', [
            'team' => $team,
            'cycles' => Cycle::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-CYCLES'),0),
            'cycles2' => Cycle::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.SELECT-CYCLE'),0),
            'periods' => Period::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-PERIODS'),0),
            'periods2' => Period::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.SELECT-PERIOD'),0),
            'branches' => Branch::get(),
            'branches2' => Branch::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-BRANCHES'),0),
        ]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create() {
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(StoreLayoutRequest $request, $team_id)
    {

        $inputs = $request->all();
        info($team_id);
        $array = [
            'name' => __('MAIN.TITLE.NEW-LAYOUT'),
            'cycle_id' => $request->cycle_id,
            'period_id' => $request->period_id,
            'meta' => (object)[
                'editors' => [],
            ],
            'content' => (object)[],
        ];
        $model = Layout::create($array);
        $model->teams()->sync([ $team_id => ['meta' => (object)['editors'=>[auth()->user()->id]]] ]);
        $model->branches()->sync( $request->branches );

        return response([
            'relocateURL' => route('tlayouts.edit', [$team_id, $model->id]),
        ], 200);
    }



    public function show(Request $request, $team_id, $id)
    {
        $model = Layout::findOrFail($id);

        return view('team.layouts.show', []);
    }


    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($team_id, $id) {
        $model = Layout::findOrFail($id);
        $model->previousUrl = route('tlayouts.index',$team_id);
        return view('team.tlayouts.form', [
                'team' => Team::findOrFail($team_id),
                'model'=>$model,
                'lines' => Line::get(),
                'periods' => Period::get(),
            ]);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $team_id, $id)
    {
        $model = Layout::findOrFail($id); //Get role specified by id

        $array = [
            'name' => $request->name ? $request->name : __('MAIN.TITLE.NEW-LAYOUT'),
            'content' => $request->layout ? json_decode($request->layout) : (object)[],
        ];

        $model->update($array);

        return response([
            'message' => __('MAIN.MESSAGE.UPDATED'),
            'exitURL' => route('tlayouts.index',$team_id),
            // 'formAction' => route('tlayouts.update', [$team_id, $model->id]),
        ], 200);
    }


    /**
     * Move to trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $team_id, $id)
    {
        $model = Layout::withTrashed()->findOrFail($id);
        $model->closeAllConnections( ['command'=>'interruption'] );
        $model->delete();
        return response([], 200);
    }

    /**
     * Restore from trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore(Request $request, $team_id, $id)
    {
        $model = Layout::onlyTrashed()->findOrFail($id);
        $model->restore();
        return response([], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $team_id, $id)
    {
        $model = Layout::withTrashed()->findOrFail($id);
        $model->forceDelete();
        return response([], 200);
    }

    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Layout::getListForDatatable($request, true) );
    }


    /**
     * Catch layout. Close all connections except owner
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function catch(Request $request, $team_id, $id)
    {
        $model = Layout::withTrashed()->findOrFail($id);
        $model->closeAllConnections( ['command'=>$request->command, 'owner' => $request->owner] );
        return json_encode(['result'=>true]);
    }


    /**
     * Share layout to teams
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function share(Request $request, $team_id, $id)
    {
        $model = Layout::withTrashed()->findOrFail($id);
        $pivot = $model->teams()->where('id','=',$team_id)->first()->pivot;
        $meta = $pivot->meta;
        $int_array = [];
        if ($request->editors){
            foreach ($request->editors as $key => $editor) {
                $int_array[] = (int)$editor;
            }
        }
        $meta->editors = $int_array;
        $pivot->meta = $meta;
        $pivot->save();
        return response([ 'result' => true, 'editors' => $request->editors ], 200);
    }

    public function send(Request $request, $team_id, $id)
    {
        $model = Layout::findOrFail($id);
        if ($model->canSendToApprove()){
            // $model->status = 'Approving';
            $model->status = 'Approved';
            $model->save();
            return response([ 'result' => true, 'editors' => $request->editors ], 200);
        } else {
            return response([ 'message' => __('MAIN.MESSAGE.LAYOUT-SEND-ERROR') ], 403);
        }
    }

    public function clone(Request $request, $team_id, $id)
    {
        $model = Layout::findOrFail($id);
        $team = Team::findOrFail($team_id);

        $new_name = mb_substr( '[' . __('MAIN.LABEL.COPY') . '] '  . $model->name, 0, 191);
        $new_model = $model->replicate();
        $new_model->name = $new_name;
        $new_model->status = 'Editing';
        $new_model->public = 0;
        $new_model->save();
        $branches = $model->branches->pluck('id','id');
        $new_model->branches()->sync( $branches );       
        foreach ($model->teams as $key => $team) {
            $teams[$team->id] = ['meta' => $team->pivot->meta];
        }
        $new_model->teams()->sync( $teams );
        $pivot = $new_model->teams()->find($team->id)->pivot;
        $pivot->save();

        return response([ 'message' => __('MAIN.MESSAGE.CLONED') ], 200);
    }

}
