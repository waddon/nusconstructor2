<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreProgramRequest;
use App\Http\Controllers\Controller;
use App\Program;
use App\Galuz;
use App\Tool;

class ProgramController extends Controller {

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request) {
        return view('user.programs.index', [
            'galuzes' => Galuz::pluck('name_galuz','id_galuz')->prepend(__('MAIN.PLACEHOLDER.ALL-GALUZES'),0),
        ]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create() {
        $model = new Program;
        $model->content_program = '{}';
        return view('user.programs.form', [
                'model' => $model,
                'tools'=>Tool::get(),
            ]);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(StoreStatusRequest $request) {

        $credentials = $request->only('name', 'color');
        $model = Program::create($credentials);
        $model->roles()->sync($request->roles);

        return response([
            'message' => __('Element created successfully'),
            'exitURL' => route('programs.index'),
            'formAction' => route('programs.update', $model->id),
        ], 200);
    }



    public function show(Request $request, $id)
    {
    }


    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id) {
        $model = Program::findOrFail($id);
        return view('user.programs.form', [
                'model'=>$model,
                'tools'=>Tool::get(),
            ]);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(StoreStatusRequest $request, $id)
    {
        $model = Program::findOrFail($id); //Get role specified by id

        $credentials = $request->only('name', 'color');
        $model->update($credentials);
        $model->roles()->sync($request->roles);

        return response([
            'message' => __('Element updated successfully'),
            'exitURL' => route('programs.index'),
            'formAction' => route('programs.update', $model->id),
        ], 200);
    }



    /**
     * Restore from trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore(Request $request, $id)
    {
        $model = Program::onlyTrashed()->findOrFail($id);
        $model->restore();
        return response([], 200);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Program::findOrFail($id);
        $model->delete();
        return response([], 200);
    }



    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Program::getListForDatatable($request) );
    }

}
