<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Option;
use App\Post;
use App\Page;
use App\Models\Level;
use App\Models\Cycle;
use App\Models\Branch;
use App\Models\Expect;
use App\Models\Description;
use App\Models\Newprogram;
use App\Models\Newplan;
use App\Models\Newpackage;
use App\Models\Layout;

class GeneralController extends Controller
{

    /**
     * Show the application homepage.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        return view('home',[
                'news' => Post::where([['id_cat','=',1]])->limit(10)->orderBy('created_at','DESC')->get(),
                'pageAbout' => Page::where('id_page','=',Option::getOption('pageAbout'))->first(),
                'options' => Option::pluck('value','key'),
            ]);
    }


    /**
     * Show the application glossary.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function glossary(Request $request, $character = 'А')
    {
        $symbols = [
            "cyr0" =>"А","cyr1" =>"Б","cyr2" =>"В","cyr3" =>"Г","cyr4" =>"Д","cyr5" =>"Е","cyr6" =>"Ж","cyr7" =>"З","cyr8" =>"И",
            "cyr9" =>"Й","cyr10"=>"К","cyr11"=>"Л","cyr12"=>"М","cyr13"=>"Н","cyr14"=>"О","cyr15"=>"П","cyr16"=>"Р","cyr17"=>"С",
            "cyr18"=>"Т","cyr19"=>"У","cyr20"=>"Ф","cyr21"=>"Х","cyr22"=>"Ц","cyr23"=>"Ч","cyr24"=>"Ш","cyr25"=>"Щ","cyr26"=>"Ъ",
            "cyr27"=>"Ы","cyr28"=>"Ь","cyr29"=>"Э","cyr30"=>"Ю","cyr31"=>"Я"
        ];
        $character = isset($symbols[$character]) ? $symbols[$character] : $character;
        return view('public.glossary',[
            'character' => $character,
            'glossaries' => Post::where([['id_cat','=',6],['name_post','like', $character.'%']])->get(),
        ]);
    }

    /**
     * Show the application glossary.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function postGetContent(Request $request, $id)
    {
        $model = Post::find($id);
        return response([
            'title' => $model->name_post ? $model->name_post : 'Детальний опис - title',
            'body' => $model->content_post ? $model->content_post : 'Детальний опис - body',
        ],200);
    }


    /**
     * Show the application news.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function news(Request $request)
    {
        $models = Post::where([['id_cat','=',1]])->orderBy('created_at','DESC')->paginate(10);
        return view('public.news',[
            'models' => $models,
        ]);
    }


    /**
     * Show the application pages.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function pages(Request $request, $slag)
    {
        $model = Page::where([['slug_page','=',$slag]])->first();
        if ($model) {
            return view('public.page',[
                'model' => $model,
            ]);
        } else {
            abort(404);
        }
    }


    /**
     * Show the application levels.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function levels(Request $request)
    {
        return view('public.levels',[
            'models' => Level::get(),
        ]);
    }


    /**
     * Show the application levels.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function branches(Request $request)
    {
        return view('public.branches',[
            'models' => Branch::get(),
        ]);
    }


    /**
     * Show the application levels.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function expects(Request $request)
    {
        return view('public.expects',[
            'branches' => Branch::get(),
            'cycles' => Cycle::get(),
        ]);
    }


    /**
     * Show the application levels.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function getDataExpects(Request $request)
    {
        $models = Expect::select('expects.*', 'skills.name as skillName', 'skills.group')->where('expects.branch_id', $request->branch_id)->where('cycle_id', $request->cycle_id)->leftjoin('skills', 'expects.marking', '=', 'skills.marking')->where('skills.branch_id',$request->branch_id)->orderBy('skills.marking', 'asc')->get();        
        return response([
            'data' => 'Some data: branch - ' . $request->branch_id . ', cycle - ' . $request->cycle_id,
            'models' => $models,
        ],200);
    }


    /**
     * Show the application materials.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function materials(Request $request)
    {
        $models = Post::where([['id_cat','=',2]])->orderBy('id_post','DESC')->paginate(10);
        return view('public.materials',[
            'models' => $models,
        ]);
    }

    public function laws(Request $request)
    {
        $models = Post::where([['id_cat','=',3]])->orderBy('id_post','DESC')->paginate(10);
        return view('public.laws',[
            'models' => $models,
        ]);
    }
    public function regulations(Request $request)
    {
        $models = Post::where([['id_cat','=',4]])->orderBy('id_post','DESC')->paginate(10);
        return view('public.regulations',[
            'models' => $models,
        ]);
    }
    public function guidelines(Request $request)
    {
        $models = Post::where([['id_cat','=',5]])->orderBy('id_post','DESC')->paginate(10);
        return view('public.guidelines',[
            'models' => $models,
        ]);
    }

    /**
     * Show the application glossary.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function notes(Request $request)
    {
        return view('public.notes',[
            'cycles' => Cycle::get(),
        ]);
    }

    /**
     * Show the application levels.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function getCycle(Request $request)
    {
        $cycle = Cycle::find($request->cycle_id);
        return response([
            'cycle' => $cycle ? $cycle : new Cycle,
        ],200);
    }


    /**
     * Show the application levels.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function specifics(Request $request)
    {
        return view('public.specifics',[
            'branches' => Branch::get(),
            'cycles' => Cycle::get(),
        ]);
    }


    /**
     * Show the application levels.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function getDataSpecifics(Request $request)
    {
        // $result = '';
        $branch = Branch::findOrFail($request->branch_id);
        // foreach ($branch->lines as $line) {
        //     $result .= '<tr><td colspan="2" style="text-align:center;">Змістова лінія "' . $line->name . '"</td></tr>';
        // }
        // $res = [];
        $result = '';
        foreach ($branch->lines as $line) {
            $expects = Expect::select('expects.*')->distinct()
                ->join('specifics','expects.id','=', 'specifics.expect_id')
                ->where('branch_id', '=', $request->branch_id)
                ->where('cycle_id', '=', $request->cycle_id)
                ->where('specifics.line_id', '=', $line->id)
                ->orderBy('specifics.marking', 'ASC')
                ->get();
            if ($expects->count()>0) {
                $result .= '<tr><td colspan="2" style="text-align:center;"><h5><strong>Змістова лінія (група обов’язкових результатів навчання) "' . $line->name . '"</strong></h5></td></tr>';
                foreach ($expects as $expect) {
                    $result .= '<tr>';
                    $result .= '<td>'.$expect->name . '</td>';
                    $result .= '<td>';
                    foreach ($expect->specifics()->where('line_id','=',$line->id)->orderBy('marking','ASC')->get() as $specific) {
                        $result .= '- '.$specific->name.' <span class="badge text-white" style="background:'.$expect->branch->color.'">'.$specific->marking.'</span><br>';
                    }
                    $result .= '</td>';
                    $result .= '</tr>';
                }
                $result .= '<tr><td colspan="2">' . $line->content . '</td></tr>';
            }
        }
        // $res['result'] = $result;
        // $galuznote = Galuznote::where('id_galuz',$id_galuz)->where('id_cikl',$id_cikl)->first();
        // if (!$galuznote) {$galuznote = new Galuznote;}
        // $res['galuznote'] = $galuznote;

        $description = Description::where([['branch_id','=',$request->branch_id],['cycle_id','=',$request->cycle_id]])->first();
        if (!$description) {$description = new Description;}

        return response([
            'data' => $result,
            'description' => $description,
            // 'models' => $models,
        ],200);
    }


    /*
     * Отображение программы по уникальной ссылке
     */
    public function showprogram(Request $request, $key)
    {
        $model = Newprogram::where('key','=',$key)->first();
        $model = $model ? $model : new Newprogram;
        return view('public.newprograms.show', [
            'model' => $model,
        ]);
    }


    /*
     * Отображение программы по уникальной ссылке
     */
    public function loadprogram(Request $request, $key = 'undefined')
    {
        $model = Newprogram::where('key','=',$key)->first();
        if (!$model) abort(404);
        // $model = $model ? $model : new Newprogram;
        return $model->getPDF(
            $model->name . '.pdf'
        );
    }


    /*
     * Отображение плана по уникальной ссылке
     */
    public function showplan(Request $request, $key)
    {
        $model = Newplan::where('key','=',$key)->first();
        $model = $model ? $model : new Newplan;
        return view('public.newplans.show', [
            'model' => $model,
            'branches' =>Branch::get(),
        ]);
    }


    /*
     * Отображение плана по уникальной ссылке
     */
    public function loadplan(Request $request, $key = 'undefined')
    {
        $model = Newplan::where('key','=',$key)->first();
        $model = $model ? $model : new Newplan;
        return $model->getPDF(
            $model->name . '.pdf'
        );
    }


    public function search(Request $request)
    {
        if (isset($request->search_text) && $request->search_text) {
            $models = Post::where('content_post','like', '%'.$request->search_text.'%')->orderBy('id_post','DESC')->paginate(10);
        } else {
            $models = [];
        }
        return view('public.search',[
            'models' => $models,
            'old_value' => isset($request->search_text) ? $request->search_text : '',
        ]);
    }

    /*
     * Отображение пакета по уникальной ссылке
     */
    public function showpackage(Request $request, $key)
    {
        $model = Newpackage::where('key','=',$key)->first();
        $model = $model ? $model : new Newpackage;
        return view('public.newpackages.show', [
            'model' => $model,
            'branches' => Branch::get(),
            'object' => $model->getUsedSpecifics(),
            // 'branches' => $model->getUsedSpecifics(),
        ]);
    }


    /*
     * Отображение шаблона программ по уникальной ссылке
     */
    public function showlayout(Request $request, $key)
    {
        $model = Layout::where('key','=',$key)->first();
        $model = $model ? $model : new Layout;
        return view('team.tlayouts.show', [
            'model' => $model,
        ]);
    }


    /*
     * Отображение плана по уникальной ссылке
     */
    public function loadlayout(Request $request, $key = 'undefined')
    {
        $model = Layout::where('key','=',$key)->first();
        $model = $model ? $model : new Layout;
        return $model->getPDF(
            $model->name . '.pdf'
        );
    }


    /*
     * Выгрузка программы в формате WORD
     */
    public function docExportProgram(Request $request, $key = 'undefined')
    {
        $model = Newprogram::where('key','=',$key)->first();
        $model = $model ? $model : new Newprogram;

        $headers = [
            "Content-type"=>"text/html",
            // "Content-Disposition"=>"attachment;Filename=" . $model->name . ".doc"
            "Content-Disposition"=>"attachment;Filename=program-" . $model->key . ".doc"
        ];
    
        $content = $model->getDOCContent();

        return response()->make($content, 200, $headers);
    }


    /**
     * Создание календарно-тематического плана
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  $id  идентификатор программы
     * @param  $teamId идентификатор команды. Если = 0, то создается в личных документах
     */
    public function createCurriculum(Request $request, $id = 0, $teamId = 0)
    {
        $teamId = ($request->cc_team_id && $request->owner == 'team') ? $request->cc_team_id : $teamId;
        switch ($teamId) {
            case 0:
                $model = Newprogram::find($id);
                if (!$model) return response(['message' => 'Program Not Found'], 404);
                $users = [auth()->user()->id];
                $teams = [];
                break;
            default:
                $team = auth()->user()->teams()->where('id', '=', $teamId)->first();
                if (!$team) return response(['message' => 'Program Not Found 1'], 404);
                $model = Newprogram::find($id);
                if (!$model) return response(['message' => 'Program Not Found 2'], 404);
                $users = [];
                $teams = [$teamId];
                break;
        }
        $newModel = $model->clone();
        $newModel->update(['parent_id' => $model->id, 'type' => 'curriculum']);
        $newModel->users()->sync($users);
        $newModel->teams()->sync($teams);
        info(__METHOD__ . ', program id: ' . $id . ', team id: ' . $teamId);
        return response(['message' => 'Календарно-тематичний план успішно створено'], 200);
    }

}
