<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreNewprogramRequest;
use App\Http\Requests\UpdateNewprogramRequest;
use App\Http\Requests\StoreNewprogramLayoutRequest;
use App\Http\Controllers\Controller;

use Storage;

use App\Models\Team;
use App\Models\Newprogram;
use App\Models\Cycle;
use App\Models\Period;
use App\Models\Branch;
use App\Models\Line;
use App\Models\Layout;
use App\Models\Specific;
use App\Tool;

class TnewprogramController extends Controller {

    public function __construct()
    {
        // $this->middleware('accessLayout', ['only' => ['edit', 'update', 'delete', 'restore', 'destroy','catch']]);
        // $this->middleware('canEditLayout', ['only' => ['edit', 'update', 'delete', 'restore', 'destroy','catch']]);
    }


    public function index(Request $request, $team_id)
    {
        return redirect()->route('tnewprograms.list', [$team_id, 'model']);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create() {
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(StoreNewprogramRequest $request, $team_id)
    {

        $inputs = $request->all();
        info($team_id);
        $array = [
            'name' => __('MAIN.TITLE.NEW-PROGRAM'),
            'cycle_id' => $request->cycle_id,
            'meta' => (object)[],
            'content' => (object)[],
        ];
        $model = Newprogram::create($array);
        $model->teams()->sync([ $team_id => ['meta' => (object)['editors'=>[auth()->user()->id]]] ]);
        $model->branches()->sync( $request->branches );

        return response([
            'relocateURL' => route('tnewprograms.edit', [$team_id, $model->id]),
        ], 200);
    }


    # Создание рпограммы на основе шаблона
    public function createOnLayout(StoreNewprogramLayoutRequest $request, $team_id)
    {
        $layout = Layout::findOrFail($request->layout_id);
        $content = (object)[];
        $i = 0;
        foreach ($layout->cycle->grades as $key => $grade) {
            foreach ($layout->period->meta as $key2 => $value) {
                $content->{$i} = (object)[
                        'name_line' => $grade->name . ', ' . $value,
                        'info_line' => '',
                        'grade_id' => $grade->id,
                        'period_element_id' => $key2,
                        'problems' => (object)[]
                    ];
                $i++;
            }
        }
        $array = [
            'name' => __('MAIN.TITLE.NEW-PROGRAM-LAYOUT'),
            'cycle_id' => $layout->cycle_id,
            'meta' => (object)[ 'layout' => $layout->id ],
            'content' => $content,
        ];
        $model = Newprogram::create($array);
        $model->teams()->sync([ $team_id => ['meta' => (object)['editors'=>[auth()->user()->id]]] ]);
        $branches = $layout->branches->pluck('id','id');
        $model->branches()->sync( $branches );

        return response([
            'relocateURL' => route('tnewprograms.edit', [$team_id, $model->id]),
        ], 200);
    }


    public function show(Request $request, $team_id, $id)
    {
        $model = Newprogram::findOrFail($id);
        $model->previousUrl = route('tnewprograms.index',$team_id);
        return view('public.newprograms.show', [
            'model' => $model,
        ]);
    }


    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($team_id, $id) {
        $model = Newprogram::findOrFail($id);
        $model->previousUrl = route('tnewprograms.index',$team_id);
        $view = (isset($model->meta->layout) && $model->meta->layout) ? 'public.newprograms.layout-form' : 'public.newprograms.form';
        return view( $view, [
                'model'=>$model,
                'tools'=>Tool::get(),
                'team' => Team::find($team_id),
            ]);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(UpdateNewprogramRequest $request, $team_id, $id)
    {
        $model = Newprogram::findOrFail($id);
        $meta = $model->meta;
        $meta->hours = $request->hours ? $request->hours : 0;

        # проверяем наличие пояснгительной записки
        if (isset($request->explanatory)) {
            $path = Storage::disk('public')->putFileAs( 'explanatory', $request->explanatory, 'explanatory_' . $model->id . '.' . $request->explanatory->extension() );
            $meta->explanatory = (object)[
                'filename' => $request->explanatory->getClientOriginalName(),
                'mimetype' => $request->explanatory->getMimeType(),
                'path' => $path,
            ];
        }

        $array = [
            'name' => $request->name ? $request->name : '',
            'content' => $request->program ? json_decode($request->program) : (object)[],
            'meta' => $meta,
        ];

        $model->update($array);

        return response([
            'message' => __('MAIN.MESSAGE.UPDATED'),
            'monitoring' => $model->selectedSpecificHTML(),
            'exitURL' => route('tnewprograms.index', $team_id),
        ], 200);
    }


    /**
     * Move to trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $team_id, $id)
    {
        $model = Newprogram::withTrashed()->findOrFail($id);
        $model->closeAllConnections( ['command'=>'interruption'] );
        $model->delete();
        return response([], 200);
    }

    /**
     * Restore from trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore(Request $request, $team_id, $id)
    {
        $model = Newprogram::onlyTrashed()->findOrFail($id);
        $model->restore();
        return response([], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $team_id, $id)
    {
        $model = Newprogram::withTrashed()->findOrFail($id);
        $model->forceDelete();
        return response([], 200);
    }

    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Newprogram::getListForDatatable($request, true) );
    }


    /**
     * Catch layout. Close all connections except owner
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function catch(Request $request, $team_id, $id)
    {
        $model = Newprogram::withTrashed()->findOrFail($id);
        $model->closeAllConnections( ['command'=>$request->command, 'owner' => $request->owner] );
        return json_encode(['result'=>true]);
    }


    /**
     * Share layout to teams
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function share(Request $request, $team_id, $id)
    {
        $model = Newprogram::withTrashed()->findOrFail($id);
        $pivot = $model->teams()->where('id','=',$team_id)->first()->pivot;
        $meta = $pivot->meta;
        $int_array = [];
        if ($request->editors){
            foreach ($request->editors as $key => $editor) {
                $int_array[] = (int)$editor;
            }
        }
        $meta->editors = $int_array;
        $pivot->meta = $meta;
        $pivot->save();
        return response([ 'result' => true, 'editors' => $request->editors ], 200);
    }


    /**
     * Публикация программы в общий доступ
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function send(Request $request, $team_id, $id)
    {
        $model = Newprogram::findOrFail($id);
        if ($model->sendToApprove()){
            return response([ 'result' => true ], 200);
        } else {
            return response([ 'message' => __('MAIN.MESSAGE.PROGRAM-SEND-ERROR') ], 403);
        }
    }


    /**
     * Изъятие из публичного доступа
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function withdraw(Request $request, $team_id, $id)
    {
        $model = Newprogram::findOrFail($id);
        $model->withdrawFromPublic();
        return response([ 'result' => true ], 200);
    }


    public function getSpecifics(Request $request, $team_id, $id)
    {
        $model = Newprogram::findOrFail($id);
        $layout = Layout::findOrFail( isset($model->meta->layout) ? $model->meta->layout : 0 );
        $specificIds = [];
        if (isset($layout->content->{$request->grade_id}->{$request->period_element_id}) && is_object($layout->content->{$request->grade_id}->{$request->period_element_id})) {
            foreach ($layout->content->{$request->grade_id}->{$request->period_element_id} as $key => $value) {
                if($value){
                    $specificIds[] = $key;
                }
            }
        }
        $specifics = Specific::whereIn('id', $specificIds)->get();
        foreach ($specifics as $key => $specific) {
            $specific->branch_id = $specific->expect->branch->id;
        }
        return response([
                'specifics' => $specifics,
                'result' => true , 
                'grade_id' => $request->grade_id ,
                'period_element_id' => $request->period_element_id,
            ], 200);
    }

    public function clone(Request $request, $team_id, $id)
    {
        // $model = Newprogram::findOrFail($id);
        // $team = Team::findOrFail($team_id);

        // $new_name = mb_substr( '[' . __('MAIN.LABEL.COPY') . '] '  . $model->name, 0, 191);
        // $new_model = $model->replicate();
        // $new_model->name = $new_name;
        // $new_model->status = 'Editing';
        // $new_model->public = 0;
        // $new_model->save();
        // $branches = $model->branches->pluck('id','id');
        // $new_model->branches()->sync( $branches );       
        // foreach ($model->teams as $key => $team) {
        //     $teams[$team->id] = ['meta' => $team->pivot->meta];
        // }
        // $new_model->teams()->sync( $teams );
        // $pivot = $new_model->teams()->find($team->id)->pivot;
        // $pivot->save();

        // return response([ 'message' => __('MAIN.MESSAGE.CLONED') ], 200);

        $model = Newprogram::findOrFail($id);
        $model->clone();
        return response([ 'result' => true ], 200);
    }


    /**
     * Индексная страница для различных типов программ
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function list(Request $request, $team_id, $type)
    {
        // $filterData = [
        //     'type' => $type,
        //     'owner' => 'team',
        //     'id' => $team_id,
        //     'tabs' => ['active', 'approving', 'published', 'deleted'],
        // ];
        // return view('common.program-list',[
        //     'title' => __('MAIN.TITLE.PROGRAM.' . mb_strtoupper($type)),
        //     'filterData' => $filterData,
        // ]);
        return view('team.tnewprograms.index', [
            'title' => __('MAIN.TITLE.PROGRAM.' . mb_strtoupper($type)),
            'team' => Team::findOrFail($team_id),
            'cycles' => Cycle::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-CYCLES'),0),
            'cycles2' => Cycle::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.SELECT-CYCLE'),0),
            // 'periods' => Period::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.SELECT-PERIOD'),0),
            'branches' => Branch::get(),
            'branches2' => Branch::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-BRANCHES'),0),
            'type' => $type,
        ]);

    }

}
