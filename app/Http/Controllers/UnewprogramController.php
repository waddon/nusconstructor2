<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreNewprogramRequest;
use App\Http\Requests\UpdateNewprogramRequest;
use App\Http\Controllers\Controller;

use Storage;

use App\Models\Newprogram;
use App\Models\Cycle;
use App\Models\Period;
use App\Models\Branch;
use App\Models\Line;
use App\Tool;

class UnewprogramController extends Controller {

    public function __construct()
    {
        // $this->middleware('log', ['only' => ['fooAction', 'barAction']]);
        // $this->middleware('accessLayout', ['only' => ['edit', 'update', 'delete', 'restore', 'destroy','catch']]);
    }


    public function index(Request $request) {
        return redirect()->route('unewprograms.list', 'model');
    }


    public function create() {
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(StoreNewprogramRequest $request)
    {
        $array = [
            'name' => __('MAIN.TITLE.NEW-PROGRAM'),
            'cycle_id' => $request->cycle_id,
            'meta' => (object)[],
            'content' => (object)[],
        ];
        if($request->type) {
            $array['type'] = 'standart';
        }
        $model = Newprogram::create($array);
        $model->users()->sync([ auth()->user()->id ]);
        $model->branches()->sync( $request->branches );

        return response([
            'relocateURL' => route('unewprograms.edit', $model->id),
        ], 200);
    }



    public function show(Request $request, $id)
    {
        $model = Newprogram::findOrFail($id);
        $model->previousUrl = route('unewprograms.index');
        return view('public.newprograms.show', [
            'model' => $model,
        ]);
    }


    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id) {
        $model = Newprogram::findOrFail($id);
        $model->previousUrl = route('unewprograms.list', $model->type);
        return view('public.newprograms.form', [
                'model'=>$model,
                'tools'=>Tool::get(),
            ]);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(UpdateNewprogramRequest $request, $id)
    {
        $model = Newprogram::findOrFail($id);
        $meta = $model->meta;
        $meta->hours = $request->hours ? $request->hours : 0;
        $meta->isTitle = $request->boolean('isTitle');
        $meta->isDescription = $request->boolean('isDescription');
        $meta->isProvisions = $request->boolean('isProvisions');
        $meta->title = $request->title;
        $meta->description = $request->description;
        $meta->provisions = $request->provisions;

        # проверяем наличие пояснгительной записки
        if (isset($request->explanatory)) {
            $path = Storage::disk('public')->putFileAs( 'explanatory', $request->explanatory, 'explanatory_' . $model->id . '.' . $request->explanatory->extension() );
            $meta->explanatory = (object)[
                'filename' => $request->explanatory->getClientOriginalName(),
                'mimetype' => $request->explanatory->getMimeType(),
                'path' => $path,
            ];
        }

        $array = [
            'name' => $request->name ? $request->name : '',
            'content' => $request->program ? json_decode($request->program) : (object)[],
            'meta' => $meta,
        ];
        $model->update($array);

        return response([
            'message' => __('MAIN.MESSAGE.UPDATED'),
            'exitURL' => route('unewprograms.list', $model->type),
        ], 200);
    }


    /**
     * Move to trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $model = Newprogram::withTrashed()->findOrFail($id);
        $model->closeAllConnections( ['command'=>'interruption'] );
        $model->delete();
        return response([], 200);
    }

    /**
     * Restore from trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore(Request $request, $id)
    {
        $model = Newprogram::onlyTrashed()->findOrFail($id);
        $model->restore();
        return response([], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Newprogram::withTrashed()->findOrFail($id);
        $model->forceDelete();
        return response([], 200);
    }

    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Newprogram::getListForDatatable($request, true) );
    }


    /**
     * Catch layout. Close all connections except owner
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function catch(Request $request, $id)
    {
        $model = Newprogram::withTrashed()->findOrFail($id);
        $model->closeAllConnections( ['command'=>$request->command, 'owner' => $request->owner] );
        return json_encode(['result'=>true]);
    }


    /**
     * Share layout to teams
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function share(Request $request, $id)
    {
        $model = Newprogram::withTrashed()->findOrFail($id);
        if ($request->team_id){
            $team = $model->teams()->where('id', '=', $request->team_id)->first();
            if (!$team) {
                $model->teams()->attach($request->team_id, ['meta' => (object)[ 'editors' => [] ] ]);
            }
        }
       return response(['result'=>true], 200);
    }


    /**
     * Индексная страница для различных типов программ
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function list(Request $request, $type)
    {
        // $filterData = [
        //     'type' => $type,
        //     'owner' => 'user',
        //     'id' => auth()->user()->id,
        //     'tabs' => ['active', 'approving', 'published', 'deleted'],
        // ];
        // return view('common.program-list',[
        //     'title' => __('MAIN.TITLE.PROGRAM.' . mb_strtoupper($type)),
        //     'filterData' => $filterData,
        // ]);
        return view('user.unewprograms.index', [
            'title' => __('MAIN.TITLE.PROGRAM.' . mb_strtoupper($type)),
            'cycles' => Cycle::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-CYCLES'),0),
            'cycles2' => Cycle::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.SELECT-CYCLE'),0),
            //'periods' => Period::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.SELECT-PERIOD'),0),
            'branches' => Branch::get(),
            'branches2' => Branch::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-BRANCHES'),0),
            'type' => $type,
        ]);
    }


    /**
     * Публикация программы в общий доступ
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function send(Request $request, $id)
    {
        $model = Newprogram::findOrFail($id);
        if ($model->sendToApprove()){
            return response([ 'result' => true ], 200);
        } else {
            return response([ 'message' => __('MAIN.MESSAGE.PROGRAM-SEND-ERROR') ], 403);
        }
    }

    /**
     * Изъятие из публичного доступа
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function withdraw(Request $request, $id)
    {
        $model = Newprogram::findOrFail($id);
        $model->withdrawFromPublic();
        return response([ 'result' => true ], 200);
    }


    /**
     * Клонирование программы
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function clone(Request $request, $id)
    {
        $model = Newprogram::findOrFail($id);
        $model->clone();
        return response([ 'result' => true ], 200);
    }
}
