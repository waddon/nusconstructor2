<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreYplanRequest;
use App\Http\Controllers\Controller;

use App\Models\Yplan;
use App\Models\Tplan;
use App\Models\Grade;
use App\Models\Cbranch;

class YplanController extends Controller {


    public function index(Request $request)
    {
    }


    public function create()
    {
    }


    public function store(StoreYplanRequest $request)
    {
        $tplan = Tplan::findOrFail($request->tplan_id);
        $grade = Grade::find($request->grade_id);

        $model = $tplan->yplans()->create([
            // 'name' => $tload->name
            'name' => __('MAIN.PLACEHOLDER.YPLAN') . ' (' . ($grade->name  ?? '') . ')',
            'grade_id' => $request->grade_id,
        ]);

        return response([
            'relocateURL' => route('yplans.edit', [$model->id]),
        ], 200);
    }



    public function show(Request $request, $id)
    {
        $model = Yplan::findOrFail($id);
        return view('team.planing.yplans.show', [
                'model' => $model,
                'data' => $model->getForShow(),
                'cbranches' => Cbranch::all(),
            ]);
    }


    public function edit($id)
    {
        $model = Yplan::findOrFail($id);
        return view('team.planing.yplans.form', [
                'model' => $model,
                'cbranches' => Cbranch::all(),
            ]);
    }


    public function update(Request $request, $id)
    {
        $model = Yplan::findOrFail($id);
        $model->update(['name' => $request->name]);
        $model->syncMeta($request->componentList, $request->additionalComponentList);
        return response([
            'message' => __('MAIN.MESSAGE.UPDATED'),
            'exitURL' => $model->tplan->tload->parentRoute(),
        ], 200);
    }


    public function delete(Request $request, $id)
    {
        $model = Yplan::withTrashed()->findOrFail($id);
        $model->delete();
        return response([], 200);
    }


    public function restore(Request $request, $id)
    {
        $model = Yplan::onlyTrashed()->findOrFail($id);
        $model->restore();
        return response([], 200);
    }


    public function destroy(Request $request, $id)
    {
        $model = Yplan::withTrashed()->findOrFail($id);
        $model->forceDelete();
        return response([], 200);
    }


    public function data(Request $request)
    {
        return json_encode( Yplan::getListForDatatable($request, true) );
    }


    # Создание копии
    public function clone(Request $request, $id)
    {
        $model = Yplan::findOrFail($id);

        $new_name = mb_substr( '[' . __('MAIN.LABEL.COPY') . '] '  . $model->name, 0, 191);
        $new_model = $model->replicate();
        $new_model->name = $new_name;
        $new_model->save();

        return response([ 'message' => __('MAIN.MESSAGE.CLONED') ], 200);
    }


    # Экспорт даных
    public function export(Request $request, $id)
    {
        $model = Yplan::findOrFail($id);

        $headers = [
            "Content-type"=>"text/html",
            "Content-Disposition"=>"attachment;Filename=" . $model->name . ".doc"
        ];
    
        $content = view('team.planing.yplans.export',[
            'model' => $model,
            'cbranches' => Cbranch::all(),
        ])->render();

        return response()->make($content, 200, $headers);
    }

}
