<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreTspecificRequest;

use App\Http\Controllers\Controller;
use App\Models\Team;
use App\Models\Tspecific;
use App\Models\Branch;

class TspecificController extends Controller 
{

    public function __construct()
    {
        $this->middleware('teamLeader')->except(['index','data']);
    }


    public function index(Request $request)
    {
        return view('team.tspecifics.index', [
                'model' => Team::findOrfail($request->team_id),
                'branches' => Branch::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-BRANCHES'),0),
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('team.tspecifics.form', [
                'model' => new Tspecific,
                'team' => Team::findOrfail($request->team_id),
                'branches' => Branch::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-BRANCHES'),0),
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTspecificRequest $request)
    {
        $array = [
                'name' => $request->name,
                'marking' => $request->marking,
                'team_id' => $request->team_id,
                'branch_id' => $request->branch_id,
            ];
        $model = Tspecific::create($array);
        return response([
            'message' => __('MAIN.MESSAGE.CREATED'),
            'exitURL' => route('tspecifics.index',$model->team->id),
            'formAction' => route('tspecifics.update', [$model->team->id, $model->id]),
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $team_id, $id)
    {
        return view('team.tspecifics.form', [
                'model' => $model = Tspecific::findOrFail($id),
                'team' => Team::findOrfail($team_id),
                'branches' => Branch::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-BRANCHES'),0),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreTspecificRequest $request, $team_id, $id)
    {
        $model = Tspecific::findOrFail($id);
        $array = [
                'name' => $request->name,
                'marking' => $request->marking,
                'branch_id' => $request->branch_id,
            ];
        $model->update($array);
        return response([
            'message' => __('MAIN.MESSAGE.UPDATED'),
            'exitURL' => route('tspecifics.index',$team_id),
            'formAction' => route('tspecifics.update', [$team_id, $model->id]),
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Tspecific::findOrFail($id);
        $model->delete();
        return response([], 200);
    }


    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Tspecific::getListForDatatable($request) );
    }

}