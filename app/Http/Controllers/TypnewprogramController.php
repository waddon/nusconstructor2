<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Team;
use App\Models\Newprogram;
use App\Models\Cycle;
use App\Models\Period;
use App\Models\Branch;
use App\Models\Line;
use App\Tool;

class TypnewprogramController extends Controller {

    public function __construct()
    {
        $this->middleware('auth')->only('copySelf');
    }


    public function index(Request $request)
    {
        // return view('public.newprograms.index', [
        //     'cycles' => Cycle::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-CYCLES'),0),
        //     'branches2' => Branch::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-BRANCHES'),0),
        // ]);
        return redirect()->route('publicDocuments', 'model');
    }


    public function create() {
    }


    public function store(StoreNewprogramRequest $request, $team_id)
    {
    }


    public function show(Request $request, $id)
    {
        $model = Newprogram::findOrFail($id);
        $model->previousUrl = route('typnewprograms.index');
        return view('public.newprograms.show', [
            'model' => $model,
        ]);
    }


    public function edit($team_id, $id) {
    }


    public function update(Request $request, $team_id, $id)
    {
    }


    public function delete(Request $request, $team_id, $id)
    {
    }

    public function restore(Request $request, $team_id, $id)
    {
    }


    public function destroy(Request $request, $team_id, $id)
    {
    }


    public function data(Request $request)
    {
        return json_encode( Newprogram::getTypicalForDatatable($request) );
        // return json_encode( Newprogram::getListForDatatable($request) );
    }


    // public function share(Request $request, $team_id, $id)
    // {
    //     $model = Newprogram::withTrashed()->findOrFail($id);
    //     $pivot = $model->teams()->where('id','=',$team_id)->first()->pivot;
    //     $meta = $pivot->meta;
    //     $int_array = [];
    //     if ($request->editors){
    //         foreach ($request->editors as $key => $editor) {
    //             $int_array[] = (int)$editor;
    //         }
    //     }
    //     $meta->editors = $int_array;
    //     $pivot->meta = $meta;
    //     $pivot->save();
    //     return response([ 'result' => true, 'editors' => $request->editors ], 200);
    // }

    public function copySelf(Request $request, $id)
    {
        $model = Newprogram::findOrFail($id);
        $new_name = mb_substr( '[' . __('MAIN.LABEL.COPY') . '] '  . $model->name, 0, 191);
        $new_model = $model->replicate();
        $new_model->name = $new_name;
        $new_model->status = 'Editing';
        $new_model->public = 0;
        $new_model->parent_id = $model->id;
        $new_model->type = $model->type !== 'model' ? $model->type : 'educational';
        $new_model->save();
        $branches = $model->branches->pluck('id','id');
        $new_model->branches()->sync($branches);       
        $new_model->users()->sync([auth()->user()->id]);
        return response([ 'message' => __('MAIN.MESSAGE.COPY-SUCCESS') ], 200);
    }


    public function copyTeam(Request $request, $id)
    {
        $model = Newprogram::findOrFail($id);
        $team = auth()->user()->teams()->find($request->team_id);
        if ($team){
            $new_name = mb_substr( '[' . __('MAIN.LABEL.COPY') . '] '  . $model->name, 0, 191);
            $new_model = $model->replicate();
            $new_model->name = $new_name;
            $new_model->status = 'Editing';
            $new_model->public = 0;
            $new_model->parent_id = $model->id;
            $new_model->type = $model->type !== 'model' ? $model->type : 'educational';
            $new_model->save();
            $branches = $model->branches->pluck('id','id');
            $new_model->branches()->sync($branches);       
            $new_model->teams()->sync([$team->id]);
            $pivot = $new_model->teams()->find($team->id)->pivot;
            $pivot->meta = (object)['editors' => [auth()->user()->id] ];
            $pivot->save();
            return response([ 'message' => __('MAIN.MESSAGE.COPY-SUCCESS') ], 200);
        } else {
            return response([ 'message' => __('MAIN.MESSAGE.TEAM-NOT-FOUND') ], 404);
        }
    }

    /**
     * Индексная страница для различных типов программ
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function list(Request $request, $type)
    {
        return view('public.newprograms.index', [
            'title' => __('MAIN.TITLE.PROGRAM-PUBLIC.' . mb_strtoupper($type)),
            'cycles' => Cycle::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-CYCLES'),0),
            'branches2' => Branch::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-BRANCHES'),0),
            'type' => $type,
        ]);
    }
}
