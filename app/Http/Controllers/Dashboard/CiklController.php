<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests\StoreCiklRequest;

use App\Http\Controllers\Controller;
use App\Cikl;
use App\Riven;


class CiklController extends Controller 
{

    public function index(Request $request)
    {
        return view('dashboard.cikls.index', []);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.cikls.form', [
                'model' => new Cikl,
                'rivens' => Riven::pluck('name_riven','id_riven')->prepend(__('MAIN.PLACEHOLDER.ALL-RIVENS'),0),
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCiklRequest $request)
    {
        $array = [
                'name_cikl' => $request->name_cikl,
                'id_riven' => $request->id_riven,
                'marking_cikl' => $request->marking_cikl,
            ];
        $model = Cikl::create($array);
        return response([
            'message' => __('MAIN.MESSAGE.CREATED'),
            'exitURL' => route('cikls.index'),
            'formAction' => route('cikls.update', $model->id_cikl),
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('cikls');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Cikl::findOrFail($id);
        return view('dashboard.cikls.form', [
                'model' => $model,
                'rivens' => Riven::pluck('name_riven','id_riven')->prepend(__('MAIN.PLACEHOLDER.ALL-RIVENS'),0),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreCiklRequest $request, $id)
    {
        $model = Cikl::findOrFail($id);
        $array = [
                'name_cikl' => $request->name_cikl,
                'id_riven' => $request->id_riven,
                'marking_cikl' => $request->marking_cikl,
            ];
        $model->update($array);
        return response([
            'message' => __('MAIN.MESSAGE.UPDATED'),
            'exitURL' => route('cikls.index'),
            'formAction' => route('cikls.update', $model->id_cikl),
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Cikl::findOrFail($id);
        $model->delete();
        return response([], 200);
    }


    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Cikl::getListForDatatable($request) );
    }

}