<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests\StoreRoleRequest;

use App\Http\Controllers\Controller;
use App\Role;
use App\Permission;


class RoleController extends Controller 
{

    public function index(Request $request)
    {
        return view('dashboard.roles.index', []);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.roles.form', [
                'permissions' => Permission::all(),
                'model' => new Role,
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRoleRequest $request)
    {
        $credentials = $request->only('name');
        $credentials['guard_name'] = 'web';
        $model = Role::create($credentials);

        $permissions = $request['permissions'];
        if($permissions){
            $model->permissions()->sync($permissions);
            foreach ($permissions as $permission) {
                $p = Permission::where('id', '=', $permission)->firstOrFail(); 
                $role = Role::where('name', '=', $request['name'])->first(); 
                $role->givePermissionTo($p);
            }
        }        
        else {
            $model->permissions()->detach();
        }

        return response([
            'message' => __('Element created successfully'),
            'exitURL' => route('roles.index'),
            'formAction' => route('roles.update', $model->id),
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('roles');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Role::findOrFail($id);
        return view('dashboard.roles.form', [
                'model' => $model,
                'permissions' => Permission::all(),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreRoleRequest $request, $id)
    {
        $model = Role::findOrFail($id);
        $credentials = $request->only('name');

        $model->update($credentials);

        $permissions = $request['permissions'];
        if($permissions){
            $model->permissions()->sync($permissions);
            foreach ($permissions as $permission) {
                $p = Permission::where('id', '=', $permission)->firstOrFail(); 
                $role = Role::where('name', '=', $request['name'])->first(); 
                $role->givePermissionTo($p);
            }
        }
        else {
            $model->permissions()->detach();
        }

        return response([
            'message' => __('Element created successfully'),
            'exitURL' => route('roles.index'),
            'formAction' => route('roles.update', $model->id),
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Role::findOrFail($id);
        $model->delete();
        return response([], 200);
    }


    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Role::getListForDatatable($request) );
    }

}