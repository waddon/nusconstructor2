<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests\StoreNoteRequest;

use App\Http\Controllers\Controller;
use App\Note;
use App\Cikl;


class NoteController extends Controller 
{

    public function index(Request $request)
    {
        return view('dashboard.notes.index', []);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.notes.form', [
                'model' => new Note,
                'cikls' => Cikl::pluck('name_cikl','id_cikl')->prepend(__('MAIN.PLACEHOLDER.ALL-CIKLS'),0),
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNoteRequest $request)
    {
        $array = [
                'id_cikl' => $request->id_cikl,
                'text_note' => $request->text_note ? $request->text_note : '',
            ];
        $model = Note::create($array);
        return response([
            'message' => __('MAIN.MESSAGE.CREATED'),
            'exitURL' => route('notes.index'),
            'formAction' => route('notes.update', $model->id_note),
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('notes');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Note::findOrFail($id);
        return view('dashboard.notes.form', [
                'model' => $model,
                'cikls' => Cikl::pluck('name_cikl','id_cikl')->prepend(__('MAIN.PLACEHOLDER.ALL-CIKLS'),0),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreNoteRequest $request, $id)
    {
        $model = Note::findOrFail($id);
        $array = [
                'id_cikl' => $request->id_cikl,
                'text_note' => $request->text_note ? $request->text_note : '',
            ];
        $model->update($array);
        return response([
            'message' => __('MAIN.MESSAGE.UPDATED'),
            'exitURL' => route('notes.index'),
            'formAction' => route('notes.update', $model->id_note),
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Note::findOrFail($id);
        $model->delete();
        return response([], 200);
    }


    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Note::getListForDatatable($request) );
    }

}