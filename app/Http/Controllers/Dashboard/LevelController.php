<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests\StoreLevelRequest;

use App\Http\Controllers\Controller;
use App\Models\Level;


class LevelController extends Controller 
{

    public function index(Request $request)
    {
        return view('dashboard.levels.index', []);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.levels.form', [
                'model' => new Level,
            ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLevelRequest $request)
    {
        $array = [
                'name' => $request->name,
            ];
        $model = Level::create($array);
        return response([
            'message' => __('MAIN.MESSAGE.CREATED'),
            'exitURL' => route('levels.index'),
            'formAction' => route('levels.update', $model->id),
        ], 200);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Level::findOrFail($id);
        return view('dashboard.levels.form', [
                'model' => $model,
            ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreLevelRequest $request, $id)
    {
        $model = Level::findOrFail($id);
        $array = [
                'name' => $request->name,
            ];
        $model->update($array);
        return response([
            'message' => __('MAIN.MESSAGE.UPDATED'),
            'exitURL' => route('levels.index'),
            'formAction' => route('levels.update', $model->id),
        ], 200);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Level::findOrFail($id);
        $model->delete();
        return response([], 200);
    }


    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Level::getListForDatatable($request) );
    }

}