<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests\StoreCycleRequest;

use App\Http\Controllers\Controller;
use App\Models\Level;
use App\Models\Cycle;


class CycleController extends Controller 
{

    public function index(Request $request)
    {
        return view('dashboard.cycles.index', []);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.cycles.form', [
                'model' => new Cycle,
                'levels' => Level::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-LEVELS'),0),
            ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCycleRequest $request)
    {
        $array = [
                'name' => $request->name,
                'marking' => $request->marking,
                'level_id' => $request->level_id,
                'content' => $request->content,
            ];
        $model = Cycle::create($array);
        return response([
            'message' => __('MAIN.MESSAGE.CREATED'),
            'exitURL' => route('cycles.index'),
            'formAction' => route('cycles.update', $model->id),
        ], 200);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Cycle::findOrFail($id);
        return view('dashboard.cycles.form', [
                'model' => $model,
                'levels' => Level::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-LEVELS'),0),
            ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreCycleRequest $request, $id)
    {
        $model = Cycle::findOrFail($id);
        $array = [
                'name' => $request->name,
                'marking' => $request->marking,
                'level_id' => $request->level_id,
                'content' => $request->content,
            ];
        $model->update($array);
        return response([
            'message' => __('MAIN.MESSAGE.UPDATED'),
            'exitURL' => route('cycles.index'),
            'formAction' => route('cycles.update', $model->id),
        ], 200);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Cycle::findOrFail($id);
        $model->delete();
        return response([], 200);
    }


    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Cycle::getListForDatatable($request) );
    }

}