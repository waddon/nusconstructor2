<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;

use App\User;
// use App\Role;
// use App\Permission;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Builder;

//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserController extends Controller {

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request) {
        return view('dashboard.users.index', [
            'roles' =>Role::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-ROLES'),0),
        ]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create() {
        return view('dashboard.users.form', [
                'roles' => Role::get(),
                'model' => new User,
            ]);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(StoreUserRequest $request) {

        $credentials = $request->only('name', 'email');
        $credentials['password'] = bcrypt($request->password);
        $model = User::create($credentials);

        $roles = $request['roles']; //Retrieving the roles field checking if a role was selected
        if (isset($roles) && $roles) {
            foreach ($roles as $role) {
                $role_r = Role::where('id', '=', $role)->firstOrFail();            
                $model->assignRole($role_r); //Assigning role to user
            }
        }
        return response([
            'message' => __('Element created successfully'),
            'exitURL' => route('users.index'),
            'formAction' => route('users.update', $model->id),
        ], 200);
    }



    public function show(Request $request, $id)
    {
        $newuser = User::findOrFail($id);
        Auth::login($newuser);
        if ($newuser->can('dashboard')){
            return redirect()->route('dashboard')
                ->with('flash_message', __('Text sing in'));
        } else {
            return redirect()->route('home')
                ->with('flash_message', __('Text sing in'));
        }
    }


    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id) {
        $model = User::findOrFail($id);
        return view('dashboard.users.form', [
                'model'=>$model,
                'roles'=>Role::get(),
            ]);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(UpdateUserRequest $request, $id)
    {
        $model = User::findOrFail($id); //Get role specified by id

        $credentials = $request->only('name', 'email');
        $model->update($credentials);

        $roles = $request['roles']; //Retreive all roles

        if (isset($roles)) {
            $model->roles()->sync($roles);  //If one or more role is selected associate user to roles          
        }        
        else {
            $model->roles()->detach(); //If no role is selected remove exisiting role associated to a user
        }
        return response([
            'message' => __('Element updated successfully'),
            'exitURL' => route('users.index'),
            'formAction' => route('users.update', $model->id),
        ], 200);
    }

    /**
     * Move to trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $model = User::withTrashed()->findOrFail($id);
        $model->delete();
        return response([], 200);
    }

    /**
     * Restore from trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore(Request $request, $id)
    {
        $model = User::onlyTrashed()->findOrFail($id);
        $model->restore();
        return response([], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = User::withTrashed()->findOrFail($id);
        $model->forceDelete();
        return response([], 200);
    }


    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( User::getListForDatatable($request) );
    }


    /**
     * Get list of data for select2.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function select2(Request $request)
    {
        $elements = [];
        $where = [];
        if ($request->searchTerm){
            $where[] = ['id', 'like', '%' . $request->searchTerm . '%', 'or'];
            $where[] = ['name', 'like', '%' . $request->searchTerm . '%', 'or'];
            $where[] = ['second_name', 'like', '%' . $request->searchTerm . '%', 'or'];
            $where[] = ['patronymic', 'like', '%' . $request->searchTerm . '%', 'or'];
            $where[] = ['email', 'like', '%' . $request->searchTerm . '%', 'or'];
        }
        $elements = User::where($where)->take(10)->get();
        foreach ($elements as $key => $element) {
            $element->avatar = $element->getAvatarPath();
            $element->combineName = $element->combineName();
        }
        return json_encode($elements);        
    }
}
