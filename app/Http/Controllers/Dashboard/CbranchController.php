<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests\StoreCbranchRequest;

use App\Http\Controllers\Controller;
use App\Models\Cbranch;


class CbranchController extends Controller 
{

    public function index(Request $request)
    {
        return view('dashboard.cbranches.index', []);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.cbranches.form', [
                'model' => new Cbranch,
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCbranchRequest $request)
    {
        $array = [
                'name' => $request->name,
            ];
        $model = Cbranch::create($array);
        return response([
            'message' => __('MAIN.MESSAGE.CREATED'),
            'exitURL' => route('cbranches.index'),
            'formAction' => route('cbranches.update', $model->id),
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('cbranches');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Cbranch::findOrFail($id);
        return view('dashboard.cbranches.form', [
                'model' => $model,
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreCbranchRequest $request, $id)
    {
        $model = Cbranch::findOrFail($id);
        $array = [
                'name' => $request->name,
            ];
        $model->update($array);
        return response([
            'message' => __('MAIN.MESSAGE.UPDATED'),
            'exitURL' => route('cbranches.index'),
            'formAction' => route('cbranches.update', $model->id),
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Cbranch::findOrFail($id);
        $model->delete();
        return response([], 200);
    }


    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Cbranch::getListForDatatable($request) );
    }

}