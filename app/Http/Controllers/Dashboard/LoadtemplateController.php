<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests\StoreLoadtemplateRequest;

use App\Http\Controllers\Controller;
use App\Models\Loadtemplate;
use App\Models\Cperiod;
use App\Models\Languagetype;
use App\Models\Cycle;
use App\Models\Cbranch;
use App\Models\Component;


class LoadtemplateController extends Controller 
{

    public function index(Request $request)
    {
        return view('dashboard.loadtemplates.index', []);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.loadtemplates.form', [
                'model' => new Loadtemplate,
                'cperiods' => Cperiod::all(),
                'languagetypes' => Languagetype::all(),
                'cycles' => Cycle::all(),
                'cbranches' => Cbranch::all(),
                'components' => Component::pluck('name','id'),
            ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLoadtemplateRequest $request)
    {
        $meta = (object)[];
        $meta->loads      = $request->loads;
        $meta->additional = $request->additional;
        $meta->financed   = $request->financed;
        $meta->admissible = $request->admissible;
        $meta->restrictions = $request->restrictions;
        $array = [
                'name' => $request->name,
                'languagetype_id' => $request->languagetype_id,
                'meta' => $meta,
            ];
        $model = Loadtemplate::create($array);
        $model->cperiods()->sync($request->cperiods);
        $model->cycles()->sync($request->cycles);
        return response([
            'message' => __('MAIN.MESSAGE.CREATED'),
            'exitURL' => route('loadtemplates.index'),
            'formAction' => route('loadtemplates.update', $model->id),
        ], 200);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Loadtemplate::findOrFail($id);
        return view('dashboard.loadtemplates.show', [
                'model' => $model,
                'cperiods' => Cperiod::all(),
                'languagetypes' => Languagetype::all(),
                'cycles' => Cycle::all(),
                'cbranches' => Cbranch::all(),
            ]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Loadtemplate::findOrFail($id);
        return view('dashboard.loadtemplates.form', [
                'model' => $model,
                'cperiods' => Cperiod::all(),
                'languagetypes' => Languagetype::all(),
                'cycles' => Cycle::all(),
                'cbranches' => Cbranch::all(),
                'components' => Component::pluck('name','id'),
            ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreLoadtemplateRequest $request, $id)
    {
        $model = Loadtemplate::findOrFail($id);
        $meta = is_object($model->meta) ? $model->meta : (object)[];
        $meta->loads      = $request->loads;
        $meta->additional = $request->additional;
        $meta->financed   = $request->financed;
        $meta->admissible = $request->admissible;
        $meta->restrictions = $request->restrictions;
        $array = [
                'name' => $request->name,
                'languagetype_id' => $request->languagetype_id,
                'meta' => $meta,
            ];
        $model->update($array);
        $model->cperiods()->sync($request->cperiods);
        $model->cycles()->sync($request->cycles);
        return response([
            'message' => __('MAIN.MESSAGE.UPDATED'),
            'exitURL' => route('loadtemplates.index'),
            'formAction' => route('loadtemplates.update', $model->id),
        ], 200);
    }


    /**
     * Move to trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $model = Loadtemplate::withTrashed()->findOrFail($id);
        $model->delete();
        return response([], 200);
    }


    /**
     * Restore from trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore(Request $request, $id)
    {
        $model = Loadtemplate::onlyTrashed()->findOrFail($id);
        $model->restore();
        return response([], 200);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Loadtemplate::withTrashed()->findOrFail($id);
        $model->forceDelete();
        return response([], 200);
    }


    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Loadtemplate::getListForDatatable($request) );
    }

}