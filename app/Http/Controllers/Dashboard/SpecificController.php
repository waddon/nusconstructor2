<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests\StoreSpecificRequest;

use App\Http\Controllers\Controller;
use App\Models\Line;
use App\Models\Specific;
use App\Models\Expect;

class SpecificController extends Controller 
{

    public function index(Request $request)
    {
        return view('dashboard.specifics.index', [
                // 'lines' => Line::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-LINES'),0),
                'lines' => Line::listWithBranch(false),
                'expects' => Expect::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-EXPECTS'),0),
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.specifics.form', [
                'model' => new Specific,
                // 'lines' => Line::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-LINES'),0),
                'lines' => Line::listWithBranch(),
                // 'expects' => Expect::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-EXPECTS'),0),
                'expects' => Expect::listWithBranch(),
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSpecificRequest $request)
    {
        $array = [
                'name' => $request->name,
                'marking' => $request->marking,
                'line_id' => $request->line_id,
                'expect_id' => $request->expect_id,
            ];
        $model = Specific::create($array);
        return response([
            'message' => __('MAIN.MESSAGE.CREATED'),
            'exitURL' => route('specifics.index'),
            'formAction' => route('specifics.update', $model->id),
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Specific::findOrFail($id);
        return view('dashboard.specifics.form', [
                'model' => $model,
                //'lines' => Line::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-LINES'),0),
                'lines' => Line::listWithBranch(),
                // 'expects' => Expect::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-EXPECTS'),0),
                'expects' => Expect::listWithBranch(),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreSpecificRequest $request, $id)
    {
        $model = Specific::findOrFail($id);
        $array = [
                'name' => $request->name,
                'marking' => $request->marking,
                'line_id' => $request->line_id,
                'expect_id' => $request->expect_id,
            ];
        $model->update($array);
        return response([
            'message' => __('MAIN.MESSAGE.UPDATED'),
            'exitURL' => route('specifics.index'),
            'formAction' => route('specifics.update', $model->id),
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Specific::findOrFail($id);
        $model->delete();
        return response([], 200);
    }


    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Specific::getListForDatatable($request) );
    }

}