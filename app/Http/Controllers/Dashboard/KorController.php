<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests\StoreKorRequest;

use App\Http\Controllers\Controller;
use App\Kor;
use App\Zmist;
use App\Zor;

class KorController extends Controller 
{

    public function index(Request $request)
    {
        return view('dashboard.kors.index', [
                'zmists' => Zmist::pluck('name_zmist','id_zmist')->prepend(__('MAIN.PLACEHOLDER.ALL-ZMISTS'),0),
                'zors' => Zor::pluck('name_zor','id_zor')->prepend(__('MAIN.PLACEHOLDER.ALL-ZORS'),0),
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.kors.form', [
                'model' => new Kor,
                'zmists' => Zmist::pluck('name_zmist','id_zmist')->prepend(__('MAIN.PLACEHOLDER.ALL-ZMISTS'),0),
                'zors' => Zor::pluck('name_zor','id_zor')->prepend(__('MAIN.PLACEHOLDER.ALL-ZORS'),0),
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreKorRequest $request)
    {
        $array = [
                'name_kor' => $request->name_kor,
                'key_kor' => $request->key_kor,
                'id_zor' => $request->id_zor,
                'id_zmist' => $request->id_zmist,
            ];
        $model = Kor::create($array);
        return response([
            'message' => __('MAIN.MESSAGE.CREATED'),
            'exitURL' => route('kors.index'),
            'formAction' => route('kors.update', $model->id_kor),
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('kors');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Kor::findOrFail($id);
        return view('dashboard.kors.form', [
                'model' => $model,
                'zmists' => Zmist::pluck('name_zmist','id_zmist')->prepend(__('MAIN.PLACEHOLDER.ALL-ZMISTS'),0),
                'zors' => Zor::pluck('name_zor','id_zor')->prepend(__('MAIN.PLACEHOLDER.ALL-ZORS'),0),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreKorRequest $request, $id)
    {
        $model = Kor::findOrFail($id);
        $array = [
                'name_kor' => $request->name_kor,
                'key_kor' => $request->key_kor,
                'id_zor' => $request->id_zor,
                'id_zmist' => $request->id_zmist,
            ];
        $model->update($array);
        return response([
            'message' => __('MAIN.MESSAGE.UPDATED'),
            'exitURL' => route('kors.index'),
            'formAction' => route('kors.update', $model->id_kor),
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Kor::findOrFail($id);
        $model->delete();
        return response([], 200);
    }


    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Kor::getListForDatatable($request) );
    }

}