<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests\StoreZmistRequest;

use App\Http\Controllers\Controller;
use App\Galuz;
use App\Zmist;

class ZmistController extends Controller 
{

    public function index(Request $request)
    {
        return view('dashboard.zmists.index', [
                'galuzes' => Galuz::pluck('name_galuz','id_galuz')->prepend(__('MAIN.PLACEHOLDER.ALL-GALUZES'),0),
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.zmists.form', [
                'model' => new Zmist,
                'galuzes' => Galuz::pluck('name_galuz','id_galuz')->prepend(__('MAIN.PLACEHOLDER.ALL-GALUZES'),0),
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreZmistRequest $request)
    {
        $array = [
                'name_zmist' => $request->name_zmist,
                'marking_zmist' => $request->marking_zmist,
                'id_galuz' => $request->id_galuz,
                'text_zmist' => $request->text_zmist ? $request->text_zmist : '',
            ];
        $model = Zmist::create($array);
        return response([
            'message' => __('MAIN.MESSAGE.CREATED'),
            'exitURL' => route('zmists.index'),
            'formAction' => route('zmists.update', $model->id_zmist),
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('zmists');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Zmist::findOrFail($id);
        return view('dashboard.zmists.form', [
                'model' => $model,
                'galuzes' => Galuz::pluck('name_galuz','id_galuz')->prepend(__('MAIN.PLACEHOLDER.ALL-GALUZES'),0),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreZmistRequest $request, $id)
    {
        $model = Zmist::findOrFail($id);
        $array = [
                'name_zmist' => $request->name_zmist,
                'marking_zmist' => $request->marking_zmist,
                'id_galuz' => $request->id_galuz,
                'text_zmist' => $request->text_zmist ? $request->text_zmist : '',
            ];
        $model->update($array);
        return response([
            'message' => __('MAIN.MESSAGE.UPDATED'),
            'exitURL' => route('zmists.index'),
            'formAction' => route('zmists.update', $model->id_zmist),
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Zmist::findOrFail($id);
        $model->delete();
        return response([], 200);
    }


    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Zmist::getListForDatatable($request) );
    }

}