<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests\StoreGaluznoteRequest;

use App\Http\Controllers\Controller;
use App\Cikl;
use App\Galuz;
use App\Galuznote;

class GaluznoteController extends Controller 
{

    public function index(Request $request)
    {
        return view('dashboard.galuznotes.index', [
                'cikls' => Cikl::pluck('name_cikl','id_cikl')->prepend(__('MAIN.PLACEHOLDER.ALL-CIKLS'),0),
                'galuzes' => Galuz::pluck('name_galuz','id_galuz')->prepend(__('MAIN.PLACEHOLDER.ALL-GALUZES'),0),
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.galuznotes.form', [
                'model' => new Galuznote,
                'cikls' => Cikl::pluck('name_cikl','id_cikl')->prepend(__('MAIN.PLACEHOLDER.ALL-CIKLS'),0),
                'galuzes' => Galuz::pluck('name_galuz','id_galuz')->prepend(__('MAIN.PLACEHOLDER.ALL-GALUZES'),0),
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGaluznoteRequest $request)
    {
        $array = [
                'id_cikl' => $request->id_cikl,
                'id_galuz' => $request->id_galuz,
                'text_galuznote' => $request->text_galuznote ? $request->text_galuznote : '',
            ];
        $model = Galuznote::create($array);
        return response([
            'message' => __('MAIN.MESSAGE.CREATED'),
            'exitURL' => route('galuznotes.index'),
            'formAction' => route('galuznotes.update', $model->id_galuznote),
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('galuznotes');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Galuznote::findOrFail($id);
        return view('dashboard.galuznotes.form', [
                'model' => $model,
                'cikls' => Cikl::pluck('name_cikl','id_cikl')->prepend(__('MAIN.PLACEHOLDER.ALL-CIKLS'),0),
                'galuzes' => Galuz::pluck('name_galuz','id_galuz')->prepend(__('MAIN.PLACEHOLDER.ALL-GALUZES'),0),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreGaluznoteRequest $request, $id)
    {
        $model = Galuznote::findOrFail($id);
        $array = [
                'id_cikl' => $request->id_cikl,
                'id_galuz' => $request->id_galuz,
                'text_galuznote' => $request->text_galuznote ? $request->text_galuznote : '',
            ];
        $model->update($array);
        return response([
            'message' => __('MAIN.MESSAGE.UPDATED'),
            'exitURL' => route('galuznotes.index'),
            'formAction' => route('galuznotes.update', $model->id_galuznote),
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Galuznote::findOrFail($id);
        $model->delete();
        return response([], 200);
    }


    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Galuznote::getListForDatatable($request) );
    }

}