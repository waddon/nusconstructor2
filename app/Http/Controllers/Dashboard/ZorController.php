<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests\StoreZorRequest;

use App\Http\Controllers\Controller;
use App\Cikl;
use App\Galuz;
use App\Zor;

class ZorController extends Controller 
{

    public function index(Request $request)
    {
        return view('dashboard.zors.index', [
                'cikls' => Cikl::pluck('name_cikl','id_cikl')->prepend(__('MAIN.PLACEHOLDER.ALL-CIKLS'),0),
                'galuzes' => Galuz::pluck('name_galuz','id_galuz')->prepend(__('MAIN.PLACEHOLDER.ALL-GALUZES'),0),
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.zors.form', [
                'model' => new Zor,
                'cikls' => Cikl::pluck('name_cikl','id_cikl')->prepend(__('MAIN.PLACEHOLDER.ALL-CIKLS'),0),
                'galuzes' => Galuz::pluck('name_galuz','id_galuz')->prepend(__('MAIN.PLACEHOLDER.ALL-GALUZES'),0),
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreZorRequest $request)
    {
        $array = [
                'name_zor' => $request->name_zor,
                'key_zor' => $request->key_zor,
                'id_cikl' => $request->id_cikl,
                'id_galuz' => $request->id_galuz,
            ];
        $model = Zor::create($array);
        return response([
            'message' => __('MAIN.MESSAGE.CREATED'),
            'exitURL' => route('zors.index'),
            'formAction' => route('zors.update', $model->id_zor),
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('zors');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Zor::findOrFail($id);
        return view('dashboard.zors.form', [
                'model' => $model,
                'cikls' => Cikl::pluck('name_cikl','id_cikl')->prepend(__('MAIN.PLACEHOLDER.ALL-CIKLS'),0),
                'galuzes' => Galuz::pluck('name_galuz','id_galuz')->prepend(__('MAIN.PLACEHOLDER.ALL-GALUZES'),0),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreZorRequest $request, $id)
    {
        $model = Zor::findOrFail($id);
        $array = [
                'name_zor' => $request->name_zor,
                'key_zor' => $request->key_zor,
                'id_cikl' => $request->id_cikl,
                'id_galuz' => $request->id_galuz,
            ];
        $model->update($array);
        return response([
            'message' => __('MAIN.MESSAGE.UPDATED'),
            'exitURL' => route('zors.index'),
            'formAction' => route('zors.update', $model->id_zor),
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Zor::findOrFail($id);
        $model->delete();
        return response([], 200);
    }


    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Zor::getListForDatatable($request) );
    }

}