<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests\StorePostRequest;

use App\Http\Controllers\Controller;
use App\Post;
use App\Cat;

class PostController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {        
        return view('dashboard.posts.index', [
            'cats' => Cat::pluck('name_cat','id_cat')->prepend(__('MAIN.PLACEHOLDER.ALL-CATEGORIES'),0),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Post;
        return view('dashboard.posts.form', [
                'model'=>$model,
                'cats' => Cat::pluck('name_cat','id_cat')->prepend(__('MAIN.PLACEHOLDER.ALL-CATEGORIES'),0),
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePostRequest $request)
    {
        $array = [
                'name_post' => $request->name_post,
                'slug_post' => $request->slug_post,
                'thumb_post' => '',
                'id_cat' => $request->id_cat,
                'layout_post' => '',
                'link_post' => '',
                'download_post' => 0,
                'excerpt_post' => '',
                'created_at' => $request->created_at,
                'content_post' => $request->content_post ? $request->content_post : '',
            ];
        $model = Post::create($array);
        return response([
            'message' => __('MAIN.MESSAGE.CREATED'),
            'exitURL' => route('posts.index'),
            'formAction' => route('posts.update', $model->id_post),
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Post::findOrFail($id);
        return view('dashboard.posts.show', [
                'model'=>$model,
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Post::findOrFail($id);
        return view('dashboard.posts.form', [
                'model'=>$model,
                'cats' => Cat::pluck('name_cat','id_cat')->prepend(__('MAIN.PLACEHOLDER.ALL-CATEGORIES'),0),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePostRequest $request, $id)
    {
        $model = Post::findOrFail($id);
        $array = [
                'name_post' => $request->name_post,
                'slug_post' => $request->slug_post,
                'thumb_post' => '',
                'id_cat' => $request->id_cat,
                'layout_post' => '',
                'link_post' => '',
                'download_post' => 0,
                'excerpt_post' => '',
                'created_at' => $request->created_at,
                'content_post' => $request->content_post ? $request->content_post : '',
            ];
        $model->update($array);
        return response([
            'message' => __('MAIN.MESSAGE.UPDATED'),
            'exitURL' => route('posts.index'),
            'formAction' => route('posts.update', $model->id_post),
        ], 200);
    }

    /**
     * Move to trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $model = Post::withTrashed()->findOrFail($id);
        $model->delete();
        return response([], 200);
    }

    /**
     * Restore from trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore(Request $request, $id)
    {
        $model = Post::onlyTrashed()->findOrFail($id);
        $model->restore();
        return response([], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Post::findOrFail($id);
        $model->forceDelete();
        return response([], 200);
    }


    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Post::getListForDatatable($request) );
    }

}
