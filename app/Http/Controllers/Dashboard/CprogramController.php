<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests\StoreCprogramRequest;

use App\Http\Controllers\Controller;
use App\Models\Cprogram;
use App\Models\Ctype;
use App\Models\Cauthor;
use App\Models\Grade;


class CprogramController extends Controller 
{

    public function index(Request $request)
    {
        return view('dashboard.cprograms.index', []);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.cprograms.form', [
                'model' => new Cprogram,
                // 'components' => Component::all(),
                'grades' => Grade::all(),
                'cauthors' => Cauthor::all(),
                'ctypes' => Ctype::all(),
            ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCprogramRequest $request)
    {
        $array = [
                'name' => $request->name,
            ];
        $model = Cprogram::create($array);
        $model->components()->sync($request->components);
        $model->grades()->sync($request->grades);
        $model->cauthors()->sync($request->cauthors);

        return response([
            'message' => __('MAIN.MESSAGE.CREATED'),
            'exitURL' => route('cprograms.index'),
            'formAction' => route('cprograms.update', $model->id),
        ], 200);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('cprograms');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Cprogram::findOrFail($id);
        return view('dashboard.cprograms.form', [
                'model' => $model,
                // 'components' => Component::all(),
                'grades' => Grade::all(),
                'cauthors' => Cauthor::all(),
                'ctypes' => Ctype::all(),
            ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreCprogramRequest $request, $id)
    {
        $model = Cprogram::findOrFail($id);
        $array = [
                'name' => $request->name,
            ];
        $model->update($array);
        $model->components()->sync($request->components);
        $model->grades()->sync($request->grades);
        $model->cauthors()->sync($request->cauthors);
        return response([
            'message' => __('MAIN.MESSAGE.UPDATED'),
            'exitURL' => route('cprograms.index'),
            'formAction' => route('cprograms.update', $model->id),
        ], 200);
    }


    /**
     * Move to trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $model = Cprogram::withTrashed()->findOrFail($id);
        $model->delete();
        return response([], 200);
    }


    /**
     * Restore from trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore(Request $request, $id)
    {
        $model = Cprogram::onlyTrashed()->findOrFail($id);
        $model->restore();
        return response([], 200);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Cprogram::withTrashed()->findOrFail($id);
        $model->forceDelete();
        return response([], 200);
    }


    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Cprogram::getListForDatatable($request) );
    }

}