<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests\StoreBranchRequest;

use App\Http\Controllers\Controller;
use App\Models\Branch;


class BranchController extends Controller 
{

    public function index(Request $request)
    {
        return view('dashboard.branches.index', []);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.branches.form', [
                'model' => new Branch,
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBranchRequest $request)
    {
        $array = [
                'name' => $request->name,
                'marking' => $request->marking,
                'color' => $request->color,
                'content' => $request->content,
            ];
        $model = Branch::create($array);
        return response([
            'message' => __('MAIN.MESSAGE.CREATED'),
            'exitURL' => route('branches.index'),
            'formAction' => route('branches.update', $model->id),
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Branch::findOrFail($id);
        return view('dashboard.branches.form', [
                'model' => $model,
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreBranchRequest $request, $id)
    {
        $model = Branch::findOrFail($id);
        $array = [
                'name' => $request->name,
                'marking' => $request->marking,
                'color' => $request->color,
                'content' => $request->content,
            ];
        $model->update($array);
        return response([
            'message' => __('MAIN.MESSAGE.UPDATED'),
            'exitURL' => route('branches.index'),
            'formAction' => route('branches.update', $model->id),
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Branch::findOrFail($id);
        $model->delete();
        return response([], 200);
    }


    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Branch::getListForDatatable($request) );
    }

}