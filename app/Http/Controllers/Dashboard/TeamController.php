<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests\StoreTeamRequest;

use App\Http\Controllers\Controller;
use App\Models\Team;
use App\Models\Ttype;

class TeamController extends Controller {

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request) {
        return view('dashboard.teams.index', [
            'ttypes' =>Ttype::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-TTYPES'),0),
        ]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create() {
        return view('dashboard.teams.form', [
                'model' => new Team,
                'ttypes' =>Ttype::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.SELECT-TTYPE'),0),
                'institutions' => Ttype::where('institution','=',1)->get()->implode('id', ', '),
            ]);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(StoreTeamRequest $request) 
    {
        $array = [
                'name' => $request->name,
                'ttype_id' => $request->ttype_id,
            ];
        $model = Team::create($array);

        $users = [];
        $users[$request->leader_id] = ['teamrole_id'=>1,'meta'=>(object)[]];
        $model->users()->sync($users);
        if ($request->institution_id && Ttype::findOrFail($request->ttype_id)->institution){
            $model->institutions()->sync([$request->institution_id]);
        } else {
            $model->institutions()->sync([]);
        }

        return response([
            'message' => __('MAIN.MESSAGE.CREATED'),
            'exitURL' => route('teams.index'),
            'formAction' => route('teams.update', $model->id),
        ], 200);
    }



    public function show(Request $request, $id)
    {
        $model = Team::findOrFail($id);
        return view('dashboard.teams.show', [
            'model' => $model,
        ]);
    }


    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id) {
        $model = Team::findOrFail($id);
        return view('dashboard.teams.form', [
                'model'=>$model,
                'ttypes' =>Ttype::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.SELECT-TTYPE'),0),
                'institutions' => Ttype::where('institution','=',1)->get()->implode('id', ', '),
            ]);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(StoreTeamRequest $request, $id)
    {
        $model = Team::findOrFail($id);
        $array = [
                'name' => $request->name,
                'ttype_id' => $request->ttype_id,
            ];
        $model->update($array);

        $users = [];
        foreach ($model->users as $user) {
            $users[ $user->id ] = [
                'teamrole_id' => $user->pivot->teamrole_id!=1 ? $user->pivot->teamrole_id : 2,
            ];
        }
        $users[$request->leader_id] = ['teamrole_id'=>1];
        $model->users()->sync($users);
        if ($request->institution_id && Ttype::findOrFail($request->ttype_id)->institution){
            $model->institutions()->sync([$request->institution_id]);
        } else {
            $model->institutions()->sync([]);
        }

        return response([
            'message' => __('Element updated successfully'),
            'exitURL' => route('teams.index'),
            'formAction' => route('teams.update', $model->id),
        ], 200);
    }

    /**
     * Move to trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $model = Team::withTrashed()->findOrFail($id);
        $model->delete();
        return response([], 200);
    }

    /**
     * Restore from trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore(Request $request, $id)
    {
        $model = Team::onlyTrashed()->findOrFail($id);
        $model->restore();
        return response([], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Team::withTrashed()->findOrFail($id);
        $model->forceDelete();
        return response([], 200);
    }


    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Team::getListForDatatable($request) );
    }

}
