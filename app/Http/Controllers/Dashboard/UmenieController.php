<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests\StoreUmenieRequest;

use App\Http\Controllers\Controller;
use App\Galuz;
use App\Umenie;

class UmenieController extends Controller 
{

    public function index(Request $request)
    {
        return view('dashboard.umenies.index', [
                'galuzes' => Galuz::pluck('name_galuz','id_galuz')->prepend(__('MAIN.PLACEHOLDER.ALL-GALUZES'),0),
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.umenies.form', [
                'model' => new Umenie,
                'galuzes' => Galuz::pluck('name_galuz','id_galuz')->prepend(__('MAIN.PLACEHOLDER.ALL-GALUZES'),0),
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUmenieRequest $request)
    {
        $array = [
                'name_umenie' => $request->name_umenie,
                'umgroupe' => $request->umgroupe,
                'key_zor' => $request->key_zor,
                'id_galuz' => $request->id_galuz,
            ];
        $model = Umenie::create($array);
        return response([
            'message' => __('MAIN.MESSAGE.CREATED'),
            'exitURL' => route('umenies.index'),
            'formAction' => route('umenies.update', $model->id_umenie),
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('umenies');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Umenie::findOrFail($id);
        return view('dashboard.umenies.form', [
                'model' => $model,
                'galuzes' => Galuz::pluck('name_galuz','id_galuz')->prepend(__('MAIN.PLACEHOLDER.ALL-GALUZES'),0),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUmenieRequest $request, $id)
    {
        $model = Umenie::findOrFail($id);
        $array = [
                'name_umenie' => $request->name_umenie,
                'umgroupe' => $request->umgroupe,
                'key_zor' => $request->key_zor,
                'id_galuz' => $request->id_galuz,
            ];
        $model->update($array);
        return response([
            'message' => __('MAIN.MESSAGE.UPDATED'),
            'exitURL' => route('umenies.index'),
            'formAction' => route('umenies.update', $model->id_umenie),
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Umenie::findOrFail($id);
        $model->delete();
        return response([], 200);
    }


    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Umenie::getListForDatatable($request) );
    }

}