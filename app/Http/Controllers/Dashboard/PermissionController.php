<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests\StorePermissionRequest;

use App\Http\Controllers\Controller;
use App\Role;
use App\Permission;


class PermissionController extends Controller 
{

    public function index(Request $request)
    {
        return view('dashboard.permissions.index', []);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.permissions.form', [
                'roles' => Role::all(),
                'model' => new Role,
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePermissionRequest $request)
    {
        $credentials = $request->only('name');
        $credentials['guard_name'] = 'web';
        $model = Permission::create($credentials);

        $roles = $request['roles'];
        if($roles){
            $model->roles()->sync($roles);
            foreach ($roles as $role) {
                $r = Role::where('id', '=', $role)->firstOrFail(); //Match input role to db record
                $permission = Permission::where('name', '=', $request['name'])->first(); //Match input //permission to db record
                $r->givePermissionTo($permission);
            }
        }        
        else {
            $model->roles()->detach();
        }

        return response([
            'message' => __('Element created successfully'),
            'exitURL' => route('permissions.index'),
            'formAction' => route('permissions.update', $model->id),
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('permissions');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Permission::findOrFail($id);
        return view('dashboard.permissions.form', [
                'model' => $model,
                'roles' => Role::all(),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePermissionRequest $request, $id)
    {
        $model = Permission::findOrFail($id);
        $credentials = $request->only('name');

        $model->update($credentials);

        $roles = $request['roles'];
        if($roles){
            $model->roles()->sync($roles);
            foreach ($roles as $role) {
                $r = Role::where('id', '=', $role)->firstOrFail(); //Match input role to db record
                $permission = Permission::where('name', '=', $request['name'])->first(); //Match input //permission to db record
                $r->givePermissionTo($permission);
            }
        }        
        else {
            $model->roles()->detach();
        }

        return response([
            'message' => __('Element created successfully'),
            'exitURL' => route('permissions.index'),
            'formAction' => route('permissions.update', $model->id),
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Permission::findOrFail($id);
        $model->delete();
        return response([], 200);
    }


    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Permission::getListForDatatable($request) );
    }

}