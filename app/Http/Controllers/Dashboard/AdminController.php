<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Artisan;

use App\Models\Option;
use App\Page;
use App\User;

use App\Models\Ttype;
use App\Models\Teamrole;
use App\Models\Period;
use App\Models\Team;
// use App\Models\Layout;

use App\Riven;
use App\Models\Level;
use App\Cikl;
use App\Models\Cycle;
use App\SchoolClass;
use App\Models\Grade;
use App\Note;
use App\Galuz;
use App\Models\Branch;
use App\Zmist;
use App\Models\Line;
use App\Zor;
use App\Models\Expect;
use App\Kor;
use App\Models\Specific;
use App\Umenie;
use App\Models\Skill;
use App\Galuznote;
use App\Models\Description;

use App\School;
use App\SchoolUser;

use App\Program;
use App\Models\Newprogram;

class AdminController extends Controller
{
    /**
     * @param  Request
     * @return [type]
     */
    public function index(Request $request)
    {
        return view('dashboard.index', [
            'data' => (object)[],
        ]);
    }


    public function options(Request $request)
    {
        if ($request->isMethod('post')) {
            foreach ($request->all() as $key => $value) {
                Option::setOption($key, $value ? $value : '');
            }
            return redirect()->route('options')
                ->with('flash_message', __('Options updated successfully'));
        }
        return view('dashboard.options', [
            'options' => Option::pluck('value','key'),
            'pages' => Page::get(),
        ]);
    }


    public function transfer(Request $request)
    {
        set_time_limit(0);
        ini_set ('memory_limit', '-1');
        # Уровни
            foreach (Riven::get() as $key => $riven) {
                $model = Level::updateOrCreate(
                    [ 'id' => $riven->id_riven ],
                    [ 'name' => $riven->name_riven ]
                );
            }
        # Циклы
            foreach (Cikl::get() as $key => $cikl) {
                $model = Cycle::updateOrCreate(
                    [ 'id' => $cikl->id_cikl ],
                    [ 'name' => $cikl->name_cikl, 'marking' => $cikl->marking_cikl, 'level_id' => $cikl->id_riven ]
                );
            }
        # Циклы
            foreach (Note::get() as $key => $note) {
                $model = Cycle::find($note->id_cikl);
                if ($model){
                    $model->content = $note->text_note;
                    $model->save();
                }
            }
        # Циклы
            foreach (SchoolClass::get() as $key => $class) {
                $model = Grade::updateOrCreate(
                    [ 'id' => $class->id_class ],
                    [ 'name' => $class->name_class, 'cycle_id' => $class->id_cikl ]
                );
            }
        # Галузи
            foreach (Galuz::get() as $key => $galuz) {
                $model = Branch::updateOrCreate(
                    [ 'id' => $galuz->id_galuz ],
                    [ 'name' => $galuz->name_galuz, 'marking' => $galuz->marking_galuz, 'color' => $galuz->color_galuz, 'content' => $galuz->text_galuz  ]
                );
            }
        # Змістові линии
            foreach (Zmist::get() as $key => $zmist) {
                $model = Line::updateOrCreate(
                    [ 'id' => $zmist->id_zmist ],
                    [ 'name' => $zmist->name_zmist, 'marking' => $zmist->marking_zmist, 'branch_id' => $zmist->id_galuz, 'content' => $zmist->text_zmist, /*'cycle_id' => 1*/  ]
                );
            }
        # ЗОРы
            foreach (Zor::get() as $key => $zor) {
                $model = Expect::updateOrCreate(
                    [ 'id' => $zor->id_zor ],
                    [ 'name' => $zor->name_zor, 'marking' => $zor->key_zor, 'branch_id' => $zor->id_galuz, 'cycle_id' => $zor->id_cikl  ]
                );
            }
        # КОРы
            foreach (Kor::get() as $key => $kor) {
                $model = Specific::updateOrCreate(
                    [ 'id' => $kor->id_kor ],
                    [ 'name' => $kor->name_kor, 'marking' => $kor->key_kor, 'line_id' => $kor->id_zmist, 'expect_id' => $kor->id_zor  ]
                );
            }
        # Умения
            foreach (Umenie::get() as $key => $umenie) {
                $model = Skill::updateOrCreate(
                    [ 'id' => $umenie->id_umenie ],
                    [ 'name' => $umenie->name_umenie, 'marking' => $umenie->key_zor, 'branch_id' => $umenie->id_galuz, 'group' => $umenie->umgroupe ]
                );
            }
        # Описание галузей
            foreach (Galuznote::get() as $key => $galuznote) {
                $model = Description::updateOrCreate(
                    [ 'id' => $galuznote->id_galuznote ],
                    [ 'branch_id' => $galuznote->id_galuz, 'cycle_id' => $galuznote->id_cikl, 'content' => $galuznote->text_galuznote ]
                );
            }
        # Первоначальные данные
            Ttype::updateOrCreate(['id'=>1],['name'=>'Шкільна команда','institution'=>1]);
            Ttype::updateOrCreate(['id'=>2],['name'=>'Творчій колектив']);
            Ttype::updateOrCreate(['id'=>3],['name'=>'Видавництво']);
            Teamrole::updateOrCreate(['id'=>1],['name'=>'Керівник']);
            Teamrole::updateOrCreate(['id'=>2],['name'=>'Учасник']);
            Teamrole::updateOrCreate(['id'=>3],['name'=>'Кандидат']);

        # Команды
            foreach (SchoolUser::get() as $key => $schooluser) {
                if (in_array($schooluser->id_school,[101,23678,23679,23680,23681,23682])){
                    $school = School::find($schooluser->id_school);
                    $model = Team::updateOrCreate(
                        [ 'meta->id_school' => $school->id_school ],
                        [ 'name' => $school->name_school, 'ttype_id' => $school->id_school<23677 ? 1 : 2, 'meta' => (object)['id_school'=>$school->id_school] ]
                    );
                    $array = [];
                    foreach ($model->users as $user) {
                        $array[$user->id] = ['teamrole_id'=>$user->pivot->teamrole_id, 'meta'=>(object)[]];
                    }
                    if ($school->supervisor_school) {$array[$school->supervisor_school] = ['teamrole_id'=>1, 'meta'=>(object)[]];}
                    $array[$schooluser->id_user] = ['teamrole_id'=> $schooluser->id_statususer==1 ? 2 : 3, 'meta'=>(object)[]];
                    $model->users()->sync($array);
                }
            }
        
        # Программы
            foreach (Program::get() as $key => $program) {
                $model = Newprogram::updateOrCreate(
                    [ 'id' => $program->id_program ],
                    [
                        'name' => $program->name_program,
                        'cycle_id' => $program->id_cikl,
                        'content' => json_decode($program->content_program),
                        'meta' => (object)[
                            'hours' => $program->hours_program,
                            'id_ptype' => $program->id_ptype,
                            'id_plan' => $program->id_plan,
                            'id_subject' => $program->id_subject,
                            'id_stype' => $program->id_stype,
                            'id_create' => $program->id_create,
                            'id_status' => $program->id_status,

                        ],
                        'created_at' => $program->created_at,
                        'updated_at' => $program->updated_at,
                    ]
                );
                $branches = [];
                foreach (json_decode($program->galuzes_program) as $key => $value) {
                    $branches[] = $value->id_galuz;
                }
                $model->branches()->sync($branches);
                if ($program->id_user && $user = User::find($program->id_user)) {$model->users()->sync([$user->id]);}
                if ($program->id_school && $team = Team::where('meta->id_school','=',$program->id_school)->first()) {
                    $editors = [];
                    foreach (json_decode($program->users_program) as $key => $value) {
                        $editors[] = (int)$value->id_user;
                    }
                    $array = [];
                    $array[$team->id] = [
                        'meta'=>(object)[
                            'editors' => $editors,
                        ]
                    ];
                    $model->teams()->sync($array);
                }

            }

        return redirect()->route('options')
            ->with('flash_message', __('Данные успешно перенесены'));
    }


    public function customseed(Request $request)
    {
        Ttype::create(['name'=>'Шкільна команда','institution'=>1]);
        Ttype::create(['name'=>'Творчій колектив']);
        Ttype::create(['name'=>'Видавництво']);
        Teamrole::create(['name'=>'Керівник']);
        Teamrole::create(['name'=>'Учасник']);
        Teamrole::create(['name'=>'Кандидат']);
        Period::create(['name'=>'9 місяців','meta'=>['Вересень','Жовтень','Листопад','Грудень','Січень','Лютий','Березень','Квітень','Травень']]);

        // $team = Team::create([
        //     'name' => 'Команда молодости нашей',
        //     'ttype_id' => 1,
        //     'meta' => (object)[],
        // ]);
        // $team->users()->sync([
        //     1=>['teamrole_id'=>2],
        //     25=>['teamrole_id'=>1],
        //     18=>['teamrole_id'=>2],
        //     13=>['teamrole_id'=>2],
        // ]);
        // $layout = Layout::create([
        //     'name' => 'Дважды шаблон Советского Союза',
        //     'cycle_id' => 1,
        //     'period_id' => 1,
        //     'meta' => (object)[],
        // ]);
        // $layout->branches()->sync([1,2,4,7]);
        // $layout->users()->sync([2]);
        // $layout->teams()->sync([1=>['meta'=>(object)[]]]);

        return redirect()->route('options')
            ->with('flash_message', __('Данные успешно созданы'));
    }

    # посев данных в ручном режиме
    public function seeds(Request $request)
    {
        if ($request->isMethod('post')) {

            if ($request->grades) {
                Artisan::call('db:seed', [
                    '--class' => 'GradeSeeder',
                    '--force' => true,
                ]);
            }
            if ($request->languagetypes) {
                Artisan::call('db:seed', [
                    '--class' => 'LanguagetypeSeeder',
                    '--force' => true,
                ]);
            }
            if ($request->cperiods) {
                Artisan::call('db:seed', [
                    '--class' => 'CperiodSeeder',
                    '--force' => true,
                ]);
            }
            if ($request->ctypes) {
                Artisan::call('db:seed', [
                    '--class' => 'CtypeSeeder',
                    '--force' => true,
                ]);
            }
            if ($request->cbranches) {
                Artisan::call('db:seed', [
                    '--class' => 'CbranchSeeder',
                    '--force' => true,
                ]);
            }
            if ($request->cauthors) {
                Artisan::call('db:seed', [
                    '--class' => 'CauthorSeeder',
                    '--force' => true,
                ]);
            }
            if ($request->components) {
                Artisan::call('db:seed', [
                    '--class' => 'ComponentSeeder',
                    '--force' => true,
                ]);
            }
            if ($request->cprograms) {
                Artisan::call('db:seed', [
                    '--class' => 'CprogramSeeder',
                    '--force' => true,
                ]);
            }
            if ($request->loadtemplates) {
                Artisan::call('db:seed', [
                    '--class' => 'LoadtemplateSeeder',
                    '--force' => true,
                ]);
            }

            if ($request->tloads) {
                Artisan::call('db:seed', [
                    '--class' => 'TloadSeeder',
                    '--force' => true,
                ]);
            }

            if ($request->tplans) {
                Artisan::call('db:seed', [
                    '--class' => 'TplanSeeder',
                    '--force' => true,
                ]);
            }

            if ($request->yplans) {
                Artisan::call('db:seed', [
                    '--class' => 'YplanSeeder',
                    '--force' => true,
                ]);
            }

            return redirect()->route('seeds')
                ->with('flash_message', 'Посев данных успешно осуществлен');
        }
        return view('dashboard.seeds',[]);
    }

    # phpinfo
    public function phpinfo()
    {
        return view('dashboard.phpinfo',[]);
    }

}
