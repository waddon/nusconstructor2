<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests\StoreGaluzRequest;

use App\Http\Controllers\Controller;
use App\Galuz;


class GaluzController extends Controller 
{

    public function index(Request $request)
    {
        return view('dashboard.galuzes.index', []);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.galuzes.form', [
                'model' => new Galuz,
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGaluzRequest $request)
    {
        $array = [
                'name_galuz' => $request->name_galuz,
                'marking_galuz' => $request->marking_galuz,
                'color_galuz' => $request->color_galuz,
                'text_galuz' => $request->text_galuz ? $request->text_galuz : '',
            ];
        $model = Galuz::create($array);
        return response([
            'message' => __('MAIN.MESSAGE.CREATED'),
            'exitURL' => route('galuzes.index'),
            'formAction' => route('galuzes.update', $model->id_galuz),
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('galuzes');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Galuz::findOrFail($id);
        return view('dashboard.galuzes.form', [
                'model' => $model,
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreGaluzRequest $request, $id)
    {
        $model = Galuz::findOrFail($id);
        $array = [
                'name_galuz' => $request->name_galuz,
                'marking_galuz' => $request->marking_galuz,
                'color_galuz' => $request->color_galuz,
                'text_galuz' => $request->text_galuz ? $request->text_galuz : '',
            ];
        $model->update($array);
        return response([
            'message' => __('MAIN.MESSAGE.UPDATED'),
            'exitURL' => route('galuzes.index'),
            'formAction' => route('galuzes.update', $model->id_galuz),
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Galuz::findOrFail($id);
        $model->delete();
        return response([], 200);
    }


    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Galuz::getListForDatatable($request) );
    }

}