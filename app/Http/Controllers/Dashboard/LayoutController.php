<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests\StoreLayoutRequest;

use App\Http\Controllers\Controller;
use App\Models\Layout;
use App\Models\Cycle;
use App\Models\Branch;
use App\Models\Period;

use App\Models\Team;
use App\Models\Ttype;

class LayoutController extends Controller {

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request) {
        return view('dashboard.layouts.index', [
            'cycles' => Cycle::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-CYCLES'),0),
            'periods' => Period::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-PERIODS'),0),
            'branches' => Branch::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-BRANCHES'),0),
        ]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create() {
        return view('dashboard.teams.form', [
                'ttypes' =>Ttype::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.SELECT-TTYPE'),0),
                'model' => new Team,
            ]);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(StoreTeamRequest $request) 
    {
        $array = [
                'name' => $request->name,
                'ttype_id' => $request->ttype_id,
            ];
        $model = Team::create($array);

        $users = [];
        $users[$request->leader_id] = ['teamrole_id'=>1,'meta'=>(object)[]];
        $model->users()->sync($users);
        $model->institutions()->sync([$request->institution_id]);

        return response([
            'message' => __('MAIN.MESSAGE.CREATED'),
            'exitURL' => route('teams.index'),
            'formAction' => route('teams.update', $model->id),
        ], 200);
    }



    public function show(Request $request, $id)
    {
        $model = Team::findOrFail($id);
        return view('dashboard.teams.show', [
            'model' => $model,
        ]);
    }


    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id) {
        $model = Team::findOrFail($id);
        return view('dashboard.teams.form', [
                'model'=>$model,
                'ttypes' =>Ttype::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.SELECT-TTYPE'),0),
            ]);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(StoreTeamRequest $request, $id)
    {
        $model = Team::findOrFail($id);
        $array = [
                'name' => $request->name,
                'ttype_id' => $request->ttype_id,
            ];
        $model->update($array);

        $users = [];
        foreach ($model->users as $user) {
            $users[ $user->id ] = [
                'teamrole_id' => $user->pivot->teamrole_id!=1 ? $user->pivot->teamrole_id : 2,
            ];
        }
        $users[$request->leader_id] = ['teamrole_id'=>1];
        $model->users()->sync($users);
        $model->institutions()->sync([$request->institution_id]);

        return response([
            'message' => __('Element updated successfully'),
            'exitURL' => route('teams.index'),
            'formAction' => route('teams.update', $model->id),
        ], 200);
    }

    /**
     * Move to trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $model = Layout::withTrashed()->findOrFail($id);
        $model->delete();
        return response([], 200);
    }

    /**
     * Restore from trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore(Request $request, $id)
    {
        $model = Layout::onlyTrashed()->findOrFail($id);
        $model->restore();
        return response([], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Layout::withTrashed()->findOrFail($id);
        $model->forceDelete();
        return response([], 200);
    }


    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Layout::getListForDatatable($request) );
    }

    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function changeStatus(Request $request, $id)
    {
        $model = Layout::findOrFail($id);
        $model->status = $request->newstatus;
        $model->public = $model->status == 'Public';
        $model->save();
        return response(['message' => $request->newstatus], 200);
    }

}
