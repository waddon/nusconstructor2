<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\Institution;


class InstitutionController extends Controller 
{

    public function index(Request $request)
    {
        return view('dashboard.institutions.index', []);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Institution::refreshData();
        return response(['message'=>__('MAIN.MESSAGE.REFRESHED')], 200);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('dashboard.institutions.show', [
            $model = Institution::findOrFail($id),
        ]);
    }


    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Institution::getListForDatatable($request) );
    }


    /**
     * Get list of data for select2.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function select2(Request $request)
    {
        $elements = [];
        $where = [];
        if ($request->searchTerm){
            $where[] = ['id', 'like', '%' . $request->searchTerm . '%', 'or'];
            $where[] = ['meta->institution_name', 'like', '%' . $request->searchTerm . '%', 'or'];
            $where[] = ['meta->region_name', 'like', '%' . $request->searchTerm . '%', 'or'];
            $where[] = ['meta->koatuu_name', 'like', '%' . $request->searchTerm . '%', 'or'];
            $where[] = ['meta->address', 'like', '%' . $request->searchTerm . '%', 'or'];
        }
        $elements = Institution::where($where)->take(10)->get();
        foreach ($elements as $key => $element) {
            $element->name = $element->meta->institution_name;
            $element->region_name = $element->meta->region_name;
            $element->koatuu_name = $element->meta->koatuu_name;
            $element->address = $element->meta->address;
        }
        return json_encode($elements);        
    }

}