<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests\StoreClassRequest;

use App\Http\Controllers\Controller;
use App\SchoolClass;
use App\Cikl;


class ClassController extends Controller 
{

    public function index(Request $request)
    {
        return view('dashboard.classes.index', []);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.classes.form', [
                'model' => new SchoolClass,
                'cikls' => Cikl::pluck('name_cikl','id_cikl')->prepend(__('MAIN.PLACEHOLDER.ALL-CIKLS'),0),
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreClassRequest $request)
    {
        $array = [
                'id_cikl' => $request->id_cikl,
                'name_class' => $request->name_class,
            ];
        $model = SchoolClass::create($array);
        return response([
            'message' => __('MAIN.MESSAGE.CREATED'),
            'exitURL' => route('classes.index'),
            'formAction' => route('classes.update', $model->id_class),
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('classes');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = SchoolClass::findOrFail($id);
        return view('dashboard.classes.form', [
                'model' => $model,
                'cikls' => Cikl::pluck('name_cikl','id_cikl')->prepend(__('MAIN.PLACEHOLDER.ALL-CIKLS'),0),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreClassRequest $request, $id)
    {
        $model = SchoolClass::findOrFail($id);
        $array = [
                'id_cikl' => $request->id_cikl,
                'name_class' => $request->name_class,
            ];
        $model->update($array);
        return response([
            'message' => __('MAIN.MESSAGE.UPDATED'),
            'exitURL' => route('classes.index'),
            'formAction' => route('classes.update', $model->id_class),
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = SchoolClass::findOrFail($id);
        $model->delete();
        return response([], 200);
    }


    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( SchoolClass::getListForDatatable($request) );
    }

}