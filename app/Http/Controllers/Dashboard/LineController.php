<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests\StoreLineRequest;

use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\Line;
use App\Models\Cycle;

class LineController extends Controller 
{

    public function index(Request $request)
    {
        return view('dashboard.lines.index', [
                'branches' => Branch::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-BRANCHES'),0),
                'cycles' => Cycle::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-CYCLES'),0),
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.lines.form', [
                'model' => new Line,
                'branches' => Branch::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-BRANCHES'),0),
                'cycles' => Cycle::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-CYCLES'),0),
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLineRequest $request)
    {
        $array = [
                'name' => $request->name,
                'marking' => $request->marking,
                'branch_id' => $request->branch_id,
                // 'cycle_id' => $request->cycle_id,
                'content' => $request->content,
            ];
        $model = Line::create($array);
        return response([
            'message' => __('MAIN.MESSAGE.CREATED'),
            'exitURL' => route('lines.index'),
            'formAction' => route('lines.update', $model->id),
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Line::findOrFail($id);
        return view('dashboard.lines.form', [
                'model' => $model,
                'branches' => Branch::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-BRANCHES'),0),
                'cycles' => Cycle::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-CYCLES'),0),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreLineRequest $request, $id)
    {
        $model = Line::findOrFail($id);
        $array = [
                'name' => $request->name,
                'marking' => $request->marking,
                'branch_id' => $request->branch_id,
                // 'cycle_id' => $request->cycle_id,
                'content' => $request->content,
            ];
        $model->update($array);
        return response([
            'message' => __('MAIN.MESSAGE.UPDATED'),
            'exitURL' => route('lines.index'),
            'formAction' => route('lines.update', $model->id),
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Line::findOrFail($id);
        if ($model->canDelete()){
            $model->delete();
            return response([], 200);
        } else {
            return response(['message'=>__('MAIN.ERROR.SPECIFICS-EXIST')], 403);
        }
    }


    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Line::getListForDatatable($request) );
    }

}