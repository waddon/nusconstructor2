<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests\StoreComponentRequest;

use App\Http\Controllers\Controller;
use App\Models\Component;
use App\Models\Ctype;
use App\Models\Cbranch;
use App\Models\Grade;
use App\Models\Languagetype;


class ComponentController extends Controller 
{

    public function index(Request $request)
    {
        return view('dashboard.components.index', [
                'languagetypes' => Languagetype::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL'),0),
                'ctypes' => Ctype::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL'),0),
                'cbranches' => Cbranch::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL'),0),
            ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.components.form', [
                'model' => new Component,
                'cbranches' => Cbranch::all(),
                'grades' =>Grade::all(),
                'ctypes' => Ctype::all(),
                'languagetypes' => Languagetype::all(),
            ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreComponentRequest $request)
    {
        $array = [
                'name' => $request->name,
                'ctype_id' => $request->ctype_id,
                'marking' => $request->marking,
            ];
        $model = Component::create($array);
        $model->languagetypes()->sync($request->languagetypes);
        $model->cbranches()->sync($request->cbranches);
        $model->grades()->sync($request->grades);
        $model->restricted()->sync($request->restricted);
        return response([
            'message' => __('MAIN.MESSAGE.CREATED'),
            'exitURL' => route('components.index'),
            'formAction' => route('components.update', $model->id),
        ], 200);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('components');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Component::findOrFail($id);
        return view('dashboard.components.form', [
                'model' => $model,
                'cbranches' => Cbranch::all(),
                'grades' => Grade::all(),
                'ctypes' => Ctype::all(),
                'languagetypes' => Languagetype::all(),
            ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreComponentRequest $request, $id)
    {
        $model = Component::findOrFail($id);
        $array = [
                'name' => $request->name,
                'ctype_id' => $request->ctype_id,
                'marking' => $request->marking,
            ];
        $model->update($array);
        $model->languagetypes()->sync($request->languagetypes);
        $model->cbranches()->sync($request->cbranches);
        $model->grades()->sync($request->grades);
        $model->restricted()->sync($request->restricted);
        return response([
            'message' => __('MAIN.MESSAGE.UPDATED'),
            'exitURL' => route('components.index'),
            'formAction' => route('components.update', $model->id),
        ], 200);
    }


    /**
     * Move to trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $model = Component::withTrashed()->findOrFail($id);
        $model->delete();
        return response([], 200);
    }


    /**
     * Restore from trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore(Request $request, $id)
    {
        $model = Component::onlyTrashed()->findOrFail($id);
        $model->restore();
        return response([], 200);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Component::withTrashed()->findOrFail($id);
        $model->forceDelete();
        return response([], 200);
    }


    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Component::getListForDatatable($request) );
    }

}