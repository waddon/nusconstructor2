<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Team;
use App\Models\Material;
use App\Models\Cycle;
use App\Models\Period;
use App\Models\Branch;
use App\Models\Newprogram;

class TypmaterialController extends Controller {

    public function __construct()
    {
    }


    public function index(Request $request)
    {
        return view('public.materials.index', []);
    }


    public function create() {
    }


    public function store(Request $request)
    {
    }


    public function show(Request $request, $id)
    {
    }


    public function edit(Request $request, $id) {
    }


    public function update(Request $request, $id)
    {
    }


    public function delete(Request $request, $id)
    {
    }

    public function restore(Request $request, $id)
    {
    }


    public function destroy(Request $request, $id)
    {
    }


    public function data(Request $request)
    {
        return json_encode( Material::getTypicalForDatatable($request, true) );
    }


    public function copyTeam(Request $request, $id)
    {
        $model = Material::findOrFail($id);
        $team = auth()->user()->teams()->find($request->team_id);
        if ($team){
            $new_name = mb_substr( '[' . __('MAIN.LABEL.COPY') . '] '  . $model->name, 0, 191);
            $new_model = $model->replicate();
            $new_model->name = $new_name;
            $new_model->status = 'Editing';
            $new_model->public = 0;
            $new_model->save();
            $new_model->teams()->sync([$team->id]);
            $specifics = $model->specifics->pluck('id','id');
            $new_model->specifics()->sync($specifics);       
            return response([ 'message' => __('MAIN.MESSAGE.COPY-SUCCESS') ], 200);
        } else {
            return response([ 'message' => __('MAIN.MESSAGE.TEAM-NOT-FOUND') ], 404);
        }
    }

    public function getList(Request $request, $program_id = 0)
    {
        $program = Newprogram::findOrFail($program_id);
        $teams = $program->teams->pluck('id','id');
        $specific = $request->kod;
        $result = '';

        $materials = Material::where('public', '=', 1)
            ->whereHas('specifics', function($q) use ($specific){
                    $q->where('marking', '=', $specific);
                })
            ->get();
        foreach ($materials as $key => $material) {
            $result .= '<p><a href="' . $material->url . '" target="_blank">' . $material->name . '</a></p>';
        }
        if ($teams){
            $materials_teams = Material::where('status', '=', 'Approved')
                ->whereHas('teams', function($q) use ($teams){
                        $q->whereIn('id', $teams);
                    })
                ->whereHas('specifics', function($q) use ($specific){
                        $q->where('marking', '=', $specific);
                    })
                ->get();
            foreach ($materials_teams as $key => $material) {
                $result .= '<p><a href="' . $material->url . '" target="_blank">' . $material->name . '</a></p>';
            }
        }
        $result = '<h3 class="text-center">' . $specific . '</h3>' . ($result ? $result : '<p class="text-center">за даними параметрами матеріали відсутні</p>');
        return response([ 'data' => $result ], 200);
    }
}
