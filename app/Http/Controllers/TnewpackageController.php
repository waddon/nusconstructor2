<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreNewpackageRequest;
use App\Http\Controllers\Controller;

use App\Models\Team;
use App\Models\Newplan;
use App\Models\Newpackage;
use App\Models\Cycle;
use App\Models\Branch;

class TnewpackageController extends Controller {

    public function __construct()
    {
        // $this->middleware('accessLayout', ['only' => ['edit', 'update', 'delete', 'restore', 'destroy','catch']]);
        // $this->middleware('canEditLayout', ['only' => ['edit', 'update', 'delete', 'restore', 'destroy','catch']]);
    }


    public function index(Request $request, $team_id)
    {
        return view('team.tnewpackages.index', [
            'team' => Team::findOrFail($team_id),
        ]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create() {
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(StoreNewpackageRequest $request, $team_id)
    {

        $newplan = Newplan::findOrFail($request->newplan_id);
        $array = [
            'name' => __('MAIN.TITLE.NEW-PACKAGE'),
            'newplan_id' => $newplan->id,
            'meta' => (object)[],
            'content' => '',            
        ];
        $model = Newpackage::create($array);
        $model->teams()->sync([ $team_id ]);

        return response([
            'relocateURL' => route('tnewpackages.edit', [$team_id, $model->id]),
        ], 200);
    }



    public function show(Request $request, $team_id, $id)
    {
        $model = Newpackage::findOrFail($id);
        $model->previousUrl = route('tnewplans.index',$team_id);
        return view('public.newplans.show', [
            'model' => $model,
        ]);
    }


    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($team_id, $id) {
        $model = Newpackage::findOrFail($id);
        $model->previousUrl = route('tnewpackages.index',$team_id);

        return view('team.tnewpackages.form', [
                'model' => $model,
                'team' => Team::find($team_id),
            ]);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(StoreNewpackageRequest $request, $team_id, $id)
    {
        $model = Newpackage::findOrFail($id);
        $meta = $model->meta;
        $meta->programs = $request->programs ? $request->programs : [];
        $array = [
            'name' => $request->name ? $request->name : '',
            'content' => $request->content ? $request->content : '',
            'meta' => $meta,
        ];
        $model->update($array);

        return response([
            'message' => __('MAIN.MESSAGE.UPDATED'),
            'exitURL' => route('tnewpackages.index', $team_id),
        ], 200);
    }


    /**
     * Move to trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $team_id, $id)
    {
        $model = Newpackage::withTrashed()->findOrFail($id);
        $model->delete();
        return response([], 200);
    }

    /**
     * Restore from trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore(Request $request, $team_id, $id)
    {
        $model = Newpackage::onlyTrashed()->findOrFail($id);
        $model->restore();
        return response([], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $team_id, $id)
    {
        $model = Newpackage::withTrashed()->findOrFail($id);
        $model->forceDelete();
        return response([], 200);
    }

    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Newpackage::getListForDatatable($request, true) );
    }


    public function send(Request $request, $team_id, $id)
    {
        $model = Newpackage::findOrFail($id);
        if ($model->canSendToApprove()){
            $model->status = 'Approving';
            $model->save();        
            return response([ 'result' => true ], 200);
        } else {
            return response([ 'message' => __('MAIN.MESSAGE.PACKAGE-SEND-ERROR') ], 403);
        }
    }

}
