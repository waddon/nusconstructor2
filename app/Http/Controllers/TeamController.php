<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Models\Team;
use App\Models\Cperiod;
use App\Models\Grade;

class TeamController extends Controller
{

    public function __construct()
    {
        $this->middleware('teamMember')->only('info');
    }

    public function info(Request $request)
    {
        return view('team.info',[
            'model' => Team::findOrfail($request->team_id),
        ]);
    }


    public function approve(Request $request, $team_id, $user_id)
    {
        if ($team_id && $user_id){
            $team = Team::find($team_id);
            $users = [];
            foreach ($team->users as $user) {
                $users[$user->id] = ['teamrole_id' => $user->pivot->teamrole_id, 'meta'=> $user->pivot->meta];
            }
            $users[$user_id] = ['teamrole_id' => 2, 'meta'=> (object)[]];
            $team->users()->sync($users);
            return response([],200);
        } else {
            return response(['message' => __('MAIN.MESSAGE.NOT-SELECTED')],403);
        }
    }

    public function remove(Request $request, $team_id, $user_id)
    {
        if ($team_id && $user_id){
            $team = Team::find($team_id);
            $users = [];
            foreach ($team->users as $user) {
                $users[$user->id] = ['teamrole_id' => $user->pivot->teamrole_id, 'meta'=> $user->pivot->meta];
            }
            unset($users[$user_id]);
            $team->users()->sync($users);
            return response([],200);
        } else {
            return response(['message' => __('MAIN.MESSAGE.NOT-SELECTED')],403);
        }
    }


    public function planing(Request $request, $team_id, $cperiod_id = 1)
    {
        return view('team.planing.index',[
            'model' => Team::findOrfail($team_id),
            'cperiod' => Cperiod::findOrfail($cperiod_id),
            'grades' => Grade::all(),
        ]);
    }
}
