<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
//use Illuminate\Support\Facades\Validator;
use Storage;

use App\Models\Team;
use App\Models\Cperiod;
use App\Models\Grade;

class UserController extends Controller
{

    public function profile(Request $request)
    {
        return view('user.profile',[]);
    }

    public function user_update(Request $request)
    {
        $validate_array = [
            'email'       => 'required|string|email|max:255|unique:table_users,email,' . auth()->user()->id,
            'name'        => 'required|string|max:255',
            'second_name' => 'required|string|max:255',
            'patronymic'  => 'required|string|max:255',
        ];
        if ($request->password || $request->password_confirmation){
            $validate_array['password'] = 'required|string|min:6|max:191|same:password_confirmation';
            $validate_array['password_confirmation'] = 'required|string|max:191|same:password';
        }
        $this->validate($request, $validate_array);
        auth()->user()->update($request->only('email', 'name', 'second_name', 'patronymic'));
        if ($request->password) {auth()->user()->update( ['password' => Hash::make($request->password)] );}

        return response(['message'=>__('MAIN.MESSAGE.REFRESHED'), 'exitURL' => route('home')],200);
    }


    public function userChangeAvatar(Request $request)
    {
        foreach ($request->file() as $file) {
            Storage::disk('public')->delete(auth()->user()->thumb);
            $path = Storage::disk('public')->putFile('avatars', $file);
            auth()->user()->thumb = $path;
            auth()->user()->save();
        }

        return response(['message'=>__('MAIN.MESSAGE.CHANGED'), 'exitURL' => route('home')],200);        
    }


    public function teams_select2(Request $request)
    {
        $elements = [];
        $where = [];
        if ($request->searchTerm){
            $where[] = ['id', 'like', '%' . $request->searchTerm . '%', 'or'];
            $where[] = ['name', 'like', '%' . $request->searchTerm . '%', 'or'];
        }
        $elements = auth()->user()->teams()->where($where)->take(10)->get();
        foreach ($elements as $key => $element) {
            // $element->name   = $element->name;
            $element->leader = $element->getLeader()->combineName();
            $element->type   = $element->ttype->name;
            $element->cusers = $element->users->count();
        }
        return json_encode($elements);        
    }


    public function teams_all_select2(Request $request)
    {
        $elements = [];
        $where = [];
        if ($request->searchTerm){
            $where[] = ['id', 'like', '%' . $request->searchTerm . '%', 'or'];
            $where[] = ['name', 'like', '%' . $request->searchTerm . '%', 'or'];
        }
        $elements = Team::where($where)->take(10)->get();
        foreach ($elements as $key => $element) {
            // $element->name   = $element->name;
            $element->leader = $element->getLeader()->combineName();
            $element->type   = $element->ttype->name;
            $element->cusers = $element->users->count();
        }
        return json_encode($elements);        
    }


    public function uteams(Request $request)
    {
        return view('user.uteams',[]);
    }


    public function apply(Request $request)
    {
        if ($request->team_id){
            $team = auth()->user()->teams()->where('id','=',$request->team_id)->first();
            if (!$team){
                $teams = [];
                foreach (auth()->user()->teams as $team) {
                    $teams[$team->id] = ['teamrole_id' => $team->pivot->teamrole_id, 'meta'=> $team->pivot->meta];
                }
                $teams[$request->team_id] = ['teamrole_id' => 3, 'meta'=> (object)[]];
                auth()->user()->teams()->sync($teams);
                return response([],200);
            } else {
                return response(['message' => __('MAIN.MESSAGE.ALREADY-IN-LIST')],403);
            }
        } else {
                return response(['message' => __('MAIN.MESSAGE.NOT-SELECTED')],403);
        }
    }


    # планирование
    public function planing(Request $request, $cperiod_id = 1)
    {
        return view('user.planing.index',[
            'model' => auth()->user(),
            'cperiod' => Cperiod::findOrfail($cperiod_id),
            'grades' => Grade::all(),
        ]);
    }

}
