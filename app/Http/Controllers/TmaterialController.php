<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreMaterialRequest;
use App\Http\Controllers\Controller;

use App\Models\Team;
use App\Models\Material;
use App\Models\Cycle;
use App\Models\Branch;
use App\Models\Specific;

class TmaterialController extends Controller {

    public function __construct()
    {
        // $this->middleware('accessLayout', ['only' => ['edit', 'update', 'delete', 'restore', 'destroy','catch']]);
        // $this->middleware('canEditLayout', ['only' => ['edit', 'update', 'delete', 'restore', 'destroy','catch']]);
    }


    public function index(Request $request, $team_id)
    {
        return view('team.tmaterials.index', [
            'team' => Team::findOrFail($team_id),
            // 'cycles' => Cycle::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-CYCLES'),0),
            // 'cycles2' => Cycle::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.SELECT-CYCLE'),0),
        ]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create(Request $request, $team_id) {
        return view('team.tmaterials.form', [
                'model' => new Material,
                'team' => Team::findOrFail($team_id),
                'specifics' => Specific::get(),
                'cycles' => Cycle::get(),
                'branches' => Branch::get(),
            ]);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(StoreMaterialRequest $request, $team_id)
    {

        $array = [
            'name' => $request->name,
            'url'  => $request->url,
        ];
        $model = Material::create($array);
        $model->teams()->sync([ $team_id ]);
        $model->specifics()->sync($request->specifics);

        return response([
            'message' => __('MAIN.MESSAGE.CREATED'),
            'exitURL' => route('tmaterials.index',$team_id),
            'formAction' => route('tmaterials.update', [$team_id, $model->id]),
        ], 200);
    }



    public function show(Request $request, $team_id, $id)
    {
        $model = Material::findOrFail($id);
        $model->previousUrl = route('tnewplans.index',$team_id);
        return view('public.materials.show', [
            'model' => $model,
        ]);
    }


    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($team_id, $id) {
        $model = Material::findOrFail($id);
        return view('team.tmaterials.form', [
                'model' => $model,
                'team' => Team::findOrFail($team_id),
                'specifics' => Specific::get(),
                'cycles' => Cycle::get(),
                'branches' => Branch::get(),
            ]);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(StoreMaterialRequest $request, $team_id, $id)
    {
        $model = Material::findOrFail($id);

        $array = [
            'name' => $request->name,
            'url'  => $request->url,
        ];
        $model->update($array);
        $model->teams()->sync([ $team_id ]);
        $model->specifics()->sync($request->specifics);

        return response([
            'message' => __('MAIN.MESSAGE.UPDATED'),
            'exitURL' => route('tmaterials.index',$team_id),
        ], 200);
    }


    /**
     * Move to trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $team_id, $id)
    {
        $model = Material::withTrashed()->findOrFail($id);
        $model->delete();
        return response([], 200);
    }

    /**
     * Restore from trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore(Request $request, $team_id, $id)
    {
        $model = Material::onlyTrashed()->findOrFail($id);
        $model->restore();
        return response([], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $team_id, $id)
    {
        $model = Material::withTrashed()->findOrFail($id);
        $model->forceDelete();
        return response([], 200);
    }

    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Material::getListForDatatable($request, true) );
    }


    public function send(Request $request, $team_id, $id)
    {
        $model = Material::findOrFail($id);
        $model->status = 'Approving';
        $model->save();        
        return response([ 'result' => true ], 200);
    }

}
