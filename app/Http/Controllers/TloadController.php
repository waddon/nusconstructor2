<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreTloadRequest;
use App\Http\Controllers\Controller;

use App\Models\Loadtemplate;
use App\Models\Tload;
use App\Models\Cbranch;
use App\Models\Team;

class TloadController extends Controller {

    public function __construct()
    {
        // $this->middleware('accessLayout', ['only' => ['edit', 'update', 'delete', 'restore', 'destroy','catch']]);
        // $this->middleware('canEditLayout', ['only' => ['edit', 'update', 'delete', 'restore', 'destroy','catch']]);
    }


    public function index(Request $request, $team_id)
    {
    }


    public function create()
    {
    }


    public function store(StoreTloadRequest $request)
    {
        $owner = $request->team_id ? Team::find($request->team_id) : auth()->user();
        // $inputs = $request->all();
        $loadtemplate = Loadtemplate::findOrFail($request->loadtemplate_id);
        $meta = $loadtemplate->getMetaByCycle($request->cycle_id);
        $array = [
            'name' => $loadtemplate->name,
            'cperiod_id' => $request->cperiod_id,
            'loadtemplate_id' => $request->loadtemplate_id,
            'cycle_id' => $request->cycle_id,
            'meta' => $meta,
        ];
        $model = $owner->tloads()->create($array);

        return response([
            'relocateURL' => route('tloads.edit', [$model->id]),
        ], 200);
    }



    public function show(Request $request, $id)
    {
        $model = Tload::findOrFail($id);
        return view('team.planing.tloads.show', [
                'model' => $model,
                'cbranches' => Cbranch::all(),
            ]);
    }


    public function edit($id)
    {
        $model = Tload::findOrFail($id);
        return view('team.planing.tloads.form', [
                'model' => $model,
                'cbranches' => Cbranch::all(),
            ]);
    }


    public function update(Request $request, $id)
    {
        $model = Tload::findOrFail($id);
        $model->update(['name' => $request->name]);
        $model->syncMeta($request->loads);
        return response([
            'message' => __('MAIN.MESSAGE.UPDATED'),
            'exitURL' => $model->parentRoute(),
        ], 200);
    }


    public function delete(Request $request, $id)
    {
        $model = Tload::withTrashed()->findOrFail($id);
        $model->delete();
        return response([], 200);
    }


    public function restore(Request $request, $id)
    {
        $model = Tload::onlyTrashed()->findOrFail($id);
        $model->restore();
        return response([], 200);
    }


    public function destroy(Request $request, $id)
    {
        $model = Tload::withTrashed()->findOrFail($id);
        $model->forceDelete();
        return response([], 200);
    }


    public function data(Request $request)
    {
        return json_encode( Tload::getListForDatatable($request) );
    }


    # Проверка учебной нагрузки на соотствие всем правилам
    // public function check(Request $request, $team_id, $id)
    // {
    //     return response([], 200);
    // }


    # Создание копии
    public function clone(Request $request, $id)
    {
        $model = Tload::findOrFail($id);

        $new_name = mb_substr( '[' . __('MAIN.LABEL.COPY') . '] '  . $model->name, 0, 191);
        $new_model = $model->replicate();
        $new_model->name = $new_name;
        $new_model->save();

        return response([ 'message' => __('MAIN.MESSAGE.CLONED') ], 200);
    }


    # Экспорт даных
    public function export(Request $request, $id)
    {
        $model = Tload::findOrFail($id);

        $headers = [
            "Content-type"=>"text/html",
            "Content-Disposition"=>"attachment;Filename=" . $model->name . ".doc"
        ];
    
        $content = view('team.planing.tloads.export',[
            'model' => $model,
            'cbranches' => Cbranch::all(),
        ])->render();

        return response()->make($content, 200, $headers);
    }

}
