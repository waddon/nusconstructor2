<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreLayoutRequest;
use App\Http\Controllers\Controller;

use App\Models\Layout;
use App\Models\Cycle;
use App\Models\Period;
use App\Models\Branch;
use App\Models\Line;

class UlayoutController extends Controller {

    public function __construct()
    {
        // $this->middleware('log', ['only' => ['fooAction', 'barAction']]);
        $this->middleware('accessLayout', ['only' => ['edit', 'update', 'delete', 'restore', 'destroy','catch']]);
    }


    public function index(Request $request) {
        return view('user.layouts.index', [
            'cycles' => Cycle::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-CYCLES'),0),
            'cycles2' => Cycle::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.SELECT-CYCLE'),0),
            'periods' => Period::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.SELECT-PERIOD'),0),
            'branches' => Branch::get(),
            'branches2' => Branch::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-BRANCHES'),0),
        ]);
    }


    public function create() {
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(StoreLayoutRequest $request)
    {

        $inputs = $request->all();
        $array = [
            'name' => __('MAIN.TITLE.NEW-LAYOUT'),
            'cycle_id' => $request->cycle_id,
            'period_id' => $request->period_id,
        ];
        $model = Layout::create($array);
        $model->users()->sync([ auth()->user()->id ]);
        $model->branches()->sync( $request->branches );

        return response([
            'relocateURL' => route('ulayouts.edit', $model->id),
        ], 200);
    }



    public function show(Request $request, $id)
    {
        $model = Layout::findOrFail($id);

        return view('user.layouts.show', []);
    }


    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id) {
        $model = Layout::findOrFail($id);

        return view('user.layouts.form', [
                'model'=>$model,
                'lines' => Line::get(),
                'periods' => Period::get(),
            ]);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(StoreStatusRequest $request, $id)
    {
        $model = Layout::findOrFail($id); //Get role specified by id

        $credentials = $request->only('name', 'color');
        $model->update($credentials);
        $model->roles()->sync($request->roles);

        return response([
            'message' => __('Element updated successfully'),
            'exitURL' => route('layouts.index'),
            'formAction' => route('layouts.update', $model->id),
        ], 200);
    }


    /**
     * Move to trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $model = Layout::withTrashed()->findOrFail($id);
        $model->closeAllConnections( ['command'=>'interruption'] );
        $model->delete();
        return response([], 200);
    }

    /**
     * Restore from trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore(Request $request, $id)
    {
        $model = Layout::onlyTrashed()->findOrFail($id);
        $model->restore();
        return response([], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Layout::withTrashed()->findOrFail($id);
        $model->forceDelete();
        return response([], 200);
    }

    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Layout::getListForDatatable($request, true) );
    }


    /**
     * Catch layout. Close all connections except owner
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function catch(Request $request, $id)
    {
        $model = Layout::withTrashed()->findOrFail($id);
        $model->closeAllConnections( ['command'=>$request->command, 'owner' => $request->owner] );
        return json_encode(['result'=>true]);
    }


    /**
     * Share layout to teams
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function share(Request $request, $id)
    {
        $model = Layout::withTrashed()->findOrFail($id);
        if ($request->team_id){
            $teams = [];
            foreach ($model->teams as $key => $team) {
                $teams[ $team->id ] = [ 'meta' => $team->meta ];
            }
            $teams[ $request->team_id ] = (isset($teams[$request->team_id])) ? $teams[$request->team_id] : [ 'meta' => (object)[] ];
            $model->teams()->sync($teams);
        }
       return response(['result'=>true], 200);
    }

}
