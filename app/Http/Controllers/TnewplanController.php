<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreNewplanRequest;
use App\Http\Controllers\Controller;

use App\Models\Team;
use App\Models\Newplan;
use App\Models\Cycle;
use App\Models\Branch;

class TnewplanController extends Controller {

    public function __construct()
    {
        // $this->middleware('accessLayout', ['only' => ['edit', 'update', 'delete', 'restore', 'destroy','catch']]);
        // $this->middleware('canEditLayout', ['only' => ['edit', 'update', 'delete', 'restore', 'destroy','catch']]);
    }


    public function index(Request $request, $team_id)
    {
        return view('team.tnewplans.index', [
            'team' => Team::findOrFail($team_id),
            'cycles' => Cycle::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-CYCLES'),0),
            'cycles2' => Cycle::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.SELECT-CYCLE'),0),
        ]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create() {
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(StoreNewplanRequest $request, $team_id)
    {

        $inputs = $request->all();
        $array = [
            'name' => __('MAIN.TITLE.NEW-PLAN'),
            'cycle_id' => $request->cycle_id,
            'meta' => (object)[],
            'content' => (object)[],            
        ];
        $model = Newplan::create($array);
        $model->teams()->sync([ $team_id ]);

        return response([
            'relocateURL' => route('tnewplans.edit', [$team_id, $model->id]),
        ], 200);
    }



    public function show(Request $request, $team_id, $id)
    {
        $model = Newplan::findOrFail($id);
        $model->previousUrl = route('tnewplans.index',$team_id);
        return view('public.newplans.show', [
            'model' => $model,
        ]);
    }


    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($team_id, $id) {
        $model = Newplan::findOrFail($id);
        $model->previousUrl = route('tnewplans.index',$team_id);

        return view('public.newplans.form', [
                'model' => $model,
                'branches' => Branch::get(),
                'team' => Team::find($team_id),
            ]);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $team_id, $id)
    {
        $model = Newplan::findOrFail($id);
        $meta = $model->meta;
        $array = [
            'name' => $request->name ? $request->name : '',
            'content' => $request->plan ? json_decode($request->plan) : (object)[],
            'meta' => $meta,
        ];
        $model->update($array);

        info($request->plan);

        return response([
            'message' => __('Element updated successfully'),
            'exitURL' => route('tnewplans.index', $team_id),
        ], 200);
    }


    /**
     * Move to trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $team_id, $id)
    {
        $model = Newplan::withTrashed()->findOrFail($id);
        $model->delete();
        return response([], 200);
    }

    /**
     * Restore from trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore(Request $request, $team_id, $id)
    {
        $model = Newplan::onlyTrashed()->findOrFail($id);
        $model->restore();
        return response([], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $team_id, $id)
    {
        $model = Newplan::withTrashed()->findOrFail($id);
        $model->forceDelete();
        return response([], 200);
    }

    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        return json_encode( Newplan::getListForDatatable($request, true) );
    }


    public function send(Request $request, $team_id, $id)
    {
        $model = Newplan::findOrFail($id);
        $model->status = 'Approving';
        $model->save();        
        return response([ 'result' => true ], 200);
    }

}
