<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\Newprogram;
use App\Tool;

class NewprogramController extends Controller 
{

    /**
     * Получение HTML элемента верхнего уровня (Темы) программы
     * 
     * @param  int  $parentId  индекс родительской программы
     * @param  int  $elementSerial  порядковый номер элемента верхнего уровня
     * @param  string  $childType  тип дочерней программы
     * 
     * @return response
     */
    public function getElementHTML(Request $request, $parentId = 0, $elementSerial = 0, $childType = 'model')
    {
        $model = Newprogram::findOrFail($parentId);
        $result = '';
        if(is_object($model->content)) {
            foreach ($model->content as $key => $line) {
                if($elementSerial == $key )
                    $result = \View::make('public.newprograms.elements.line', [
                        'type'=> $childType,
                        'parentId' => $parentId,
                        'line' => $line,
                        'lineKey' => $elementSerial,
                        'tools' => Tool::get(),
                    ])->render();
            }
        }

        return response([
            'elementHTML' => $result
        ], 200);
        // return json_encode( $result, JSON_UNESCAPED_UNICODE );
    }

}