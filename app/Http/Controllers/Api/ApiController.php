<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Requests\StoreLoadtemplateRequest;

use App\Http\Controllers\Controller;
use App\Models\Loadtemplate;
use App\Models\Team;
use App\Models\Tplan;
use App\Models\Yplan;
use App\Models\Cbranch;
use App\Models\Component;
use App\Models\Cprogram;
use App\User;

class ApiController extends Controller 
{

    # Выбор шаблонов нагрузки с перечнем циклов, к которым они относятся
    public function loadtemplates(Request $request, $cperiod_id = 0)
    {
        $result = (object)[];
        $loadtemplates = Loadtemplate
            ::when( $cperiod_id, function($query) use ($cperiod_id) {
                    return $query->whereHas('cperiods', function($q) use ($cperiod_id){
                        $q->where('id','=',$cperiod_id);
                    });
                })
            ->get();
        foreach ($loadtemplates as $loadtemplate) {
            $cycles = (object)[];
            foreach ($loadtemplate->cycles as $cycle) {
                $cycles->{$cycle->id} = $cycle->name;
            }
            $result->{$loadtemplate->id} = (object)[
                'name' => $loadtemplate->name,
                'cycles' => $cycles,
            ];
        }

        return json_encode( $result, JSON_UNESCAPED_UNICODE );
    }


    # Выбор валидных учебных планов с перечнем классов, к которым они относятся
    public function tplans(Request $request, $model_id = 0, $model_type = 'Team', $cperiod_id = 0)
    {
        $result = (object)[];

        $model = ($model_type == 'Team') ? Team::findOrFail($model_id) : User::findOrFail($model_id);
        // $team = Team::findOrFail($team_id);

        $tloads = $model->tloads()->where('cperiod_id','=',$cperiod_id)->get();
        foreach ($tloads as $key => $tload) {
            if (!$tload->validate()) unset($tloads[$key]);
        }
        $tplansIds = [];
        foreach ($tloads as $key => $tload) {
            $tplansIds[] = $tload->id;
        }

        $tplans = Tplan::whereHas('tload', function($query) use ($tplansIds){
            return $query->whereIn( 'id', $tplansIds );
        })->get();
        foreach ($tplans as $key => $tplan) {
            if (!$tplan->validate()) unset($tplans[$key]);
        }
        // return $tplans;

        foreach ($tplans as $tplan) {
            $grades = (object)[];
            // foreach ($tplan->cycles as $cycle) {
            //     $cycles->{$cycle->id} = $cycle->name;
            // }
            $result->{$tplan->id} = (object)[
                'name' => $tplan->name,
                'grades' => $tplan->tload->cycle->grades->pluck('name','id'),
            ];
        }

        // return json_encode( $tplans, JSON_UNESCAPED_UNICODE );
        return json_encode( $result, JSON_UNESCAPED_UNICODE );
    }


    # Выбор данных учебного плана
    public function yplanData(Request $request, $yplan_id)
    {
        $componentList = (object)[];
        $cprogramList = (object)[];
        $cprogramList->additional = [];
        $componentList->intersectoral = [];
        $yplan = Yplan::findOrFail($yplan_id);
        $tplan = $yplan->tplan;
        $additionalComponentList = $yplan->meta->additionalComponentList ?? (object)[];

        $componentsIds = $tplan->meta->components ?? [];

        foreach (Cbranch::get() as $key => $cbranch) {
            $branch_id = $cbranch->id;
            $components = Component::whereHas('cbranches', function($query) use ($branch_id){
                    return $query->where('id', '=', $branch_id);
                })
                ->whereIn('id',$componentsIds)
                ->get();
            $componentList->{$branch_id} = [];
            foreach ($components as $key => $component) {
                if ($component->ctype_id != 3) {
                    $currentComponent = $branch_id;
                } else {
                    $currentComponent = 'intersectoral';
                }
                // $componentList->{$branch_id}[$component->id] = (object)[
                $componentList->{$currentComponent}[$component->id] = (object)[
                    'id' => $component->id,
                    'name' => $component->name,
                    'checked' => isset($yplan->meta->usedComponents->{$component->id}) ? true : false,
                    'value' => $yplan->meta->usedComponents->{$component->id}->value ?? 0,
                    'cprogram_id' => $yplan->meta->usedComponents->{$component->id}->cprogram_id ?? 0,
                    'cprogram_name' => $yplan->meta->usedComponents->{$component->id}->cprogram_name ?? 'Програма не обрана',
                    'restricted' => $component->restricted->pluck('id','id'),
                ];

                $component_id = $component->id;
                $languagetype_id = $yplan->tplan->tload->loadtemplate->languagetype_id;
                $cprograms = Cprogram::whereHas('components', function($q) use ($component_id){
                            return $q->where('id','=',$component_id);
                        })
                    ->whereHas('languagetypes', function($q) use ($languagetype_id) {
                            return $q->where('id','=',$languagetype_id);
                        })
                    ->get();
                $cprogramList->{$component->id} = [];                    
                foreach ($cprograms as $key => $cprogram) {
                    $cprogramList->{$component->id}[$cprogram->id] = (object)[ 
                        'id' => $cprogram->id,
                        'name' => $cprogram->name,
                        'authors' => $cprogram->cauthors->implode('name', ', '),
                        'url' => $cprogram->meta->url ?? '',
                    ];
    
                    $cprogramList->additional[$cprogram->id] = (object)[ 
                        'id' => $cprogram->id,
                        'name' => $cprogram->name,
                        'authors' => $cprogram->cauthors->implode('name', ', '),
                        'url' => $cprogram->meta->url ?? '',
                    ];
                }
            }
        }

        return response([
            'componentList' => $componentList,
            'additionalComponentList' => $additionalComponentList,
            'cprogramList' => $cprogramList,
        ], 200);

    }

}