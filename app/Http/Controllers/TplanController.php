<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreTplanRequest;
use App\Http\Controllers\Controller;

use App\Models\Tload;
use App\Models\Tplan;
use App\Models\Cbranch;

class TplanController extends Controller {


    public function index(Request $request)
    {
    }


    public function create()
    {
    }


    public function store(StoreTplanRequest $request)
    {
        $tload = Tload::findOrFail($request->tload_id);

        $model = $tload->tplans()->create([
            // 'name' => $tload->name
            'name' => __('MAIN.PLACEHOLDER.TPLAN') . ' (' . $tload->cycle->name . ')'
        ]);

        return response([
            'relocateURL' => route('tplans.edit', [$model->id]),
        ], 200);
    }



    public function show(Request $request, $id)
    {
        $model = Tplan::findOrFail($id);
        return view('team.planing.tplans.show', [
                'model' => $model,
                'cbranches' => Cbranch::all(),
            ]);
    }


    public function edit($id)
    {
        $model = Tplan::findOrFail($id);
        return view('team.planing.tplans.form', [
                'model' => $model,
                'cbranches' => Cbranch::all(),
            ]);
    }


    public function update(Request $request, $id)
    {
        $model = Tplan::findOrFail($id);
        $model->update(['name' => $request->name]);
        $model->syncMeta($request->components, $request->details);
        return response([
            'message' => __('MAIN.MESSAGE.UPDATED'),
            'exitURL' => $model->tload->parentRoute(),
        ], 200);
    }


    public function delete(Request $request, $id)
    {
        $model = Tplan::withTrashed()->findOrFail($id);
        $model->delete();
        return response([], 200);
    }


    public function restore(Request $request, $id)
    {
        $model = Tplan::onlyTrashed()->findOrFail($id);
        $model->restore();
        return response([], 200);
    }


    public function destroy(Request $request, $id)
    {
        $model = Tplan::withTrashed()->findOrFail($id);
        $model->forceDelete();
        return response([], 200);
    }


    public function data(Request $request)
    {
        return json_encode( Tplan::getListForDatatable($request, true) );
    }


    # Создание копии
    public function clone(Request $request, $id)
    {
        $model = Tplan::findOrFail($id);

        $new_name = mb_substr( '[' . __('MAIN.LABEL.COPY') . '] '  . $model->name, 0, 191);
        $new_model = $model->replicate();
        $new_model->name = $new_name;
        $new_model->save();

        return response([ 'message' => __('MAIN.MESSAGE.CLONED') ], 200);
    }


    # Экспорт даных
    public function export(Request $request, $id)
    {
        $model = Tplan::findOrFail($id);

        $headers = [
            "Content-type"=>"text/html",
            "Content-Disposition"=>"attachment;Filename=" . $model->name . ".doc"
        ];
    
        $content = view('team.planing.tplans.export',[
            'model' => $model,
            'cbranches' => Cbranch::all(),
        ])->render();

        return response()->make($content, 200, $headers);
    }

}
