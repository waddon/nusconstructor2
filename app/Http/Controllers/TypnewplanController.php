<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Team;
use App\Models\Newplan;
use App\Models\Cycle;
use App\Models\Period;
use App\Models\Branch;
use App\Models\Line;
use App\Tool;

class TypnewplanController extends Controller {

    public function __construct()
    {
    }


    public function index(Request $request)
    {
        return view('public.newplans.index', [
            'cycles' => Cycle::pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.ALL-CYCLES'),0),
        ]);
    }


    public function create() {
    }


    public function store(Request $request)
    {
    }


    public function show(Request $request, $id)
    {
        $model = Newplan::findOrFail($id);
        $model->previousUrl = route('typnewprograms.index');
        return view('public.newprograms.show', [
            'model' => $model,
        ]);
    }


    public function edit(Request $request, $id) {
    }


    public function update(Request $request, $id)
    {
    }


    public function delete(Request $request, $id)
    {
    }

    public function restore(Request $request, $id)
    {
    }


    public function destroy(Request $request, $id)
    {
    }


    public function data(Request $request)
    {
        return json_encode( Newplan::getTypicalForDatatable($request, true) );
    }


    public function copyTeam(Request $request, $id)
    {
        $model = Newplan::findOrFail($id);
        $team = auth()->user()->teams()->find($request->team_id);
        if ($team){
            $new_name = mb_substr( '[' . __('MAIN.LABEL.COPY') . '] '  . $model->name, 0, 191);
            $new_model = $model->replicate();
            $new_model->name = $new_name;
            $new_model->status = 'Editing';
            $new_model->public = 0;
            $new_model->save();
            $new_model->teams()->sync([$team->id]);
            // $pivot = $new_model->teams()->find($team->id)->pivot;
            // $pivot->meta = (object)['editors' => [auth()->user()->id] ];
            // $pivot->save();
            return response([ 'message' => __('MAIN.MESSAGE.COPY-SUCCESS') ], 200);
        } else {
            return response([ 'message' => __('MAIN.MESSAGE.TEAM-NOT-FOUND') ], 404);
        }
    }

}
