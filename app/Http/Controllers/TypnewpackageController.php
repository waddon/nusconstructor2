<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Team;
use App\Models\Newpackage;
use App\Models\Newprogram;
use App\Models\Cycle;
use App\Models\Branch;
use App\Models\Line;
use App\Tool;

class TypnewpackageController extends Controller {

    public function __construct()
    {
    }


    public function index(Request $request)
    {
        return view('public.newpackages.index', []);
    }


    public function create() {
    }


    public function store(Request $request)
    {
    }


    public function show(Request $request, $id)
    {
        $model = Newplan::findOrFail($id);
        $model->previousUrl = route('typnewpackage.index');
        return view('public.newpackages.show', [
            'model' => $model,
        ]);
    }


    public function edit(Request $request, $id) {
    }


    public function update(Request $request, $id)
    {
    }


    public function delete(Request $request, $id)
    {
    }

    public function restore(Request $request, $id)
    {
    }


    public function destroy(Request $request, $id)
    {
    }


    public function data(Request $request)
    {
        return json_encode( Newpackage::getTypicalForDatatable($request, true) );
    }


    public function copyTeam(Request $request, $id)
    {
        $model = Newpackage::findOrFail($id);
        $team = auth()->user()->teams()->find($request->team_id);
        if ($team){
            $new_plan = $model->newplan->replicate();
                $new_plan->name = mb_substr( '[' . __('MAIN.LABEL.COPY') . '] '  . $new_plan->name, 0, 191);
                $new_plan->status = 'Approved';
                $new_plan->public = 0;
                $new_plan->save();
                $new_plan->teams()->sync([$team->id]);
            $programs = $model->meta->programs;
                $new_programs = [];
                foreach ($programs as $program_id) {
                    $new_program_id = 0;
                    $program = Newprogram::find($program_id);
                    if ($program){
                        $new_program = $program->replicate();
                        $new_program->name = mb_substr( '[' . __('MAIN.LABEL.COPY') . '] '  . $program->name, 0, 191);
                        $new_program->status = 'Editing';
                        $new_program->public = 0;
                        $new_program->save();
                        $branches = $program->branches->pluck('id','id');
                        $new_program->branches()->sync($branches);       
                        $new_program->teams()->sync([$team->id]);
                    }
                    $new_programs[] = $new_program_id;
                }
            $new_model = $model->replicate();
            $new_model->name = mb_substr( '[' . __('MAIN.LABEL.COPY') . '] '  . $model->name, 0, 191);
            $new_model->status = 'Editing';
            $new_model->newplan_id = $new_plan->id;
            $new_model->meta = (object)['programs'=>$new_programs];
            $new_model->public = 0;
            $new_model->save();
            $new_model->teams()->sync([$team->id]);

            return response([ 'message' => __('MAIN.MESSAGE.COPY-SUCCESS') ], 200);
        } else {
            return response([ 'message' => __('MAIN.MESSAGE.TEAM-NOT-FOUND') ], 404);
        }
    }

}
