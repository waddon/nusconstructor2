<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreKorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_kor' => 'required|string|max:191',
            'key_kor' => 'required|string|max:191',
            'id_zor' => 'integer|min:1',
            'id_zmist' => 'integer|min:1',
        ];
    }
}
