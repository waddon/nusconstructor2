<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->parameter('user') ? $this->route()->parameter('user') : 'NULL';
        return [
            'email'=>'required|email|unique:table_users,email,' . $id,
            'name' => 'required|string|max:191',
            'password' => 'required|string|min:6|max:191|same:password_confirmation',
            'password_confirmation' => 'required|string|max:191|same:password',
        ];
    }
}
