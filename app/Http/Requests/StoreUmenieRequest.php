<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUmenieRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_umenie' => 'required|string|max:191',
            'umgroupe' => 'required|string|max:191',
            'key_zor' => 'required|string|max:191',
            'id_galuz' => 'integer|min:1',
        ];
    }
}
