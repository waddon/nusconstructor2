<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Layout;

class CanEditLayoutMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $team_id = $request->route('team_id') ? $request->route('team_id') : 0;
        $id = $request->route('ulayout') ? $request->route('ulayout') : ( $request->route('tlayout') ? $request->route('tlayout') : ($request->route('id') ? $request->route('id') : 0));
        $model = layout::withTrashed()->findOrFail( $id );
        return $model->canEdit($team_id) ? $next($request) : abort('403');
    }
}