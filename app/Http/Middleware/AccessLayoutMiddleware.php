<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Layout;

class AccessLayoutMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = $request->route('ulayout') ? $request->route('ulayout') : ( $request->route('tlayout') ? $request->route('tlayout') : ($request->route('id') ? $request->route('id') : 0));
        $model = layout::withTrashed()->findOrFail( $id );
        if($model->belongToMe() || $model->belongToMyTeams()) {
            return $next($request);
        }
        return abort('404');
    }
}