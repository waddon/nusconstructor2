<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class PermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        $user = User::all()->count();
        if ( auth()->check() && (auth()->user()->can($permission) || auth()->user()->can('admin') || $user==1) ) { 
            return $next($request);
        }

        return abort(403);
    }
}