<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Team;

class TeamMemberMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $team = Team::findOrFail( $request->team_id );
        return $team->isMember( auth()->user()->id ) ? $next($request) : abort(403);
    }
}