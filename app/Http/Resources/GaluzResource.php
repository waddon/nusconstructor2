<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GaluzResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'      => $this->id_galuz,
            'name'    => $this->name_galuz,
            'marking' => $this->marking_galuz,
            'color'   => '<div class="pt-3 pr-5 pb-3 pl-5" style="background: '.$this->color_galuz.';"></div>',
            'actions' => '<div class="list-icons">'
                    . '<a href="'.route('galuzes.edit', $this->id_galuz).'" class="list-icons-item" title="'.__('MAIN.HINT.EDIT').'"><i class="icon-pencil7"></i></a>'
                    . '<a href="'.route('galuzes.destroy', $this->id_galuz).'" class="list-icons-item text-danger-600 btn-ajax-action" title="'.__('MAIN.HINT.DESTROY').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.DESTROY.TITLE').'" data-text="'.__('MAIN.MODAL.DESTROY.MESSAGE').'" data-color="bg-danger" data-method="DELETE"><i class="icon-x"></i></a>'
                    . '</div>',
        ];
    }
}
