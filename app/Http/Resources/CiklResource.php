<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CiklResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'      => $this->id_cikl,
            'name'    => $this->name_cikl,
            'riven'   => $this->riven->name_riven,
            'count'   => $this->classes->count(),
            'actions' => '<div class="list-icons">'
                    . '<a href="'.route('cikls.edit', $this->id_cikl).'" class="list-icons-item" title="'.__('MAIN.HINT.EDIT').'"><i class="icon-pencil7"></i></a>'
                    . '<a href="'.route('cikls.destroy', $this->id_cikl).'" class="list-icons-item text-danger-600 btn-ajax-action" title="'.__('MAIN.HINT.DESTROY').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.DESTROY.TITLE').'" data-text="'.__('MAIN.MODAL.DESTROY.MESSAGE').'" data-color="bg-danger" data-method="DELETE"><i class="icon-x"></i></a>'
                    . '</div>',
        ];
    }
}
