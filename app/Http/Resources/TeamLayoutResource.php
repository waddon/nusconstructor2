<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\User;
use DB;

class TeamLayoutResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $team_id = $request->team_id ? $request->team_id : 0;
        $meta = $this->teams()->where('id','=',$team_id)->first()->pivot->meta;
        $editors = isset($meta->editors) ? $meta->editors : [];
        
        return [
            'id'      => $this->id,
            'name'    => $this->name /*. '(' . $this->belongToMe() . ') (' . $this->belongToMyTeams() . ')'*/,
            'cikl'    => $this->cycle->name,
            'period'  => $this->period->name,
            'galuzes' => $this->branches->implode('name', ', '),
            'users'   => User::select( DB::raw("CONCAT(name, ' ',second_name) AS display_name") )->whereIn('id', $editors)->pluck('display_name')->implode(', '),
            // 'teams'   => $this->teams->implode('name', ', '),
            'info'    => '<strong>' . __('MAIN.COLUMN.CREATED') . ':</strong>&nbsp;' . $this->created_at->format('d.m.Y') . '<br>'
                . '<strong>' . __('MAIN.COLUMN.UPDATED') . ':</strong>&nbsp;' . $this->created_at->format('d.m.Y') . '<br>'
                . '<strong>' . __('MAIN.COLUMN.ELEMENTS') . ':</strong>&nbsp;<span class="badge bg-warning-400 align-self-center">' . $this->elementCount() . '</span><br>'
                . '<strong>' . __('MAIN.COLUMN.USED') . ':</strong>&nbsp;<span class="badge bg-success align-self-center">' . $this->elementUsed() . '</span>',
            'actions' => !$this->trashed() 
                ? '<div class="list-icons">'
                    . ( $this->canEdit($team_id) && in_array($this->status, ['Editing'])
                        ? '<a href="'.route('tlayouts.edit', [$team_id, $this->id]).'" class="list-icons-item" title="'.__('MAIN.HINT.EDIT').'"><i class="icon-pencil7"></i></a>' : '')
                    . '<a href="'.route('showlayout', $this->key).'" target="_blank" class="list-icons-item" title="'.__('MAIN.HINT.SHOW').'"><i class="icon-eye"></i></a>'
                    . ( $this->canManage($team_id) && in_array($this->status, ['Editing'])
                        ? '<a href="'.route('tlayouts.share', [$team_id, $this->id]).'" class="list-icons-item btn-share-layout" title="'.__('MAIN.HINT.SHARE').'" data-toggle="modal" data-target="#modal-share-layout" data-title="'.__('MAIN.MODAL.SHARE.TITLE').'" data-text="'.__('MAIN.MODAL.SHARE.MESSAGE').'" data-color="bg-success" data-method="POST" data-editors="' . json_encode( $editors ) . '"><i class="icon-share4"></i></a>' : '')
                    // . ($this->canManage($team_id) && in_array($this->status, ['Editing'])
                    . ($this->canEdit($team_id) && in_array($this->status, ['Editing'])
                        ? '<a href="'.route('tlayouts.send', [$team_id, $this->id]).'" class="list-icons-item btn-send-approve-layout" title="'.__('MAIN.HINT.SEND-APPROVE').'" data-toggle="modal" data-target="#modal-send-approve-layout" data-title="'.__('MAIN.MODAL.SEND-APPROVE.TITLE').'" data-text="'.__('MAIN.MODAL.SEND-APPROVE.MESSAGE').'" data-color="bg-success" data-method="POST"><i class="icon-library2"></i></a>' 
                        : '')
                    . ( $this->canEdit($team_id)
                        ? '<a href="'.route('tlayouts.clone', [$team_id, $this->id]).'" class="list-icons-item btn-ajax-action" title="'.__('MAIN.HINT.CLONE').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.CLONE.TITLE').'" data-text="'.__('MAIN.MODAL.CLONE.MESSAGE').'" data-color="bg-success" data-method="POST"><i class="icon-copy3"></i></a>' : '')
                    . ( $this->canEdit($team_id) && in_array($this->status, ['Editing']) 
                        ? '<a href="'.route('tlayouts.delete', [$team_id, $this->id]).'" class="list-icons-item text-danger-600 btn-ajax-action" title="'.__('MAIN.HINT.DELETE').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.DELETE.TITLE').'" data-text="'.__('MAIN.MODAL.DELETE.MESSAGE').'" data-color="bg-danger" data-method="POST"><i class="icon-trash"></i></a>' : '')

                    . '</div>'
                : '<div class="list-icons">'
                    . ( $this->canEdit($team_id) && in_array($this->status, ['Editing']) 
                        ? '<a href="'.route('tlayouts.restore', [$team_id, $this->id]).'" class="list-icons-item text-success-600 btn-ajax-action" title="'.__('MAIN.HINT.RESTORE').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.RESTORE.TITLE').'" data-text="'.__('MAIN.MODAL.RESTORE.MESSAGE').'" data-color="bg-success" data-method="POST"><i class="icon-undo"></i></a>' : '')
                    . ( auth()->user()->can('admin') ? '<a href="'.route('tlayouts.destroy', [$team_id, $this->id]).'" class="list-icons-item text-danger-600 btn-ajax-action" title="'.__('MAIN.HINT.DESTROY').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.DESTROY.TITLE').'" data-text="'.__('MAIN.MODAL.DESTROY.MESSAGE').'" data-color="bg-danger" data-method="DELETE"><i class="icon-x"></i></a>' : '')
                    . '</div>',
        ];
    }
}
