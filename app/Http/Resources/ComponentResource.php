<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ComponentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'       => $this->id,
            'name'     => $this->name,
            'languagetypes' => $this->languagetypes->implode('name', ', '),
            'type'     => $this->ctype->name ?? __('MAIN.LABEL.UNDEFINED'),
            'branches' => $this->cbranches->implode('name', ', '),
            'grades'   => $this->grades->implode('name', ', '),
            'restricted' => $this->restricted->implode('name', ', ') . ($this->restricted->count() > 0 ? ' [' . $this->restricted->implode('id', ', ') . ']' : ''),
            'actions'  => !$this->trashed() 
                ? '<div class="list-icons">'
                    . '<a href="'.route('components.edit', $this->id).'" class="list-icons-item" title="'.__('MAIN.HINT.EDIT').'"><i class="icon-pencil7"></i></a>'
                    . '<a href="'.route('components.show', $this->id).'" class="list-icons-item" title="'.__('MAIN.HINT.SHOW').'"><i class="icon-eye"></i></a>'
                    . '<a href="'.route('components.delete', $this->id).'" class="list-icons-item text-danger-600 btn-ajax-action" title="'.__('MAIN.HINT.DELETE').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.DELETE.TITLE').'" data-text="'.__('MAIN.MODAL.DELETE.MESSAGE').'" data-color="bg-danger" data-method="POST"><i class="icon-bin"></i></a>'
                : '<div class="list-icons">'
                    . '<a href="'.route('components.restore', $this->id).'" class="list-icons-item text-success-600 btn-ajax-action" title="'.__('MAIN.HINT.RESTORE').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.RESTORE.TITLE').'" data-text="'.__('MAIN.MODAL.RESTORE.MESSAGE').'" data-color="bg-success" data-method="POST"><i class="icon-undo"></i></a>'
                    . ( auth()->user()->can('admin') ? '<a href="'.route('components.destroy', $this->id).'" class="list-icons-item text-danger-600 btn-ajax-action" title="'.__('MAIN.HINT.DESTROY').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.DESTROY.TITLE').'" data-text="'.__('MAIN.MODAL.DESTROY.MESSAGE').'" data-color="bg-danger" data-method="DELETE"><i class="icon-x"></i></a>' : '')
                    . '</div>',
        ];
    }
}
