<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TeamNewplanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $team_id = $request->team_id ? $request->team_id : 0;
        
        return [
            'id'      => $this->id,
            'name'    => $this->name,
            'cikl'    => $this->cycle->name,
            'info'    => '<strong>' . __('MAIN.COLUMN.CREATED') . ':</strong>&nbsp;' . $this->created_at->format('d.m.Y') . '<br>'
                . '<strong>' . __('MAIN.COLUMN.UPDATED') . ':</strong>&nbsp;' . $this->created_at->format('d.m.Y') . '<br>' . '<strong>' . __('MAIN.COLUMN.PROGRAMS') . ':</strong>&nbsp;' . $this->countSubjects(),
            'actions' => !$this->trashed() 
                ? '<div class="list-icons">'
                    . ( in_array($this->status, ['Editing'])
                        ? '<a href="'.route('tnewplans.edit', [$team_id, $this->id]).'" class="list-icons-item" title="'.__('MAIN.HINT.EDIT').'"><i class="icon-pencil7"></i></a>' 
                        : '')
                    . '<a href="'.route('showplan', $this->key).'" target="_blank" class="list-icons-item" title="'.__('MAIN.HINT.SHOW').'"><i class="icon-eye"></i></a>'
                    . ($this->canManage($team_id) && in_array($this->status, ['Editing'])
                        ? '<a href="'.route('tnewplans.send', [$team_id, $this->id]).'" class="list-icons-item btn-send-approve-program" title="'.__('MAIN.HINT.SEND-APPROVE').'" data-toggle="modal" data-target="#modal-send-approve-program" data-title="'.__('MAIN.MODAL.SEND-APPROVE.TITLE').'" data-text="'.__('MAIN.MODAL.SEND-APPROVE.MESSAGE').'" data-color="bg-success" data-method="POST"><i class="icon-library2"></i></a>' 
                        : '')
                    . ( $this->canManage($team_id) && in_array($this->status, ['Editing'])
                        ? '<a href="'.route('tnewplans.delete', [$team_id, $this->id]).'" class="list-icons-item text-danger-600 btn-ajax-action" title="'.__('MAIN.HINT.DELETE').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.DELETE.TITLE').'" data-text="'.__('MAIN.MODAL.DELETE.MESSAGE').'" data-color="bg-danger" data-method="POST"><i class="icon-trash"></i></a>' 
                        : '')

                    . '</div>'
                : '<div class="list-icons">'
                    . ( $this->canManage($team_id) && in_array($this->status, ['Editing'])
                        ? '<a href="'.route('tnewplans.restore', [$team_id, $this->id]).'" class="list-icons-item text-success-600 btn-ajax-action" title="'.__('MAIN.HINT.RESTORE').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.RESTORE.TITLE').'" data-text="'.__('MAIN.MODAL.RESTORE.MESSAGE').'" data-color="bg-success" data-method="POST"><i class="icon-undo"></i></a>' 
                        : '')
                    . ( auth()->user()->can('admin') 
                        ? '<a href="'.route('tnewplans.destroy', [$team_id, $this->id]).'" class="list-icons-item text-danger-600 btn-ajax-action" title="'.__('MAIN.HINT.DESTROY').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.DESTROY.TITLE').'" data-text="'.__('MAIN.MODAL.DESTROY.MESSAGE').'" data-color="bg-danger" data-method="DELETE"><i class="icon-x"></i></a>' 
                        : '')
                    . '</div>',
        ];
    }
}
