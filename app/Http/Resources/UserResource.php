<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'      => $this->id,
            'avatar'  => '<img src="' . $this->getAvatarPath() . '" class="img-fluid rounded-circle shadow-1" width="80" height="80" alt="">',
            'name'    => $this->name,
            'email'    => $this->email,
            'roles'    => $this->roles->implode('name',', '),
            'created_at'    => $this->created_at->format('d.m.Y'),
            'actions' => !$this->trashed() 
                ? '<div class="list-icons">'
                    . '<a href="'.route('users.edit', $this->id).'" class="list-icons-item" title="'.__('MAIN.HINT.EDIT').'"><i class="icon-pencil7"></i></a>'
                    . ( auth()->user()->can('admin') ? '<a href="'.route('users.show', $this->id).'" class="list-icons-item" title="'.__('MAIN.HINT.LOGIN').'"><i class="icon-enter6"></i></a>' : '')
                    . '<a href="'.route('users.delete', $this->id).'" class="list-icons-item text-danger-600 btn-ajax-action" title="'.__('MAIN.HINT.DELETE').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.DELETE.TITLE').'" data-text="'.__('MAIN.MODAL.DELETE.MESSAGE').'" data-color="bg-danger" data-method="POST"><i class="icon-trash"></i></a>'

                    . '</div>'
                : '<div class="list-icons">'
                    . '<a href="'.route('users.restore', $this->id).'" class="list-icons-item text-success-600 btn-ajax-action" title="'.__('MAIN.HINT.RESTORE').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.RESTORE.TITLE').'" data-text="'.__('MAIN.MODAL.RESTORE.MESSAGE').'" data-color="bg-success" data-method="POST"><i class="icon-undo"></i></a>'
                    . ( auth()->user()->can('admin') ? '<a href="'.route('users.destroy', $this->id).'" class="list-icons-item text-danger-600 btn-ajax-action" title="'.__('MAIN.HINT.DESTROY').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.DESTROY.TITLE').'" data-text="'.__('MAIN.MODAL.DESTROY.MESSAGE').'" data-color="bg-danger" data-method="DELETE"><i class="icon-x"></i></a>' : '')
                    . '</div>',
        ];
    }
}
