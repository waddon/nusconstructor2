<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UmenieResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'      => $this->id_umenie,
            'name'    => $this->name_umenie,
            'group'   => $this->umgroupe,
            'key'     => $this->key_zor,
            'galuz'   => $this->galuz->name_galuz,
            'actions' => '<div class="list-icons">'
                    . '<a href="'.route('umenies.edit', $this->id_umenie).'" class="list-icons-item" title="'.__('MAIN.HINT.EDIT').'"><i class="icon-pencil7"></i></a>'
                    . '<a href="'.route('umenies.destroy', $this->id_umenie).'" class="list-icons-item text-danger-600 btn-ajax-action" title="'.__('MAIN.HINT.DESTROY').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.DESTROY.TITLE').'" data-text="'.__('MAIN.MODAL.DESTROY.MESSAGE').'" data-color="bg-danger" data-method="DELETE"><i class="icon-x"></i></a>'
                    . '</div>',
        ];
    }
}
