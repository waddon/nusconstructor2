<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserLayoutResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'      => $this->id,
            'name'    => $this->name . '(' . $this->belongToMe() . ') (' . $this->belongToMyTeams() . ')',
            'cikl'    => $this->cycle->name,
            'period'  => $this->period->name,
            'galuzes' => $this->branches->implode('name', ', '),
            'users'   => $this->users->implode('name', ', '),
            'teams'   => $this->teams->implode('name', ', '),
            'actions' => !$this->trashed() 
                ? '<div class="list-icons">'
                    . ( $this->belongToMe() ? '<a href="'.route('ulayouts.edit', $this->id).'" class="list-icons-item" title="'.__('MAIN.HINT.EDIT').'"><i class="icon-pencil7"></i></a>' : '')
                    . '<a href="'.route('ulayouts.show', $this->id).'" class="list-icons-item" title="'.__('MAIN.HINT.SHOW').'"><i class="icon-eye"></i></a>'
                    . ( $this->belongToMe() ? '<a href="'.route('ulayouts.share', $this->id).'" class="list-icons-item btn-share-layout" title="'.__('MAIN.HINT.SHARE').'" data-toggle="modal" data-target="#modal-share-layout" data-title="'.__('MAIN.MODAL.SHARE.TITLE').'" data-text="'.__('MAIN.MODAL.SHARE.MESSAGE').'" data-color="bg-success" data-method="POST"><i class="icon-share4"></i></a>' : '')
                    //. ( $this->belongToMe() ? '<a href="#" class="list-icons-item" title="'.__('MAIN.HINT.SHARE').'"><i class="icon-share4"></i></a>' : '')
                    . ( $this->belongToMe() ? '<a href="'.route('ulayouts.delete', $this->id).'" class="list-icons-item text-danger-600 btn-ajax-action" title="'.__('MAIN.HINT.DELETE').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.DELETE.TITLE').'" data-text="'.__('MAIN.MODAL.DELETE.MESSAGE').'" data-color="bg-danger" data-method="POST"><i class="icon-trash"></i></a>' : '')

                    . '</div>'
                : '<div class="list-icons">'
                    . ( $this->belongToMe() ? '<a href="'.route('ulayouts.restore', $this->id).'" class="list-icons-item text-success-600 btn-ajax-action" title="'.__('MAIN.HINT.RESTORE').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.RESTORE.TITLE').'" data-text="'.__('MAIN.MODAL.RESTORE.MESSAGE').'" data-color="bg-success" data-method="POST"><i class="icon-undo"></i></a>' : '')
                    . ( auth()->user()->can('admin') ? '<a href="'.route('ulayouts.destroy', $this->id).'" class="list-icons-item text-danger-600 btn-ajax-action" title="'.__('MAIN.HINT.DESTROY').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.DESTROY.TITLE').'" data-text="'.__('MAIN.MODAL.DESTROY.MESSAGE').'" data-color="bg-danger" data-method="DELETE"><i class="icon-x"></i></a>' : '')
                    . '</div>',
        ];
    }
}
