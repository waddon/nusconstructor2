<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\User;
use DB;

class NewprogramResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'      => $this->id,
            'name'    => $this->name,
            'cikl'    => $this->cycle->name,
            'galuzes' => $this->branches->implode('name', ', '),
            'users'   => $this->userList(),
            'teams'   => $this->teams->implode('name', ', '),
            'info'    => '<strong>' . __('MAIN.COLUMN.CREATED') . ':</strong>&nbsp;' . $this->created_at->format('d.m.Y') . '<br>'
                . '<strong>' . __('MAIN.COLUMN.UPDATED') . ':</strong>&nbsp;' . $this->updated_at->format('d.m.Y') . '<br>'
                . '<strong>' . __('MAIN.COLUMN.ELEMENTS') . ':</strong>&nbsp;<span class="badge bg-warning-400 align-self-center">' . $this->elementCount() . '</span>',
            'actions' => !$this->trashed() 
                ? '<div class="list-icons">'
                    . '<a href="'.route('newprograms.changeStatus', $this->id).'" class="list-icons-item btn-change-status" title="'.__('MAIN.HINT.WITHDRAW').'" data-toggle="modal" data-target="#modal-change-status" data-status="withdraw" data-title="'.__('MAIN.MODAL.WITHDRAW.TITLE').'" data-text="'.__('MAIN.MODAL.WITHDRAW.MESSAGE').'" data-button="'.__('MAIN.BUTTON.WITHDRAW').'" data-color="bg-warning" data-method="POST"><i class="icon-cloud-download2"></i></a>'
                    . '<a href="'.route('showprogram', $this->key).'" target="_blank" class="list-icons-item" title="'.__('MAIN.HINT.SHOW').'"><i class="icon-eye"></i></a>'
                    . ( !$this->public
                        ? '<a href="'.route('newprograms.changeStatus', $this->id).'" class="list-icons-item text-success btn-change-status" title="'.__('MAIN.HINT.PUBLIC').'" data-toggle="modal" data-target="#modal-change-status" data-status="public" data-title="'.__('MAIN.MODAL.PUBLIC.TITLE').'" data-text="'.__('MAIN.MODAL.PUBLIC.MESSAGE').'"  data-button="'.__('MAIN.BUTTON.PUBLIC').'" data-color="bg-success" data-method="POST"><i class="icon-cloud-upload2"></i></a>' : '' )
                    . '</div>'
                : '<div class="list-icons">'
                    . '<a href="'.route('newprograms.restore', $this->id).'" class="list-icons-item text-success-600 btn-ajax-action" title="'.__('MAIN.HINT.RESTORE').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.RESTORE.TITLE').'" data-text="'.__('MAIN.MODAL.RESTORE.MESSAGE').'" data-color="bg-success" data-method="POST"><i class="icon-undo"></i></a>'
                    . ( auth()->user()->can('admin') ? '<a href="'.route('newprograms.destroy', $this->id).'" class="list-icons-item text-danger-600 btn-ajax-action" title="'.__('MAIN.HINT.DESTROY').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.DESTROY.TITLE').'" data-text="'.__('MAIN.MODAL.DESTROY.MESSAGE').'" data-color="bg-danger" data-method="DELETE"><i class="icon-x"></i></a>' : '')
                    . '</div>',
        ];
    }
}
