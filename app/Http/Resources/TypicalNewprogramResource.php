<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TypicalNewprogramResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($this->type !== 'model') {
            $titleSelf = __('MAIN.HINT.COPY-SELF');
            $modalTitleSelf = __('MAIN.MODAL.COPY-SELF.TITLE');
            $modalTextSelf = __('MAIN.MODAL.COPY-SELF.MESSAGE');
            $titleTeam = __('MAIN.HINT.COPY-TEAM');
            $modalTitleTeam = __('MAIN.MODAL.COPY-TEAM.TITLE');
            $modalTextTeam = __('MAIN.MODAL.COPY-TEAM.MESSAGE');
        } else {
            $titleSelf = __('MAIN.HINT.BASE-SELF');
            $modalTitleSelf = __('MAIN.MODAL.BASE-SELF.TITLE');
            $modalTextSelf = __('MAIN.MODAL.BASE-SELF.MESSAGE');
            $titleTeam = __('MAIN.HINT.BASE-TEAM');
            $modalTitleTeam = __('MAIN.MODAL.BASE-TEAM.TITLE');
            $modalTextTeam = __('MAIN.MODAL.BASE-TEAM.MESSAGE');
        }

        return [
            'id'      => $this->id,
            'name'    => $this->name,
            'cikl'    => $this->cycle->name,
            'galuzes' => $this->branches->implode('name', ', '),
            'info'    => '<strong>' . __('MAIN.COLUMN.CREATED') . ':</strong>&nbsp;' . $this->created_at->format('d.m.Y') . '<br>'
                . '<strong>' . __('MAIN.COLUMN.UPDATED') . ':</strong>&nbsp;' . $this->updated_at->format('d.m.Y') . '<br>'
                . '<strong>' . __('MAIN.COLUMN.ELEMENTS') . ':</strong>&nbsp;<span class="badge bg-warning-400 align-self-center">' . $this->elementCount() . '</span>',
            'actions' => !$this->trashed() 
                ? '<div class="list-icons">'
                    . '<a href="'.route('showprogram', $this->key).'" target="_blank" class="list-icons-item" title="'.__('MAIN.HINT.SHOW').'"><i class="icon-eye"></i></a>'
                    . ( auth()->check()
                        ? '<a href="'.route('typnewprograms.copySelf', $this->id).'" class="list-icons-item btn-copy-program" title="'.$titleSelf.'" data-toggle="modal" data-target="#modal-copy-program" data-title="'.$modalTitleSelf.'" data-text="'.$modalTextTeam.'" data-color="bg-success" data-method="POST"><i class="icon-files-empty"></i></a>' 
                        : '')
                    . (  (auth()->check() && auth()->user()->teams->count() ) 
                        ? '<a href="'.route('typnewprograms.copyTeam', $this->id).'" class="list-icons-item btn-copy-team-program" title="'.$titleTeam.'" data-toggle="modal" data-target="#modal-copy-team-program" data-title="'.$modalTitleTeam.'" data-text="'.$modalTextTeam.'" data-color="bg-success" data-method="POST"><i class="icon-files-empty2"></i></a>' 
                        : '')
                    . ( (auth()->check() && in_array($this->type, ['educational', 'standart']))
                        ? '<a href="' . route('createCurriculum', [$this->id]). '" class="list-icons-item btn-create-curriculum" title="'.__('MAIN.HINT.CREATE-CURRICULUM').'" data-toggle="modal" data-target="#modal-create-curriculum" data-title="'.__('MAIN.MODAL.CREATE-CURRICULUM.TITLE').'" data-text="'.__('MAIN.MODAL.CREATE-CURRICULUM.MESSAGE').'" data-color="bg-success" data-method="POST"><i class="icon-table2"></i></a>' : '')

                    . '</div>'
                : '',
        ];
    }
}
