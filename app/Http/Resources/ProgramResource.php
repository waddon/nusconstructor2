<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProgramResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'      => $this->id_program,
            'name'    => $this->name_program,
            'galuzes' => $this->getGaluzesList(),
            'param'   => '',
            'teams'   => '',
            'users'   => '',
            'dates'   => '<strong>'.__('MAIN.COLUMN.CREATED').':</strong>&nbsp;' . ($this->created_at ? $this->created_at->format('d.m.Y') : '')
                    . '<br><strong>'.__('MAIN.COLUMN.UPDATED').':</strong>&nbsp;' . ($this->updated_at ? $this->updated_at->format('d.m.Y') : ''),
            'actions' => 
                '<div class="list-icons">'
                    . '<a href="'.route('programs.edit', $this->id_program).'" class="list-icons-item" title="'.__('Edit').'"><i class="icon-pencil7"></i></a>'
                    . '<a href="'.route('programs.destroy', $this->id_program).'" class="list-icons-item text-danger-600 btn-ajax-action" title="'.__('Destroy').'" data-toggle="modal" data-target="#modal-ajax-action" data-text="'.__('Text destroy').'" data-color="bg-danger" data-method="DELETE"><i class="icon-x"></i></a>'
                    . '</div>',
        ];
    }
}
