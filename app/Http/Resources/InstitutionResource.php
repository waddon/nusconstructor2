<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InstitutionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'      => $this->id,
            'name'    => isset($this->meta->institution_name) ? $this->meta->institution_name : '',
            'region'  => isset($this->meta->region_name) ? $this->meta->region_name : '',
            'koatuu'  => isset($this->meta->koatuu_name) ? $this->meta->koatuu_name : '',
            'address' => isset($this->meta->address) ? $this->meta->address : '',
            'actions' => '<div class="list-icons">'
                    . '<a href="'.route('institutions.show', $this->id).'" class="list-icons-item" title="'.__('MAIN.HINT.SHOW').'"><i class="icon-eye"></i></a>'
                    . '</div>',
        ];
    }
}
