<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LayoutResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'      => $this->id,
            'name'    => $this->name,
            'cikl'    => $this->cycle->name,
            'period'  => $this->period->name,
            'galuzes' => $this->branches->implode('name', ', '),
            'users'   => $this->users->implode('name', ', '),
            'teams'   => $this->teams->implode('name', ', '),
            'actions' => !$this->trashed() 
                ? '<div class="list-icons">'
                    . ( in_array($this->status, ['Approving'] )
                        ? '<a href="'.route('layouts.changeStatus', $this->id).'" class="list-icons-item text-warning btn-change-status" title="'.__('MAIN.HINT.DISAPPROVE').'" data-toggle="modal" data-target="#modal-change-status" data-status="Editing" data-title="'.__('MAIN.MODAL.DISAPPROVE.TITLE').'" data-text="'.__('MAIN.MODAL.DISAPPROVE.MESSAGE').'" data-button="'.__('MAIN.BUTTON.DISAPPROVE').'" data-color="bg-warning" data-method="POST"><i class="icon-bell-cross"></i></a>' : '')
                    . '<a href="'.route('showlayout', $this->key).'" target="_blank" class="list-icons-item" title="'.__('MAIN.HINT.SHOW').'"><i class="icon-eye"></i></a>'
                    . ( in_array($this->status, ['Approving'] )
                        ? '<a href="'.route('layouts.changeStatus', $this->id).'" class="list-icons-item text-success btn-change-status" title="'.__('MAIN.HINT.APPROVE').'" data-toggle="modal" data-target="#modal-change-status" data-status="Approved" data-title="'.__('MAIN.MODAL.APPROVE.TITLE').'" data-text="'.__('MAIN.MODAL.APPROVE.MESSAGE').'"  data-button="'.__('MAIN.BUTTON.APPROVE').'" data-color="bg-success" data-method="POST"><i class="icon-bell-check"></i></a>' : '' )
                    . '</div>'
                : '<div class="list-icons">'
                    . '<a href="'.route('layouts.restore', $this->id).'" class="list-icons-item text-success-600 btn-ajax-action" title="'.__('MAIN.HINT.RESTORE').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.RESTORE.TITLE').'" data-text="'.__('MAIN.MODAL.RESTORE.MESSAGE').'" data-color="bg-success" data-method="POST"><i class="icon-undo"></i></a>'
                    . ( auth()->user()->can('admin') ? '<a href="'.route('layouts.destroy', $this->id).'" class="list-icons-item text-danger-600 btn-ajax-action" title="'.__('MAIN.HINT.DESTROY').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.DESTROY.TITLE').'" data-text="'.__('MAIN.MODAL.DESTROY.MESSAGE').'" data-color="bg-danger" data-method="DELETE"><i class="icon-x"></i></a>' : '')
                    . '</div>',
        ];
    }
}
