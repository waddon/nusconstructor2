<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Auth;

class TypicalMaterialResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $team_id = $request->team_id ? $request->team_id : 0;
        
        return [
            'id'      => $this->id,
            'name'    => $this->name,
            'specific'=> $this->implodeSpecifics(),
            'url'     => '<a href="' . $this->url . '" target="_blank">' . $this->url . '</a>',
            'info'    => '<strong>' . __('MAIN.COLUMN.CREATED') . ':</strong>&nbsp;' . $this->created_at->format('d.m.Y') . '<br>'
                . '<strong>' . __('MAIN.COLUMN.UPDATED') . ':</strong>&nbsp;' . $this->created_at->format('d.m.Y'),
            'actions' => !$this->trashed() 
                ? '<div class="list-icons">'
                    . '<a href="'.$this->url.'" target="_blank" class="list-icons-item" title="'.__('MAIN.HINT.SHOW').'"><i class="icon-eye"></i></a>'
                    . (  (Auth::check() && auth()->user()->teams->count()) ? '<a href="'.route('typmaterials.copyTeam', $this->id).'" class="list-icons-item btn-copy-team-program" title="'.__('MAIN.HINT.COPY-TEAM').'" data-toggle="modal" data-target="#modal-copy-team-program" data-title="'.__('MAIN.MODAL.COPY-TEAM.TITLE').'" data-text="'.__('MAIN.MODAL.COPY-TEAM.MESSAGE').'" data-color="bg-success" data-method="POST"><i class="icon-files-empty2"></i></a>' : '')
                    . '</div>'
                : '',
        ];
    }
}
