<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TplanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        
        return [
            'id'      => $this->id,
            'name' => $this->name,
            'cycle'   => $this->tload->cycle->name,
            'info' => ($this->tload->validate() ? '<i class="icon-checkmark text-success-600" title="'.__('MAIN.HINT.NO-ERRORS').'"></i>' : '<i class="icon-warning22 text-danger-600" title="'.__('MAIN.HINT.ERROR-EXISTS').'"></i>') . ' ' .
                ($this->validate() ? '<i class="icon-checkmark text-success-600" title="'.__('MAIN.HINT.NO-ERRORS').'"></i>' : '<i class="icon-warning22 text-danger-600" title="'.__('MAIN.HINT.ERROR-EXISTS').'"></i>'),
            'actions' => !$this->trashed()
                    ? '<a href="'.route('tplans.edit', [ $this->id]).'" class="list-icons-item mr-1" title="'.__('MAIN.HINT.EDIT').'"><i class="icon-pencil7"></i></a>'
                    . '<a href="'.route('tplans.show', [ $this->id]).'" class="list-icons-item mr-1" title="'.__('MAIN.HINT.SHOW').'"><i class="icon-eye"></i></a>'
                    . '<a href="'.route('tplans.clone', [$this->id]).'" class="list-icons-item  mr-1 btn-ajax-action" title="'.__('MAIN.HINT.CLONE').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.CLONE.TITLE').'" data-text="'.__('MAIN.MODAL.CLONE.MESSAGE').'" data-color="bg-success" data-method="POST"><i class="icon-copy4"></i></a>'
                    . '<a href="'.route('tplans.delete', [ $this->id]).'" class="list-icons-item text-danger-600 btn-ajax-action" title="'.__('MAIN.HINT.DELETE').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.DELETE.TITLE').'" data-text="'.__('MAIN.MODAL.DELETE.MESSAGE').'" data-color="bg-danger" data-method="POST"><i class="icon-trash"></i></a>'
                    . '</div>'
                : '<div class="list-icons">'
                    . '<a href="'.route('tplans.restore', [ $this->id]).'" class="list-icons-item text-success-600 btn-ajax-action" title="'.__('MAIN.HINT.RESTORE').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.RESTORE.TITLE').'" data-text="'.__('MAIN.MODAL.RESTORE.MESSAGE').'" data-color="bg-success" data-method="POST"><i class="icon-undo"></i></a>'
                    . '<a href="'.route('tplans.destroy', [ $this->id]).'" class="list-icons-item text-danger-600 btn-ajax-action" title="'.__('MAIN.HINT.DESTROY').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.DESTROY.TITLE').'" data-text="'.__('MAIN.MODAL.DESTROY.MESSAGE').'" data-color="bg-danger" data-method="DELETE"><i class="icon-x"></i></a>'
        ];
    }
}
