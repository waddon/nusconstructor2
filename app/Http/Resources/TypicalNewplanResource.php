<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Auth;

class TypicalNewplanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'      => $this->id,
            'name'    => $this->name,
            'cikl'    => $this->cycle->name,
            'info'    => '<strong>' . __('MAIN.COLUMN.CREATED') . ':</strong>&nbsp;' . $this->created_at->format('d.m.Y') . '<br>'
                . '<strong>' . __('MAIN.COLUMN.UPDATED') . ':</strong>&nbsp;' . $this->created_at->format('d.m.Y'),
            'actions' => !$this->trashed() 
                ? '<div class="list-icons">'
                    . '<a href="'.route('showplan', $this->key).'" target="_blank" class="list-icons-item" title="'.__('MAIN.HINT.SHOW').'"><i class="icon-eye"></i></a>'
                    . (  (Auth::check() && auth()->user()->teams->count()) ? '<a href="'.route('typnewplans.copyTeam', $this->id).'" class="list-icons-item btn-copy-team-program" title="'.__('MAIN.HINT.COPY-TEAM').'" data-toggle="modal" data-target="#modal-copy-team-program" data-title="'.__('MAIN.MODAL.COPY-TEAM.TITLE').'" data-text="'.__('MAIN.MODAL.COPY-TEAM.MESSAGE').'" data-color="bg-success" data-method="POST"><i class="icon-files-empty2"></i></a>' : '')
                    . '</div>'
                : '',
        ];
    }
}
