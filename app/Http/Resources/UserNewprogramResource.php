<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserNewprogramResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'      => $this->id,
            'name'    => $this->name /*. '(' . $this->belongToMe() . ') (' . $this->belongToMyTeams() . ')'*/,
            'cikl'    => $this->cycle->name,
            'period'  => ''/*$this->period->name*/,
            'galuzes' => $this->branches->implode('name', ', '),
            'users'   => $this->userList(),
            'teams'   => $this->teams->implode('name', ', '),
            'info'    => '<strong>' . __('MAIN.COLUMN.CREATED') . ':</strong>&nbsp;' . $this->created_at->format('d.m.Y') . '<br>'
                . '<strong>' . __('MAIN.COLUMN.UPDATED') . ':</strong>&nbsp;' . $this->updated_at->format('d.m.Y') . '<br>'
                . '<strong>' . __('MAIN.COLUMN.ELEMENTS') . ':</strong>&nbsp;<span class="badge bg-warning-400 align-self-center">' . $this->elementCount() . '</span>',
            'actions' => !$this->trashed() 
                ? '<div class="list-icons">'
                    . ( $this->belongToMe()  && in_array($this->status, ['Editing'])
                        ? '<a href="'.route('unewprograms.edit', $this->id).'" class="list-icons-item" title="'.__('MAIN.HINT.EDIT').'"><i class="icon-pencil7"></i></a>' : '')
                    . '<a href="'.route('showprogram', $this->key).'" target="_blank" class="list-icons-item" title="'.__('MAIN.HINT.SHOW').'"><i class="icon-eye"></i></a>'
                    . ( $this->belongToMe()  && in_array($this->status, ['Editing'])
                        ? '<a href="'.route('unewprograms.share', $this->id).'" class="list-icons-item btn-share-program" title="'.__('MAIN.HINT.SHARE').'" data-toggle="modal" data-target="#modal-share-program" data-title="'.__('MAIN.MODAL.SHARE.TITLE').'" data-text="'.__('MAIN.MODAL.SHARE.MESSAGE').'" data-color="bg-success" data-method="POST"><i class="icon-share4"></i></a>' : '')
                    . ( $this->belongToMe() && in_array($this->status, ['Editing'])
                        ? '<a href="'.route('unewprograms.send', $this->id).'" class="list-icons-item btn-ajax-action" title="'.__('MAIN.HINT.SEND-APPROVE').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.SEND-APPROVE.TITLE').'" data-text="'.__('MAIN.MODAL.SEND-APPROVE.MESSAGE').'" data-color="bg-success" data-method="POST"><i class="icon-cloud-upload2"></i></a>' : '')
                    . ( $this->belongToMe() && in_array($this->status, ['Editing', 'Approving'])
                        ? '<a href="'.route('unewprograms.clone', $this->id).'" class="list-icons-item btn-ajax-action" title="'.__('MAIN.HINT.CLONE').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.CLONE.TITLE').'" data-text="'.__('MAIN.MODAL.CLONE.MESSAGE').'" data-color="bg-success" data-method="POST"><i class="icon-copy3"></i></a>' : '')                    
                    . ( in_array($this->type, ['educational', 'standart'])
                        ? '<a href="' . route('createCurriculum', $this->id). '" class="list-icons-item btn-ajax-action" title="'.__('MAIN.HINT.CREATE-CURRICULUM').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.CREATE-CURRICULUM.TITLE').'" data-text="'.__('MAIN.MODAL.CREATE-CURRICULUM.MESSAGE').'" data-color="bg-success" data-method="POST"><i class="icon-table2"></i></a>' : '')
                    . ( $this->belongToMe()  && in_array($this->status, ['Editing'])
                        ? '<a href="'.route('unewprograms.delete', $this->id).'" class="list-icons-item text-danger-600 btn-ajax-action" title="'.__('MAIN.HINT.DELETE').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.DELETE.TITLE').'" data-text="'.__('MAIN.MODAL.DELETE.MESSAGE').'" data-color="bg-danger" data-method="POST"><i class="icon-trash"></i></a>' : '')
                    . ( $this->belongToMe() && ($this->status == 'Approving' || (in_array($this->type, ['educational', 'curriculum']) && $this->public))
                        ? '<a href="'.route('unewprograms.withdraw', $this->id).'" class="list-icons-item btn-ajax-action" title="'.__('MAIN.HINT.WITHDRAW').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.WITHDRAW.TITLE').'" data-text="'.__('MAIN.MODAL.WITHDRAW.MESSAGE').'" data-color="bg-secondary" data-method="POST"><i class="icon-cloud-download2"></i></a>' 
                        : '')
                    . '</div>'
                : '<div class="list-icons">'
                    . ( $this->belongToMe() ? '<a href="'.route('unewprograms.restore', $this->id).'" class="list-icons-item text-success-600 btn-ajax-action" title="'.__('MAIN.HINT.RESTORE').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.RESTORE.TITLE').'" data-text="'.__('MAIN.MODAL.RESTORE.MESSAGE').'" data-color="bg-success" data-method="POST"><i class="icon-undo"></i></a>' : '')
                    . ( auth()->user()->can('admin') ? '<a href="'.route('unewprograms.destroy', $this->id).'" class="list-icons-item text-danger-600 btn-ajax-action" title="'.__('MAIN.HINT.DESTROY').'" data-toggle="modal" data-target="#modal-ajax-action" data-title="'.__('MAIN.MODAL.DESTROY.TITLE').'" data-text="'.__('MAIN.MODAL.DESTROY.MESSAGE').'" data-color="bg-danger" data-method="DELETE"><i class="icon-x"></i></a>' : '')
                    . '</div>',
        ];
    }
}
