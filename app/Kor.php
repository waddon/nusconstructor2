<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\KorResource;
use Illuminate\Http\Request;

class Kor extends Model
{
    protected $table = 'kors';
    protected $guarded = [];
    public $primaryKey  = 'id_kor';    
    public $timestamps = FALSE;

    public function zor()
    {
        return $this->belongsTo('App\Zor','id_zor','id_zor');
    }

    public function zmist()
    {
        return $this->belongsTo('App\Zmist','id_zmist','id_zmist');
    }

    public static $columns = [
            0 => 'id_kor',
            1 => 'name_kor',
            2 => 'key_kor',
        ];

    static public function getListForDatatable(Request $request)
    {
        $where = [];
        if ($request->id_zor) {$where[] = ['id_zor','=',$request->id_zor];}
        if ($request->id_zmist) {$where[] = ['id_zmist','=',$request->id_zmist];}
        $search = $request->input('search.value');
        $totalData = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();

        $totalFiltered = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id_kor', '=', (int)$search, 'or'],
                            ['name_kor', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->where($where)
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id_kor', '=', (int)$search, 'or'],
                            ['name_kor', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->where($where)
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => KorResource::collection($models),
        ];

        return $json_data;
    }


}
