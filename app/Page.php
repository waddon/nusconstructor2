<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\PageResource;
use Illuminate\Http\Request;

class Page extends Model
{
    //protected $table = 'pages';
    protected $primaryKey = 'id_page';
    protected $guarded = [];
    public $incrementing = 'id_page';
    public $timestamps = TRUE;


    public static $columns = [
            0 => 'id_page',
            1 => 'name_page',
            2 => 'created_at',
        ];


    static public function getListForDatatable(Request $request)
    {
        $search = $request->input('search.value');
        $totalData = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();

        $totalFiltered = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id_page', '=', (int)$search, 'or'],
                            ['name_page', 'like', "%{$search}%", 'or'],
                        ]);
                })
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id_page', '=', (int)$search, 'or'],
                            ['name_page', 'like', "%{$search}%", 'or'],
                        ]);
                })
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => PageResource::collection($models),
        ];

        return $json_data;
    }


    public function getExcerpt($words = 150)
    {
        $array = explode(" ", strip_tags($this->content_page) );
        $array = array_slice($array, 0, $words);
        return implode(" ", $array) . '...';
    }
}