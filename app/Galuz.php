<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\GaluzResource;
use Illuminate\Http\Request;

class Galuz extends Model
{
    protected $table = 'galuzes';
    protected $guarded = [];
    public $primaryKey  = 'id_galuz';
    public $timestamps = FALSE;

    public static $columns = [
            0 => 'id_galuz',
            1 => 'name_galuz',
            2 => 'marking_galuz',
        ];

    static public function getListForDatatable(Request $request)
    {
        $search = $request->input('search.value');
        $totalData = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();

        $totalFiltered = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id_galuz', '=', (int)$search, 'or'],
                            ['name_galuz', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id_galuz', '=', (int)$search, 'or'],
                            ['name_galuz', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => GaluzResource::collection($models),
        ];

        return $json_data;
    }

}
