<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\PostResource;
use Illuminate\Http\Request;

class Post extends Model
{
    protected $primaryKey = 'id_post';
    protected $guarded = [];
    public $incrementing = 'id_post';
    public $timestamps = TRUE;

    public function cat()
    {
        return $this->belongsTo('App\Cat','id_cat','id_cat');
    }

    public static $columns = [
            0 => 'id_post',
            1 => 'name_post',
            3 => 'created_at',
        ];

    static public function getListForDatatable(Request $request)
    {
        $category_id = $request->category_id ? $request->category_id : 0;
        $search = $request->input('search.value');
        $totalData = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();

        $totalFiltered = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id_post', '=', (int)$search, 'or'],
                            ['name_post', 'like', "%{$search}%", 'or'],
                        ]);
                })
            ->when( $category_id,  function($query) use ($category_id) {
                    return $query->whereHas('cat', function($q) use ($category_id){
                        $q->where('id_cat', '=', $category_id);
                    });
                })
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id_post', '=', (int)$search, 'or'],
                            ['name_post', 'like', "%{$search}%", 'or'],
                        ]);
                })
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( $category_id,  function($query) use ($category_id) {
                    return $query->whereHas('cat', function($q) use ($category_id){
                        $q->where('id_cat', '=', $category_id);
                    });
                })
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => PostResource::collection($models),
        ];

        return $json_data;
    }


}