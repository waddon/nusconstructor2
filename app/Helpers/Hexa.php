<?php
namespace App\Helpers;
use Carbon\Carbon;
 
class Hexa {

    /**
     * @param date
     * 
     * @return string
     */
    public static function monthTranslate($date) {
        $date = Carbon::parse($date);
        return $date->format('d ') . __($date->format('F')) . $date->format(' Y');
    }


    /**
     * @param int
     * 
     * @return string
     */
    public static function addZeroBefore($number, $digits=6) {
        return substr(str_repeat('0',$digits) . $number, -$digits);
    }


    public static function mb_wordwrap($str, $width = 75, $break = "\n", $cut = false)
    {
        $lines = explode($break, $str);
        foreach ($lines as &$line) {
            $line = rtrim($line);
            if (mb_strlen($line) <= $width)
                continue;
            $words = explode(' ', $line);
            $line = '';
            $actual = '';
            foreach ($words as $word) {
                if (mb_strlen($actual.$word) <= $width)
                    $actual .= $word.' ';
                else {
                    if ($actual != '')
                        $line .= rtrim($actual).$break;
                    $actual = $word;
                    if ($cut) {
                        while (mb_strlen($actual) > $width) {
                            $line .= mb_substr($actual, 0, $width).$break;
                            $actual = mb_substr($actual, $width);
                        }
                    }
                    $actual .= ' ';
                }
            }
            $line .= trim($actual);
        }
        return implode($break, $lines);
    }


    static public function getDataFromUrl($attr=[])
    {
        # определяем начальные параметры
            $result = false;
            $url = (isset($attr['url']) && $attr['url']) ? $attr['url'] : '';
            $repeat = (isset($attr['repeat'])) ? (int)$attr['repeat'] : 10;
        # снимаем временное ограничение выполнения скрипта и добавляем памяти
            set_time_limit(0);
            ini_set ('memory_limit', '256M');
        # получаем данные и формируем ответ
            if ($url){
                $opts = [
                    'http' => [
                        'method' => "GET",
                        'header' => "Accept-language: en\r\n"
                    ]
                ];
                $context = stream_context_create($opts);

                for ($i=0; $i < $repeat; $i++) { 
                    $html_request = @file_get_contents($url, false, $context);
                    if ($html_request!==false){
                        break;
                    }
                }

                $result = (object)[
                        'html_request' => $html_request,
                        'attempts' => $i+1,
                        'http_response_header' => isset($http_response_header) ? $http_response_header : false,
                ];

            }

        return $result;
    }
}