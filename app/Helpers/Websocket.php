<?php

namespace App\Helpers;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class Websocket implements MessageComponentInterface {

    protected $clients;

    protected $channels;
    protected $users;
    protected $userNames;

    public function __construct() {
        $this->clients = new \SplObjectStorage;
    }


    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);

        echo "New connection! ({$conn->resourceId})\n";
    }


    public function onMessage(ConnectionInterface $from, $msg) {
        $objectMsg = json_decode( $msg );
        if(!json_last_error() && isset( $objectMsg->commandType )) {
            if( $objectMsg->commandType == 'connect' ) {

                $this->channels[ $objectMsg->channel ][ $from->resourceId ] = $from;
                $this->users[ $from->resourceId ] = $objectMsg->channel;
                $this->userNames[ $objectMsg->channel ][ $from->resourceId ] = $objectMsg->user;

                $this->sendMessages(
                    $this->channels[ $objectMsg->channel ],
                    json_encode(['commandType' => 'interruption', 'user' => $objectMsg->user])
                );
            }
        }
    }


    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);

        $channel = $this->users[ $conn->resourceId ];
        unset($this->channels[ $channel ][ $conn->resourceId ]);
        unset($this->users[ $conn->resourceId ]);
        unset($this->userNames[ $channel ][ $conn->resourceId ]);

        echo "Connection {$conn->resourceId} has disconnected\n";
    }


    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }


    # отправка сообщений всем пользователям согласно списка
    protected function sendMessages($userList, $message, $author = null)
    {
        foreach ($userList as $client) {
            $client->send( $message );
        }        
    }

}