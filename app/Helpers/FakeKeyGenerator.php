<?php

namespace App\Helpers;

class FakeKeyGenerator implements KeyGenerator
{

	public function generate()
	{
		return 'TESTKEY12345';
	}

}
