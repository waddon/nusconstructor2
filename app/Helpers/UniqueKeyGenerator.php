<?php

namespace App\Helpers;

class UniqueKeyGenerator implements KeyGenerator
{

    public function generate()
    {
        $pool = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        return substr(str_shuffle(str_repeat($pool, 32)), 0, 32);
    }

}
