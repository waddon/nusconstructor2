<?php

namespace App\Helpers;

interface KeyGenerator
{

	public function generate();

}