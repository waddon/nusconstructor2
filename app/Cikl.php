<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\CiklResource;
use Illuminate\Http\Request;

class Cikl extends Model
{
    protected $table = 'cikls';
    protected $guarded = [];
    public $primaryKey  = 'id_cikl';
    public $timestamps = FALSE;

    public function riven()
    {
        return $this->belongsTo('App\Riven','id_riven','id_riven');
    }

    public function note()
    {
        return $this->hasMany('App\Note', 'id_cikl', 'id_cikl');
    }

    public function classes()
    {
        return $this->hasMany('App\SchoolClass', 'id_cikl', 'id_cikl');
    }

    public static $columns = [
            0 => 'id_cikl',
            1 => 'name_cikl',
        ];

    static public function getListForDatatable(Request $request)
    {
        $search = $request->input('search.value');
        $totalData = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();

        $totalFiltered = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id_cikl', '=', (int)$search, 'or'],
                            ['name_cikl', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id_cikl', '=', (int)$search, 'or'],
                            ['name_cikl', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => CiklResource::collection($models),
        ];

        return $json_data;
    }


}