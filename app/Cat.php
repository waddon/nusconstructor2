<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\CatResource;
use Illuminate\Http\Request;

class Cat extends Model
{
    protected $table = 'cats';
    protected $primaryKey = 'id_cat';
    protected $guarded = [];
    public $timestamps = FALSE;

    public function posts()
    {
        return $this->hasMany('App\Post', 'id_cat', 'id_cat');
    }

    public static $columns = [
            0 => 'id_cat',
            1 => 'name_cat',
            2 => 'slug_cat',
        ];

    static public function getListForDatatable(Request $request)
    {
        $search = $request->input('search.value');
        $totalData = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();

        $totalFiltered = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id_cat', '=', (int)$search, 'or'],
                            ['name_cat', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id_cat', '=', (int)$search, 'or'],
                            ['name_cat', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => CatResource::collection($models),
        ];

        return $json_data;
    }


}