<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\ZmistResource;
use Illuminate\Http\Request;

class Zmist extends Model
{
    protected $table = 'zmists';
    protected $guarded = [];
    public $primaryKey  = 'id_zmist';    
    public $timestamps = FALSE;

    public function galuz()
    {
        return $this->belongsTo('App\Galuz','id_galuz','id_galuz');
    }

    public static $columns = [
            0 => 'id_zmist',
            1 => 'name_zmist',
        ];

    static public function getListForDatatable(Request $request)
    {
        $where = [];
        if ($request->id_galuz) {$where[] = ['id_galuz','=',$request->id_galuz];}
        $search = $request->input('search.value');
        $totalData = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();

        $totalFiltered = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id_zmist', '=', (int)$search, 'or'],
                            ['name_zmist', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->where($where)
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id_zmist', '=', (int)$search, 'or'],
                            ['name_zmist', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->where($where)
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => ZmistResource::collection($models),
        ];

        return $json_data;
    }

}
