<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\PermissionResource;
use Illuminate\Http\Request;

use Spatie\Permission\Models\Permission as OldPirmission;

class Permission extends OldPirmission
{
    protected $table = 'permissions';
    public $primaryKey  = 'id';

    public static $columns = [
            0 => 'id',
            1 => 'name',
            3 => 'created_at',
        ];

    protected $fillable = [
        'name', 'guard_name'
    ];


    static public function getListForDatatable(Request $request)
    {
        $search = $request->input('search.value');
        $totalData = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();

        $totalFiltered = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => PermissionResource::collection($models),
        ];

        return $json_data;
    }

}
