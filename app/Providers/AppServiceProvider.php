<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Helpers\KeyGenerator;
use App\Helpers\UniqueKeyGenerator;
use URL;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $registrar = new \App\Routing\ResourceAddRouts($this->app['router']);

        $this->app->bind('Illuminate\Routing\ResourceRegistrar', function () use ($registrar) {
            return $registrar;
        });
        
        $this->app->singleton(KeyGenerator::class, UniqueKeyGenerator::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        if(env('FORCE_HTTPS',false)) { 
            URL::forceScheme('https');
        }
    }
}
