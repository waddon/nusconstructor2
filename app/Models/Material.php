<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Resources\MaterialResource;
use App\Http\Resources\TeamMaterialResource;
use App\Http\Resources\TypicalMaterialResource;
use Illuminate\Http\Request;
use App\Helpers\KeyGenerator;
use App\Models\Branch;

class Material extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $casts = [
        'content' => 'object',
    ];

    public static function boot()
    {
        parent::boot();
        self::created(function($model){
            $key = app(KeyGenerator::class)->generate();
            info($key);
            $model->key = $key;
            $model->save();
        });
    }

    public function teams()
    {
        return $this->belongsToMany('App\Models\Team');
    }

    public function specifics()
    {
        return $this->belongsToMany('App\Models\Specific');
    }

    public static $columns = [
            0 => 'id',
            1 => 'name',
        ];


    static public function getListForDatatable(Request $request, $onlymy = false)
    {
        $where = [];
        if ($request->cycle_id) {$where[] = ['cycle_id','=',$request->cycle_id];}
        if ($request->status) {$where[] = ['status','=',$request->status];} else {}
        $team_id = $request->team_id ? $request->team_id : null;
        $user_id = ($request->team_id || !$onlymy) ? null : auth()->user()->id;

        $branch_id = $request->branch_id ? $request->branch_id : 0;
        $search = $request->input('search.value');
        $totalData = self::when( $team_id,  function($query) use ($team_id) {
                    return $query->whereHas('teams', function($q) use ($team_id){
                        $q->where('id', '=', $team_id);
                    });
                })
            ->when( !$request->status, function($query){
                    $query->whereNotIn('status',['Approving','Approved','Public']);
                })
            ->when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();

        $totalFiltered = self::when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when( $team_id,  function($query) use ($team_id) {
                    return $query->whereHas('teams', function($q) use ($team_id){
                        $q->where('id', '=', $team_id);
                    });
                })
            ->when( !$request->status, function($query){
                    $query->whereNotIn('status',['Approving','Approved','Public']);
                })
            ->where($where)
            ->when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when( $team_id,  function($query) use ($team_id) {
                    return $query->whereHas('teams', function($q) use ($team_id){
                        $q->where('id', '=', $team_id);
                    });
                })
            ->when( !$request->status, function($query){
                    $query->whereNotIn('status',['Approving','Approved','Public']);
                })
            ->where($where)
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => (!$onlymy) ? MaterialResource::collection($models) : TeamMaterialResource::collection($models),
        ];

        return $json_data;
    }


    public function belongToMyTeams()
    {
        $result = $this->teams()->whereHas('users', function($q){
                $q->where('id', '=', auth()->user()->id);
            })->count();
        return $result;
    }


    public function canManage($team_id)
    {
        $team = $this->teams()->where('id','=',$team_id)->first();
        $result = $team ? $team->isLeader( auth()->user()->id ) : false;
        return $result;
    }


    static public function getTypicalForDatatable(Request $request)
    {
        $where = [['public','=',1]];
        if ($request->cycle_id) {$where[] = ['cycle_id','=',$request->cycle_id];}

        $search = $request->input('search.value');
        $totalData = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->where($where)
            ->count();

        $totalFiltered = self::when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->where($where)
            ->when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->where($where)
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => TypicalMaterialResource::collection($models),
        ];

        return $json_data;
    }

    public function implodeSpecifics()
    {
        $array = [];
        foreach ($this->specifics as $key => $specific) {
            $array[] = '<span class="badge text-white" style="background:' . $specific->expect->branch->color . '" title="' . $specific->name . '">' . $specific->marking . '</span>';
        }
        return implode(' ', $array);
    }
}
