<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\DescriptionResource;
use Illuminate\Http\Request;
use Hexa;

class Description extends Model
{
    // protected $connection = 'new';
    protected $guarded = [];

    public function branch()
    {
        return $this->belongsTo('App\Models\Branch');
    }

    public function cycle()
    {
        return $this->belongsTo('App\Models\Cycle');
    }

    public static $columns = [
            0 => 'id',
        ];

    static public function getListForDatatable(Request $request)
    {
        $where = [];
        if ($request->branch_id) {$where[] = ['branch_id','=',$request->branch_id];}
        if ($request->cycle_id) {$where[] = ['cycle_id','=',$request->cycle_id];}
        $search = $request->input('search.value');
        $totalData = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();

        $totalFiltered = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->where($where)
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->where($where)
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => DescriptionResource::collection($models),
        ];

        return $json_data;
    }

    public function getExcerpt()
    {
        $string = strip_tags($this->content);
        $excerpt = mb_substr($string, 0, mb_strpos(Hexa::mb_wordwrap($string, 300), "\n"));
        return strlen($string) == strlen($excerpt) ? $string : $excerpt . '...';
    }

}
