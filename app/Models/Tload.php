<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\TloadResource;
use Illuminate\Http\Request;

class Tload extends Model
{
    use SoftDeletes;
    // protected $connection = 'new';
    protected $guarded = [];

    protected $casts = [
        'meta' => 'object'
    ];

    public function loadtemplate()
    {
        return $this->belongsTo('App\Models\Loadtemplate');
    }

    public function cycle()
    {
        return $this->belongsTo('App\Models\Cycle');
    }

    // public function team()
    // {
    //     return $this->belongsTo('App\Models\Team');
    // }

    public function loadable()
    {
        return $this->morphTo();
    }

    public function tplans()
    {
        return $this->hasMany('App\Models\Tplan');
    }

    public static $columns = [
            0 => 'id',
            1 => 'name',
        ];

    static public function getListForDatatable(Request $request)
    {
        $where = $request->cperiod_id ? [['cperiod_id', '=', $request->cperiod_id]] : [];
        if ($request->team_id) {
            $where[] = ['loadable_id' , '=', $request->team_id];
            $where[] = ['loadable_type' , '=', 'App\Models\Team'];
        } else {
            $where[] = ['loadable_id' , '=', auth()->user()->id];
            $where[] = ['loadable_type' , '=', 'App\User'];
        }

        $search = $request->input('search.value');
        $totalData = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->where($where)
            ->count();

        $totalFiltered = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->where($where)
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->where($where)
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => TloadResource::collection($models),
        ];

        return $json_data;
    }


    public function syncMeta($loads)
    {
        $meta = $this->meta;
        foreach ($loads as $key1 => $grade) {
            foreach ($grade as $key2 =>  $cbranch) {
                foreach ($cbranch as $key3 => $parameter) {
                    if (!isset($meta->loads)) $meta->loads = (object)[];
                    if (!isset($meta->loads->{$key1})) $meta->loads->{$key1} = (object)[];
                    if (!isset($meta->loads->{$key1}->{$key2})) $meta->loads->{$key1}->{$key2} = (object)[];
                    $meta->loads->{$key1}->{$key2}->{$key3} = $parameter;
                }
            }
        }
        $this->update(['meta' => $meta]);
        return;
    }


    # Проверка правильности заполнения нагрузки
    public function check()
    {
        $result = [];
        $loads = $this->meta->loads ?? [];
        $loadtemplate = $this->loadtemplate->meta->loads ?? [];
        foreach ($loads as $key1 => $grade) {
            foreach ($grade as $key2 =>  $cbranch) {
                $recommended = (float)($cbranch->value ?? 0);
                $minimum = (float)($loadtemplate->{$key1}->{$key2}->minimum ?? 0);
                $maximum = (float)($loadtemplate->{$key1}->{$key2}->maximum ?? 0);
                if ($minimum > $recommended || $maximum < $recommended)
                    $result[] = (object)[];
            }
        }
        return $result;
    }


    # Индикатор правильности заполнения нагрузки
    public function validate()
    {
        $errors = $this->check();
        return !$errors;
    }


    # Возвращает ссылку на родительскую страницу
    public function parentRoute()
    {
        if ($this->loadable_type == 'App\Models\Team') {
            // return route('planing.index', [$this->loadable->id, $this->cperiod_id]);
            return route('planing.index', [$this->loadable->id]);
        } else {
            // return route('uplaning.index', [$this->cperiod_id]);
            return route('uplaning.index');
        }
    }

}
