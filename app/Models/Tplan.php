<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\TplanResource;
use Illuminate\Http\Request;

class Tplan extends Model
{
    use SoftDeletes;
    // protected $connection = 'new';
    protected $guarded = [];

    protected $casts = [
        'meta' => 'object'
    ];

    public function tload()
    {
        return $this->belongsTo('App\Models\Tload');
    }

    public function yplans()
    {
        return $this->hasMany('App\Models\Yplan');
    }

    public static $columns = [
            0 => 'id',
            1 => 'name',
        ];

    static public function getListForDatatable(Request $request)
    {
        $cperiod_id = $request->cperiod_id ?? 0;
        $where = $request->cperiod_id ? [['cperiod_id', '=', $request->cperiod_id]] : [];
        if ($request->team_id) {
            $where[] = ['loadable_id' , '=', $request->team_id];
            $where[] = ['loadable_type' , '=', 'App\Models\Team'];
        } else {
            $where[] = ['loadable_id' , '=', auth()->user()->id];
            $where[] = ['loadable_type' , '=', 'App\User'];
        }

        $search = $request->input('search.value');
        $totalData = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( $cperiod_id, function($query) use ($where) {
                    return $query->whereHas('tload', function($q) use ($where){
                        $q->where($where);
                    });
                })
            ->count();

        $totalFiltered = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                        ]);
                })
            ->when( $cperiod_id, function($query) use ($where) {
                    return $query->whereHas('tload', function($q) use ($where){
                        $q->where($where);
                    });
                })
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                        ]);
                })
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( $cperiod_id, function($query) use ($where) {
                    return $query->whereHas('tload', function($q) use ($where){
                        $q->where($where);
                    });
                })
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => TplanResource::collection($models),
        ];

        return $json_data;
    }


    # получение списка компонентов в зависимости от типа, языка преподавания и цикла обучений
    public function getComponents($ctype_id = 1, $languagetype_id = 1, $cycle_id = 1, $cbranch_id = 0)
    {
        return Component::getComponents($ctype_id, $languagetype_id, $cycle_id, $cbranch_id);
    }


    # Проверка правильности заполнения нагрузки
    public function check()
    {
        $result = [];
        $object = (object)[];

        # считывание данных из нагрузки учебного заведения
        foreach ($this->tload->meta->loads as $key => $value) {
            foreach ($value as $key2 => $value2) {
               $object->{$key2} =  $object->{$key2} ?? (object)[];
               $object->{$key2}->{$key} = $object->{$key2}->{$key} ?? (object)[];
               $object->{$key2}->{$key}->value = (float)($value2->value ?? 0);
               $object->{$key2}->{$key}->sumDetails = 0;
            }
        }

        # сбор данных из учебного плана в разрезе галузей
        if (isset($this->meta->details) && is_object($this->meta->details)) {
            foreach ($this->meta->details as $key => $value) {
                $component = Component::where([['id','=',$key],['ctype_id','=',1]])->first();
                $cbranch_id = $component->cbranches[0]->id ?? 0;

                // проверка на индивидуальные ограничения компонента
                // info($key . ' - ' . json_encode($value));
                if (!$this->tload->loadtemplate->validateRestriction($key, $value)) {
                    $result['c' . $key] = 'error';
                }

                if( $component ){
                    foreach ($value as $key2 => $value2) {
                        if (isset($object->{$cbranch_id}->{$key2}->sumDetails)) $object->{$cbranch_id}->{$key2}->sumDetails += $value2;
                    }
                }
            }
        }

        # сравнение нагрузки и данных из учебного плана
        foreach ($object as $key => $value) {
            foreach ($value as $key2 => $value2) {
                if ($value2->value != $value2->sumDetails) {
                    $result[$key] = $result[$key] ?? [];
                    $result[$key][] = $key2;
                }
            }
        }

        return $result;
    }


    # Индикатор правильности заполнения нагрузки
    public function validate()
    {
        $errors = $this->check();
        return !$errors;
    }


    public function syncMeta($components = [], $details = [])
    {
        $meta = is_object($this->meta) ? $this->meta : (object)[];
        $meta->components = $components;
        $meta->details = $details;
        $this->update(['meta' => $meta]);
        return;
    }


    public function getCalculatedData($grade_id = 0)
    {
        $result = 0;

        if (isset($this->meta->details) && (is_array($this->meta->details) || is_object($this->meta->details))) {
            foreach ($this->meta->details as $key => $component) {
                $result += isset($component->{$grade_id}) ? $component->{$grade_id} : 0;
            }
        }

        return $result;
    }


    # сравнение индивидуальной нагрузки компонента (если предусмотрена в шаблоне)
    private function compareComponentLoad()
    {
        
    }
}
