<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\InstitutionResource;
use Illuminate\Http\Request;
use Hexa;

class Institution extends Model
{
    // protected $connection = 'new';
    protected $guarded = [];

    protected $casts = [
        'meta' => 'object',
    ];

    protected function asJson($value)
    {
        return json_encode($value, JSON_UNESCAPED_UNICODE);
    }

    public static $columns = [
            0 => 'id',
            1 => 'meta->name',
            2 => 'meta->region_name',
            3 => 'meta->koatuu_name',
            4 => 'meta->address',
        ];

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    static public function getListForDatatable(Request $request)
    {
        $search = $request->input('search.value');
        $totalData = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();

        $totalFiltered = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['meta->institution_name', 'like', "%{$search}%", 'or'],
                            ['meta->region_name', 'like', "%{$search}%", 'or'],
                            ['meta->koatuu_name', 'like', "%{$search}%", 'or'],
                            ['meta->address', 'like', "%{$search}%", 'or'],
                        ]);
                })
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['meta->institution_name', 'like', "%{$search}%", 'or'],
                            ['meta->region_name', 'like', "%{$search}%", 'or'],
                            ['meta->koatuu_name', 'like', "%{$search}%", 'or'],
                            ['meta->address', 'like', "%{$search}%", 'or'],
                        ]);
                })
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => InstitutionResource::collection($models),
        ];

        return $json_data;
    }


    static public function refreshData()
    {
        $loadingData = Hexa::getDataFromUrl([
                // 'url' => 'https://registry.edbo.gov.ua/api/schools/?exp=json',
                'url' => 'https://registry.edbo.gov.ua/api/institutions/?ut=3&exp=json',
            ]);
        $elements = json_decode( $loadingData->html_request );
        //$elements = [];
        foreach ($elements as $key => $row) {
                self::updateOrCreate(
                    [
                        'id' => $row->institution_id,
                    ],
                    [
                        'parent_id' => $row->parent_institution_id ? $row->parent_institution_id : 0,
                        'meta'      => $row,
                    ]
                );
        }

        return true;
    }


}
