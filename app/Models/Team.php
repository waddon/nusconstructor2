<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Resources\TeamResource;
use Illuminate\Http\Request;

use App\Models\Cperiod;

class Team extends Model
{
    // protected $connection = 'new';
    use SoftDeletes;

    protected $casts = [
        'meta' => 'object'
    ];

    protected $guarded = [];

    public static $columns = [
            0 => 'id',
            1 => 'name',
        ];

    public function ttype()
    {
        return $this->belongsTo('App\Models\Ttype');
    }

    public function users()
    {
        return $this->belongsToMany('App\User')->withPivot(['teamrole_id','meta'])->using('App\Models\CustomPivot')->orderBy('team_user.teamrole_id');
    }

    public function institutions()
    {
        return $this->belongsToMany('App\Models\Institution');
    }

    public function newprograms()
    {
        return $this->belongsToMany('App\Models\Newprogram');
    }

    public function approvedNewplans()
    {
        return $this->belongsToMany('App\Models\Newplan')->where('status','=','Approved');
    }

    public function approvedNewprograms()
    {
        return $this->belongsToMany('App\Models\Newprogram')->where('status','=','Approved');
    }

    public function approvedLayouts()
    {
        return $this->belongsToMany('App\Models\Layout')->where('status','=','Approved');
    }

    // public function tloads()
    // {
    //     return $this->hasMany('App\Models\Tload');
    // }
    public function tloads()
    {
        return $this->morphMany('App\Models\Tload', 'loadable');
    }

    public function tplans()
    {
        return $this->hasMany('App\Models\Tplan');
    }

    static public function getListForDatatable(Request $request)
    {
        $where = [];
        if ($request->ttype_id) {$where[] = ['ttype_id','=',$request->ttype_id];}
        $search = $request->input('search.value');
        $totalData = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();

        $totalFiltered = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->where($where)
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->where($where)
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => TeamResource::collection($models),
        ];

        return $json_data;
    }

    public function getLeader()
    {
        $leader = $this->users()->wherePivot('teamrole_id', '=', 1)->first();
        if (!$leader) {
            $leader = (object)[
                'id' => 0,
                'name' => __('MAIN.PLACEHOLDER.SELECT-USER'),
                'second_name' => '',
            ];
        }
        return $leader;
    }


    public function isLeader($id)
    {
        $leader = $this->users()->wherePivot('teamrole_id', '=', 1)->first();
        $leader_id = (isset($leader->id)) ? $leader->id : 0;
        return $leader_id == $id;
    }

    public function isMember($id)
    {
        $members = $this->users()->where('id', '=', $id)->get();
        return $members->count() > 0;
    }

    public function isActiveMember($id)
    {
        $members = $this->users()->where('id', '=', $id)->wherePivot('teamrole_id', '!=', 3)->get();
        return $members->count() > 0;
    }

    public function candidateCount()
    {
        return $this->users()->wherePivot('teamrole_id', '=', 3)->count();
    }


    public function getActivePlaningPeriods()
    {
        # позже добавить условие наличия шаблонов учебной нагрузки
        return Cperiod::has('loadtemplates')->get();
    }


    # Возвращает список валидных навантажень
    public function getValidTloads($cperiod_id)
    {
        $tloads = $this->tloads()->where('cperiod_id','=',$cperiod_id)->get();
        foreach ($tloads as $key => $tload) {
            if (!$tload->validate()) unset($tloads[$key]);
        }
        return $tloads;
    }

    public function getValidTplans($cperiod_id)
    {
        $tloads = $this->tloads()->where('cperiod_id','=',$cperiod_id)->get();
        foreach ($tloads as $key => $tload) {
            if (!$tload->validate()) unset($tloads[$key]);
        }
        $tplansIds = [];
        foreach ($tloads as $key => $tload) {
            $tplansIds[] = $tload->id;
        }
        $tplans = Tplan::whereHas('tload', function($query) use ($tplansIds){
            return $query->whereIn( 'id', $tplansIds );
        })->get();
        foreach ($tplans as $key => $tplan) {
            if (!$tplan->validate()) unset($tplans[$key]);
        }
        return $tplans;
    }

}
