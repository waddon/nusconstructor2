<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Resources\NewplanResource;
use App\Http\Resources\TeamNewplanResource;
use App\Http\Resources\TypicalNewplanResource;
use Illuminate\Http\Request;
use App\Helpers\KeyGenerator;
use App\Models\Branch;

class Newplan extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $casts = [
        'meta' => 'object',
        'content' => 'object',
    ];

    public static function boot()
    {
        parent::boot();
        self::created(function($model){
            $key = app(KeyGenerator::class)->generate();
            info($key);
            $model->key = $key;
            $model->save();
        });
    }

    public function teams()
    {
        return $this->belongsToMany('App\Models\Team');
    }

    public function cycle()
    {
        return $this->belongsTo('App\Models\Cycle');
    }

    public static $columns = [
            0 => 'id',
            1 => 'name',
        ];


    static public function getListForDatatable(Request $request, $onlymy = false)
    {
        $where = [];
        if ($request->cycle_id) {$where[] = ['cycle_id','=',$request->cycle_id];}
        if ($request->status) {$where[] = ['status','=',$request->status];} else {}
        $team_id = $request->team_id ? $request->team_id : null;
        $user_id = ($request->team_id || !$onlymy) ? null : auth()->user()->id;

        $branch_id = $request->branch_id ? $request->branch_id : 0;
        $search = $request->input('search.value');
        $totalData = self::when( $team_id,  function($query) use ($team_id) {
                    return $query->whereHas('teams', function($q) use ($team_id){
                        $q->where('id', '=', $team_id);
                    });
                })
            ->when( !$request->status, function($query){
                    $query->whereNotIn('status',['Approving','Approved','Public']);
                })
            ->when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();

        $totalFiltered = self::when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when( $team_id,  function($query) use ($team_id) {
                    return $query->whereHas('teams', function($q) use ($team_id){
                        $q->where('id', '=', $team_id);
                    });
                })
            ->when( !$request->status, function($query){
                    $query->whereNotIn('status',['Approving','Approved','Public']);
                })
            ->where($where)
            ->when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when( $team_id,  function($query) use ($team_id) {
                    return $query->whereHas('teams', function($q) use ($team_id){
                        $q->where('id', '=', $team_id);
                    });
                })
            ->when( !$request->status, function($query){
                    $query->whereNotIn('status',['Approving','Approved','Public']);
                })
            ->where($where)
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => (!$onlymy) ? NewplanResource::collection($models) : TeamNewplanResource::collection($models),
        ];

        return $json_data;
    }


    public function belongToMyTeams()
    {
        $result = $this->teams()->whereHas('users', function($q){
                $q->where('id', '=', auth()->user()->id);
            })->count();
        return $result;
    }


    public function canManage($team_id)
    {
        $team = $this->teams()->where('id','=',$team_id)->first();
        $result = $team ? $team->isLeader( auth()->user()->id ) : false;
        return $result;
    }


    static public function getTypicalForDatatable(Request $request)
    {
        $where = [['public','=',1]];
        if ($request->cycle_id) {$where[] = ['cycle_id','=',$request->cycle_id];}

        $search = $request->input('search.value');
        $totalData = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->where($where)
            ->count();

        $totalFiltered = self::when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->where($where)
            ->when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->where($where)
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => TypicalNewplanResource::collection($models),
        ];

        return $json_data;
    }


    /**
     * Генерация PDF файла
     * @param array $attr
     * 
     * @return string
     */
    public function getPDF(string $filename = 'document', $attr = [])
    {
        $mpdf = new \Mpdf\Mpdf([
            'tempDir'       => '/tmp',
            'margin_left'   => 10,
            'margin_right'  => 10,
            'margin_top'    => 15,
            'margin_bottom' => 20,
            'margin_header' => 8,
            'margin_footer' => 10,
        ]);
        $html = \View::make('pdf.plan')->with('title', $filename)->with('model', $this)->with('branches', Branch::get())->with('attr', $attr);
        $html = $html->render();
        $mpdf->SetHTMLHeader('<div style="text-align: right; font-size:10px;">'.$this->name.'</div>');
        $mpdf->SetHTMLFooter('
        <table width="100%" style="font-size:10px;">
            <tr>
                <td width="50%">{DATE j-m-Y}</td>
                <td width="50%" style="text-align: right;">{PAGENO}/{nbpg}</td>
            </tr>
        </table>');
        $mpdf->WriteHTML($html);
        $mpdf->Output($filename . '.pdf','I');
    }

    public function countSubjects()
    {
        $result = 0;
        if(isset($this->content) && $this->content){
            foreach ($this->content as $key => $subject) {
                $result++;
            }
        }
        return $result;
    }
}
