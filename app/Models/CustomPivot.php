<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class CustomPivot extends Pivot
{
    protected $casts = [
        'meta' => 'object'
    ];

    protected function asJson($value)
    {
        return json_encode($value, JSON_UNESCAPED_UNICODE);
    }

    public function teamrole()
    {
        return $this->belongsTo('App\Models\Teamrole');
    }

}