<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Resources\NewpackageResource;
use App\Http\Resources\TeamNewpackageResource;
use App\Http\Resources\TypicalNewpackageResource;
use Illuminate\Http\Request;
use App\Helpers\KeyGenerator;
use App\Models\Branch;

class Newpackage extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $casts = [
        'meta' => 'object',
    ];

    public static function boot()
    {
        parent::boot();
        self::created(function($model){
            $key = app(KeyGenerator::class)->generate();
            info($key);
            $model->key = $key;
            $model->save();
        });
    }

    public function teams()
    {
        return $this->belongsToMany('App\Models\Team');
    }

    public function newplan()
    {
        return $this->belongsTo('App\Models\Newplan');
    }

    public static $columns = [
            0 => 'id',
            1 => 'name',
        ];


    static public function getListForDatatable(Request $request, $onlymy = false)
    {
        $where = [];
        if ($request->cycle_id) {$where[] = ['cycle_id','=',$request->cycle_id];}
        if ($request->status) {$where[] = ['status','=',$request->status];} else {}
        $team_id = $request->team_id ? $request->team_id : null;
        $user_id = ($request->team_id || !$onlymy) ? null : auth()->user()->id;

        $branch_id = $request->branch_id ? $request->branch_id : 0;
        $search = $request->input('search.value');
        $totalData = self::when( $team_id,  function($query) use ($team_id) {
                    return $query->whereHas('teams', function($q) use ($team_id){
                        $q->where('id', '=', $team_id);
                    });
                })
            ->when( !$request->status, function($query){
                    $query->whereNotIn('status',['Approving','Approved','Public']);
                })
            ->when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();

        $totalFiltered = self::when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when( $team_id,  function($query) use ($team_id) {
                    return $query->whereHas('teams', function($q) use ($team_id){
                        $q->where('id', '=', $team_id);
                    });
                })
            ->when( !$request->status, function($query){
                    $query->whereNotIn('status',['Approving','Approved','Public']);
                })
            ->where($where)
            ->when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when( $team_id,  function($query) use ($team_id) {
                    return $query->whereHas('teams', function($q) use ($team_id){
                        $q->where('id', '=', $team_id);
                    });
                })
            ->when( !$request->status, function($query){
                    $query->whereNotIn('status',['Approving','Approved','Public']);
                })
            ->where($where)
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => (!$onlymy) ? NewpackageResource::collection($models) : TeamNewpackageResource::collection($models),
        ];

        return $json_data;
    }


    public function belongToMyTeams()
    {
        $result = $this->teams()->whereHas('users', function($q){
                $q->where('id', '=', auth()->user()->id);
            })->count();
        return $result;
    }


    public function canManage($team_id)
    {
        $team = $this->teams()->where('id','=',$team_id)->first();
        $result = $team ? $team->isLeader( auth()->user()->id ) : false;
        return $result;
    }


    static public function getTypicalForDatatable(Request $request)
    {
        $where = [['public','=',1]];
        if ($request->cycle_id) {$where[] = ['cycle_id','=',$request->cycle_id];}

        $search = $request->input('search.value');
        $totalData = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->where($where)
            ->count();

        $totalFiltered = self::when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->where($where)
            ->when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->where($where)
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => TypicalNewpackageResource::collection($models),
        ];

        return $json_data;
    }

    public function countPrograms()
    {
        $result = 0;
        if(isset($this->meta->programs) && $this->meta->programs){
            foreach ($this->meta->programs as $key => $program) {
                $result = $program ? $result + 1 : $result;
            }
        }
        return $result;
    }


    public function canSendToApprove()
    {
        return $this->newplan->countSubjects() == $this->countPrograms();
    }

    public function getProgramLink($newprogram_id = 0)
    {
        $newprogram = Newprogram::find($newprogram_id);
        return $newprogram ? '<a href="' . route('showprogram',$newprogram->key) . '" target="_blank">' . $newprogram->name . '</a>' : '';
    }


    public function getUsedSpecifics()
    {
        $result = (object)[];
        foreach (Branch::get() as $key => $branch) {
            $result->{$branch->id} = (object)[
                    'id' => $branch->id,
                    'name' => $branch->name,
                    'specifics' => (object)[],
                ];
            foreach ($branch->expects()->where('cycle_id','=',$this->newplan->cycle_id)->get() as $expect) {
                foreach($expect->specifics as $specific){
                    $result->{$branch->id}->specifics->{$specific->marking} = (object)[
                            'marking' => $specific->marking,
                            'name' => $specific->name,
                            'frequency' => 0,
                        ];
                }
            }
        }

        if(isset($this->meta->programs) && $this->meta->programs){
            foreach ($this->meta->programs as $key => $program) {
                $newprogram = Newprogram::find($program);
                if ($newprogram && is_object($newprogram->content)){
                    foreach ($newprogram->content as $line){
                        foreach ($line->problems as $problem){
                            foreach ($problem->opises as $opis){
                                foreach ($opis->kors as $kor){
                                    if (isset($result->{$kor->galuz}->specifics->{$kor->kod}->frequency)){
                                        $result->{$kor->galuz}->specifics->{$kor->kod}->frequency++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }
}
