<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\YplanResource;
use Illuminate\Http\Request;

class Yplan extends Model
{
    use SoftDeletes;
    // protected $connection = 'new';
    protected $guarded = [];

    protected $casts = [
        'meta' => 'object'
    ];

    protected function asJson($value)
    {
        return json_encode($value, JSON_UNESCAPED_UNICODE);
    }

    public function tplan()
    {
        return $this->belongsTo('App\Models\Tplan');
    }

    public function grade()
    {
        return $this->belongsTo('App\Models\Grade');
    }

    public static $columns = [
            0 => 'id',
            1 => 'name',
        ];

    static public function getListForDatatable(Request $request)
    {
        $cperiod_id = $request->cperiod_id ?? 0;
        $where = $request->cperiod_id ? [['cperiod_id', '=', $request->cperiod_id]] : [];
        if ($request->team_id) {
            $where[] = ['loadable_id' , '=', $request->team_id];
            $where[] = ['loadable_type' , '=', 'App\Models\Team'];
        } else {
            $where[] = ['loadable_id' , '=', auth()->user()->id];
            $where[] = ['loadable_type' , '=', 'App\User'];
        }

        $search = $request->input('search.value');
        $totalData = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( $cperiod_id, function($query) use ($where) {
                    return $query->whereHas('tplan', function($q) use ($where){
                        return $q->whereHas('tload', function($q2) use ($where){
                            $q2->where($where);
                        });
                    });
                })
            ->count();

        $totalFiltered = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                        ]);
                })
            ->when( $cperiod_id, function($query) use ($where) {
                    return $query->whereHas('tplan', function($q) use ($where){
                        return $q->whereHas('tload', function($q2) use ($where){
                            $q2->where($where);
                        });
                    });
                })
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                        ]);
                })
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( $cperiod_id, function($query) use ($where) {
                    return $query->whereHas('tplan', function($q) use ($where){
                        return $q->whereHas('tload', function($q2) use ($where){
                            $q2->where($where);
                        });
                    });
                })
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => YplanResource::collection($models),
        ];

        return $json_data;
    }


    # получение списка компонентов в зависимости от типа, языка преподавания и цикла обучений
    // public function getComponents($ctype_id = 1, $languagetype_id = 1, $cycle_id = 1, $cbranch_id = 0)
    // {
    //     return Component::getComponents($ctype_id, $languagetype_id, $cycle_id, $cbranch_id);
    // }


    # Проверка правильности заполнения нагрузки
    public function check()
    {
        $result = [];

        $loadtemplate = $this->tplan->tload->loadtemplate->meta->loads ?? [];
        $loads = $loadtemplate->{$this->grade_id} ?? [];

        # суммируем нагрузку по галузям
        if(isset($this->meta->usedComponents) && is_object($this->meta->usedComponents)) {
            foreach ($this->meta->usedComponents as $key => $usedComponent) {
                $component = Component::find($usedComponent->id);
                // info($component->ctype_id);
                // info($component->cbranches[0]->id);
                if ( in_array($component->ctype_id,[1,2]) ) {
                    $loads->{$component->cbranches[0]->id}->value = isset($loads->{$component->cbranches[0]->id}->value)
                        ? $loads->{$component->cbranches[0]->id}->value
                        : 0;
                    $loads->{$component->cbranches[0]->id}->value += (float)$usedComponent->value;
                }
            }
        }

        foreach ($loads as $key => $load) {
            $value   = $load->value ?? 0;
            $minimum = $load->minimum ?? 0;
            $maximum = $load->maximum ?? 0;
            if ($minimum > $value || $maximum < $value)
                $result[] = (object)[];
        }

        return $result;
    }


    # Индикатор правильности заполнения нагрузки
    public function validate()
    {
        $errors = $this->check();
        return !$errors;
    }


    public function syncMeta($componentList = '{}', $additionalComponentList = '{}')
    {
        $componentList = json_decode($componentList);
        $usedComponents = (object)[];
        foreach ($componentList as $key => $cbranch) {
            foreach ($cbranch as $key2 => $component) {
                if(isset($component->checked) && $component->checked) {
                    $usedComponents->{$key2} = $component;
                }
            }
        }

        $meta = is_object($this->meta) ? $this->meta : (object)[];
        $meta->usedComponents = $usedComponents;
        $meta->additionalComponentList = $additionalComponentList;
        $this->update(['meta' => $meta]);

        return;
    }


    public function getForShow()
    {
        $result = (object)[
            'sectoral' => [],
            'intersectoral' => [],
            'sumSectoral' => 0,
            'sumIntersectoral' => 0,            
        ];
        foreach(Cbranch::get() as $cbranch) {
            $result->sectoral[$cbranch->id] = [
                'name' => $cbranch->name,
                'components' => [],
            ];
        }
        if(isset($this->meta->usedComponents) && (is_array($this->meta->usedComponents) || is_object($this->meta->usedComponents))) {
            foreach ($this->meta->usedComponents as $key => $usedComponent) {
                $component = Component::find($usedComponent->id);
                if ($component && in_array($component->ctype_id, [1,2]) ){
                    $result->sectoral[$component->cbranches[0]->id]['components'][] = $usedComponent;
                    $result->sumSectoral += $usedComponent->value;
                } else {
                    $result->intersectoral[] = $usedComponent;
                    $result->sumIntersectoral += $usedComponent->value;
                }
            }
        }
        if(isset($this->meta->additionalComponentList) && (is_array($this->meta->additionalComponentList) || is_object($this->meta->additionalComponentList))) {
            foreach ($this->meta->additionalComponentList as $key => $usedComponent) {
                $result->intersectoral[] = $usedComponent;
                $result->sumIntersectoral += $usedComponent->value;
            }
        }
        return $result;
    }

}
