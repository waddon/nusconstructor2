<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\LoadtemplateResource;
use Illuminate\Http\Request;

class Loadtemplate extends Model
{
    use SoftDeletes;
    // protected $connection = 'planing';
    protected $guarded = [];

    protected $casts = [
        'meta' => 'object'
    ];

    public function languagetype()
    {
        return $this->belongsTo('App\Models\Languagetype');
    }

    public function cperiods()
    {
        return $this->belongsToMany('App\Models\Cperiod');
    }

    public function cycles()
    {
        return $this->belongsToMany('App\Models\Cycle');
    }


    public static $columns = [
            0 => 'id',
            1 => 'name',
        ];

    static public function getListForDatatable(Request $request)
    {
        $search = $request->input('search.value');
        $totalData = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();

        $totalFiltered = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => LoadtemplateResource::collection($models),
        ];

        return $json_data;
    }


    # Рассчет каких-либо данных из меты
    # $grade - код класса
    # $type - loads, additional, financed, admissible
    # $scale - тип, может быть одним из 2-х: weekly, annual
    # $option - параметр: recommended, minimum, maximum
    public function getCalculatedData($grade, $type = 'loads', $scale = 'weekly', $option = 'recommended')
    {
        $result = 0;
        $factor = $scale == 'weekly' ? 1 : 35;

        if( in_array($type,['additional', 'financed', 'admissible']) ) {
            $result = ($this->meta->{$type}->{$grade} ?? 0) * $factor;
        } else {
            foreach ($this->meta->{$type}->{$grade} as $key => $cbnanch) {
                $result += $cbnanch->{$option} ?? 0;
            }
            $result = $result * $factor;
        }

        return $result;
    }


    public function getMetaByCycle($cycle_id)
    {
        $cycle = Cycle::find($cycle_id);
        $cbranches = Cbranch::all();
        $result = (object)['loads'=>(object)[]];
        foreach ($cycle->grades as $key1 => $grade) {
            $result->loads->{$grade->id} = (object)[];
            foreach ($cbranches as $key2 => $cbranch) {
                $result->loads->{$grade->id}->{$cbranch->id} = ['value' => $this->meta->loads->{$grade->id}->{$cbranch->id}->recommended];
            }
            // $result->loads->{$grade->id} = $this->meta->loads->{$grade->id} ?? (object)[];
        }
        return $result;
    }


    # возвращает значение индивидуального ограничения для предмета
    public function getRestrictionValue($component_id, $grade_id, $name_parameter)
    {
        return $this->meta->restrictions->{$component_id}->{$grade_id}->{$name_parameter} ?? 0;
    }


    # проверка на соответствие индивидуальным ограничениям компонента
    public function validateRestriction($component_id, $values = [])
    {
        $result = true;
        if (is_array($values) || is_object($values)) {
            foreach ($values as $key => $value) {
                $min = $this->getRestrictionValue($component_id, $key, 'minimum');
                $max = $this->getRestrictionValue($component_id, $key, 'maximum');
                // info('id: ' . $this->id . ', component_id: ' . $component_id . ', grade: ' . $key . ', min: ' . $min . ', max: ' . $max . ', value: ' . $value);
                if (($min > 0 && $min > $value) || ($max > 0 && $max < $value)) $result = false;
            }
        }
        return $result;
    }

}
