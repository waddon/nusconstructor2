<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class MetaPivot extends Pivot
{

    protected $casts = [
        'meta' => 'object'
    ];

    protected function asJson($value)
    {
        return json_encode($value, JSON_UNESCAPED_UNICODE);
    }

}