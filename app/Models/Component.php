<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\ComponentResource;
use Illuminate\Http\Request;

class Component extends Model
{
    use SoftDeletes;
    // protected $connection = 'planing';
    protected $guarded = [];

    public function ctype()
    {
        return $this->belongsTo('App\Models\Ctype');
    }

    public function cbranches()
    {
        return $this->belongsToMany('App\Models\Cbranch');
    }

    public function grades()
    {
        return $this->belongsToMany('App\Models\Grade');
    }

    public function languagetypes()
    {
        return $this->belongsToMany('App\Models\Languagetype');
    }

    public function restricted()
    {
        return $this->belongsToMany('App\Models\Component', 'restricted_component', 'component_id', 'restricted_id');
    }

    public static $columns = [
            0 => 'id',
            1 => 'name',
        ];

    static public function getListForDatatable(Request $request)
    {
        $languagetype_id = $request->languagetype_id ? $request->languagetype_id : 0;
        $where = $request->ctype_id ? [['ctype_id', '=', $request->ctype_id]] : [];
        $cbranch_id = $request->cbranch_id ? $request->cbranch_id : 0;
        $search = $request->input('search.value');
        $totalData = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();

        $totalFiltered = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when( $languagetype_id,  function($query) use ($languagetype_id) {
                    return $query->whereHas('languagetypes', function($q) use ($languagetype_id){
                        $q->where('id', '=', $languagetype_id);
                    });
                })
            ->when( $cbranch_id,  function($query) use ($cbranch_id) {
                    return $query->whereHas('cbranches', function($q) use ($cbranch_id){
                        $q->where('id', '=', $cbranch_id);
                    });
                })
            ->where($where)
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( $languagetype_id,  function($query) use ($languagetype_id) {
                    return $query->whereHas('languagetypes', function($q) use ($languagetype_id){
                        $q->where('id', '=', $languagetype_id);
                    });
                })
            ->when( $cbranch_id,  function($query) use ($cbranch_id) {
                    return $query->whereHas('cbranches', function($q) use ($cbranch_id){
                        $q->where('id', '=', $cbranch_id);
                    });
                })
            ->where($where)
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => ComponentResource::collection($models),
        ];

        return $json_data;
    }

    # получение списка компонентов в зависимости от типа, языка преподавания и цикла обучений
    static public function getComponents($ctype_id = 1, $languagetype_id = 1, $cycle_id = 1, $cbranch_id = 0)
    {
        $cycle = Cycle::find($cycle_id);
        $grades = $cycle->grades->pluck('id', 'id');
        $result = Component::where([['ctype_id', '=', $ctype_id]])
            ->whereHas('languagetypes', function($q) use ($languagetype_id) {
                return $q->whereIn('id', [$languagetype_id]);
            })
            ->whereHas('grades', function($q) use ($grades) {
                return $q->whereIn('id', $grades);
            })
            ->when($cbranch_id, function($q) use ($cbranch_id) {
                return $q->whereHas('cbranches', function($query) use ($cbranch_id) {
                    return $query->where('id', '=', $cbranch_id);
                });
            })
            ->get();

        return $result;
    }


    # получает индекс галузи, которая почему-то может не соответстовать галузи. Бред!
    public function getMarking()
    {
        $result = '-';
        if ($this->cbranches->count() > 0) $result = $this->cbranches[0]->marking;
        if ($this->marking) $result = $this->marking;
        return $result;
    }


    # проверяется наличие связи между компонентом и классом
    public function canEdit($grade_id)
    {
        return $this->grades()->where('id','=', $grade_id)->exists();
    }
}
