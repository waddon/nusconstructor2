<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Resources\NewprogramResource;
use App\Http\Resources\UserNewprogramResource;
use App\Http\Resources\TeamNewprogramResource;
use App\Http\Resources\TypicalNewprogramResource;
use Illuminate\Http\Request;
use App\Helpers\KeyGenerator;

use Storage;

use App\Classes\Socket\Pusher;
use App\Tool;
use App\Models\Option;
use App\User;
use DB;

class Newprogram extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $casts = [
        'meta' => 'object',
        'content' => 'object',
    ];

    public $head = [
        'model' => [
            'Очікувані результати навчання',
            'Пропонований зміст навчального предмета / інтегрованого курсу',
            'Види навчальної діяльності',
        ],
        'educational' => [
            'Кількість годин',
            'Очікувані результати навчання',
            'Пропонований зміст навчального предмета / інтегрованого курсу',
            'Види навчальної діяльності',
        ],
        'standart' => [
            'Кількість годин',
            'Очікувані результати навчання',
            'Пропонований зміст навчального предмета / інтегрованого курсу',
            'Види навчальної діяльності',
        ],
        'curriculum' => [
            'Дата',
            'Групи результатів',
            'Очікувані результати за навчальною програмою',
            'Очікувані результати за змістом навчання',
            'Тема уроку / Пропонований зміст',
            'Види навчальної діяльності / Навчальні ресурси',
        ],        
    ];

    public static $columns = [
            0 => 'id',
            1 => 'name',
        ];

    public static $publicColumns = [
            0 => 'name',
        ];


    protected function asJson($value)
    {
        return json_encode($value, JSON_UNESCAPED_UNICODE);
    }

    public static function boot()
    {
        parent::boot();
        self::created(function($model){
            $key = app(KeyGenerator::class)->generate();
            info($key);
            $model->key = $key;
            $model->save();
        });
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function teams()
    {
        return $this->belongsToMany('App\Models\Team')->withPivot('meta')->using('App\Models\MetaPivot');
    }

    public function branches()
    {
        return $this->belongsToMany('App\Models\Branch');
    }

    public function cycle()
    {
        return $this->belongsTo('App\Models\Cycle');
    }

    // public function period()
    // {
    //     return $this->belongsTo('App\Models\Period');
    // }

    public function scopeType($query, $type)
    {
        return $query->where('type', '=', $type);
    }

    /**
     * Определение принадлежности пользователю, команде или широкому кругу
     * 
     * @param  string  $owner
     * @param  string  $id
     */
    public function scopeOwner($query, $owner, $id = 0)
    {
        switch ($owner) {
            case 'user':
                return $query->whereHas('users', function($q) use ($id){
                    $q->where('id', '=', $id);
                });
            case 'team':
                return $query->whereHas('teams', function($q) use ($id){
                    $q->where('id', '=', $id);
                });
                break;
            default:
                return $query->where('public', '=', true);
                break;
        }
    }


    public function scopeTab($query, $tab)
    {
        switch ($tab) {
            case 'active':
                return $query->where('status', '=', 'Editing');
                break;
            case 'approving':
                return $query->where('status', '=', 'Approving');
                break;
            case 'published':
                return $query->where('public', '=', true);
                break;
            case 'deleted':
                return $query->onlyTrashed();
                break;
            default:
                return $query;
                break;
        }
    }

    /*
     * Close all connections
     **/
    public function closeAllConnections($attr)
    {

        $connectionString = (env("FORCE_HTTPS",false) ? "wss" : "ws") 
            . '://' . Option::getOption("socketLink","localhost") 
            . ':' . (env("FORCE_HTTPS",false) ? "443" : "8080");
        $message = [
            'commandType' => 'connect',
            'user' => '0',
            'channel' => 'program-' . $this->id,
        ];

        info($connectionString);
        $client = new \WebSocket\Client($connectionString);
        $client->text(json_encode($message));
        $client->close();

        return true;
    }


    static public function getListForDatatable(Request $request, $onlymy = false)
    {
        $where = [];
        if ($request->cycle_id) {$where[] = ['cycle_id','=',$request->cycle_id];}
        if ($request->status) {$where[] = ['status','=',$request->status];} else {}
        $team_id = $request->team_id ? $request->team_id : null;
        $user_id = ($request->team_id || !$onlymy) ? null : auth()->user()->id;
        $tab = $request->tab ?? 'active';
        $type = $request->type ?? 'model';

        $branch_id = $request->branch_id ? $request->branch_id : 0;
        $search = $request->input('search.value');
        $totalData = self::when( $team_id,  function($query) use ($team_id) {
                    return $query->whereHas('teams', function($q) use ($team_id){
                        $q->where('id', '=', $team_id);
                    });
                })
            ->when( $user_id,  function($query) use ($user_id) {
                    return $query->whereHas('users', function($q) use ($user_id){
                        $q->where('id', '=', $user_id);
                    });
                })
            // ->when( !$request->status, function($query){
            //         $query->whereNotIn('status',['Approving','Approved','Public']);
            //     })
            ->when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( $tab, function($query) use ($tab) {
                $query->tab($tab);
            })
            ->when( $type, function($query) use ($type) {
                $query->type($type);
            })
            ->count();

        $totalFiltered = self::when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when( $branch_id,  function($query) use ($branch_id) {
                    return $query->whereHas('branches', function($q) use ($branch_id){
                        $q->where('id', '=', $branch_id);
                    });
                })
            ->when( $team_id,  function($query) use ($team_id) {
                    return $query->whereHas('teams', function($q) use ($team_id){
                        $q->where('id', '=', $team_id);
                    });
                })
            ->when( $user_id,  function($query) use ($user_id) {
                    return $query->whereHas('users', function($q) use ($user_id){
                        $q->where('id', '=', $user_id);
                    });
                })
            // ->when( !$request->status, function($query){
            //         $query->whereNotIn('status',['Approving','Approved','Public']);
            //     })
            ->where($where)
            ->when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( $tab, function($query) use ($tab) {
                $query->tab($tab);
            })
            ->when( $type, function($query) use ($type) {
                $query->type($type);
            })
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when( $branch_id,  function($query) use ($branch_id) {
                    return $query->whereHas('branches', function($q) use ($branch_id){
                        $q->where('id', '=', $branch_id);
                    });
                })
            ->when( $team_id,  function($query) use ($team_id) {
                    return $query->whereHas('teams', function($q) use ($team_id){
                        $q->where('id', '=', $team_id);
                    });
                })
            ->when( $user_id,  function($query) use ($user_id) {
                    return $query->whereHas('users', function($q) use ($user_id){
                        $q->where('id', '=', $user_id);
                    });
                })
            // ->when( !$request->status, function($query){
            //         $query->whereNotIn('status',['Approving','Approved','Public']);
            //     })
            ->where($where)
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( $tab, function($query) use ($tab) {
                $query->tab($tab);
            })
            ->when( $type, function($query) use ($type) {
                $query->type($type);
            })
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => (!$onlymy) ? NewprogramResource::collection($models) : ( $team_id ? TeamNewprogramResource::collection($models) : UserNewprogramResource::collection($models) ),
        ];

        return $json_data;
    }


    public function userList()
    {
        $users = [];
        foreach ($this->users as $key => $user) {
            $users[] = $user->combineName();
        }
        return implode(', ', $users);
    }


    public function belongToMe()
    {
        $result = $this->users()->where('id', '=', auth()->user()->id)->count();
        // info( '(сам) ID пользователя - ' . auth()->user()->id );
        // info( '(сам) ID шаблона - ' . $this->id );
        // info( '(сам) Количество - ' . $result );
        return $result;
    }


    public function belongToMyTeams()
    {
        $result = $this->teams()->whereHas('users', function($q){
                $q->where('id', '=', auth()->user()->id);
            })->count();
        // info( '(команда) ID пользователя - ' . auth()->user()->id );
        // info( '(команда) ID шаблона - ' . $this->id );
        // info( '(команда) Количество - ' . $result );
        return $result;
    }


    public function canEdit($team_id)
    {
        $result = false;
        $team = $this->teams()->where('id','=',$team_id)->first();
        if ($team){
            $result = $team->isLeader( auth()->user()->id );
            if ( isset($this->teams()->where('id','=',$team_id)->first()->pivot->meta->editors) && !$result){
                $result = in_array(auth()->user()->id, $this->teams()->where('id','=',$team_id)->first()->pivot->meta->editors);
            }
        }
        return $result;
    }


    public function canManage($team_id)
    {
        $team = $this->teams()->where('id','=',$team_id)->first();
        $result = $team ? $team->isLeader( auth()->user()->id ) : false;
        return $result;
    }

    public function getTree()
    {
        $result = '';
        if (is_object($this->content)){
            foreach ($this->content as $line){
                $result .= '<div class="element pb-1 line" data-children="problem" ' . ((isset($this->meta->layout) && $this->meta->layout) ? 'data-grade_id="' . $line->grade_id . '" data-period_element_id="' . $line->period_element_id . '"' : '') . '>';
                $result .= '<div class="card-header header-elements-inline bg-primary" ' . ((isset($this->meta->layout) && $this->meta->layout) ? 'style="cursor:auto;"' : '') . '>';
                $result .= '<div class="card-title flex-grow-1"><span class="element-name">' . $line->name_line . '</span><br><span class="element-info">' . $line->info_line . '</span></div>';
                $result .= '<div class="header-elements"><div class="list-icons">';
                $result .= '<a class="list-icons-item ml-2 element-toggle rotate-180" data-action="toggle" title="'.__('MAIN.HINT.TOGGLE').'"><i class="icon-arrow-down5"></i></a>';
                $result .= '<a class="list-icons-item ml-2 btn-edit-element" data-action="" title="'.__('MAIN.HINT.EDIT').'"><i class="icon-pencil7"></i></a>';
                if (in_array($this->type,['model', 'standart']) || !($condition = \App\Models\Option::getOption('programTypeRestrictions'))) {
                    $result .= '<a class="list-icons-item ml-2 btn-add-children" data-action="" title="'.__('MAIN.HINT.ADD').'"><i class="icon-googleplus5"></i></a>';
                }
                if ((!isset($this->meta->layout) || $this->meta->layout==0) && (in_array($this->type,['model', 'standart']) || !($condition = \App\Models\Option::getOption('programTypeRestrictions')))){
                    $result .= '<a class="list-icons-item ml-2 btn-copy-element" data-action="" title="'.__('MAIN.HINT.COPY').'"><i class="icon-gallery"></i></a>';
                    $result .= '<a class="list-icons-item ml-2 element-remove" data-action="" title="'.__('MAIN.HINT.DELETE').'"><i class="icon-cross3"></i></a>';
                }
                if (!in_array($this->type,['model', 'standart'])) {
                    $result .= '<a class="list-icons-item ml-2 element-undo" data-action="" title="'.__('MAIN.HINT.UNDO').'"><i class="icon-undo"></i></a>';
                }

                $result .= '</div></div>';
                $result .= '</div>';
                $result .= '<div class="element-container pt-2 pr-0 pb-1 pl-5 problems" style="display: none;">';
                    foreach ($line->problems as $problem){
                        $result .= '<div class="element pb-1 problem" data-children="opis">';
                        $result .= '<div class="card-header header-elements-inline bg-teal">';
                        $result .= '<div class="card-title flex-grow-1"><span class="element-name">' . $problem->name_problem . '</span></div>';
                        $result .= '<div class="header-elements"><div class="list-icons">';
                        $result .= '<a class="list-icons-item ml-2 element-toggle rotate-180" data-action="toggle"><i class="icon-arrow-down5"></i></a>';
                        $result .= '<a class="list-icons-item ml-2 btn-edit-green" data-action="delete"><i class="icon-pencil7"></i></a>';
                        if (in_array($this->type,['model', 'standart']) || !($condition = \App\Models\Option::getOption('programTypeRestrictions'))) {
                            $result .= '<a class="list-icons-item ml-2 ml-2 btn-add-children" data-action="" title="'.__('MAIN.HINT.ADD').'"><i class="icon-googleplus5"></i></a>';
                        }
                        $result .= '<a class="list-icons-item ml-2 btn-copy-element" data-action="" title="'.__('MAIN.HINT.COPY').'"><i class="icon-gallery"></i></a>';
                        $result .= '<a class="list-icons-item ml-2 element-remove" data-action="delete"><i class="icon-cross3"></i></a>';
                        $result .= '</div></div>';
                        $result .= '</div>';
                        $result .= '<div class="element-container pt-2 pr-0 pb-1 pl-5 opises" style="display: none;">';
                            foreach ($problem->opises as $opis){
                                $result .= '<div class="element pb-1 opis">';
                                $result .= '<div class="card-header header-elements-inline bg-warning">';
                                $result .= '<div class="card-title flex-grow-1"><span class="element-name">' . $opis->name_opis . '</span></div>';
                                $result .= '<div class="header-elements"><div class="list-icons">';
                                $result .= '<a class="list-icons-item ml-2 element-toggle rotate-180" data-action="toggle"><i class="icon-arrow-down5"></i></a>';
                                $result .= '<a class="list-icons-item ml-2 btn-edit-element" data-action="delete"><i class="icon-pencil7"></i></a>';
                                if (in_array($this->type,['model', 'standart']) || !($condition = \App\Models\Option::getOption('programTypeRestrictions'))) {
                                    $result .= '<a class="list-icons-item ml-2 btn-add-kors" data-action="" title="'.__('MAIN.HINT.ADD-KORS').'"><i class="icon-googleplus5"></i></a>';
                                }
                                $result .= '<a class="list-icons-item ml-2 btn-copy-element" data-action="" title="'.__('MAIN.HINT.COPY').'"><i class="icon-gallery"></i></a>';
                                $result .= '<a class="list-icons-item ml-2 element-remove" data-action="delete"><i class="icon-cross3"></i></a>';
                                $result .= '</div></div>';
                                $result .= '</div>';
                                $result .= '<div class="element-container p-0 kors" style="display: none;">';
                                        //$result .= '<div class="element pb-1 kors">';
                                        //$result .= '<div class="card-header header-elements-inline bg-gray">';
                                        $result .= '<table class="table table-striped text-dark w-100">';
                                        $result .= '<thead class="bg-dark">';
                                        $result .= '<tr>';
                                        $result .= '<th class="text-center pl-5 pr-5">Код</th>';
                                        $result .= '<th class="text-center">Очікуваний результат</th>';
                                        $result .= '<th class="text-center pl-5 pr-5">Інструмент</th>';
                                        $result .= '<th class="text-center"></th>';
                                        $result .= '</tr>';
                                        $result .= '</thead>';
                                        $result .= '<tbody>';
                                        foreach ($opis->kors as $kor){
                                            $result .= '<tr class="kor">';
                                            $result .= '<td class="kor-kod" data-galuz="'.$kor->galuz.'" data-owner="'.$kor->owner.'">'.$kor->kod.'</td>';
                                            $result .= '<td class="kor-name">'.$kor->name.'</td>';
                                            $result .= '<td class="kor-tool"><select class="form-control">';
                                            foreach (Tool::get() as $tool){
                                                $result .= '<option ' . ($tool->name_tool == $kor->tool ? 'selected' : '') . '>' . $tool->name_tool . '</option>';
                                            }
                                            $result .= '</select></td>';
                                            $result .= '<td>';
                                            $result .= '<div class="btn-group">';
                                            $result .= '    <button type="button" class="btn btn-sm btn-icon btn-light kor-edit"><i class="icon-pencil7"></i></button>';
                                            $result .= '    <button type="button" class="btn btn-sm btn-icon btn-danger kor-remove"><i class="icon-cross3"></i></button>';
                                            $result .= '</div>';
                                            $result .= '</td>';
                                            $result .= '</tr>';
                                        }
                                        $result .= '</tbody>';
                                        $result .= '</table>';
                                        //$result .= '</div>';
                                        //$result .= '</div>';
                                        //$result .= '<div class="empty-container"></div>';
                                $result .= '</div>';
                                $result .= '</div>';
                            }
                        $result .= '</div>';
                        $result .= '</div>';
                    }
                $result .=  '</div>';
                $result .= '</div>';
            }
        }
        return $result;
    }


    public function elementCount()
    {
        $result = 0;
        if (is_object($this->content)){
            foreach ($this->content as $line){
                $result++;
                foreach ($line->problems as $problem){
                    $result++;
                    foreach ($problem->opises as $opis){
                        $result++;
                        foreach ($opis->kors as $kor){
                            $result++;
                        }
                    }
                }
            }
        }
        return $result;
    }


    static public function getTypicalForDatatable(Request $request)
    {
        $tab = $request->tab ?? 'published';
        $type = $request->type ?? 'model';
        $where = [];
        if ($request->cycle_id) {$where[] = ['cycle_id','=',$request->cycle_id];}

        $branch_id = $request->branch_id ? $request->branch_id : 0;
        $search = $request->input('search.value');
        $totalData = self::where($where)
            ->when( $tab, function($query) use ($tab) {
                $query->tab($tab);
            })
            ->when( $type, function($query) use ($type) {
                $query->type($type);
            })
            ->count();

        $totalFiltered = self::when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when( $branch_id,  function($query) use ($branch_id) {
                    return $query->whereHas('branches', function($q) use ($branch_id){
                        $q->where('id', '=', $branch_id);
                    });
                })
            ->where($where)
            ->when( $tab, function($query) use ($tab) {
                $query->tab($tab);
            })
            ->when( $type, function($query) use ($type) {
                $query->type($type);
            })
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when( $branch_id,  function($query) use ($branch_id) {
                    return $query->whereHas('branches', function($q) use ($branch_id){
                        $q->where('id', '=', $branch_id);
                    });
                })
            ->where($where)
            ->when( $tab, function($query) use ($tab) {
                $query->tab($tab);
            })
            ->when( $type, function($query) use ($type) {
                $query->type($type);
            })
            ->offset($request->input('start'))
            ->orderby(self::$publicColumns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => TypicalNewprogramResource::collection($models),
        ];

        return $json_data;
    }

    public function getViewTree()
    {
        $result = '';
        if (is_object($this->content)){
            foreach ($this->content as $line){
                $result .= '<div class="element pb-1 line" data-children="problem">';
                $result .= '<div class="card-header header-elements-inline bg-primary">';
                $result .= '<div class="card-title flex-grow-1"><span class="element-name">' . $line->name_line . '</span><br><span class="element-info">' . $line->info_line . '</span></div>';
                $result .= '<div class="header-elements"><div class="list-icons">';
                $result .= '<a class="list-icons-item ml-2 element-toggle rotate-180" data-action="toggle" title="'.__('MAIN.HINT.TOGGLE').'"><i class="icon-arrow-down5"></i></a>';
                $result .= '</div></div>';
                $result .= '</div>';
                $result .= '<div class="element-container pt-2 pr-0 pb-1 pl-5 problems" style="display: none;">';
                    foreach ($line->problems as $problem){
                        $result .= '<div class="element pb-1 problem" data-children="opis">';
                        $result .= '<div class="card-header header-elements-inline bg-teal">';
                        $result .= '<div class="card-title flex-grow-1"><span class="element-name">' . $problem->name_problem . '</span></div>';
                        $result .= '<div class="header-elements"><div class="list-icons">';
                        $result .= '<a class="list-icons-item element-toggle rotate-180" data-action="toggle"><i class="icon-arrow-down5"></i></a>';
                        $result .= '</div></div>';
                        $result .= '</div>';
                        $result .= '<div class="element-container pt-2 pr-0 pb-1 pl-5 opises" style="display: none;">';
                            foreach ($problem->opises as $opis){
                                $result .= '<div class="element pb-1 opis">';
                                $result .= '<div class="card-header header-elements-inline bg-warning">';
                                $result .= '<div class="card-title flex-grow-1"><span class="element-name">' . $opis->name_opis . '</span></div>';
                                $result .= '<div class="header-elements"><div class="list-icons">';
                                $result .= '<a class="list-icons-item element-toggle rotate-180" data-action="toggle"><i class="icon-arrow-down5"></i></a>';
                                $result .= '</div></div>';
                                $result .= '</div>';
                                $result .= '<div class="element-container p-0 kors" style="display: none;">';
                                        $result .= '<table class="table table-striped text-dark w-100">';
                                        $result .= '<thead class="bg-dark">';
                                        $result .= '<tr>';
                                        $result .= '<th class="text-center pl-5 pr-5">Код</th>';
                                        $result .= '<th class="text-center">Очікуваний результат</th>';
                                        $result .= '<th class="text-center pl-5 pr-5">Інструмент</th>';
                                        $result .= '<th class="text-center">Матеріали</th>';
                                        $result .= '</tr>';
                                        $result .= '</thead>';
                                        $result .= '<tbody>';
                                        foreach ($opis->kors as $kor){
                                            $result .= '<tr class="kor">';
                                            $result .= '<td class="kor-kod" data-galuz="'.$kor->galuz.'" data-owner="'.$kor->owner.'">'.$kor->kod.'</td>';
                                            $result .= '<td class="kor-name">'.$kor->name.'</td>';
                                            $result .= '<td class="text-center kor-tool">'.$kor->tool.'</td>';
                                            $result .= '<td class="text-center"><a href="#" class="btn-show-materials" data-toggle="modal" data-target="#modal-show-materials" data-kod="' . $kor->kod . '"><i class="icon-images2 icon-2x"></i></a></td>';
                                            $result .= '</tr>';
                                        }
                                        $result .= '</tbody>';
                                        $result .= '</table>';
                                $result .= '</div>';
                                $result .= '</div>';
                            }
                        $result .= '</div>';
                        $result .= '</div>';
                    }
                $result .=  '</div>';
                $result .= '</div>';
            }
        }
        return $result;
    }


    /**
     * Генерация PDF файла
     * @param array $attr
     * 
     * @return string
     */
    public function getPDF(string $filename = 'document', $attr = [])
    {
        $mpdf = new \Mpdf\Mpdf([
            'tempDir'       => '/tmp',
            'margin_left'   => 10,
            'margin_right'  => 10,
            'margin_top'    => 15,
            'margin_bottom' => 20,
            'margin_header' => 8,
            'margin_footer' => 10,
        ]);

        $header = '<div style="text-align: right; font-size:10px; font-family: arial;">'.$this->name.'</div>';
        $footer = '
                <table width="100%" style="font-size:10px; font-family: arial;">
                    <tr>
                        <td width="50%">{DATE j-m-Y}</td>
                        <td width="50%" style="text-align: right;">{PAGENO}/{nbpg}</td>
                    </tr>
                </table>';

        # Титульный лист
            if(isset($this->meta->isTitle) && $this->meta->isTitle && isset($this->meta->title)) {
                $content = \View::make('pdf.content')->with('content', $this->meta->title);
                $content = $content->render();
                $mpdf->WriteHTML($content);
                $mpdf->SetHTMLHeader($header);
                $mpdf->AddPage();
                $mpdf->SetHTMLFooter($footer);
            } else {
                $mpdf->SetHTMLHeader($header);
                $mpdf->SetHTMLFooter($footer);            
            }

        # Описательная часть
            if(isset($this->meta->isDescription) && $this->meta->isDescription && isset($this->meta->description)) {
                $content = \View::make('pdf.content')->with('content', $this->meta->description);
                $content = $content->render();
                $mpdf->WriteHTML($content);
                $mpdf->AddPage();
            }

        # Программа
            $html = \View::make('pdf.content-program-table')->with('model', $this);
            $html = $html->render();
            $mpdf->WriteHTML($html);

        # Заключительная часть
            if(isset($this->meta->isProvisions) && $this->meta->isProvisions && isset($this->meta->provisions)) {
                $mpdf->AddPage();
                $content = \View::make('pdf.content')->with('content', $this->meta->provisions);
                $content = $content->render();
                $mpdf->WriteHTML($content);
            }

        $mpdf->Output($filename . '.pdf','I');

    }


    /**
     * проверка выбора всех КОРов в разрезе змиcтовых линий,
     * если програма создана на шаблоне
     * @param void
     * 
     * @return bool
     */
    public function canSendToApprove() : bool
    {
        $wrong = 0;
        // if (isset($this->meta->layout) && $this->meta->layout){
        //     $object = $this->selectedSpecific();
        //     foreach ($object as $key => $grade) {
        //         foreach ($grade as $key2 => $period) {
        //             $wrong += count($period->absent);
        //         }
        //     }
        // }

        return ($wrong==0);
    }


    /**
     * формирование объекта для проверки выбора всех КОРов
     * в разрезе змитовых линий, если програма создана на шаблоне
     * @param void
     * 
     * @return object
     */
    public function selectedSpecific() : object
    {
        $result = (object)[];
        if (isset($this->meta->layout) && $this->meta->layout){
            # находим шаблон
                $layout = Layout::findOrFail($this->meta->layout);
                $specificIds = [];

            # формируем объект
                foreach ($layout->cycle->grades as $key => $grade) {
                    $result->{$grade->id} = (object)[];
                    foreach ($layout->period->meta as $key2 => $value) {
                        $result->{$grade->id}->$key2 = (object)[
                            'name'    => 'Name of line',
                            'grade'   => $grade->name,
                            'period'  => $value,
                            'ids'     => [],
                            'layout'  => [],
                            'program' => [],
                            'absent'  => [],
                        ];
                    }
                }

            # вытаскиваем Id-шники КОРов и распихиваем их
                foreach ($layout->content as $key => $grade) {
                    foreach ($grade as $key2 => $period) {
                        foreach ($period as $key3 => $specific) {
                            if ($specific){
                                $result->{$key}->{$key2}->ids[] = $key3;
                                $specificIds[] = $key3;
                            }
                        }
                    }
                }

            # вытаскиваем КОРы
                $temp = Specific::whereIn('id', $specificIds)->get()->toArray();
                foreach ($temp as $key => $specific) {
                    $specifics[$specific['id']] = $specific;
                }

            # заполняем кодами коров из шаблона
                foreach ($result as $key => $grade) {
                    foreach ($grade as $key2 => $period) {
                        foreach ($period->ids as $key3 => $specific) {
                            $value = isset($specifics[$specific]['marking']) ? $specifics[$specific]['marking'] : 'undefined';
                            $result->{$key}->{$key2}->layout[] = $value;
                            $result->{$key}->{$key2}->absent[] = $value;
                        }
                    }
                }

            # заполняем кодами коров из программы
                foreach ($this->content as $key => $line) {
                    $result->{$line->grade_id}->{$line->period_element_id}->name = isset($line->name_line) ? $line->name_line : 'undefined';
                    foreach ($line->problems as $key2 => $problem) {
                        foreach ($problem->opises as $key3 => $opis) {
                            foreach ($opis->kors as $key4 => $kor) {
                                $value = isset($kor->kod) ? $kor->kod : 'undefined';
                                $result->{$line->grade_id}->{$line->period_element_id}->program[] = $value;
                                $result->{$line->grade_id}->{$line->period_element_id}->absent = array_diff($result->{$line->grade_id}->{$line->period_element_id}->absent, [$value]);
                            }
                        }
                    }
                }

        }
        return $result;
    }


    /**
     * формирование HTML из объекта для проверки выбора всех КОРов
     * в разрезе змитовых линий, если програма создана на шаблоне
     * @param void
     * 
     * @return string
     */
    public function selectedSpecificHTML() : string
    {
        $result = '';
        if (isset($this->meta->layout) && $this->meta->layout){
            $object = $this->selectedSpecific();
            foreach ($object as $key => $grade) {
                foreach ($grade as $key2 => $period) {
                    $result .= '<tr>';
                    $result .= '<td>' . $period->name . '</td>';
                    $result .= '<td class="text-center">' . count($period->layout) . '</td>';
                    $result .= '<td class="text-center">' . count($period->program) . '</td>';
                    $result .= '<td class="text-center">' . (count($period->absent)>0 ? '<span class="badge badge-primary">' . implode('</span> <span class="badge badge-primary">', $period->absent) . '</span>' : '')  . '</td>';
                    $result .= '<td class="text-center">' . (count($period->absent)>0 ? '<i class="text-warning icon-warning22 icon-2x"></i>' : '<i class="text-success icon-checkmark icon-2x"></i>')  . '</td>';
                    $result .= '</tr>';
                }
            }
        }
        return $result;
    }

    public function getExplanatoryPath()
    {
        $result = '';

        if ( isset($this->meta->explanatory->path) && Storage::disk('public')->exists( $this->meta->explanatory->path ) ) {
            if (isset($this->meta->explanatory->mimetype) && in_array($this->meta->explanatory->mimetype, 
                    [
                        'application/msword',
                        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                        'application/vnd.ms-excel',
                        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                        'application/vnd.ms-powerpoint',
                        'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                    ]
                )) {
                $result = 'https://view.officeapps.live.com/op/embed.aspx?src=' . request()->getHost() . Storage::url($this->meta->explanatory->path);
            } else {
                $result = Storage::url($this->meta->explanatory->path);
            }
        }

        return $result;
    }


    /**
     * Создание копии программы
     * Доступы копируются в зависимости от параметра owner.
     * 
     * @param  string  $owner
     */
    public function clone()
    {
        $model = $this;

        $new_name = mb_substr( '[' . __('MAIN.LABEL.COPY') . '] '  . $model->name, 0, 191);
        $new_model = $model->replicate();
        $new_model->name = $new_name;
        $new_model->status = 'Editing';
        $new_model->public = 0;
        $new_model->save();
        $branches = $model->branches->pluck('id','id');
        $new_model->branches()->sync( $branches );
        $users = [];
        foreach ($model->users as $key => $user) {
            $users[] = $user->id;
        }
        $new_model->users()->sync($users);
        $teams = [];
        foreach ($model->teams as $key => $team) {
            $teams[$team->id] = ['meta' => $team->pivot->meta];
        }
        $new_model->teams()->sync( $teams );
        // $pivot = $new_model->teams()->find($team->id)->pivot;
        // $pivot->save();

        return $new_model;
    }


    /**
     * Возвращение списка редакторов
     * 
     * @param  int  $team_id
     */
    public function teamEditorList($team_id = 0)
    {
        $team = $this->teams()->where('id', '=', $team_id)->first();
        if (!$team) return '';

        $meta = $team->pivot->meta;
        $editors = isset($meta->editors) ? $meta->editors : [];

        $result = User::query()
            ->select( DB::raw("CONCAT(name, ' ',second_name) AS display_name") )
            ->whereIn('id', $editors)
            ->pluck('display_name')
            ->implode(', ');
        return $result;
    }


    /**
     * Отправка на публикацию
     */
    public function sendToApprove()
    {
        $result = false;
        if ($this->canSendToApprove()) {
            switch ($this->type) {
                case 'model':
                case 'standart':
                    $this->update(['status' => 'Approving']);
                    break;
                case 'educational':
                case 'curriculum':
                    $this->update(['status' => 'Public', 'public' => true]);
                    break;
            }
            $result = true;
        }
        return $result;
    }


    /**
     * Изъятие из публикацию
     */
    public function withdrawFromPublic()
    {
        $this->update(['status' => 'Editing', 'public' => false]);
        return true;
    }

    /**
     * Публикация
     */
    public function doPublic()
    {
        $this->update(['status' => 'Public', 'public' => true]);
        return true;
    }


    /**
     * Формирование программы для выгрузки в формате WORD
     */
    public function getDOCContent()
    {
        return view('doc.program-table' ,[
            'model' => $this,
        ])->render();
    }


    /**
     * Заголовок таблицы для последующего отображения при экспорте в PDF или WORD
     */
    public function getOutputTableHead()
    {
        return $this->head[$this->type] ?? [];
    }

    /**
     * Сборка таблицы для последующего отображения при экспорте в PDF или WORD
     */
    public function getOutputTableBody()
    {
        $result = [];

        $colspan = count($this->getOutputTableHead());

        if(!is_object($this->content)) return $result;

        foreach($this->content as $line) {
            $result[] = [ ($line->name_line ?? '') . '<br>' . ($line->info_line ?? '') ];
            foreach($line->problems as $problem) {
                $green = $problem->name_problem ?? '';
                $tempArray = [];
                $orangeArray = [];
                $grayArray = [];
                $grayMarkings = [];
                $resultGroups = [];
                foreach($problem->opises as $opis) {
                    $orangeArray[] = $opis->name_opis ?? '';
                    foreach($opis->kors as $key => $kor) {
                        $grayArray[$kor->kod] = '- ' . ($kor->name ?? '');
                        $grayMarkings[$kor->kod] = $kor->kod ?? 'undefined';
                    }
                }

                # выбираем группы результатов
                    // info($grayMarkings);
                    $temp = Line::select('name')
                        ->distinct()
                        ->whereHas('specifics', function($q) use ($grayMarkings) {
                            return $q->whereIn('marking', $grayMarkings);
                        })
                        ->get()
                        ->toArray();
                    foreach ($temp as $key => $element) {
                        $resultGroups[] = $element['name'];
                    }
                #

                if(in_array($this->type, ['educational', 'standart'])) {
                    $tempArray[] = $problem->hours_problem ?? '';
                }
                if(in_array($this->type, ['curriculum'])) {
                    $tempArray[] = $problem->date_problem ?? '';
                    $tempArray[] = implode('<br>', $resultGroups);
                }
                $tempArray[] = implode('<br>', $grayArray);
                if(in_array($this->type, ['curriculum'])) {
                    $tempArray[] = $problem->custom_expected_problem ?? '';
                }
                $tempArray[] = $green;
                $tempArray[] = implode('<br>', $orangeArray) . (in_array($this->type, ['curriculum']) ? ('<br>' . ($problem->materials_problem ?? '')) : '');
                $result[] = $tempArray;
            }
        }

        return $result;
    }
}
