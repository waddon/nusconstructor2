<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\ExpectResource;
use Illuminate\Http\Request;

class Expect extends Model
{
    // protected $connection = 'new';
    protected $guarded = [];

    public function branch()
    {
        return $this->belongsTo('App\Models\Branch');
    }

    public function cycle()
    {
        return $this->belongsTo('App\Models\Cycle');
    }

    public function specifics()
    {
        return $this->hasMany('App\Models\Specific');
    }

    public static $columns = [
            0 => 'id',
            1 => 'name',
            2 => 'marking',
        ];

    static public function getListForDatatable(Request $request)
    {
        $where = [];
        if ($request->branch_id) {$where[] = ['branch_id','=',$request->branch_id];}
        if ($request->cycle_id) {$where[] = ['cycle_id','=',$request->cycle_id];}
        $search = $request->input('search.value');
        $totalData = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();

        $totalFiltered = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->where($where)
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->where($where)
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => ExpectResource::collection($models),
        ];

        return $json_data;
    }

    static public function listWithBranch()
    {
        $result = [ 0 => __('MAIN.PLACEHOLDER.ALL-EXPECTS')];
        foreach (self::get() as $key => $expect) {
            $result[$expect->id] = '(' . $expect->branch->marking . ') ' . $expect->name;
        }
        return $result;
    }

    public function canDelete()
    {
        return ($this->specifics->count() == 0);
    }

}
