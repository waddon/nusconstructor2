<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Resources\LayoutResource;
use App\Http\Resources\UserLayoutResource;
use App\Http\Resources\TeamLayoutResource;
use Illuminate\Http\Request;
use App\Helpers\KeyGenerator;

use App\Classes\Socket\Pusher;
use App\Models\Specific;
use App\Models\Line;
use App\Models\Grade;

class Layout extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $casts = [
        'meta' => 'object',
        'content' => 'object',
    ];

    protected function asJson($value)
    {
        return json_encode($value, JSON_UNESCAPED_UNICODE);
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function teams()
    {
        return $this->belongsToMany('App\Models\Team')->withPivot('meta')->using('App\Models\MetaPivot');
    }

    public function branches()
    {
        return $this->belongsToMany('App\Models\Branch');
    }

    public function cycle()
    {
        return $this->belongsTo('App\Models\Cycle');
    }

    public function period()
    {
        return $this->belongsTo('App\Models\Period');
    }

    public static function boot()
    {
        parent::boot();
        self::created(function($model){
            $key = app(KeyGenerator::class)->generate();
            info($key);
            $model->key = $key;
            $model->save();
        });
    }

    public static $columns = [
            0 => 'id',
            1 => 'name',
        ];


    /*
     * Close all connections
     **/
    public function closeAllConnections($attr)
    {
        $data = [
            'topic_id' => 'layout-' . $this->id,
            // 'data' => 'someData: ' . rand(1,100),
            'command' => ( isset($attr['command']) && $attr['command'] ) ? $attr['command'] : '---',
            'owner' => ( isset($attr['owner']) && $attr['owner'] ) ? $attr['owner'] : '---',
        ];
        Pusher::sentDataToServer($data);
        return true;
    }


    static public function getListForDatatable(Request $request, $onlymy = false)
    {
        $where = [];
        if ($request->cycle_id) {$where[] = ['cycle_id','=',$request->cycle_id];}
        if ($request->period_id) {$where[] = ['period_id','=',$request->period_id];}
        if ($request->status) {$where[] = ['status','=',$request->status];} else {}
        $team_id = $request->team_id ? $request->team_id : null;
        $user_id = ($request->team_id || !$onlymy) ? null : auth()->user()->id;

        $branch_id = $request->branch_id ? $request->branch_id : 0;
        $search = $request->input('search.value');
        $totalData = self::when( $team_id,  function($query) use ($team_id) {
                    return $query->whereHas('teams', function($q) use ($team_id){
                        $q->where('id', '=', $team_id);
                    });
                })
            ->when( $user_id,  function($query) use ($user_id) {
                    return $query->whereHas('users', function($q) use ($user_id){
                        $q->where('id', '=', $user_id);
                    });
                })
            ->when( !$request->status, function($query){
                    $query->whereNotIn('status',['Approving','Approved','Public']);
                })
            ->when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();

        $totalFiltered = self::when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when( $branch_id,  function($query) use ($branch_id) {
                    return $query->whereHas('branches', function($q) use ($branch_id){
                        $q->where('id', '=', $branch_id);
                    });
                })
            ->when( $team_id,  function($query) use ($team_id) {
                    return $query->whereHas('teams', function($q) use ($team_id){
                        $q->where('id', '=', $team_id);
                    });
                })
            ->when( $user_id,  function($query) use ($user_id) {
                    return $query->whereHas('users', function($q) use ($user_id){
                        $q->where('id', '=', $user_id);
                    });
                })
            ->when( !$request->status, function($query){
                    $query->whereNotIn('status',['Approving','Approved','Public']);
                })
            ->where($where)
            ->when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();

        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when( $branch_id,  function($query) use ($branch_id) {
                    return $query->whereHas('branches', function($q) use ($branch_id){
                        $q->where('id', '=', $branch_id);
                    });
                })
            ->when( $team_id,  function($query) use ($team_id) {
                    return $query->whereHas('teams', function($q) use ($team_id){
                        $q->where('id', '=', $team_id);
                    });
                })
            ->when( $user_id,  function($query) use ($user_id) {
                    return $query->whereHas('users', function($q) use ($user_id){
                        $q->where('id', '=', $user_id);
                    });
                })
            ->when( !$request->status, function($query){
                    $query->whereNotIn('status',['Approving','Approved','Public']);
                })
            ->where($where)
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => (!$onlymy) ? LayoutResource::collection($models) : ( $team_id ? TeamLayoutResource::collection($models) : UserLayoutResource::collection($models) ),
        ];

        return $json_data;
    }


    public function belongToMe()
    {
        $result = $this->users()->where('id', '=', auth()->user()->id)->count();
        // info( '(сам) ID пользователя - ' . auth()->user()->id );
        // info( '(сам) ID шаблона - ' . $this->id );
        // info( '(сам) Количество - ' . $result );
        return $result;
    }


    public function belongToMyTeams()
    {
        $result = $this->teams()->whereHas('users', function($q){
                $q->where('id', '=', auth()->user()->id);
            })->count();
        // info( '(команда) ID пользователя - ' . auth()->user()->id );
        // info( '(команда) ID шаблона - ' . $this->id );
        // info( '(команда) Количество - ' . $result );
        return $result;
    }


    public function canEdit($team_id)
    {
        $result = false;
        $team = $this->teams()->where('id','=',$team_id)->first();
        if ($team){
            $result = $team->isLeader( auth()->user()->id );
            if ( isset($this->teams()->where('id','=',$team_id)->first()->pivot->meta->editors) && !$result){
                $result = in_array(auth()->user()->id, $this->teams()->where('id','=',$team_id)->first()->pivot->meta->editors);
            }
        }
        return $result;
    }


    public function canManage($team_id)
    {
        $team = $this->teams()->where('id','=',$team_id)->first();
        $result = $team ? $team->isLeader( auth()->user()->id ) : false;
        return $result;
    }


    public function elementCount()
    {
        $result = 0;
        foreach ($this->content as $key => $grade) {
            foreach ($grade as $key2 => $period) {
                $result = 0;
                foreach ($period as $key3 => $specific) {
                    $result++;
                }
            }
        }
        return $result;
    }


    public function elementUsed()
    {
        $result = 0;
        $array = [];
        foreach ($this->content as $key => $grade) {
            foreach ($grade as $key2 => $period) {
                foreach ($period as $key3 => $specific) {
                    if (!isset($array[$key3])) {$array[$key3] = 0;}
                    $array[$key3] = $specific ? $array[$key3]+1 : $array[$key3];
                }
            }
        }
        foreach ($array as $key => $row) {
            if ($row > 0) {$result++;}
        }
        return $result;
    }


    public function canSendToApprove()
    {
        return $this->elementCount() == $this->elementUsed();
    }


    # Отображение шаблона в виде кластеров
    public function showClusters( $small = false )
    {
        $result = [];
        $specificIds = [];
        $s_start  = $small ? '<small>' : '';
        $s_finish = $small ? '</small>' : '';
        # вытаскиваем id-шники КОРов
            foreach ($this->content as $key => $grade) {
                foreach ($grade as $key2 => $period) {
                    foreach ($period as $key3 => $specific) {
                        $specificIds[] = $key3;
                    }
                }
            }

        # вытаскиваем змістові лінії, которіе используются
            $lines = Line::when( $specificIds,  function($query) use ($specificIds) {
                            return $query->whereHas('specifics', function($q) use ($specificIds){
                                $q->whereIn('id', $specificIds);
                            });
                        })->pluck('name','id')->toArray();
            foreach ($lines as $key => $line) {
                $lines_container[$key] = [];
            }
        # вытаскиваем КОРы
            $temp = Specific::whereIn('id', $specificIds)->get()->toArray();
            foreach ($temp as $key => $specific) {
                $specifics[$specific['id']] = $specific;
            }

        # формируем готовый объект
            foreach ($this->content as $key => $grade) {
                //$grade = Grade::find($key);
                $result[$key] = [];
                foreach ($grade as $key2 => $period) {
                    $result[$key][$key2] = $lines_container;
                    foreach ($period as $key3 => $value) {
                        if ($value){
                            $specific = $specifics[$key3];
                            $result[$key][$key2][ $specific['line_id'] ][] = $specific['name'] . ' <span class="badge badge-primary">' . $specific['marking'] . '</span>';
                        }
                    }
                }
            }

        # формируем html-контент
        $html = '';
        $html .= '<table class="table table-striped table-bordered">';
        foreach ($result as $key => $row) {
            $grade = Grade::find($key);
            $html .= '<tr><td class="bg-white text-center" colspan="'.count($lines).'"><h3 class="text-center">' . $grade->name . '</h3></td></tr>';
            foreach ($row as $key2 => $row2) {
                $html .= '<tr><td class="bg-white text-center" colspan="'.count($lines).'"><h4 class="text-center">' . $this->period->meta[$key2] . '</h4></td></tr>';
                $html .= '    <tr class="bg-primary">';
                foreach ($lines as $key3 => $line) {
                    $html .= '<th class="text-center">' . $line . '</th>';
                }
                $html .= '    </tr>';
                $max = 0;
                foreach ($row2 as $key3 => $row3) {
                    $max = count($row3) > $max ? count($row3) : $max;
                }
                for ($i=0; $i < $max; $i++) { 
                    $html .= '  <tr>';
                        foreach ($lines as $key3 => $line) {
                            $html .= '<td class="text-center" style="vertical-align: top;">' . $s_start . (isset( $result[$key][$key2][$key3][$i] ) ? $result[$key][$key2][$key3][$i] : '') . $s_finish . '</td>';
                        }
                    $html .= '  </tr>';
                }
            }
        }
        $html .= '</table>';
        //dump($result);
        return $html;
    }

    /**
     * Генерация PDF файла
     * @param array $attr
     * 
     * @return string
     */
    public function getPDF(string $filename = 'document', $attr = [])
    {
        $mpdf = new \Mpdf\Mpdf([
            'tempDir'       => '/tmp',
            'margin_left'   => 10,
            'margin_right'  => 10,
            'margin_top'    => 15,
            'margin_bottom' => 20,
            'margin_header' => 8,
            'margin_footer' => 10,
        ]);
        $html = \View::make('pdf.layout')->with('title', $filename)->with('model', $this)->with('attr', $attr);
        $html = $html->render();
        $mpdf->SetHTMLHeader('<div style="text-align: right; font-size:10px;">'.$this->name.'</div>');
        $mpdf->SetHTMLFooter('
        <table width="100%" style="font-size:10px;">
            <tr>
                <td width="50%">{DATE j-m-Y}</td>
                <td width="50%" style="text-align: right;">{PAGENO}/{nbpg}</td>
            </tr>
        </table>');
        $mpdf->WriteHTML($html);
        $mpdf->Output($filename . '.pdf','I');
    }

    public function showUsed()
    {
        $count = 0;
        foreach ($this->period->meta as $key => $value) {
            $count++;
        }
        $result = '<h3 class="text-center">'.__('MAIN.TITLE.SPECIFIC-USED-COUNT').'</h3>';
        $result .= '<table class="table w-100">';
            $result .= '<tr></tr>';
        foreach ($this->content as $key => $row) {
            $grade = Grade::findOrFail($key);
            $result .= '<tr>';
            $result .= '<th class="text-center">' . $grade->name . '</th>';
            $result .= '<th><table class="table w-100">';
                foreach ($this->period->meta as $key2 => $value) {
                    $result .= '<tr><th>' . $value . '</th><th class="text-right">' . $this->positivePropertyCount($this->content->{$key}->{$key2}) . '</th></tr>';
                }
            $result .= '</table></th>';
            $result .= '</tr><tr><th colspan="2"></th></tr>';
        }
        $result .= '</table>';

        return $result;
    }

    public function positivePropertyCount($array = [])
    {
        $result = 0;
        foreach ($array as $key => $value) {
            $result = $value ? $result+1 : $result;
        }
        return $result;
    }    
}
