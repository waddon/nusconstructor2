<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\TspecificResource;
use Illuminate\Http\Request;

class Tspecific extends Model
{
    protected $guarded = [];

    public function branch()
    {
        return $this->belongsTo('App\Models\Branch');
    }

    public function team()
    {
        return $this->belongsTo('App\Models\Team');
    }

    public static $columns = [
            0 => 'id',
            1 => 'name',
            2 => 'marking',
            3 => 'branch_id',
        ];

    static public function getListForDatatable(Request $request)
    {
        $whereTeam = [];
        if ($request->team_id) {$whereTeam[] = ['team_id','=',$request->team_id];}
        $where = [];
        if ($request->branch_id) {$where[] = ['branch_id','=',$request->branch_id];}
        $search = $request->input('search.value');
        $totalData = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->where($whereTeam)
            ->count();

        $totalFiltered = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->where($where)
            ->where($whereTeam)
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->where($where)
            ->where($whereTeam)
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => TspecificResource::collection($models),
        ];

        return $json_data;
    }

}
