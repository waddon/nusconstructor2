<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\SpecificResource;
use Illuminate\Http\Request;

class Specific extends Model
{
    // protected $connection = 'new';
    protected $guarded = [];

    public function line()
    {
        return $this->belongsTo('App\Models\Line');
    }

    public function expect()
    {
        return $this->belongsTo('App\Models\Expect');
    }

    public static $columns = [
            0 => 'id',
            1 => 'name',
            2 => 'marking',
        ];

    static public function getListForDatatable(Request $request)
    {
        $where = [];
        if ($request->line_id) {$where[] = ['line_id','=',$request->line_id];}
        if ($request->expect_id) {$where[] = ['expect_id','=',$request->expect_id];}
        $search = $request->input('search.value');
        $totalData = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();

        $totalFiltered = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or'],
                            ['marking', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->where($where)
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or'],
                            ['marking', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->where($where)
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => SpecificResource::collection($models),
        ];

        return $json_data;
    }

}
