<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\SubjectResource;
use Illuminate\Http\Request;

class Subject extends Model
{
    protected $table = 'subjects';
    protected $guarded = [];
    public $primaryKey  = 'id_subject';    
    public $timestamps = FALSE;

    public function galuz()
    {
        return $this->belongsTo('App\Galuz','id_galuz','id_galuz');
    }

    public static $columns = [
            0 => 'id_subject',
            1 => 'name_subject',
        ];

    static public function getListForDatatable(Request $request)
    {
        $where = [];
        if ($request->id_galuz) {$where[] = ['id_galuz','=',$request->id_galuz];}
        $search = $request->input('search.value');
        $totalData = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();

        $totalFiltered = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id_subject', '=', (int)$search, 'or'],
                            ['name_subject', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->where($where)
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id_subject', '=', (int)$search, 'or'],
                            ['name_subject', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->where($where)
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => SubjectResource::collection($models),
        ];

        return $json_data;
    }


}
