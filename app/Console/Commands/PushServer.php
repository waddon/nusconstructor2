<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;

use App\Classes\Socket\Pusher;
use React\EventLoop\Factory as ReactLoop;
use React\ZMQ\Context as ReactContext;
use React\Socket\Server as ReactServer;

use Ratchet\Wamp\WampServer;

use App\Models\Option;

class PushServer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'socketpush:serve';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $previousPid = Option::getOption($this->signature);
        if ( $previousPid && function_exists ('posix_getpgid') && posix_getpgid($previousPid) ) {

            if (Option::getOption('socketProcessBusy')) { info( __('MAIN.MESSAGE.BUSY') . ' - ' . $this->signature ); }

        } else {

            info( __('MAIN.MESSAGE.RUN') . ' - ' . $this->signature );
            Option::setOption( $this->signature, getmypid() );
            $loop = ReactLoop::create();

            $pusher = new Pusher;

            $context = new ReactContext($loop);

            $pull = $context->getSocket(\ZMQ::SOCKET_PULL);
            $pull->bind('tcp://127.0.0.1:5555');
            $pull->on('message', [$pusher, 'broadcast']);

            $webSock = new ReactServer('tcp://0.0.0.0:8080', $loop);
            //$webSock->listen(8080, '0.0.0.0');
            $webServer = new IoServer(
                new HttpServer(
                    new WsServer(
                        new WampServer(
                            $pusher
                        )
                    )
                ),
                $webSock
            );

            $this->info('Run handle');

            $loop->run();

        }


    }
}
