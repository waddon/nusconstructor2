<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use App\Models\Option;

class Websocket extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'websocket:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Запуск сервера вебсокетов';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $previousPid = Option::getOption($this->signature);
        if ( $previousPid && function_exists ('posix_getpgid') && posix_getpgid($previousPid) ) {
            if (Option::getOption('socketProcessBusy')) { info( __('MAIN.MESSAGE.BUSY') . ' - ' . $this->signature); }
        } else {
            info( __('MAIN.MESSAGE.RUN') . ' - ' . $this->signature . ' - PID: ' . getmypid());
            Option::setOption( $this->signature, getmypid() );

            $port = env('FORCE_HTTPS',false) ? 443 : 8080;
            $server = IoServer::factory(
                new HttpServer(
                    new WsServer(
                        new \App\Helpers\Websocket()
                    )
                ),
                $port
            );

            $this->info('Run handle');
            $server->run();
        }        
    }
}
