<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $table = 'schools';
    public $primaryKey  = 'id_school';    
    public $timestamps = FALSE;
}
