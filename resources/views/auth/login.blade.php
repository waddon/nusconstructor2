@extends('layouts.guest')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <h3 class="text-center">@lang('MAIN.TITLE.LOGIN')</h3>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                                <input id="email" type="email" class="form-control mb-3 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="@lang('MAIN.PLACEHOLDER.EMAIL')">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                                <input id="password" type="password" class="form-control mb-3 @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="@lang('MAIN.PLACEHOLDER.PASSWORD')">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="custom-control-label book-value" for="remember">@lang('MAIN.BUTTON.REMEMBER-ME')</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-lg-8 offset-lg-2">
                                <div class="btn-group d-flex">
                                    <button type="submit" class="btn bg-teal-400 mr-1 ml-1" style="flex: 1 1 1px;">@lang('MAIN.BUTTON.LOGIN')</button>
                                    <a href="{{route('register')}}" class="btn btn-light mr-1 ml-1" style="flex: 1 1 1px;">@lang('MAIN.BUTTON.REGISTER')</a>
                                    @if (Route::has('password.request'))
                                        <a class="btn btn-light mr-1 ml-1" href="{{ route('password.request') }}"  style="flex: 1 1 1px;">
                                            @lang('MAIN.BUTTON.FORGOT-PASSWORD')
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
