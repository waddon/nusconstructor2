@extends('layouts.guest')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                <h3 class="text-center">@lang('MAIN.TITLE.REGISTER')</h3>
                    <form method="POST" action="{{ route('register') }}" id="register-form">
                        @csrf

                        <div class="form-group">
                            <input id="name" type="text" class="form-control" name="name" placeholder="@lang('MAIN.PLACEHOLDER.NAME') *">
                            <label class="message validation-invalid-label"></label>
                        </div>

                        <div class="form-group">
                            <input id="email" type="email" class="form-control" name="email" placeholder="@lang('MAIN.PLACEHOLDER.EMAIL') *">
                            <label class="message validation-invalid-label"></label>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" placeholder="@lang('MAIN.PLACEHOLDER.PASSWORD') *">
                                <label class="message validation-invalid-label"></label>
                            </div>
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="@lang('MAIN.PLACEHOLDER.PASSWORD-CONFIRM') *">
                                <label class="message validation-invalid-label"></label>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-lg-6 offset-lg-3">
                                <div class="btn-group d-flex">
                                    <button type="submit" class="btn bg-teal-400 mr-1 ml-1" style="flex: 1 1 1px;">@lang('MAIN.BUTTON.REGISTER')</button>
                                    <a class="btn btn-light mr-1 ml-1" href="{{route('home')}}"  style="flex: 1 1 1px;">@lang('MAIN.BUTTON.HOME')</a>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
$(document).ready(function(){

    $('#register-form').submit(function(e){
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url: '/register',
            data: $(this).serialize(),
        }).done((data, status, xhr) => {
            window.location.href = '/';
        }).fail((xhr) => {
            let data = xhr.responseJSON;
            swal({
                title: data.message,
                type: 'error',
                confirmButtonClass: 'btn btn-primary',
            })
        }).always((xhr, type, status) => {
            let data = xhr.responseJSON || status.responseJSON,
            errors = data.errors || [];
            $( 'input' ).each(function( index ) {
                let fieldname = $( this ).attr('name');
                $(this).siblings('.message').first().text('');
                if (errors[fieldname]){
                    $(this).siblings('.message').first().text( errors[fieldname].toString() );
                    $(this).parent().siblings('.message').first().text( errors[fieldname].toString() );
                }
            });
        });
    });
});
</script>
@endsection

