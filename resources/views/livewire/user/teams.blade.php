<div class="livewire-container">
    <!-- Search panel -->
        <div class="search-panel p-0 mb-3">
            <div class="text-right">
                <div class="btn-group">
                    <button
                        class="btn btn-success livewire-click"
                        title="@lang('MAIN.BUTTON.CREATE-TEAM')"
                        wire:click="$set('teamId', '0')"
                        data-element-type="input"
                        data-placeholder="@lang('MAIN.PLACEHOLDER.TEAM-NAME')"
                        data-title="@lang('MAIN.MODAL.CREATE-TEAM.TITLE')"
                        data-btn-confirm="@lang('MAIN.BUTTON.CREATE')"
                        data-action="store"
                        data-btn-type="btn-success"
                        data-btn-cancel="@lang('MAIN.BUTTON.CANCEL')"
                    >
                        @lang('MAIN.BUTTON.CREATE-TEAM')
                    </button>
                    <a class="btn btn-primary btn-apply" href="{{route('teams.create')}}" data-toggle="modal" data-target="#modal-apply" title="@lang('MAIN.BUTTON.APPLY')">@lang('MAIN.BUTTON.APPLY-TEAM')</a>
                </div>
            </div>
        </div>
    <!-- /Search panel -->

    <table class="table table-bordered table-styled table-striped mb-3 w-100">
        <thead class="bg-indigo">
            <tr>
                <th class="text-center">@lang('MAIN.COLUMN.NAME')</th>
                <th class="text-center">@lang('MAIN.COLUMN.TEAMROLE')</th>
                <th class="text-center">@lang('MAIN.COLUMN.ACTIONS')</th>
            </tr>
        </thead>
        <tbody>
            @foreach(auth()->user()->teams as $key => $team)
                <tr>
                    <td><a class="team-name" href="{{route('uteam.info',[$team->id])}}">{{$team->name}}</a></td>
                    <td class="text-center">{{$team->pivot->teamrole->name}}</td>
                    <td class="text-center">
                        @switch($team->pivot->teamrole->id)
                            @case(1)
                                <button
                                    class="btn btn-sm btn-icon btn-light livewire-click"
                                    title="@lang('MAIN.HINT.EDIT')"
                                    wire:click="$set('teamId', '{{ $team->id }}')"
                                    data-element-type="input"
                                    data-placeholder="@lang('MAIN.PLACEHOLDER.TEAM-NAME')"
                                    data-title="@lang('MAIN.MODAL.EDIT-TEAM.TITLE')"
                                    data-btn-confirm="@lang('MAIN.BUTTON.SAVE')"
                                    data-action="store"
                                    data-btn-type="btn-success"
                                    data-btn-cancel="@lang('MAIN.BUTTON.CANCEL')"
                                >
                                    <i class="icon-pencil5"></i>
                                </button>
                                <button
                                    class="btn btn-sm btn-icon btn-warning livewire-click" 
                                    title="@lang('MAIN.HINT.DESTROY')"
                                    wire:click="$set('teamId', '{{ $team->id }}')"
                                    data-element-type="confirm"
                                    data-title="@lang('MAIN.MODAL.DESTROY-TEAM.TITLE')"
                                    data-text="@lang('MAIN.MODAL.DESTROY-TEAM.MESSAGE')"
                                    data-btn-confirm="@lang('MAIN.BUTTON.DESTROY')"
                                    data-action="destroy"
                                    data-type="warning"
                                    data-btn-type="btn-warning"
                                    data-btn-cancel="@lang('MAIN.BUTTON.CANCEL')"
                                >
                                    <i class="icon-x"></i>
                                </button>
                                @break
                            @default
                                <button 
                                    class="btn btn-sm btn-icon btn-primary livewire-click" 
                                    title="@lang('MAIN.HINT.EXIT')"
                                    wire:click="$set('teamId', '{{ $team->id }}')"
                                    data-element-type="confirm"
                                    data-title="@lang('MAIN.MODAL.LEAVE.TITLE')"
                                    data-text="@lang('MAIN.MODAL.LEAVE.MESSAGE')"
                                    data-btn-confirm="@lang('MAIN.BUTTON.LEAVE')"
                                    data-action="leave"
                                    data-type="question"
                                    data-btn-type="btn-primary"
                                    data-btn-cancel="@lang('MAIN.BUTTON.CANCEL')"
                                >
                                    <i class="icon-exit3"></i>
                                </button>
                        @endswitch
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
