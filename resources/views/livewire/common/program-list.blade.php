<div>
  <div class="search-panel">
    <div class="d-flex">
      <div>{{ $modelId }}</div>
      <div class="ml-auto">{{ $activeTab }} - {{ json_encode($filterData) }}</div>
    </div>
    <div class="text-right">
      <div class="btn-group">
        <div class="btn-group justify-content-center">
          <a href="#" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">{{ $cycles[$filterCycle] ?? __('MAIN.BUTTON.CIKL')}}</a>
          <div class="dropdown-menu">
            @foreach($cycles as $key => $cycle)
              <a
                wire:click.prevent="$set('filterCycle', '{{ $key }}')"
                href="#"
                class="dropdown-item element-cycle {{ $key == $filterCycle ? 'active' : ''}}"
                data-key="{{$key}}"
              >{{$cycle}}</a>
            @endforeach
          </div>
        </div>
        <div class="btn-group justify-content-center">
          <a href="#" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">{{ $branches2[$filterBranch] ?? __('MAIN.BUTTON.GALUZ')}}</a>
          <div class="dropdown-menu">
            @foreach($branches2 as $key => $branch)
              <a
                wire:click.prevent="$set('filterBranch', '{{ $key }}')"
                href="#"
                class="dropdown-item element-branch {{ $key == $filterBranch ? 'active' : ''}}"
                data-key="{{$key}}"
              >{{$branch}}</a>
            @endforeach
          </div>
        </div>
        <a class="btn btn-success btn-create-program" href="{{route('unewprograms.create')}}" title="@lang('MAIN.HINT.CREATE')"  title="'.__('MAIN.HINT.RESTORE').'" data-toggle="modal" data-target="#modal-create-program" data-title="'.__('MAIN.MODAL.RESTORE.TITLE').'" data-text="'.__('MAIN.MODAL.RESTORE.MESSAGE').'" data-color="bg-success" data-method="POST"><i class="icon-file-plus"></i></a>
      </div>
      
    </div>
  </div>

  @if(count($tabs) > 1)
    <ul class="nav nav-tabs nav-tabs-highlight">
      @foreach($tabs as $tab)
        <li class="nav-item">
          <a
            wire:click.prevent="$set('activeTab', '{{ $tab['name'] }}')"
            href="#"
            class="nav-link {{ $tab['name'] === $activeTab ? 'active show' : '' }}"
          >
            <i class="{{ $tab['icon'] }} mr-2"></i> {{ $tab['title'] }}
          </a>
        </li>
      @endforeach
    </ul>
  @endif

  <div class="programs livewire-container">
    <div class="programs__filter p-3 d-flex">
      <div class="d-flex align-items-center">
        Пошук:
        <input wire:model.debounce.500ms="search" type="text" class="programs__search form-control mx-2">
        <i class="icon-search4"></i>
      </div>
      <div class="d-flex ml-auto align-items-center">
        Показати
        <a href="#" class="btn btn-primary dropdown-toggle mx-2" data-toggle="dropdown">{{ $perPage }}</a>
        <div class="dropdown-menu">
          @foreach($countVariants as $countVariant)
            <a 
              wire:click.prevent="$set('perPage', '{{ $countVariant }}')"
              href="#"
              class="dropdown-item element-cycle {{ $perPage == $countVariant ? 'active' : '' }}"
              data-key="{{$countVariant}}"
            >{{$countVariant}}</a>
          @endforeach
        </div>
        записів
      </div>
    </div>
    <div class="programs__head bg-indigo">
      <div class="programs__cell text-center">@lang('MAIN.COLUMN.ID')</div>
      <div class="programs__cell text-center">@lang('MAIN.COLUMN.NAME')</div>
      <div class="programs__cell text-center">@lang('MAIN.COLUMN.CYCLE')</div>
      <div class="programs__cell text-center">@lang('MAIN.COLUMN.BRANCHES')</div>
      @if(in_array($filterData['owner'], ['user', 'team']))
        <div class="programs__cell text-center">
          @switch($filterData['owner'])
            @case('user')
              @lang('MAIN.COLUMN.TEAMS')
              @break
            @case('team')
              @lang('MAIN.COLUMN.EDITORS')
              @break
          @endswitch
        </div>
      @endif
      <div class="programs__cell text-center">@lang('MAIN.COLUMN.INFO')</div>
      <div class="programs__cell text-center">@lang('MAIN.COLUMN.ACTIONS')</div>
    </div>
    @if($models->count())
      @foreach($models as $model)
        <div class="programs__row">
          <div class="programs__cell text-center">{{ $model->id }}</div>
          <div class="programs__cell"><div class="programs__label">@lang('MAIN.COLUMN.NAME'): </div>{{ $model->name }}</div>
          <div class="programs__cell"><div class="programs__label">@lang('MAIN.COLUMN.CYCLE'): </div>{{ $model->cycle->name }}</div>
          <div class="programs__cell"><div class="programs__label">@lang('MAIN.COLUMN.BRANCHES'): </div>{{ $model->branches->implode('name', ', ') }}</div>

          @if(in_array($filterData['owner'], ['user', 'team']))
            <div class="programs__cell">
              @switch($filterData['owner'])
                @case('user')
                  <div class="programs__label">@lang('MAIN.COLUMN.TEAMS'): </div>
                  {{ $model->teams->implode('name', ', ') }}
                  @break
                @case('team')
                  <div class="programs__label">@lang('MAIN.COLUMN.EDITORS'): </div>
                  {{ $model->teamEditorList($filterData['id']) }}
                  @break
              @endswitch
            </div>
          @endif

          <div class="programs__cell">
            <strong>@lang('MAIN.COLUMN.CREATED'):</strong>&nbsp;{{ $model->created_at->format('d.m.Y') }}<br>
            <strong>@lang('MAIN.COLUMN.UPDATED'):</strong>&nbsp;{{ $model->updated_at->format('d.m.Y') }}<br>
            <strong>@lang('MAIN.COLUMN.ELEMENTS'):</strong>&nbsp;<span class="badge bg-warning-400 align-self-center">{{ $model->elementCount() }}</span>
          </div>
          <div class="programs__cell list-icons d-flex justify-content-center text-center">

            @if($activeTab === 'active')
              
              @include('elements.button-edit',['filterData' => $filterData, 'modelId' => $model->id])
              
              @include('elements.button-show',['modelKey' => $model->key])
              @include('elements.button-clone',['modelId' => $model->id])
              @include('elements.button-delete',['modelId' => $model->id])
            @endif
    
            @if($activeTab === 'deleted')
              @include('elements.button-restore',['modelId' => $model->id])
              @can('admin')
                @include('elements.button-destroy',['modelId' => $model->id])
              @endcan
            @endif
          </div>
          
        </div>
      @endforeach
    @else
      <div class="d-flex justify-content-center pt-4">@lang('MAIN.LABEL.NO-DATA')</div>
    @endif
    <div class="programs__pagination p-3">
      <div class="text-center">
        @if ($models->count())
          @lang('MAIN.LABEL.PAGINATOR',[
            'firstItem' => $models->firstItem(),
            'lastItem' => $models->lastItem(),
            'total' => $models->total(),
          ])
        @endif
      </div>
      <div class="ml-auto">{{ $models->links() }}</div>
    </div>
  </div>
</div>
