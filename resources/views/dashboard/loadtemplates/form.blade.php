@extends('layouts.dashboard')

@section('content')

{{ Form::model($model, [ 'url' => $model->id ? route('loadtemplates.update', $model->id) : route('loadtemplates.store'), 'method' => $model->id ? 'PUT' : 'POST', 'class' => 'ajax-form' ]) }}

<div class="d-flex align-items-start flex-column flex-md-row">
    <style>.bg-light{background: rgba(0,0,0,0.025)!important;}</style>
    <div class="order-2 order-md-1 w-100">
        <div class="card mb-3">
            <div class="card-header bg-transparent header-elements-inline">
                <h5 class="card-title">@lang('MAIN.TITLE.PARAMETERS')</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    {{ Form::label('name', __('MAIN.COLUMN.NAME'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                    <div class="col-md-8">
                        {{ Form::text('name', $model->name, array('class' => 'field form-control')) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('languagetype_id', __('MAIN.COLUMN.LANGUAGETYPE'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                    <div class="col-md-8">
                        <select name="languagetype_id" id="languagetype_id" class="field form-control">
                            <option value="0">@lang('MAIN.LABEL.UNDEFINED')</option>
                            @foreach($languagetypes as $languagetype)
                                <option value="{{ $languagetype->id }}" {{ $model->languagetype_id == $languagetype->id ? 'selected' : ''}} >{{ $languagetype->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <hr>
                <div class="form-group">
                    {{ Form::label('cperiods', __('MAIN.COLUMN.PERIODS'), ['class'=>'text-muted font-italic mb-3']) }}
                    <div class="row">
                        @foreach ($cperiods as $cperiod)
                            <div class="col-lg-3 col-md-12 col-sm-6 mb-3">
                                <div class="custom-control custom-checkbox">
                                    {{ Form::checkbox('cperiods[]',  $cperiod->id , null, ['id' => $cperiod->id, 'class' => 'custom-control-input']) }}
                                    {{ Form::label($cperiod->id, $cperiod->name, ['class' => 'custom-control-label']) }}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <hr>
                <div class="form-group">
                    {{ Form::label('cycles', __('MAIN.COLUMN.CYCLES'), ['class'=>'text-muted font-italic mb-3']) }}
                    <div class="row">
                        @foreach ($cycles as $cycle)
                            <div class="col-lg-3 col-md-12 col-sm-6 mb-3">
                                <div class="custom-control custom-checkbox">
                                    {{ Form::checkbox('cycles[]',  $cycle->id , null, ['id' => 'c'.$cycle->id, 'class' => 'custom-control-input']) }}
                                    {{ Form::label('c'.$cycle->id, $cycle->name, ['class' => 'custom-control-label']) }}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <hr>
                <div class="form-group">
                    <div class="table-load" style="display:none;">
                    <table class="table w-100">
                        <thead>
                            <tr>
                                <th rowspan="2">Галузь</th>
                                @foreach($cycles as $cycle)
                                    @foreach($cycle->grades as $key => $grade)
                                        <th colspan="3" class="text-center c{{$cycle->id}} {{ !($key & 1) ? 'bg-light' : ''}}">{{ $grade->name }}</th>
                                    @endforeach
                                @endforeach
                            </tr>
                            <tr>
                                @foreach($cycles as $cycle)
                                    @foreach($cycle->grades as $key => $grade)
                                        <th class="px-1 text-center c{{$cycle->id}} {{ !($key & 1) ? 'bg-light' : ''}}">@lang('MAIN.LABEL.REC')</th>
                                        <th class="px-1 text-center c{{$cycle->id}} {{ !($key & 1) ? 'bg-light' : ''}}">@lang('MAIN.LABEL.MIN')</th>
                                        <th class="px-1 text-center c{{$cycle->id}} {{ !($key & 1) ? 'bg-light' : ''}}">@lang('MAIN.LABEL.MAX')</th>
                                    @endforeach
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($cbranches as $cbranch)
                            <tr>
                                <td>{{ $cbranch->name }}</td>
                                @foreach($cycles as $cycle)
                                    @foreach($cycle->grades as $key => $grade)
                                        <td class="px-1 c{{$cycle->id}} {{ !($key & 1) ? 'bg-light' : ''}}">
                                            <input type="number" name="loads[{{ $grade->id }}][{{$cbranch->id}}][recommended]" class="form-control text-center" value="{{ $model->meta->loads->{$grade->id}->{$cbranch->id}->recommended ?? ''}}" step="0.5">
                                        </td>
                                        <td class="px-1 c{{$cycle->id}} {{ !($key & 1) ? 'bg-light' : ''}}">
                                            <input type="number" name="loads[{{ $grade->id }}][{{$cbranch->id}}][minimum]" class="form-control text-center" value="{{ $model->meta->loads->{$grade->id}->{$cbranch->id}->minimum ?? ''}}" step="0.5">
                                        </td>
                                        <td class="px-1 c{{$cycle->id}} {{ !($key & 1) ? 'bg-light' : ''}}">
                                            <input type="number" name="loads[{{ $grade->id }}][{{$cbranch->id}}][maximum]" class="form-control text-center" value="{{ $model->meta->loads->{$grade->id}->{$cbranch->id}->maximum ?? ''}}" step="0.5">
                                        </td>
                                    @endforeach
                                @endforeach
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>@lang('MAIN.LABEL.ADDITIONAL')</td>
                                @foreach($cycles as $cycle)
                                    @foreach($cycle->grades as $key => $grade)
                                        <td colspan="3" class="px-1 text-center c{{$cycle->id}} {{ !($key & 1) ? 'bg-light' : ''}}">
                                            <input type="number" name="additional[{{ $grade->id }}]" class="form-control text-center" value="{{ $model->meta->additional->{$grade->id} ?? ''}}" step="0.5">
                                        </td>
                                    @endforeach
                                @endforeach
                            </tr>
                            <tr>
                                <td>@lang('MAIN.LABEL.FINANCED')</td>
                                @foreach($cycles as $cycle)
                                    @foreach($cycle->grades as $key => $grade)
                                        <td colspan="3" class="px-1 text-center c{{$cycle->id}} {{ !($key & 1) ? 'bg-light' : ''}}">
                                            <input type="number" name="financed[{{ $grade->id }}]" class="form-control text-center" value="{{ $model->meta->financed->{$grade->id} ?? ''}}" step="0.5">
                                        </td>
                                    @endforeach
                                @endforeach
                            </tr>
                            <tr>
                                <td>@lang('MAIN.LABEL.ADMISSIBLE')</td>
                                @foreach($cycles as $cycle)
                                    @foreach($cycle->grades as $key => $grade)
                                        <td colspan="3" class="px-1 text-center c{{$cycle->id}} {{ !($key & 1) ? 'bg-light' : ''}}">
                                            <input type="number" name="admissible[{{ $grade->id }}]" class="form-control text-center" value="{{ $model->meta->admissible->{$grade->id} ?? ''}}" step="0.5">
                                        </td>
                                    @endforeach
                                @endforeach
                            </tr>
                        </tfoot>
                    </table>
                    </div>
                </div>

                <hr>
                <div class="form-group">
                    {{ Form::label('restrictions', __('MAIN.COLUMN.RESTRICTIONS'), ['class'=>'text-muted font-italic mb-3']) }}
                    <table class="table w-100">
                        <thead>
                            <tr>
                                <th class="text-center" rowspan="2">Компонент</th>
                                @foreach($cycles as $cycle)
                                    @foreach($cycle->grades as $key => $grade)
                                        <th colspan="2" class="text-center c{{$cycle->id}} {{ !($key & 1) ? 'bg-light' : ''}}">{{ $grade->name }}</th>
                                    @endforeach
                                @endforeach
                                <th class="text-center" rowspan="2">@lang('MAIN.COLUMN.ACTIONS')</th>
                            </tr>
                            <tr>
                                @foreach($cycles as $cycle)
                                    @foreach($cycle->grades as $key => $grade)
                                        <th class="px-1 text-center c{{$cycle->id}} {{ !($key & 1) ? 'bg-light' : ''}}">@lang('MAIN.LABEL.MIN')</th>
                                        <th class="px-1 text-center c{{$cycle->id}} {{ !($key & 1) ? 'bg-light' : ''}}">@lang('MAIN.LABEL.MAX')</th>
                                    @endforeach
                                @endforeach
                            </tr>
                        </thead>
                        <tbody id="components-area">
                            @if(isset($model->meta->restrictions) && (is_array($model->meta->restrictions) || is_object($model->meta->restrictions)))
                            @foreach($model->meta->restrictions as $restrictionKey => $restriction)
                                <tr>
                                    <td>
                                        <input type="text" class="form-control" value="{{$restriction->name ?? ''}}" name="restrictions[{{$restrictionKey}}][name]" readonly>
                                    </td>
                                    @foreach($cycles as $cycle)
                                        @foreach($cycle->grades as $key => $grade)
                                            <td class="px-1 c{{$cycle->id}} {{ !($key & 1) ? 'bg-light' : ''}}">
                                                <input type="number" name="restrictions[{{$restrictionKey}}][{{ $grade->id }}][minimum]" class="form-control text-center" value="{{ $restriction->{$grade->id}->minimum ?? ''}}" step="0.5">
                                            </td>
                                            <td class="px-1 c{{$cycle->id}} {{ !($key & 1) ? 'bg-light' : ''}}">
                                                <input type="number" name="restrictions[{{$restrictionKey}}][{{ $grade->id }}][maximum]" class="form-control text-center" value="{{ $restriction->{$grade->id}->maximum ?? ''}}" step="0.5">
                                            </td>
                                        @endforeach
                                    @endforeach
                                    <td class="text-right"><button type="button" class="btn btn-danger btn-icon btn-remove"><i class="icon-cross"></i></button></td>
                                </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                    <div class="text-right p-3">
                        <button type="button" class="btn btn-success btn-icon add-component"><i class="icon-plus22"></i></button>
                    </div>

                </div>

            </div>
        </div>

    </div>

    <!-- Right sidebar component -->
    <div class="sidebar-mobile-component w-100 w-md-auto order-1 order-md-2">
        <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
            <div class="sidebar-content">
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">@lang('MAIN.TITLE.NAVIGATION')</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="btn-group w-100">
                            <button type="submit" id="submit" class="btn btn-success w-50" title="@lang('MAIN.HINT.SAVE')"><i class="icon-floppy-disk"></i></button>
                            <a href="{{ route('loadtemplates.index') }}" class="btn btn-danger w-50" title="@lang('MAIN.HINT.EXIT')"><i class="icon-esc"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /right sidebar component -->

</div>

<template id="empty-component">
    <tr>
        <td>
            <input type="text" class="form-control" value="{name}" name="restrictions[{key}][name]" readonly>
        </td>
        @foreach($cycles as $cycle)
            @foreach($cycle->grades as $key => $grade)
                <td class="px-1 c{{$cycle->id}} {{ !($key & 1) ? 'bg-light' : ''}}">
                    <input type="number" name="restrictions[{key}][{{ $grade->id }}][minimum]" class="form-control text-center">
                </td>
                <td class="px-1 c{{$cycle->id}} {{ !($key & 1) ? 'bg-light' : ''}}">
                    <input type="number" name="restrictions[{key}][{{ $grade->id }}][maximum]" class="form-control text-center">
                </td>
            @endforeach
        @endforeach
        <td class="text-right"><button type="button" class="btn btn-danger btn-icon btn-remove"><i class="icon-cross"></i></button></td>
    </tr>
</template>

{{ Form::close() }}
@endsection

@section('scripts')
<script>
    window.onload = () => {
        let $tableLoad = document.querySelector('.table-load').style.display = 'block'

        let $cycles = document.querySelectorAll('[name="cycles[]"]')
        showColumns()

        for (var item of $cycles) {
            item.addEventListener('change', showColumns)
        }

        function showColumns() {
            for (var item of $cycles) {
                let $cells = document.querySelectorAll('.' + item.id)
                if (item.checked) {
                    for (var cell of $cells) {
                        cell.style.display = 'table-cell'
                    }
                } else {
                    for (var cell of $cells) {
                        cell.style.display = 'none'
                    }
                }
            }
        }

        let components = @json($components)

        $('.add-component').on('click', function() {
            swal({
                title: `Додавання елементу`,
                input: 'select',
                inputOptions: components,
                inputPlaceholder: 'Оберіть елемент',
                inputClass: 'form-control',
                showCancelButton: true,
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-light',
            }).then(function (result) {
                if (result.value) {
                    $('#components-area').append( renderTemplate('empty-component',{'key': result.value, 'name': components[result.value]}))
                    showColumns()
                }
            });
        });

        $(document).on('click','.btn-remove', function() {
            $(this).closest('tr').remove()
        })
    }

</script>
@endsection
