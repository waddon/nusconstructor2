@extends('layouts.dashboard')

@section('content')

<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">
        <div class="card mb-3">
            <div class="card-header bg-transparent header-elements-inline">
                <h5 class="card-title">@lang('MAIN.TITLE.PARAMETERS')</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <h1 class="text-center">{{ $model->name }}</h1>

                <table class="table table-bordered w-100">
                    <thead>
                        <tr>
                            <th class="text-center" rowspan="2" colspan="2">@lang('MAIN.COLUMN.BRANCH')</th>
                            @foreach ($model->cycles as $cycle)
                                @foreach($cycle->grades as $key => $grade)
                                    <th colspan="3" class="text-center c{{$cycle->id}} {{ !($key & 1) ? 'bg-light' : ''}}">{{ $grade->name }}</th>
                                @endforeach
                            @endforeach
                        </tr>
                        <tr>
                            @foreach ($model->cycles as $cycle)
                                @foreach($cycle->grades as $key => $grade)
                                    <th class="px-1 text-center c{{$cycle->id}} {{ !($key & 1) ? 'bg-light' : ''}}">@lang('MAIN.LABEL.REC')</th>
                                    <th class="px-1 text-center c{{$cycle->id}} {{ !($key & 1) ? 'bg-light' : ''}}">@lang('MAIN.LABEL.MIN')</th>
                                    <th class="px-1 text-center c{{$cycle->id}} {{ !($key & 1) ? 'bg-light' : ''}}">@lang('MAIN.LABEL.MAX')</th>
                                @endforeach
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($cbranches as $cbranch)
                        <tr>
                            <td rowspan="2">{{ $cbranch->name }}</td>
                            <td class="text-center">на тиждень</td>
                            @foreach($model->cycles as $cycle)
                                @foreach($cycle->grades as $key => $grade)
                                    <td class="text-center px-1 {{ !($key & 1) ? 'bg-light' : ''}}">
                                        {{ $model->meta->loads->{$grade->id}->{$cbranch->id}->recommended ?? ''}}
                                    </td>
                                    <td class="text-center px-1 {{ !($key & 1) ? 'bg-light' : ''}}">
                                        {{ $model->meta->loads->{$grade->id}->{$cbranch->id}->minimum ?? ''}}
                                    </td>
                                    <td class="text-center px-1 {{ !($key & 1) ? 'bg-light' : ''}}">
                                        {{ $model->meta->loads->{$grade->id}->{$cbranch->id}->maximum ?? ''}}
                                    </td>
                                @endforeach
                            @endforeach
                        </tr>
                        <tr>
                            <td class="text-center">на рік</td>
                            @foreach($model->cycles as $cycle)
                                @foreach($cycle->grades as $key => $grade)
                                    <td class="text-center px-1 {{ !($key & 1) ? 'bg-light' : ''}}">
                                        {{ $model->meta->loads->{$grade->id}->{$cbranch->id}->recommended * 35 ?? ''}}
                                    </td>
                                    <td class="text-center px-1 {{ !($key & 1) ? 'bg-light' : ''}}">
                                        {{ $model->meta->loads->{$grade->id}->{$cbranch->id}->minimum * 35 ?? ''}}
                                    </td>
                                    <td class="text-center px-1 {{ !($key & 1) ? 'bg-light' : ''}}">
                                        {{ $model->meta->loads->{$grade->id}->{$cbranch->id}->maximum * 35 ?? ''}}
                                    </td>
                                @endforeach
                            @endforeach
                        </tr>
                        @endforeach
                    </tbody>
                        <tr>
                            <td rowspan="2">Усього</td>
                            <td class="text-center">на тиждень</td>
                            @foreach($model->cycles as $cycle)
                                @foreach($cycle->grades as $key => $grade)
                                    <td class="px-1 text-center {{ !($key & 1) ? 'bg-light' : ''}}">
                                        {{ $model->getCalculatedData($grade->id, 'loads', 'weekly', 'recommended') }}
                                    </td>
                                    <td class="px-1 text-center {{ !($key & 1) ? 'bg-light' : ''}}">
                                        {{ $model->getCalculatedData($grade->id, 'loads', 'weekly', 'minimum') }}
                                    </td>
                                    <td class="px-1 text-center {{ !($key & 1) ? 'bg-light' : ''}}">
                                        {{ $model->getCalculatedData($grade->id, 'loads', 'weekly', 'maximum') }}
                                    </td>
                                @endforeach
                            @endforeach
                        </tr>
                        <tr>
                            <td class="text-center">на рік</td>
                            @foreach($model->cycles as $cycle)
                                @foreach($cycle->grades as $key => $grade)
                                    <td class="px-1 text-center {{ !($key & 1) ? 'bg-light' : ''}}">
                                        {{ $model->getCalculatedData($grade->id, 'loads', 'annual', 'recommended') }}
                                    </td>
                                    <td class="px-1 text-center {{ !($key & 1) ? 'bg-light' : ''}}">
                                        {{ $model->getCalculatedData($grade->id, 'loads', 'annual', 'minimum') }}
                                    </td>
                                    <td class="px-1 text-center {{ !($key & 1) ? 'bg-light' : ''}}">
                                        {{ $model->getCalculatedData($grade->id, 'loads', 'annual', 'maximum') }}
                                    </td>
                                @endforeach
                            @endforeach
                        </tr>

                        <tr>
                            <td rowspan="2">@lang('MAIN.LABEL.ADDITIONAL')</td>
                            <td class="text-center">на тиждень</td>
                            @foreach($model->cycles as $cycle)
                                @foreach($cycle->grades as $key => $grade)
                                    <td colspan="3" class="px-1 text-center {{ !($key & 1) ? 'bg-light' : ''}}">
                                        {{ $model->getCalculatedData($grade->id, 'additional', 'weekly') }}
                                    </td>
                                @endforeach
                            @endforeach
                        </tr>
                        <tr>
                            <td class="text-center">на рік</td>
                            @foreach($model->cycles as $cycle)
                                @foreach($cycle->grades as $key => $grade)
                                    <td colspan="3" class="px-1 text-center {{ !($key & 1) ? 'bg-light' : ''}}">
                                        {{ $model->getCalculatedData($grade->id, 'additional', 'annual') }}
                                    </td>
                                @endforeach
                            @endforeach
                        </tr>

                        <tr>
                            <td rowspan="2">@lang('MAIN.LABEL.FINANCED')</td>
                            <td class="text-center">на тиждень</td>
                            @foreach($model->cycles as $cycle)
                                @foreach($cycle->grades as $key => $grade)
                                    <td colspan="3" class="px-1 text-center {{ !($key & 1) ? 'bg-light' : ''}}">
                                        {{ $model->getCalculatedData($grade->id, 'financed', 'weekly') }}
                                    </td>
                                @endforeach
                            @endforeach
                        </tr>
                        <tr>
                            <td class="text-center">на рік</td>
                            @foreach($model->cycles as $cycle)
                                @foreach($cycle->grades as $key => $grade)
                                    <td colspan="3" class="px-1 text-center {{ !($key & 1) ? 'bg-light' : ''}}">
                                        {{ $model->getCalculatedData($grade->id, 'financed', 'annual') }}
                                    </td>
                                @endforeach
                            @endforeach
                        </tr>

                        <tr>
                            <td rowspan="2">@lang('MAIN.LABEL.ADMISSIBLE')</td>
                            <td class="text-center">на тиждень</td>
                            @foreach($model->cycles as $cycle)
                                @foreach($cycle->grades as $key => $grade)
                                    <td colspan="3" class="px-1 text-center {{ !($key & 1) ? 'bg-light' : ''}}">
                                        {{ $model->getCalculatedData($grade->id, 'admissible', 'weekly') }}
                                    </td>
                                @endforeach
                            @endforeach
                        </tr>
                        <tr>
                            <td class="text-center">на рік</td>
                            @foreach($model->cycles as $cycle)
                                @foreach($cycle->grades as $key => $grade)
                                    <td colspan="3" class="px-1 text-center {{ !($key & 1) ? 'bg-light' : ''}}">
                                        {{ $model->getCalculatedData($grade->id, 'admissible', 'annual') }}
                                    </td>
                                @endforeach
                            @endforeach
                        </tr>
                    <tfoot>
                        
                    </tfoot>
                </table>

            </div>
        </div>

    </div>

    <!-- Right sidebar component -->
    <div class="sidebar-mobile-component w-100 w-md-auto order-1 order-md-2">
        <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
            <div class="sidebar-content">
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">@lang('MAIN.TITLE.NAVIGATION')</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="btn-group w-100">
                            <a href="{{ route('loadtemplates.index') }}" class="btn btn-danger w-100" title="@lang('MAIN.HINT.EXIT')"><i class="icon-esc"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /right sidebar component -->

</div>

@endsection

