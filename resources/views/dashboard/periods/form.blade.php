@extends('layouts.dashboard')

@section('content')

{{ Form::model($model, [ 'url' => $model->id ? route('periods.update', $model->id) : route('periods.store'), 'method' => $model->id ? 'PUT' : 'POST', 'class' => 'ajax-form' ]) }}

<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">
        <div class="card mb-3">
            <div class="card-header bg-transparent header-elements-inline">
                <h5 class="card-title">@lang('MAIN.TITLE.PARAMETERS')</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    {{ Form::label('name', __('MAIN.COLUMN.NAME'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                    <div class="col-md-8">
                        {{ Form::text('name', $model->name, array('class' => 'field form-control')) }}
                    </div>
                </div>
                <div class="form-group">
                    <input type="hidden" name="meta" value="[]">
                    {{ Form::label('meta', __('MAIN.COLUMN.ELEMENTS'), ['class'=>'small text-muted font-italic col-form-label']) }}
                    <div class="elements mb-3">
                        @if(isset($model->meta) && $model->meta)
                            @foreach($model->meta as $key => $element)
                                <div class="element d-flex align-items-center">
                                    <div class="element-element element-move p-2"><i class="icon-grid"></i></div>
                                    <div class="element-element element-name p-2 flex-grow-1 flex-shrink-1" contenteditable="true">{{$element}}</div>
                                    <div class="element-element element-remove p-2"><i class="icon-bin"></i></div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <button type="button" class="btn btn-success element-add" title="@lang('MAIN.HINT.APPEND')"> + </button>
                </div>
            </div>
        </div>

    </div>

    <!-- Right sidebar component -->
    <div class="sidebar-mobile-component w-100 w-md-auto order-1 order-md-2">
        <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
            <div class="sidebar-content">
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">@lang('MAIN.TITLE.NAVIGATION')</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="btn-group w-100">
                            <button type="submit" id="submit" class="btn btn-success w-50" title="@lang('MAIN.HINT.SAVE')"><i class="icon-floppy-disk"></i></button>
                            <a href="{{ route('periods.index') }}" class="btn btn-danger w-50" title="@lang('MAIN.HINT.EXIT')"><i class="icon-esc"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /right sidebar component -->

</div>

{{ Form::close() }}

<style type="text/css">
    .element-move {cursor: move;}
    .element-name {border-bottom: 1px solid #eee;}
    .element-remove {cursor: pointer;}
</style>
<template id="element-empty">
    <div class="element d-flex align-items-center">
        <div class="element-element element-move p-2"><i class="icon-grid"></i></div>
        <div class="element-element element-name p-2 flex-grow-1 flex-shrink-1" contenteditable="true"></div>
        <div class="element-element element-remove p-2"><i class="icon-bin"></i></div>
    </div>
</template>
@endsection

@section('scripts')
<script src="/js/jquery_ui/interactions.min.js"></script>
<script>
    $('form').on('submit', function(e){
        elementlist = $(this).find('.elements');
        elementlist.siblings('input[type=hidden]').val(JSON.stringify( create_array(elementlist) ));
    });

    $(document).on('click','.element-remove',function() {
        elementlist = $(this).closest('.elements');
        $(this).closest('.element').remove();
        elementlist.siblings('input[type=hidden]').val(JSON.stringify( create_array(elementlist) ));
    })

    $(document).on('click','.element-add',function(){
        elementlist = $(this).siblings('.elements');
        elementlist.append( renderTemplate('element-empty',{
        }) );
        elementlist.siblings('input[type=hidden]').val(JSON.stringify( create_array(elementlist) ));
    });

    $('.elements').sortable({
        handle: ".element-move",
        tolerance: 'pointer',
        opacity: 0.6,
        //containment: "parent",
        placeholder: 'sortable-placeholder',
        start: function(e, ui){
            ui.placeholder.height(ui.item.outerHeight());
        },
        stop : function(e,ui){
            elementlist = $(this);
            elementlist.siblings('input[type=hidden]').val(JSON.stringify( create_array(elementlist) ));
        }
    });

    function create_array(elementlist) {
        var array = [];
        elementlist.find('.element-name').each(function(){
            console.log($(this).text());
            array.push($(this).text());
        });
        return array;
    }

    // function renderTemplate(name, data) {
    //     var template = document.getElementById(name).innerHTML;
    //     for (var property in data) {
    //         if (data.hasOwnProperty(property)) {
    //             var search = new RegExp('{' + property + '}', 'g');
    //             template = template.replace(search, data[property]);
    //         }
    //     }
    //     return template;
    // }

</script>
@endsection
