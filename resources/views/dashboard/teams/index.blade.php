@extends('layouts.dashboard')

@section('content')
<div class="card mb-3">

    <!-- Search panel -->
        <div class="search-panel">
            <div class="text-right">
                <div class="btn-group">
                    <div class="btn-group justify-content-center">
                        <a href="#" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">@lang('MAIN.BUTTON.TTYPE')</a>
                        <div class="dropdown-menu">
                                @foreach($ttypes as $key => $ttype)
                                    <a href="#" class="dropdown-item element-ttype {{!$key ? 'active' : ''}}" data-key="{{$key}}">{{$ttype}}</a>
                                @endforeach
                        </div>
                    </div>
                    <a class="btn btn-primary" href="{{route('teams.create')}}" title="@lang('Create')"><i class="icon-file-plus"></i></a>
                    <button type="button" class="btn btn-primary datatableRefresh" title="@lang('Refresh')"><i class="icon-sync"></i></button>
                </div>                
                
            </div>
        </div>
    <!-- /Search panel -->

    <!-- Tabs -->
            <ul class="nav nav-tabs nav-tabs-highlight">
                <li class="nav-item"><a href="#tab-active" class="nav-link active" data-toggle="tab"><i class="icon-menu7 mr-2"></i> @lang('MAIN.TAB.ACTIVE')</a></li>
                <li class="nav-item"><a href="#tab-trash" class="nav-link" data-toggle="tab"><i class="icon-trash mr-2"></i> @lang('MAIN.TAB.TRASH')</a></li>
            </ul>

            <div class="tab-content">
                <!-- Active Table -->
                    <div class="tab-pane fade show active" id="tab-active">
                        <div class="table-responsive">
                            <table class="table table-bordered table-styled table-striped mb-3 w-100 datatable" id="datatableActive">
                                <thead class="bg-indigo">
                                    <tr>
                                        <th>@lang('MAIN.COLUMN.ID')</th>
                                        <th>@lang('MAIN.COLUMN.NAME')</th>
                                        <th>@lang('MAIN.COLUMN.LEADER')</th>
                                        <th>@lang('MAIN.COLUMN.TTYPE')</th>
                                        <th>@lang('MAIN.COLUMN.USERS')</th>
                                        <th>@lang('MAIN.COLUMN.CREATED')</th>
                                        <th>@lang('MAIN.COLUMN.ACTIONS')</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                <!-- /Active Table -->
                <!-- Trash Table -->
                    <div class="tab-pane fade show" id="tab-trash">
                        <div class="table-responsive">
                            <table class="table table-bordered table-styled table-striped mb-3 w-100 datatable" id="datatableTrash">
                                <thead class="bg-indigo">
                                    <tr>
                                        <th>@lang('MAIN.COLUMN.ID')</th>
                                        <th>@lang('MAIN.COLUMN.NAME')</th>
                                        <th>@lang('MAIN.COLUMN.LEADER')</th>
                                        <th>@lang('MAIN.COLUMN.TTYPE')</th>
                                        <th>@lang('MAIN.COLUMN.USERS')</th>
                                        <th>@lang('MAIN.COLUMN.CREATED')</th>
                                        <th>@lang('MAIN.COLUMN.ACTIONS')</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                <!-- /Trash Table -->
            </div>
    <!-- /Tabs -->

</div>
@endsection

@section('scripts')
<script src="/js/datatables/datatables.min.js"></script>
<script>
    var language = {"url": "/js/datatables/languages/ukrainian.json"};
    var pageLength = {{ isset(auth()->user()->options->datatablePageLength) ? auth()->user()->options->datatablePageLength : 10 }};
    var current_ttype = 0;
    var columns = [
            {"data":"id"},
            {"data":"name"},
            {"data":"leader","searchable":false,"orderable":false},
            {"data":"ttype","searchable":false,"orderable":false},
            {"data":"users","searchable":false,"orderable":false},
            {"data":"created_at"},
            {"data":"actions","searchable":false,"orderable":false}
        ];
    var columnDefs = [
            { "targets": 0, "className": "text-center", },
            { "targets": 3, "className": "text-center", },
            { "targets": 4, "className": "text-center", },
            { "targets": 5, "className": "text-center", },
            { "targets": 6, "className": "text-center", },
        ];


    var tableActive = $("#datatableActive").DataTable({
        autoWidth : true,
        responsive: true,
        processing: true,
        serverSide: true,
        pageLength: pageLength,
        bSortCellsTop: true,
        order: [[ 0, "desc" ]],
        ajax: {
            url: "{{route('teams.data')}}",
            dataType: "json",
            type: "POST",
            //data: {"_token":"{{ csrf_token() }}", "table":"active"},
            data: function ( d ) {
                d._token = "{{ csrf_token() }}";
                d.ttype_id = current_ttype;
                d.table = "active";
            }
        },
        columns:columns,
        columnDefs: columnDefs,
        language: language,
    });
    var tableTrash = $("#datatableTrash").DataTable({
        autoWidth : true,
        responsive: true,
        processing: true,
        serverSide: true,
        pageLength: pageLength,
        bSortCellsTop: true,
        order: [[ 0, "desc" ]],
        ajax: {
            url: "{{route('teams.data')}}",
            dataType: "json",
            type: "POST",
            //data: {"_token":"{{ csrf_token() }}", "table":"trash"},
            data: function ( d ) {
                d._token = "{{ csrf_token() }}";
                d.ttype_id = current_ttype;
                d.table = "trash";
            }
        },
        columns:columns,
        columnDefs: columnDefs,
        language: language,
    });


    $('div.search-panel').on('click','.element-ttype',function(e){
        e.preventDefault();
        current_ttype = $(this).attr('data-key');
        $('.element-ttype').removeClass('active');
        $(this).addClass('active');
        $('.datatableRefresh').click();
    });
</script>
@endsection
