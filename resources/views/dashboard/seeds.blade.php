@extends('layouts.dashboard')

@section('content')
    <h1 class="mb-3"><i class="icon-database-refresh icon-2x mr-3"></i>@lang('MAIN.TITLE.SEEDS')</h1>
    <div class="card">
        <div class="card-body">
            <div class=" text-center mb-3"><span class="text-danger font-weight-bold">ОСТОРОЖНО!</span> Данные в выбранных таблицах буду полностью уничтожены и заменены первоначальными</div>
            <form method="POST">
                @csrf
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="grades" id="grades">
                    <label class="custom-control-label" for="grades">@lang('MAIN.MENU.GRADES')</label>
                </div>
                <hr>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="languagetypes" id="languagetypes">
                    <label class="custom-control-label" for="languagetypes">@lang('MAIN.MENU.LANGUAGETYPES')</label>
                </div>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="cperiods" id="cperiods">
                    <label class="custom-control-label" for="cperiods">@lang('MAIN.MENU.CPERIODS')</label>
                </div>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="ctypes" id="ctypes">
                    <label class="custom-control-label" for="ctypes">@lang('MAIN.MENU.CTYPES')</label>
                </div>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="cbranches" id="cbranches">
                    <label class="custom-control-label" for="cbranches">@lang('MAIN.MENU.CBRANCHES')</label>
                </div>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="cauthors" id="cauthors">
                    <label class="custom-control-label" for="cauthors">@lang('MAIN.MENU.CAUTHORS')</label>
                </div>
                <hr>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="components" id="components">
                    <label class="custom-control-label" for="components">@lang('MAIN.MENU.COMPONENTS')</label>
                </div>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="cprograms" id="cprograms">
                    <label class="custom-control-label" for="cprograms">@lang('MAIN.MENU.CPROGRAMS')</label>
                </div>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="loadtemplates" id="loadtemplates">
                    <label class="custom-control-label" for="loadtemplates">@lang('MAIN.MENU.LOADTEMPLATES')</label>
                </div>
                <hr>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="tloads" id="tloads">
                    <label class="custom-control-label" for="tloads">@lang('MAIN.MENU.TLOADS')</label>
                </div>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="tplans" id="tplans">
                    <label class="custom-control-label" for="tplans">@lang('MAIN.MENU.TPLANS')</label>
                </div>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="yplans" id="yplans">
                    <label class="custom-control-label" for="yplans">@lang('MAIN.MENU.YPLANS')</label>
                </div>

                <div class="text-center">
                    <button class="btn btn-primary">@lang('MAIN.BUTTON.SAVE')</button>
                </div>
            </form>
        </div>
    </div>
@endsection
