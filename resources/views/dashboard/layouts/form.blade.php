@extends('layouts.dashboard')

@section('content')

{{ Form::model($model, [ 'url' => $model->id ? route('teams.update', $model->id) : route('teams.store'), 'method' => $model->id ? 'PUT' : 'POST', 'class' => 'ajax-form' ]) }}

<div class="d-flex align-items-start flex-column flex-md-row flex-wrap flex-md-nowrap flex-lg-nowrap flex-xl-nowrap">

    <div class="order-2 order-md-1 w-100">
        <div class="card mb-3">
            <div class="card-header bg-transparent header-elements-inline">
                <h5 class="card-title">@lang('MAIN.TITLE.PARAMETERS')</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    {{ Form::label('name', __('MAIN.COLUMN.NAME'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                    <div class="col-md-8">
                        {{ Form::text('name', $model->name, array('class' => 'field form-control')) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('ttype_id', __('MAIN.COLUMN.TTYPE'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                    <div class="col-md-8">
                        {{ Form::select('ttype_id', $ttypes, $model->ttype_id, array('class' => 'field form-control')) }}
                    </div>
                </div>
                <div class="form-group row" id="institution-selector">
                    {{ Form::label('institution_id', __('MAIN.COLUMN.INSTITUTION'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                    <div class="col-md-8">
                        <select class="field form-control select-remote-data" data-fouc id="institution_id" name="institution_id">
                            <option value="{{$model->institutions->first()->id ?? 0}}" selected>{{$model->institutions->first()->meta->name ?? __('MAIN.PLACEHOLDER.SELECT-INSTITUTION')}}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('leader_id', __('MAIN.COLUMN.LEADER'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                    <div class="col-md-8">
                        <select class="field form-control select-remote-data" data-fouc id="leader_id" name="leader_id">
                            <option value="{{$model->getLeader()->id}}" selected>{{$model->getLeader()->name}} {{$model->getLeader()->second_name}}</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Right sidebar component -->
    <div class="sidebar-mobile-component w-100 w-md-auto order-1 order-md-2">
        <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
            <div class="sidebar-content">
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">@lang('MAIN.TITLE.NAVIGATION')</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <p>
                            <span class="small text-muted font-italic">@lang('MAIN.COLUMN.CREATED')</span>
                            <span class="float-right">{{$model->created_at}}</span>
                        </p>
                        <p>
                            <span class="small text-muted font-italic">@lang('MAIN.COLUMN.UPDATED')</span>
                            <span class="float-right">{{$model->updated_at}}</span>
                        </p>
                    </div>
                    <div class="card-body">
                        <div class="btn-group w-100">
                            <button type="submit" id="submit" class="btn btn-success w-50" title="@lang('MAIN.HINT.SAVE')"><i class="icon-floppy-disk"></i></button>
                            <a href="{{ route('teams.index') }}" class="btn btn-danger w-50" title="@lang('MAIN.HINT.EXIT')"><i class="icon-esc"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /right sidebar component -->

</div>

{{ Form::close() }}
@endsection

@section('scripts')
<style type="text/css">
    .select2-selection__rendered{
        white-space: pre-wrap!important;
    }
</style>
<script src="/js/select2/select2.min.js"></script>
<script>
    showInstitutionSelect();
    $('#leader_id').select2({
        ajax: {
            url: "{{route('users.select2')}}",
            type: 'get',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term,
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        minimumInputLength: 0,
        templateResult: formatRepo, // omitted for brevity, see the source of this page
        templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
    });
    
    function formatRepo (repo) {
        if (repo.loading) return repo.name;
        var markup = '<div class="select2-result-repository clearfix">';
                markup += '<div class="select2-result-repository__avatar"><img src="' + repo.avatar + '"></div>';
                markup += '<div class="select2-result-repository__meta">';
                    markup += '<div class="select2-result-repository__title">' + repo.combineName + '</div>';
                    markup += '<div class="select2-result-repository__description"></div>';
                    markup += '<div class="select2-result-repository__statistics">';
                        markup += '<div class="select2-result-repository__forks"></div>';
                        markup += '<div class="select2-result-repository__stargazers">@lang("MAIN.COLUMN.EMAIL"): ' + repo.email + '</div>';
                        markup += '<div class="select2-result-repository__watchers"></div>';
                    markup += '</div>';
                markup += '</div>';
            markup += '</div>';
        return markup;
    }

    function formatRepoSelection (repo) {
        return repo.combineName || repo.text;
    }

    $('#institution_id').select2({
        ajax: {
            url: "{{route('institutions.select2')}}",
            type: 'get',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term,
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        minimumInputLength: 0,
        templateResult: formatRepoInstitution, // omitted for brevity, see the source of this page
        templateSelection: formatRepoSelectionInstitution // omitted for brevity, see the source of this page
    });
    
    function formatRepoInstitution (repo) {
        if (repo.loading) return repo.name;
        var markup = '<div class="select2-result-repository clearfix">';
                //markup += '<div class="select2-result-repository__avatar"><img src="' + repo.avatar + '"></div>';
                //markup += '<div class="select2-result-repository__meta">';
                    markup += '<div class="select2-result-repository__title">' + repo.name + '</div>';
                    markup += '<div class="select2-result-repository__description">' + repo.id + '</div>';
                    markup += '<div class="select2-result-repository__statistics">';
                        markup += '<div class="select2-result-repository__forks">@lang("MAIN.COLUMN.REGION"): ' + repo.region_name + '</div>';
                        markup += '<div class="select2-result-repository__stargazers">@lang("MAIN.COLUMN.KOATUU"): ' + repo.koatuu_name + '</div>';
                        markup += '<div class="select2-result-repository__watchers">@lang("MAIN.COLUMN.ADDRESS"): ' + repo.address + '</div>';
                    markup += '</div>';
                //markup += '</div>';
            markup += '</div>';
        return markup;
    }

    function formatRepoSelectionInstitution (repo) {
        return repo.name || repo.text;
    }

    $('#ttype_id').on('change', function () {
        showInstitutionSelect();
    });

    function showInstitutionSelect() {
        if ( $('#ttype_id').val() == 2 ){
            $('#institution-selector').show(100);
        } else{
            $('#institution-selector').hide(100);
        }
    }
</script>
@endsection
