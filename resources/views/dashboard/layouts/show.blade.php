@extends('layouts.dashboard')

@section('content')

<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">
        <div class="card mb-3">
            <div class="card-header bg-transparent header-elements-inline">
                <h5 class="card-title">@lang('MAIN.TITLE.PARAMETERS')</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <h1 class="text-center">{{$model->name}}</h1>
                <h3 class="text-center">{{$model->ttype->name}}</h3>
                <table class="table table-bordered table-styled table-striped mb-3 w-100 datatable" id="datatableActive">
                    <thead class="bg-indigo">
                        <tr>
                            <th>@lang('MAIN.COLUMN.ID')</th>
                            <th>@lang('MAIN.COLUMN.IMAGE')</th>
                            <th>@lang('MAIN.COLUMN.NAME')</th>
                            <th>@lang('MAIN.COLUMN.EMAIL')</th>
                            <th>@lang('MAIN.COLUMN.TEAMROLE')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($model->users as $user)
                            <tr>
                                <td class="text-center">{{$user->id}}</td>
                                <td class="text-center"><img src="{{$user->getAvatarPath()}}" class="rounded-circle shadow-1" style="object-fit: cover;" width="80" height="80" alt=""></td>
                                <td>{{$user->combineName()}}</td>
                                <td>{{$user->email}}</td>
                                <td class="text-center">{{$user->pivot->teamrole->name}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>

    <!-- Right sidebar component -->
    <div class="sidebar-mobile-component w-100 w-md-auto order-1 order-md-2">
        <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
            <div class="sidebar-content">
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">@lang('MAIN.TITLE.NAVIGATION')</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="btn-group w-100">
                            <a href="{{ route('teams.edit',$model->id) }}" class="btn btn-primary w-50" title="@lang('MAIN.HINT.EDIT')"><i class="icon-pencil7"></i></a>
                            <a href="{{ route('teams.index') }}" class="btn btn-danger w-50" title="@lang('MAIN.HINT.EXIT')"><i class="icon-esc"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /right sidebar component -->

</div>

@endsection


