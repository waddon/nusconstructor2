@extends('layouts.dashboard')

@section('content')
<div class="card mb-3">

    <!-- Search panel -->
        <div class="search-panel">
            <div class="text-right">
                <div class="btn-group">
                    {{--<div class="btn-group justify-content-center">
                        <a href="#" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">@lang('MAIN.BUTTON.ZOR')</a>
                        <div class="dropdown-menu">
                                @foreach($zors as $key => $zor)
                                    <a href="#" class="dropdown-item element-zorl {{!$key ? 'active' : ''}}" data-key="{{$key}}">{{$zor}}</a>
                                @endforeach
                        </div>
                    </div>--}}
                    <div class="btn-group justify-content-center">
                        <a href="#" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">@lang('MAIN.BUTTON.ZMIST')</a>
                        <div class="dropdown-menu">
                                @foreach($zmists as $key => $zmist)
                                    <a href="#" class="dropdown-item element-zmist {{!$key ? 'active' : ''}}" data-key="{{$key}}">{{$zmist}}</a>
                                @endforeach
                        </div>
                    </div>
                    <a class="btn btn-primary" href="{{route('kors.create')}}" title="@lang('MAIN.HINT.CREATE')"><i class="icon-file-plus"></i></a>
                    <button type="button" class="btn btn-primary datatableRefresh" title="@lang('MAIN.HINT.REFRESH')"><i class="icon-sync"></i></button>
                </div>                
                
            </div>
        </div>
    <!-- /Search panel -->

    <!-- Tabs -->
            <ul class="nav nav-tabs nav-tabs-highlight">
                <li class="nav-item"><a href="#tab-active" class="nav-link active" data-toggle="tab"><i class="icon-menu7 mr-2"></i> @lang('MAIN.TAB.ACTIVE')</a></li>
            </ul>

            <div class="tab-content">
                <!-- Active Table -->
                    <div class="tab-pane fade show active" id="tab-active">
                        <div class="table-responsive">
                            <table class="table table-bordered table-styled table-striped mb-3 w-100 datatable" id="datatableActive">
                                <thead class="bg-indigo">
                                    <tr>
                                        <th>#</th>
                                        <th>@lang('MAIN.COLUMN.NAME')</th>
                                        <th>@lang('MAIN.COLUMN.KEY')</th>
                                        <th>@lang('MAIN.COLUMN.ZOR')</th>
                                        <th>@lang('MAIN.COLUMN.ZMIST')</th>
                                        <th>@lang('MAIN.COLUMN.ACTIONS')</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                <!-- /Active Table -->
            </div>
    <!-- /Tabs -->

</div>
@endsection

@section('scripts')
<style type="text/css">
    .search-panel .dropdown-menu{max-height: 500px;overflow-y: scroll;}
</style>
<script src="/js/datatables/datatables.min.js"></script>
<script>
    var language = {"url": "/js/datatables/languages/ukrainian.json"};
    var pageLength = {{ isset(auth()->user()->options->datatablePageLength) ? auth()->user()->options->datatablePageLength : 10 }};
    var current_zor = 0;
    var current_zmist = 0;
    var columns = [
            {"data":"id"},
            {"data":"name"},
            {"data":"key"},
            {"data":"zor","searchable":false,"orderable":false},
            {"data":"zmist","searchable":false,"orderable":false},
            {"data":"actions","searchable":false,"orderable":false}
        ];
    var columnDefs = [
            { "targets": 0, "className": "text-center", },
            { "targets": 2, "className": "text-center", },
            { "targets": 3, "className": "text-center", },
            { "targets": 4, "className": "text-center", },
            { "targets": 5, "className": "text-center", },
        ];


    var tableActive = $("#datatableActive").DataTable({
        autoWidth : true,
        responsive: true,
        processing: true,
        serverSide: true,
        pageLength: pageLength,
        bSortCellsTop: true,
        order: [[ 0, "desc" ]],
        ajax: {
            url: "{{route('kors.data')}}",
            dataType: "json",
            type: "POST",
            data: function ( d ) {
                d._token = "{{ csrf_token() }}";
                d.id_zor  = current_zor;
                d.id_zmist = current_zmist;
                d.table = "active";
            }
        },
        columns:columns,
        columnDefs: columnDefs,
        language: language,
    });

    {{--$('div.search-panel').on('click','.element-zor',function(e){
        e.preventDefault();
        current_zor = $(this).attr('data-key');
        $('.element-zor').removeClass('active');
        $(this).addClass('active');
        $('.datatableRefresh').click();
    });--}}

    $('div.search-panel').on('click','.element-zmist',function(e){
        e.preventDefault();
        current_zmist = $(this).attr('data-key');
        $('.element-zmist').removeClass('active');
        $(this).addClass('active');
        $('.datatableRefresh').click();
    });
</script>
@endsection
