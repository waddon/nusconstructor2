@extends('layouts.dashboard')

@section('content')
    <div class="card">
        <div class="card-header header-elements-inline">
            <h6 class="card-title">@lang('MAIN.TITLE.OPTIONS')</h6>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                </div>
            </div>
        </div>
        <form method="POST">
            <div class="card-body">
                @csrf
                <h3>@lang('MAIN.TITLE.HOME-PAGE')</h3>
                <div class="form-group row">
                    <label for="" class="small text-muted font-italic col-form-label col-sm-2">@lang('MAIN.OPTION.PAGE-ABOUT')</label>
                    <div class="col-sm-4">
                        <select name="pageAbout" id="pageAbout" class="form-control">
                            <option value="0"> - @lang('MAIN.PLACEHOLDER.SELECT-PAGE') - </option>
                            @if(isset($pages))
                                @foreach($pages as $key => $page)
                                    <option value="{{$page->id_page}}" {{(isset($options['pageAbout']) and $options['pageAbout']==$page->id_page) ? 'selected' : ''}}>{{$page->name_page}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="" class="small text-muted font-italic col-form-label col-sm-2">@lang('MAIN.OPTION.EMBED-TITLE')</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="embedTitle1" id="embedTitle1" value="{{isset($options['embedTitle1']) ? $options['embedTitle1'] : ''}}">
                    </div>
                    <label for="" class="small text-muted font-italic col-form-label col-sm-2">@lang('MAIN.OPTION.EMBED-LINK')</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="embedLink1" id="embedLink1" value="{{isset($options['embedLink1']) ? $options['embedLink1'] : ''}}">
                    </div>
                    <label for="" class="small text-muted font-italic col-form-label col-sm-2">@lang('MAIN.OPTION.EMBED-TITLE')</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="embedTitle2" id="embedTitle2" value="{{isset($options['embedTitle2']) ? $options['embedTitle2'] : ''}}">
                    </div>
                    <label for="" class="small text-muted font-italic col-form-label col-sm-2">@lang('MAIN.OPTION.EMBED-LINK')</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="embedLink2" id="embedLink2" value="{{isset($options['embedLink2']) ? $options['embedLink2'] : ''}}">
                    </div>
                </div>
            </div>
            <!-- AutoSave-->
                <div class="card-body">
                    <h3>@lang('MAIN.TITLE.PROGRAM-EDITING')</h3>
                    <div class="form-group row">
                        <label for="" class="small text-muted font-italic col-form-label col-sm-2 mb-3">@lang('MAIN.OPTION.PROGRAM-AUTOSAVE')</label>
                        <div class="col-sm-4 mb-3">
                            <select name="programAutosave" id="programAutosave" class="form-control">
                                <option value="0" @if(isset($options['programAutosave']) and $options['programAutosave']==0) selected @endif>@lang('MAIN.ANSWER.NO')</option>
                                <option value="1" @if(isset($options['programAutosave']) and $options['programAutosave']==1) selected @endif>@lang('MAIN.ANSWER.YES')</option>
                            </select>
                        </div>
                        <label for="" class="small text-muted font-italic col-form-label col-sm-2 mb-3">@lang('MAIN.OPTION.PROGRAM-AUTOSAVE-INTERVAL')</label>
                        <div class="col-sm-4 mb-3">
                            <input type="number" class="form-control" name="programAutosaveInterval" id="programAutosaveInterval" value="{{isset($options['programAutosaveInterval']) ? $options['programAutosaveInterval'] : '300'}}">
                        </div>

                        <label for="" class="small text-muted font-italic col-form-label col-sm-2 mb-3">Функціональні обмеження в залежності від типу програми</label>
                        <div class="col-sm-4 mb-3">
                            <select name="programTypeRestrictions" id="programTypeRestrictions" class="form-control">
                                <option value="0" @if(isset($options['programTypeRestrictions']) and $options['programTypeRestrictions']==0) selected @endif>Викл</option>
                                <option value="1" @if(isset($options['programTypeRestrictions']) and $options['programTypeRestrictions']==1) selected @endif>Вкл</option>
                            </select>
                        </div>
                    </div>
                </div>
            <!-- /AutoSave-->
            <!-- Sockets-->
                <div class="card-body">
                    <h3>@lang('MAIN.TITLE.SOCKETS')</h3>
                    <div class="form-group row">
                        <label for="" class="small text-muted font-italic col-form-label col-sm-2 mb-3">@lang('MAIN.OPTION.SOCKET-LINK')</label>
                        <div class="col-sm-4 mb-3">
                            <input type="text" class="form-control" name="socketLink" id="socketLink" value="{{isset($options['socketLink']) ? $options['socketLink'] : 'localhost'}}">
                        </div>
                        <label for="" class="small text-muted font-italic col-form-label col-sm-2 mb-3">@lang('MAIN.OPTION.SOCKET-SCHEDULE-RUN')</label>
                        <div class="col-sm-4 mb-3">
                            <select name="socketScheduleRun" id="socketScheduleRun" class="form-control">
                                <option value="0" @if(isset($options['socketScheduleRun']) and $options['socketScheduleRun']==0) selected @endif>@lang('MAIN.ANSWER.NO')</option>
                                <option value="1" @if(isset($options['socketScheduleRun']) and $options['socketScheduleRun']==1) selected @endif>@lang('MAIN.ANSWER.YES')</option>
                            </select>
                        </div>
                        <label for="" class="small text-muted font-italic col-form-label col-sm-2 mb-3">@lang('MAIN.OPTION.SOCKET-PROCESS-BUSY')</label>
                        <div class="col-sm-4 mb-3">
                            <select name="socketProcessBusy" id="socketProcessBusy" class="form-control">
                                <option value="0" @if(isset($options['socketProcessBusy']) and $options['socketProcessBusy']==0) selected @endif>@lang('MAIN.ANSWER.NO')</option>
                                <option value="1" @if(isset($options['socketProcessBusy']) and $options['socketProcessBusy']==1) selected @endif>@lang('MAIN.ANSWER.YES')</option>
                            </select>
                        </div>
                        <label for="" class="small text-muted font-italic col-form-label col-sm-2 mb-3">@lang('MAIN.OPTION.SOCKET-SHOW-MESSAGES')</label>
                        <div class="col-sm-4 mb-3">
                            <select name="socketShowMessages" id="socketShowMessages" class="form-control">
                                <option value="0" @if(isset($options['socketShowMessages']) and $options['socketShowMessages']==0) selected @endif>@lang('MAIN.ANSWER.NO')</option>
                                <option value="1" @if(isset($options['socketShowMessages']) and $options['socketShowMessages']==1) selected @endif>@lang('MAIN.ANSWER.YES')</option>
                            </select>
                        </div>
                    </div>
                </div>
            <!-- /Sockets-->

            <div class="card-body">
                <button type="submit" class="btn btn-success">@lang('MAIN.BUTTON.SAVE')</button>
                <a href="{{route('transfer')}}" class="btn btn-primary">Перенос данных</a>
                {{--<a href="{{route('customseed')}}" class="btn btn-primary">Создание справочных данных</a>--}}
            </div>
        </form>
    </div>
@endsection


