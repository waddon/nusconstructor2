@extends('layouts.dashboard')

@section('content')

{{ Form::model($model, [ 'url' => $model->id ? route('components.update', $model->id) : route('components.store'), 'method' => $model->id ? 'PUT' : 'POST', 'class' => 'ajax-form' ]) }}

<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">
        <div class="card mb-3">
            <div class="card-header bg-transparent header-elements-inline">
                <h5 class="card-title">@lang('MAIN.TITLE.PARAMETERS')</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    {{ Form::label('name', __('MAIN.COLUMN.NAME'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                    <div class="col-md-8">
                        {{ Form::text('name', $model->name, array('class' => 'field form-control')) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('ctype_id', __('MAIN.COLUMN.TYPE'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                    <div class="col-md-8">
                        <select name="ctype_id" id="ctype_id" class="field form-control">
                            <option value="0">@lang('MAIN.LABEL.UNDEFINED')</option>
                            @foreach($ctypes as $ctype)
                                <option value="{{ $ctype->id }}" {{ $model->ctype_id == $ctype->id ? 'selected' : ''}} >{{ $ctype->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('marking', __('MAIN.COLUMN.MARKING'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                    <div class="col-md-8">
                        {{ Form::text('marking', $model->marking, array('class' => 'field form-control')) }}
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    {{ Form::label('languagetypes', __('MAIN.COLUMN.LANGUAGETYPES'), ['class'=>'text-muted font-italic mb-3']) }}
                    <div class="row">
                        @foreach ($languagetypes as $languagetype)
                            <div class="col-lg-6 col-md-12 col-sm-6 mb-3">
                                <div class="custom-control custom-checkbox">
                                    {{ Form::checkbox('languagetypes[]',  $languagetype->id , null, ['id' => 'l'.$languagetype->id, 'class' => 'custom-control-input']) }}
                                    {{ Form::label('l'.$languagetype->id, $languagetype->name, ['class' => 'custom-control-label']) }}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <hr>
                <div class="form-group">
                    {{ Form::label('cbranches', __('MAIN.COLUMN.BRANCHES'), ['class'=>'text-muted font-italic mb-3']) }}
                    <div class="row">
                        @foreach ($cbranches as $cbranch)
                            <div class="col-lg-3 col-md-12 col-sm-6 mb-3">
                                <div class="custom-control custom-checkbox">
                                    {{ Form::checkbox('cbranches[]',  $cbranch->id , null, ['id' => $cbranch->id, 'class' => 'custom-control-input']) }}
                                    {{ Form::label($cbranch->id, $cbranch->name, ['class' => 'custom-control-label']) }}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    {{ Form::label('grades', __('MAIN.COLUMN.GRADES'), ['class'=>'text-muted font-italic mb-3']) }}
                    <div class="row">
                        @foreach ($grades as $grade)
                            <div class="col-lg-2 col-md-12 col-sm-6 mb-3">
                                <div class="custom-control custom-checkbox">
                                    {{ Form::checkbox('grades[]',  $grade->id , null, ['id' => 'g'.$grade->id, 'class' => 'custom-control-input']) }}
                                    {{ Form::label('g'.$grade->id, $grade->name, ['class' => 'custom-control-label']) }}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <hr>
                @foreach($ctypes as $ctype)
                    {{ Form::label('restricted'.$ctype->id, $ctype->name , ['class'=>'text-muted font-italic mb-3']) }}
                    <div class="form-group">
                        <div class="row">
                            @foreach ($ctype->components as $component)
                                <div class="col-lg-3 col-md-12 col-sm-6 mb-3">
                                    <div class="custom-control custom-checkbox">
                                        {{ Form::checkbox('restricted[]',  $component->id , null, ['id' => 'c'.$component->id, 'class' => 'custom-control-input']) }}
                                        {{ Form::label('c'.$component->id, $component->name, ['class' => 'custom-control-label']) }}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endforeach

            </div>
        </div>

    </div>

    <!-- Right sidebar component -->
    <div class="sidebar-mobile-component w-100 w-md-auto order-1 order-md-2">
        <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
            <div class="sidebar-content">
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">@lang('MAIN.TITLE.NAVIGATION')</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="btn-group w-100">
                            <button type="submit" id="submit" class="btn btn-success w-50" title="@lang('MAIN.HINT.SAVE')"><i class="icon-floppy-disk"></i></button>
                            <a href="{{ route('components.index') }}" class="btn btn-danger w-50" title="@lang('MAIN.HINT.EXIT')"><i class="icon-esc"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /right sidebar component -->

</div>

{{ Form::close() }}
@endsection

