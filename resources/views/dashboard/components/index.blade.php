@extends('layouts.dashboard')

@section('content')
<div class="card mb-3">

        <div class="navbar navbar-expand-xl navbar-light bg-light border-top">
            <div class="text-center d-xl-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle collapsed" data-toggle="collapse" data-target="#navbar-second" aria-expanded="false">
                    <i class="icon-menu7 mr-2"></i>@lang('MAIN.TITLE.NAVIGATION')
                </button>
            </div>

            <div class="navbar-collapse collapse" id="navbar-second" style="">
                <ul class="nav navbar-nav">
                    <li class="nav-item">
                        <a href="#tab-active" class="navbar-nav-link active" data-toggle="tab">
                            <i class="icon-menu7 mr-2"></i> @lang('MAIN.TAB.ACTIVE')</a>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#tab-trash" class="navbar-nav-link" data-toggle="tab">
                            <i class="icon-bin mr-2"></i> @lang('MAIN.TAB.TRASH')</a>
                        </a>
                    </li>
                </ul>

                <ul class="navbar-nav ml-lg-auto">
                    <li class="nav-item" style="position: relative;">
                        <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <i class="icon-filter3 icon-2m mr-2"></i>@lang('MAIN.MENU.LANGUAGETYPES')
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            @foreach($languagetypes as $key => $languagetype)
                                <a href="#" class="dropdown-item filter-item {{!$key ? 'active' : ''}}" data-value="{{$key}}" data-type="languagetype">{{$languagetype}}</a>
                            @endforeach
                        </div>
                    </li>
                    <li class="nav-item" style="position: relative;">
                        <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <i class="icon-filter3 icon-2m mr-2"></i>@lang('MAIN.MENU.CTYPES')
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            @foreach($ctypes as $key => $ctype)
                                <a href="#" class="dropdown-item filter-item {{!$key ? 'active' : ''}}" data-value="{{$key}}" data-type="ctype">{{$ctype}}</a>
                            @endforeach
                        </div>
                    </li>
                    <li class="nav-item" style="position: relative;">
                        <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <i class="icon-filter3 icon-2m mr-2"></i>@lang('MAIN.MENU.CBRANCHES')
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            @foreach($cbranches as $key => $cbranch)
                                <a href="#" class="dropdown-item filter-item {{!$key ? 'active' : ''}}" data-value="{{$key}}" data-type="cbranch">{{$cbranch}}</a>
                            @endforeach
                        </div>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('components.create')}}" class="navbar-nav-link">
                            <i class="icon-file-plus mr-2"></i>{{--@lang('MAIN.HINT.CREATE')--}}</a>
                        </a>
                    </li>
                    <li class="nav-item">
                        <span class="navbar-nav-link datatableRefresh">
                            <i class="icon-loop3 mr-2"></i>{{--@lang('MAIN.HINT.REFRESH')--}}</a>
                        </span>
                    </li>
                </ul>
            </div>
        </div>

    <!-- Tabs -->
            <div class="tab-content mt-3">
                <!-- Active Table -->
                    <div class="tab-pane fade show active" id="tab-active">
                        <div class="table-responsive">
                            <table class="table table-bordered table-styled table-striped mb-3 w-100 datatable" id="datatableActive">
                                <thead class="bg-indigo">
                                    <tr>
                                        <th>#</th>
                                        <th>@lang('MAIN.COLUMN.NAME')</th>
                                        <th>@lang('MAIN.COLUMN.LANGUAGETYPES')</th>
                                        <th>@lang('MAIN.COLUMN.TYPE')</th>
                                        <th>@lang('MAIN.COLUMN.BRANCHES')</th>
                                        <th>@lang('MAIN.COLUMN.GRADES')</th>
                                        <th>@lang('MAIN.COLUMN.RESTRICTED')</th>
                                        <th>@lang('MAIN.COLUMN.ACTIONS')</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                <!-- /Active Table -->
                <!-- Trash Table -->
                    <div class="tab-pane fade" id="tab-trash">
                        <div class="table-responsive">
                            <table class="table table-bordered table-styled table-striped mb-3 w-100 datatable" id="datatableTrash">
                                <thead class="bg-indigo">
                                    <tr>
                                        <th>#</th>
                                        <th>@lang('MAIN.COLUMN.NAME')</th>
                                        <th>@lang('MAIN.COLUMN.TYPE')</th>
                                        <th>@lang('MAIN.COLUMN.BRANCHES')</th>
                                        <th>@lang('MAIN.COLUMN.GRADES')</th>
                                        <th>@lang('MAIN.COLUMN.RESTRICTED')</th>
                                        <th>@lang('MAIN.COLUMN.ACTIONS')</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                <!-- /Trash Table -->
            </div>
    <!-- /Tabs -->

</div>
@endsection

@section('scripts')
<script src="/js/datatables/datatables.min.js"></script>
<script>
    var language = {"url": "/js/datatables/languages/ukrainian.json"};
    var pageLength = {{ isset(auth()->user()->options->datatablePageLength) ? auth()->user()->options->datatablePageLength : 10 }};
    var filter = {'languagetype' : 0, 'ctype' : 0, 'cbranch' : 0};

    var columns = [
            {"data":"id"},
            {"data":"name"},
            {"data":"languagetypes"},
            {"data":"type"},
            {"data":"branches","searchable":false,"orderable":false},
            {"data":"grades","searchable":false,"orderable":false},
            {"data":"restricted","searchable":false,"orderable":false},
            {"data":"actions","searchable":false,"orderable":false}
        ];
    var columnDefs = [
            { "targets": 0, "className": "text-center", },
            { "targets": 3, "className": "text-center", },
            { "targets": 5, "className": "text-center", },
            { "targets": 7, "className": "text-center", },
        ];


    var tableActive = $("#datatableActive").DataTable({
        autoWidth : true,
        responsive: true,
        processing: true,
        serverSide: true,
        pageLength: pageLength,
        bSortCellsTop: true,
        order: [[ 0, "desc" ]],
        ajax: {
            url: "{{route('components.data')}}",
            dataType: "json",
            type: "POST",
            data: function ( d ) {
                d._token = "{{ csrf_token() }}";
                d.languagetype_id = filter['languagetype'];
                d.ctype_id = filter['ctype'];
                d.cbranch_id = filter['cbranch'];
                d.table = "active";
            }
        },
        columns:columns,
        columnDefs: columnDefs,
        language: language,
    });

    var tableActive = $("#datatableTrash").DataTable({
        autoWidth : true,
        responsive: true,
        processing: true,
        serverSide: true,
        pageLength: pageLength,
        bSortCellsTop: true,
        order: [[ 0, "desc" ]],
        ajax: {
            url: "{{route('components.data')}}",
            dataType: "json",
            type: "POST",
            data: {"_token":"{{ csrf_token() }}", "table":"trash"},
        },
        columns:columns,
        columnDefs: columnDefs,
        language: language,
    });

    $('.filter-item').on('click',function(e){
        e.preventDefault();
        filter[$(this).attr('data-type')] = $(this).attr('data-value');
        $('[data-type=' + $(this).attr('data-type') + ']').removeClass('active');
        $(this).addClass('active');
        $('.datatableRefresh').click();
    });

</script>
@endsection
