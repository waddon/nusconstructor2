@extends('layouts.dashboard')

@section('content')

{{ Form::model($model, [ 'url' => $model->id_cat ? route('cats.update', $model->id_cat) : route('cats.store'), 'method' => $model->id_cat ? 'PUT' : 'POST', 'class' => 'ajax-form' ]) }}

<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">
        <div class="card mb-3">
            <div class="card-header bg-transparent header-elements-inline">
                <h5 class="card-title">@lang('MAIN.TITLE.PARAMETERS')</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    {{ Form::label('name_cat', __('MAIN.COLUMN.NAME'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                    <div class="col-md-8">
                        {{ Form::text('name_cat', $model->name_cat, array('class' => 'field form-control')) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('slug_cat', __('MAIN.COLUMN.SLUG'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                    <div class="col-md-8">
                        {{ Form::text('slug_cat', $model->slug_cat, array('class' => 'field form-control')) }}
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Right sidebar component -->
    <div class="sidebar-mobile-component w-100 w-md-auto order-1 order-md-2">
        <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
            <div class="sidebar-content">
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">@lang('MAIN.TITLE.NAVIGATION')</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="btn-group w-100">
                            <button type="submit" id="submit" class="btn btn-success w-50" title="@lang('MAIN.HINT.SAVE')"><i class="icon-floppy-disk"></i></button>
                            <a href="{{ route('cats.index') }}" class="btn btn-danger w-50" title="@lang('MAIN.HINT.EXIT')"><i class="icon-esc"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /right sidebar component -->

</div>

{{ Form::close() }}
@endsection

