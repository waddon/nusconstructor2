@extends('layouts.dashboard')

@section('content')
<div class="card mb-3">

    <!-- Search panel -->
        <div class="search-panel">
            <div class="text-right">
                <div class="btn-group">
                    <a class="btn btn-primary" href="{{route('permissions.create')}}" title="@lang('Create')"><i class="icon-file-plus"></i></a>
                    <button type="button" class="btn btn-primary datatableRefresh" title="@lang('Refresh')"><i class="icon-sync"></i></button>
                </div>                
                
            </div>
        </div>
    <!-- /Search panel -->

    <!-- Tabs -->
            <ul class="nav nav-tabs nav-tabs-highlight">
                <li class="nav-item"><a href="#tab-active" class="nav-link active" data-toggle="tab"><i class="icon-menu7 mr-2"></i> @lang('Active')</a></li>
            </ul>

            <div class="tab-content">
                <!-- Active Table -->
                    <div class="tab-pane fade show active" id="tab-active">
                        <div class="table-responsive">
                            <table class="table table-bordered table-styled table-striped mb-3 w-100 datatable" id="datatableActive">
                                <thead class="bg-indigo">
                                    <tr>
                                        <th>#</th>
                                        <th>@lang('Name')</th>
                                        <th>@lang('Roles')</th>
                                        <th>@lang('Created_at')</th>
                                        <th>@lang('Actions')</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                <!-- /Active Table -->
            </div>
    <!-- /Tabs -->

</div>
@endsection

@section('scripts')
<script src="/js/datatables/datatables.min.js"></script>
<script>
    var language = {"url": "/js/datatables/languages/ukrainian.json"};
    var pageLength = {{ isset(auth()->user()->options->datatablePageLength) ? auth()->user()->options->datatablePageLength : 10 }};

    var columns = [
            {"data":"id"},
            {"data":"name"},
            {"data":"roles","searchable":false,"orderable":false},
            {"data":"created_at"},
            {"data":"actions","searchable":false,"orderable":false}
        ];
    var columnDefs = [
            { "targets": 0, "className": "text-center", },
            { "targets": 2, "className": "text-center", },
            { "targets": 3, "className": "text-center", },
            { "targets": 4, "className": "text-center", },
        ];


    var tableActive = $("#datatableActive").DataTable({
        autoWidth : true,
        responsive: true,
        processing: true,
        serverSide: true,
        pageLength: pageLength,
        bSortCellsTop: true,
        order: [[ 0, "desc" ]],
        ajax: {
            url: "{{route('permissions.data')}}",
            dataType: "json",
            type: "POST",
            data: {"_token":"{{ csrf_token() }}", "table":"active"},
        },
        columns:columns,
        columnDefs: columnDefs,
        language: language,
    });
</script>
@endsection
