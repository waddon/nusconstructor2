@extends('layouts.dashboard')

@section('content')
<div class="card mb-3">
    <div class="card-header">
        <h2>{{ $title ?? '' }}</h2>
    </div>

    <!-- Search panel -->
        <div class="search-panel">
            <div class="text-right">
                <div class="btn-group">
                    <div class="btn-group justify-content-center">
                        <a href="#" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">@lang('MAIN.BUTTON.CIKL')</a>
                        <div class="dropdown-menu">
                                @foreach($cycles as $key => $cycle)
                                    <a href="#" class="dropdown-item element-cycle {{!$key ? 'active' : ''}}" data-key="{{$key}}">{{$cycle}}</a>
                                @endforeach
                        </div>
                    </div>
                    <div class="btn-group justify-content-center">
                        <a href="#" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">@lang('MAIN.BUTTON.GALUZ')</a>
                        <div class="dropdown-menu">
                                @foreach($branches as $key => $branch)
                                    <a href="#" class="dropdown-item element-branch {{!$key ? 'active' : ''}}" data-key="{{$key}}">{{$branch}}</a>
                                @endforeach
                        </div>
                    </div>
                    <button type="button" class="btn btn-primary datatableRefresh" title="@lang('Refresh')"><i class="icon-sync"></i></button>
                </div>                
                
            </div>
        </div>
    <!-- /Search panel -->

    <!-- Tabs -->
        <ul class="nav nav-tabs nav-tabs-highlight">
            @if(in_array($type, ['model', 'standart']))
                <li class="nav-item"><a href="#tab-approving" class="nav-link active" data-toggle="tab"><i class="icon-cloud-upload2 mr-2"></i> @lang('MAIN.TAB.APPROVING')</a></li>
            @endif
            <li class="nav-item"><a href="#tab-approved" class="nav-link {{!in_array($type, ['model', 'standart']) ? 'active' : ''}}" data-toggle="tab"><i class="icon-cloud-check2 mr-2"></i> @lang('MAIN.TAB.PUBLISHED')</a></li>
            {{--<li class="nav-item"><a href="#tab-active" class="nav-link" data-toggle="tab"><i class="icon-hammer-wrench mr-2"></i> @lang('MAIN.TAB.DEVELOPING')</a></li>--}}
            @can('admin')
                <li class="nav-item"><a href="#tab-trash" class="nav-link" data-toggle="tab"><i class="icon-trash mr-2"></i> @lang('MAIN.TAB.TRASH')</a></li>
            @endcan
        </ul>

        <div class="tab-content">
            <!-- Active Table -->
                {{--<div class="tab-pane fade" id="tab-active">
                    <div class="table-responsive">
                        <table class="table table-bordered table-styled table-striped mb-3 w-100 datatable" data-tab="active">
                            <thead class="bg-indigo">
                                <tr>
                                    <th>@lang('MAIN.COLUMN.ID')</th>
                                    <th>@lang('MAIN.COLUMN.NAME')</th>
                                    <th>@lang('MAIN.COLUMN.CYCLE')</th>
                                    <th>@lang('MAIN.COLUMN.BRANCHES')</th>
                                    <th>@lang('MAIN.COLUMN.USERS')</th>
                                    <th>@lang('MAIN.COLUMN.TEAMS')</th>
                                    <th>@lang('MAIN.COLUMN.INFO')</th>
                                    <th>@lang('MAIN.COLUMN.ACTIONS')</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>--}}
            <!-- /Active Table -->
            @if(in_array($type, ['model', 'standart']))
            <!-- Approving Table -->
                <div class="tab-pane fade show active" id="tab-approving">
                    <div class="table-responsive">
                        <table class="table table-bordered table-styled table-striped mb-3 w-100 datatable" data-tab="approving">
                            <thead class="bg-indigo">
                                <tr>
                                    <th>@lang('MAIN.COLUMN.ID')</th>
                                    <th>@lang('MAIN.COLUMN.NAME')</th>
                                    <th>@lang('MAIN.COLUMN.CYCLE')</th>
                                    <th>@lang('MAIN.COLUMN.BRANCHES')</th>
                                    <th>@lang('MAIN.COLUMN.USERS')</th>
                                    <th>@lang('MAIN.COLUMN.TEAMS')</th>
                                    <th>@lang('MAIN.COLUMN.INFO')</th>
                                    <th>@lang('MAIN.COLUMN.ACTIONS')</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            <!-- /Approving Table -->
            @endif
            <!-- Approved Table -->
                <div class="tab-pane fade {{!in_array($type, ['model', 'standart']) ? 'show active' : ''}}" id="tab-approved">
                    <div class="table-responsive">
                        <table class="table table-bordered table-styled table-striped mb-3 w-100 datatable" data-tab="published">
                            <thead class="bg-indigo">
                                <tr>
                                    <th>@lang('MAIN.COLUMN.ID')</th>
                                    <th>@lang('MAIN.COLUMN.NAME')</th>
                                    <th>@lang('MAIN.COLUMN.CYCLE')</th>
                                    <th>@lang('MAIN.COLUMN.BRANCHES')</th>
                                    <th>@lang('MAIN.COLUMN.USERS')</th>
                                    <th>@lang('MAIN.COLUMN.TEAMS')</th>
                                    <th>@lang('MAIN.COLUMN.INFO')</th>
                                    <th>@lang('MAIN.COLUMN.ACTIONS')</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            <!-- /Approved Table -->
            @can('admin')
            <!-- Trash Table -->
                <div class="tab-pane fade show" id="tab-trash">
                    <div class="table-responsive">
                        <table class="table table-bordered table-styled table-striped mb-3 w-100 datatable" data-tab="deleted">
                            <thead class="bg-indigo">
                                <tr>
                                    <th>@lang('MAIN.COLUMN.ID')</th>
                                    <th>@lang('MAIN.COLUMN.NAME')</th>
                                    <th>@lang('MAIN.COLUMN.CIKL')</th>
                                    <th>@lang('MAIN.COLUMN.GALUZES')</th>
                                    <th>@lang('MAIN.COLUMN.USERS')</th>
                                    <th>@lang('MAIN.COLUMN.TEAMS')</th>
                                    <th>@lang('MAIN.COLUMN.INFO')</th>
                                    <th>@lang('MAIN.COLUMN.ACTIONS')</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            <!-- /Trash Table -->
            @endcan
        </div>
    <!-- /Tabs -->

</div>
@endsection

@section('scripts')
<script src="/js/datatables/datatables.min.js"></script>
<script>
    var language = {"url": "/js/datatables/languages/ukrainian.json"};
    var pageLength = {{ isset(auth()->user()->options->datatablePageLength) ? auth()->user()->options->datatablePageLength : 10 }};
    var current_cycle = 0;
    var current_branch = 0;
    var columns = [
            {"data":"id"},
            {"data":"name"},
            {"data":"cikl","searchable":false,"orderable":false},
            {"data":"galuzes","searchable":false,"orderable":false},
            {"data":"users","searchable":false,"orderable":false},
            {"data":"teams","searchable":false,"orderable":false},
            {"data":"info","searchable":false,"orderable":false},
            {"data":"actions","searchable":false,"orderable":false}
        ];
    var columnDefs = [
            { "targets": 0, "className": "text-center", },
            { "targets": 2, "className": "text-center", },
            { "targets": 3, "className": "text-center", },
            { "targets": 4, "className": "text-center", },
            { "targets": 5, "className": "text-center", },
            { "targets": 7, "className": "text-center", },
        ];

    $(".datatable").each(function( index ) {
        let $table = $(this);
        $(this).DataTable({
            autoWidth : true,
            responsive: true,
            processing: true,
            serverSide: true,
            pageLength: pageLength,
            bSortCellsTop: true,
            order: [[ 0, "desc" ]],
            ajax: {
                url: "{{route('newprograms.data')}}",
                dataType: "json",
                type: "POST",
                data: function ( d ) {
                    d._token = "{{ csrf_token() }}";
                    d.cycle_id = current_cycle;
                    d.branch_id = current_branch;
                    d.tab = $table.attr('data-tab');
                    d.type = '{{ $type }}'
                }
            },
            columns:columns,
            columnDefs: columnDefs,
            language: language,
        });
    });

    $('div.search-panel').on('click','.element-cycle',function(e){
        e.preventDefault();
        current_cycle = $(this).attr('data-key');
        $('.element-cycle').removeClass('active');
        $(this).addClass('active');
        $('.datatableRefresh').click();
    });

    $('div.search-panel').on('click','.element-branch',function(e){
        e.preventDefault();
        current_branch = $(this).attr('data-key');
        $('.element-branch').removeClass('active');
        $(this).addClass('active');
        $('.datatableRefresh').click();
    });
</script>
@endsection

@section('modals')

<div id="modal-change-status" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h6 class="modal-title">@lang('MAIN.MODAL.SHARE.TITLE')</h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="modal-message"></div>
                    <form id="modal-change-status-form" action="{{ route('unewprograms.store') }}" method="POST">
                        @csrf
                        <input type="hidden" name="newstatus">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">@lang('MAIN.BUTTON.CANCEL')</button>
                    <button type="button" class="btn bg-primary" id="modal-change-status-submit" data-dismiss="modal">@lang('MAIN.BUTTON.SHARE')</button>
                </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){

        $(document).on('click','.btn-change-status',function () {
            $('#modal-change-status').find('.modal-header').removeClass('bg-primary bg-warning bg-success').addClass($(this).attr('data-color'));
            $('#modal-change-status-submit').removeClass('bg-primary bg-warning bg-success').addClass($(this).attr('data-color'));
            $('#modal-change-status').find('.modal-title').empty().text($(this).attr('data-title'));
            $('#modal-change-status-submit').empty().text($(this).attr('data-button'));
            $('#modal-change-status').find('.modal-message').empty().text($(this).attr('data-text'));
            $('#modal-change-status-form').attr('action', $(this).attr('href'));
            $('#modal-change-status').find('[name=newstatus]').val($(this).attr('data-status'));
        })

        $('#modal-change-status-submit').click(function(e){
            let form = $('#modal-change-status-form');
            e.preventDefault();
            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                dataType : "json",
                data: form.serialize(),
            }).done((data, status, xhr) => {
                console.log(data.message);
                $('.datatableRefresh').click();
            }).fail((xhr) => {
                let data = xhr.responseJSON;
                swal({
                    title: data.message,
                    type: 'error',
                    confirmButtonClass: 'btn btn-primary',
                })
            });
        });

});
</script>
@endsection