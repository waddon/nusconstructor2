@extends('layouts.app')

@section('content')
    <div class="row">

        <div class="col-xl-8">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Формула НУШ</h5>
                    </div>
                    <div class="card-body">
                        <p>Наскрізне застосування інформаційно-комунікаційних технологій в освітньому процесі та управлінні закладами освіти і системою освіти має стати інструментом забезпечення успіху нової української школи. Запровадження ІКТ в освітній галузі має перейти від одноразових проектів у системний процес, який охоплює всі види діяльності. ІКТ суттєво розширять можливості педагога, оптимізують управлінські процеси, таким чином формуючи в учня важливі для нашого сторіччя технологічні компетентності</p>
                    </div>
                    <div class="card-footer d-flex justify-content-end align-items-center">
                        <a href="https://nus.org.ua/about/" class="btn btn-primary">НУШ коротко</a>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Конструктор програм відеопосібник користувача</h5>
                    </div>
                    <div class="card-body">
                        <div class="embed-responsive embed-responsive-16by9 mb-3">
                            <embed class="embed-responsive-item" src="https://www.youtube.com/embed/Z2uQYOasJgs?rel=0"></embed>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title"><a style="color:#000;" href="https://activemedia.ua/projects/zagalnoievropejskij-hab-dlya-pidtrimki-ukrainskih-vchiteliv-ta-uchniv" target="_blank">Загальноєвропейський хаб для підтримки українських вчителів та учнів</a></h5>
                    </div>
                    <div class="card-body">
                        <img class="card-img" src="/img/projects.jpg">
                    </div>
                </div>

                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Конструктор навчальних планів і програм</h5>
                    </div>
                    <div class="card-body">
                        <div class="embed-responsive embed-responsive-16by9 mb-3">
                            <embed class="embed-responsive-item" src="https://www.youtube.com/embed/Q37yq0o6VW0?rel=0"></embed>
                        </div>
                    </div>
                </div>

        </div>

        <div class="col-xl-4">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">@lang('MAIN.TITLE.PROJECT-NEWS')</h5>
                </div>

                @foreach($news as $one_news)
                    <div class="card-body">
                        <p class="font-weight-bold"><a href="{{route('postGetContent', $one_news->id_post)}}" class="list-icons-item btn-ajax-data" data-toggle="modal" data-target="#modal-ajax-data" data-title="{{$one_news->name_post}}" data-color="bg-success" data-method="POST">{{ $one_news->name_post }}</a></p>
                        <span class="text-muted">{{ $one_news->created_at->format('d.m.Y') }}</span>
                    </div>
                @endforeach

                <div class="card-footer d-flex justify-content-center align-items-center">
                    <a href="{{route('news')}}" class="btn btn-primary">@lang('MAIN.BUTTON.ALL-NEWS')</a>
                </div>
            </div>
        </div>
    </div>
@endsection
