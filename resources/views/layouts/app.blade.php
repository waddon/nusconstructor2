<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}{{isset($model->name) ? ' - '.$model->name : ''}}</title>
    <link rel="icon" type="image/png" href="/img/favicon.png" />

    <link href="{{mix('css/app.css')}}" rel="stylesheet" type="text/css">
	@section('styles')
	@show
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-WR5E5KXGXM"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-WR5E5KXGXM');
    </script>    
    @livewireStyles
</head>

<body>

	@include('layouts-parts.app-main-navbar')

	<!-- Page content -->
	<div class="page-content">

		<!-- Main sidebar -->
		<div class="sidebar sidebar-light sidebar-main sidebar-expand-md">

			<!-- Sidebar mobile toggler -->
			<div class="sidebar-mobile-toggler text-center">
				<a href="#" class="sidebar-mobile-main-toggle">
					<i class="icon-arrow-left8"></i>
				</a>
				<span class="font-weight-semibold">Navigation</span>
				<a href="#" class="sidebar-mobile-expand">
					<i class="icon-screen-full"></i>
					<i class="icon-screen-normal"></i>
				</a>
			</div>
			<!-- /sidebar mobile toggler -->


			<div class="sidebar-content">
				@auth
					@include('layouts-parts.app-user-menu')
				@else
					@include('layouts-parts.app-login-form')
				@endauth
				@include('layouts-parts.app-main-navigation')
			</div>
			
		</div>
		<!-- /main sidebar -->


		<div class="content-wrapper">
			@include('layouts-parts.app-page-header')
			<div class="content">
				@include('layouts-parts.dashboard-alerts')
				@section('content')
				@show
			</div>
			@include('layouts-parts.app-page-footer')
		</div>

	</div>
	<!-- /page content -->

    <script src="{{mix('js/core.js')}}" ></script>
  @livewireScripts
	@section('templates')
	@show
	@section('scripts')
	@show
	@section('modals')
	@show
	@include('layouts-parts.app-modals')
	@include('layouts-parts.common-modals')

</body>
</html>
