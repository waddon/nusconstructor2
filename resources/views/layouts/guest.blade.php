<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}{{isset($model->name) ? ' - '.$model->name : ''}}</title>
    <link rel="icon" type="image/png" href="/global_assets/images/logo_notext.png" />

    <link href="{{mix('css/app.css')}}" rel="stylesheet" type="text/css">
	@section('styles')
	@show
</head>

<body>

	@include('layouts-parts.app-main-navbar')

	<!-- Page content -->
	<div class="page-content">

		<div class="content-wrapper">
			@include('layouts-parts.app-page-header')
			<div class="content">
				@include('layouts-parts.dashboard-alerts')
				@section('content')
				@show
			</div>
			@include('layouts-parts.app-page-footer')
		</div>

	</div>
	<!-- /page content -->

    <script src="{{mix('js/core.js')}}" ></script>
	@section('scripts')
	@show
	{{--@include('layouts-parts.guest-modals')--}}
	@include('layouts-parts.common-modals')

</body>
</html>
