<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}{{isset($model->name) ? ' - '.$model->name : ''}}</title>
    <link rel="icon" type="image/png" href="/img/favicon.png" />

    <link href="{{mix('css/dashboard.css')}}" rel="stylesheet" type="text/css">
    <script src="{{mix('js/core.js')}}" ></script>
	@section('styles')
	@show
</head>

<body>

	@include('layouts-parts.dashboard-main-navbar')

	<!-- Page content -->
	<div class="page-content">

		<!-- Main sidebar -->
		<div class="sidebar sidebar-light sidebar-main sidebar-expand-md">

			<!-- Sidebar mobile toggler -->
			<div class="sidebar-mobile-toggler text-center">
				<a href="#" class="sidebar-mobile-main-toggle">
					<i class="icon-arrow-left8"></i>
				</a>
				<span class="font-weight-semibold">Navigation</span>
				<a href="#" class="sidebar-mobile-expand">
					<i class="icon-screen-full"></i>
					<i class="icon-screen-normal"></i>
				</a>
			</div>
			<!-- /sidebar mobile toggler -->


			<div class="sidebar-content">
				@include('layouts-parts.dashboard-user-menu')
				@include('layouts-parts.dashboard-main-navigation')
			</div>
			
		</div>
		<!-- /main sidebar -->


		<div class="content-wrapper">
			@include('layouts-parts.dashboard-page-header')
			<div class="content">
				@include('layouts-parts.dashboard-alerts')
				@section('content')
				@show
			</div>
			@include('layouts-parts.dashboard-page-footer')
		</div>

	</div>
	<!-- /page content -->

	@section('modals')
	@show

	@section('scripts')
	@show

	@include('layouts-parts.dashboard-modals')
	@include('layouts-parts.common-modals')

</body>
</html>
