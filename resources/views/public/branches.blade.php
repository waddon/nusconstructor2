@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header header-elements-inline alpha-1">
            <h1 class="card-title">@LANG('MAIN.TITLE.BRANCHES')</h1>
        </div>
        <div class="card-body">
            @foreach($models as $model)
                <div class="card">
                    <div class="card-header" style="background: {{$model->color}};"">
                        <h6 class="card-title">{{$model->name}}</h6>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
