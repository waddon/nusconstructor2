@extends('layouts.app')

@section('content')
<style type="text/css">
    .element-container{
        border-left: 1px solid #ddd;
        border-bottom: 1px solid #ddd;
    }
    .buttons{
        padding: 15px 15px 15px 30px;
    }
    .element .card-header .header-elements i{
        font-size: 24px;
    }
    .element.line>.element-container{ background: #e4e4e4; }
    .element.problem>.element-container{ background: #eaeaea; }
    .element.opis>.element-container {background: #f1f1f1;}
    .modal-scroll-body{
        height: calc(100vh - 350px);
        overflow-y: scroll;
        border-top: 1px solid #ddd;
        border-bottom: 1px solid #ddd;
    }
    .sticked thead th {
        position: sticky;
        top: 0;
        z-index: 1;
    }
    .sticked thead th:first-child {
      left: 0;
      z-index: 1;
    }
    div.medium-editor-element{
        margin-top: .75rem;
        border-color: #ddd;
        border-style: solid;
        border-width: 0 0 1px 0;
        transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;    
    }
    div.medium-editor-element:focus-visible {
        outline: none;
        border-color: #009688;
        box-shadow: 0 0 0 0 transparent, 0 1px 0 #009688;
    }
    .medium-editor-toolbar li button {
        height: auto!important;
        min-width: auto!important;
    }
    .medium-editor-toolbar ul {
        box-shadow: 5px 5px 10px 0px rgb(0 0 0 / 50%);
    }
</style>

{{ Form::model($model, [ 'url' => isset($team) ? route('tnewprograms.update', [ $team->id, $model->id ]) : route('unewprograms.update',$model->id), 'method' => 'PUT', 'id' => 'program-form', 'name' => 'fileinfo' ]) }}

<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">
        <div class="card mb-3 program-loader">
            <!-- Search panel -->
                <h2 class="p-3 pb-0">Створення програми (@lang('MAIN.TITLE.PROGRAM.' . mb_strtoupper($model->type)))</h2>
                <div class="search-panel">
                    <div class="text-right">
                        <div class="btn-group">
                            <a href="{{ $model->previousUrl }}" class="btn btn-danger" title="@lang('MAIN.HINT.EXIT')"><i class="icon-esc"></i></a>
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <h2 class="mb-3">@lang('MAIN.TITLE.CONNECTING')</h2>
                    {{--<img class="mb-5" src="/img/loader2.gif" style="width: 50%;height: 50%;">--}}
                    <div id="fountainG" class="mb-5">
                        <div id="fountainG_1" class="fountainG"></div>
                        <div id="fountainG_2" class="fountainG"></div>
                        <div id="fountainG_3" class="fountainG"></div>
                        <div id="fountainG_4" class="fountainG"></div>
                        <div id="fountainG_5" class="fountainG"></div>
                        <div id="fountainG_6" class="fountainG"></div>
                        <div id="fountainG_7" class="fountainG"></div>
                        <div id="fountainG_8" class="fountainG"></div>
                    </div>
                </div>
        </div>

        <div class="card mb-3 program-constructor" style="display: none;">
                <!-- Search panel -->
                <h2 class="p-3 pb-0">Створення програми (@lang('MAIN.TITLE.PROGRAM.' . mb_strtoupper($model->type)))</h2>
                    <div class="search-panel">
                        <div class="text-right">
                            <div class="btn-group">

                                <button type="button" class="btn btn-primary open-all" title="@lang('MAIN.HINT.TOGGLE-OPEN')"><i class="icon-menu-open"></i></button>
                                <button type="button" class="btn btn-primary close-all" title="@lang('MAIN.HINT.TOGGLE-CLOSE')"><i class="icon-menu-close"></i></button>
                                <button type="button" class="btn btn-success save-program" title="@lang('MAIN.HINT.SAVE')"><i class="icon-floppy-disk"></i></button>
                                <a href="{{ $model->previousUrl }}" class="btn btn-danger" title="@lang('MAIN.HINT.EXIT')"><i class="icon-esc"></i></a>
                            </div>
                            
                        </div>
                    </div>
                <!-- /Search panel -->

                <!-- Tabs -->
                        <ul class="nav nav-tabs nav-tabs-highlight">
                            <li class="nav-item"><a href="#tab-constructor" class="nav-link active" data-toggle="tab"><i class="icon-cube3 mr-2"></i> @lang('MAIN.TAB.CONSTRUCTOR')</a></li>
                            <li class="nav-item"><a href="#tab-supervisor" class="nav-link" data-toggle="tab"><i class="icon-pie-chart3 mr-2"></i> @lang('MAIN.TAB.SUPERVISION')</a></li>
                            <li class="nav-item"><a href="#tab-additional" class="nav-link" data-toggle="tab"><i class="icon-insert-template mr-2"></i> @lang('MAIN.TAB.ADDITIONAL')</a></li>
                        </ul>

                        <div class="tab-content">
                            <!-- Constructor Table -->
                                <div class="tab-pane fade show active" id="tab-constructor">

                                    <div class="card-body">
                                        <div class="form-group row">
                                            {{ Form::label('name', __('MAIN.COLUMN.NAME'), ['class'=>'small text-muted font-italic col-lg-2 col-form-label']) }}
                                            <div class="col-lg-6">
                                                {{ Form::text('name', $model->name, array('class' => 'field form-control')) }}
                                            </div>
                                            {{ Form::label('hours', __('MAIN.COLUMN.HOURS'), ['class'=>'small text-muted font-italic col-lg-2 col-form-label']) }}
                                            <div class="col-lg-2">
                                                {{ Form::number('hours', isset($model->meta->hours) ? $model->meta->hours : 0, array('class' => 'form-control', 'step' => '0.5', 'readonly' => 'readonly')) }}
                                                {{-- Form::text('hours', isset($model->meta->hours) ? $model->meta->hours : 0, array('class' => 'field form-control', 'onkeyup' => 'this.value = this.value.replace(/[^\d]/g,"");')) --}}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            {{ Form::label('branches', __('MAIN.COLUMN.BRANCHES'), ['class'=>'small text-muted font-italic col-lg-2 col-form-label']) }}
                                            <div class="col-lg-6">{{$model->branches->implode('name', ', ')}}</div>
                                            {{ Form::label('cycle', __('MAIN.COLUMN.CYCLE'), ['class'=>'small text-muted font-italic col-lg-2 col-form-label']) }}
                                            <div class="col-lg-2">{{$model->cycle->name}}</div>
                                        </div>
                                        <div class="form-group row">
                                            {{ Form::label('filename', __('MAIN.COLUMN.EXPLANATORY-NOTE'), ['class'=>'small text-muted font-italic col-lg-2 col-form-label']) }}
                                            <div class=" col-lg-10">
                                                <div class="custom-file">
                                                    <input type="file" name="explanatory" class="field custom-file-input" id="explanatory">
                                                    <label class="custom-file-label" for="explanatory">{{ $model->meta->explanatory->filename ?? __('MAIN.LABEL.UNDEFINED') }}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="display: none;">
                                        <textarea class="editable" cols="30" rows="10"></textarea>
                                    </div>
                                    <div id="program" class="card-body program p-3" data-children="line">
                                        @if (is_object($model->content))
                                            @foreach ($model->content as $lineKey => $line)
                                                @include('public.newprograms.elements.line', [
                                                    'type' => $model->type,
                                                    'parentId' => $model->parent_id ?? 0,
                                                    'line' => $line,
                                                    'lineKey' => $lineKey,
                                                ])
                                            @endforeach
                                        @endif
                                        {{-- {!!$model->getTree()!!} --}}
                                    </div>
                                    @if(in_array($model->type,['model','standart']) || !($condition = \App\Models\Option::getOption('programTypeRestrictions')))
                                    <div class="p-3">
                                        <button type="button" class="btn btn-light btn-add-children"><i class="icon-googleplus5 icon-2x mr-2"></i>@lang(
                                        'MAIN.BUTTON.ADD-LINE')</button>
                                    </div>
                                    @endif
                                </div>
                            <!-- /Constructor Table -->
                            <!-- Supervisor Table -->
                                <div class="tab-pane fade" id="tab-supervisor">
                                    <h4 class="text-center">@lang('MAIN.TITLE.PROGRAM-CHART')</h4>
                                    <div class="p-3">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div style="width: 100%;">
                                                    <canvas id="canvas-doughnut" width="300" height="250"></canvas>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div style="width: 100%;">
                                                    <canvas id="canvas-bar" width="300" height="250"></canvas>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                @foreach ($model->branches as $branch)
                                                    <h5 class="count-kors branch{{$branch->id}}" data-count-kors="{{$branch->count_kors or '0'}}" style="color: {{$branch->color}};"><i class="icon-square mr-3"></i> {{$branch->name}}</h5>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-3" style="border-top: 1px solid #dddddd;">
                                        <div class="row search-panel mb-3">
                                            <label for="" class="small text-muted font-italic col-form-label col-sm-2">@lang('MAIN.COLUMN.BRANCH')</label>
                                            <div class="col-sm-4">
                                                <select class="form-control" id="selectBranch2" onchange="selectBranch.value=this.value;">
                                                    <option value="0">@lang('MAIN.PLACEHOLDER.ALL-BRANCHES')</option>
                                                    @foreach($model->branches as $key => $branch)
                                                        <option value="{{$branch->id}}">{{$branch->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <label for="" class="small text-muted font-italic col-form-label col-sm-2">@lang('MAIN.COLUMN.USING')</label>
                                            <div class="col-sm-4">
                                                <select class="form-control" id="selectUsing2" onchange="selectUsing.value=this.value;">
                                                    <option value="0">@lang('MAIN.PLACEHOLDER.ALL')</option>
                                                    <option value="1">@lang('MAIN.PLACEHOLDER.USING')</option>
                                                    <option value="2">@lang('MAIN.PLACEHOLDER.NOT-USING')</option>
                                                </select>
                                            </div>
                                        </div>
                                        <table class="change-kors table table-striped">
                                            <thead>
                                                <tr>
                                                    <th class="bg-dark text-center pl-4 pr-4">@lang('MAIN.COLUMN.MARKING')</th>
                                                    <th class="bg-dark text-center">@lang('MAIN.COLUMN.NAME')</th>
                                                    <th class="bg-dark text-center">@lang('MAIN.COLUMN.COUNT')</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($model->branches as $branch)
                                                    @foreach($branch->expects()->where('cycle_id','=',$model->cycle_id)->get() as $expect)
                                                        @foreach($expect->specifics as $specific)
                                                            <tr class="kor" data-kod="{{$specific->marking}}" data-frequency="0" data-branch="{{$branch->id}}" data-owner="1">
                                                                <td class="kor-kod">{{$specific->marking}}</td>
                                                                <td class="kor-name">{{$specific->name}}</td>
                                                                <td class="text-center kor-frequency" >0</td>
                                                            </tr>
                                                        @endforeach
                                                    @endforeach
                                                @endforeach
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            <!-- /Supervisor Table -->
                            <!-- Additional -->
                                <div class="tab-pane fade" id="tab-additional">

                                    <ul class="nav nav-tabs nav-tabs-highlight">
                                        <li class="nav-item"><a href="#tab-title" class="nav-link active" data-toggle="tab">@lang('MAIN.COLUMN.TITLE')</a></li>
                                        <li class="nav-item"><a href="#tab-description" class="nav-link" data-toggle="tab">@lang('MAIN.COLUMN.DESCRIPTION')</a></li>
                                        <li class="nav-item"><a href="#tab-provision" class="nav-link" data-toggle="tab">@lang('MAIN.COLUMN.PROVISIONS')</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane p-3 fade show active" id="tab-title">
                                            <div class="form-group row">
                                                <div class="col-lg-3 py-2">
                                                    <div class="field custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="isTitle" name="isTitle" {{ (isset($model->meta->isTitle) && $model->meta->isTitle) ? 'checked' : ''}}>
                                                        <label for="isTitle" class="custom-control-label">@lang('MAIN.LABEL.SHOW')</label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-9">
                                                    {{ Form::textarea('title', isset($model->meta->title) ? $model->meta->title : null, array('class' => 'field editable form-control mt-0')) }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane p-3 fade" id="tab-description">
                                            <div class="form-group row">
                                                <div class="col-lg-3 py-2">
                                                    <div class="field custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="isDescription" name="isDescription" {{ (isset($model->meta->isDescription) && $model->meta->isDescription) ? 'checked' : ''}}>
                                                        <label for="isDescription" class="custom-control-label">@lang('MAIN.LABEL.SHOW')</label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-9">
                                                    {{ Form::textarea('description', isset($model->meta->description) ? $model->meta->description : null, array('class' => 'field editable form-control mt-0')) }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane p-3 fade" id="tab-provision">
                                            <div class="form-group row">
                                                <div class="col-lg-3 py-2">
                                                    <div class="field custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="isProvisions" name="isProvisions" {{ (isset($model->meta->isProvisions) && $model->meta->isProvisions) ? 'checked' : ''}}>
                                                        <label for="isProvisions" class="custom-control-label">@lang('MAIN.LABEL.SHOW')</label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-9">
                                                    {{ Form::textarea('provisions', isset($model->meta->provisions) ? $model->meta->provisions : null, array('class' => 'field editable form-control')) }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <!-- /Additional -->
                        </div>
                <!-- /Tabs -->

        </div>

    </div>

</div>


{{ Form::close() }}
@endsection

@section('scripts')
<script src="//cdn.jsdelivr.net/npm/medium-editor@latest/dist/js/medium-editor.min.js"></script>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/medium-editor@latest/dist/css/medium-editor.min.css" type="text/css" media="screen" charset="utf-8">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/medium-editor/5.23.3/css/themes/bootstrap.css" integrity="sha512-hjKD/+g/2H0XQkec8ud4NcGfPG+pTVea7eXtSncjm7pLOkW0MOPHbLUv9ibOlhCIx2DpYg9OYBVlz7NTR2/zhA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<!--script src="/js/autobahn-old-master/autobahn.min.js" type="text/javascript"></script-->
<script src="/js/chart/Chart.min.js" type="text/javascript"></script>
<script src="{{ asset('/js/jquery_ui/interactions.min.js') }}"></script>
<script type="text/javascript">
    var buttons = [
            { name: 'bold', contentDefault: '<i class="icon-bold2"></i>' },
            { name: 'italic', contentDefault: '<i class="icon-italic2"></i>' },
            { name: 'underline', contentDefault: '<i class="icon-underline2"></i>' },
            { name: 'orderedlist', contentDefault: '<i class="icon-list-numbered"></i>' },
            { name: 'unorderedlist', contentDefault: '<i class="icon-list2"></i>' },
            { name: 'indent', contentDefault: '<i class="icon-indent-increase2"></i>' },
            { name: 'outdent', contentDefault: '<i class="icon-indent-decrease2"></i>' },
            { name: 'justifyLeft', contentDefault: '<i class="icon-paragraph-left3"></i>' },
            { name: 'justifyCenter', contentDefault: '<i class="icon-paragraph-center3"></i>' },
            { name: 'justifyRight', contentDefault: '<i class="icon-paragraph-right3"></i>' },
            { name: 'justifyFull', contentDefault: '<i class="icon-paragraph-justify3"></i>' },
            { name: 'removeFormat', contentDefault: '<i class="icon-eraser2"></i>' },
            { name: 'h2', contentDefault: 'H2' },
            { name: 'h3', contentDefault: 'H3' }
        ];

    var editor = new MediumEditor('.editable', {
                toolbar: {
                    buttons: buttons
                },
                placeholder: {
                    text: "",
                    hideOnClick: true
                }
            });

    var currentElement;
    sortableRun();

    @if( \App\Models\Option::getOption('programAutosave') )
        var mytime = {{ \App\Models\Option::getOption('programAutosaveInterval',0) }};
        var make_save = true;
        var timerId = setInterval(function() {
            mytime -= 1;
            if (mytime <1 && make_save){
                make_save = false;
                $('.save-program').click();
            }
        }, 1000);
    @endif

    const channel = 'program-{{$model->id}}'
    const user = Date.now()
    let socket
    function websocketConnect() {
        socket = new WebSocket("{{ env('FORCE_HTTPS',false) ? 'wss' : 'ws' }}://{{ \App\Models\Option::getOption('socketLink','localhost') }}:{{ env('FORCE_HTTPS',false) ? '443' : '8080' }}");

        socket.onopen = function(e) {
            console.log(`[open] Connection established`);
            socket.send(`{"commandType":"connect", "user":"${user}", "channel":"${channel}"}`)
            $(".program-constructor").show()
            $(".program-loader").hide()
        };

        socket.onmessage = function(event) {
            // console.log(`[message] Данные получены с сервера: ${event.data}`);
            let json = JSON.parse(event.data)
            if (json.commandType === 'interruption' && user !== parseInt(json.user)) {
                $(".program-loader").empty();
                $(".program-constructor").empty();
                quit();
            }
        };

        socket.onclose = function(event) {
            if (event.wasClean) {
                console.log(`[close] Connection closed clearly, code=${event.code} reason=${event.reason}`)
            } else {
                $(".program-constructor").hide();
                $(".program-loader").show();
                console.log('[close] Connection terminated');
                setTimeout(function() {
                    websocketConnect();
                }, 10000);
            }
        };

    }
    websocketConnect()

    function quit() {
        swal({
            title: 'Редагування було перехоплено, або елемент було видалено из системи',
            type: 'warning',
            confirmButtonText: "Вийти",
            confirmButtonClass: 'btn btn-primary',
        }).then((confirm) => {
            window.location.href = "{{$model->previousUrl}}";
        });
    }

    $(document).on('click','.open-all',function(){
        $( ".element" ).each(function( index ) {
            $(this).removeClass('mb-1');
            $(this).children('.card-header').find('.element-toggle').removeClass('rotate-180');
            $(this).children('.element-container').show();
        });
    });
    $(document).on('click','.close-all',function(){
        $( ".element" ).each(function( index ) {
            $(this).addClass('mb-1');
            $(this).children('.card-header').find('.element-toggle').addClass('rotate-180');
            $(this).children('.element-container').hide();
        });
    });

    function sortableRun() {
        @if (in_array($model->type,['model', 'standart']))
        $('.program').sortable({
            // connectWith: '.element-container',
            handle: ".card-header",
            tolerance: 'pointer',
            opacity: 0.6,
            placeholder: 'sortable-placeholder',
            start: function(e, ui){
                ui.placeholder.height(ui.item.outerHeight());
            }
        }).disableSelection();
        @endif

        $('.problems').sortable({
            connectWith: '.problems',
            handle: ".card-header",
            tolerance: 'pointer',
            opacity: 0.6,
            placeholder: 'sortable-placeholder',
            start: function(e, ui){
                ui.placeholder.height(ui.item.outerHeight());
            }
        }).disableSelection();

        $('.opises').sortable({
            connectWith: '.opises',
            handle: ".card-header",
            tolerance: 'pointer',
            opacity: 0.6,
            placeholder: 'sortable-placeholder',
            start: function(e, ui){
                ui.placeholder.height(ui.item.outerHeight());
            }
        }).disableSelection();
    }


    $(document).on('click','.element-remove',function(){
        swal({
            title: "@lang('MAIN.MODAL.DESTROY.MESSAGE')",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "@lang('MAIN.BUTTON.DESTROY')",
            confirmButtonClass: "btn btn-warning",
            cancelButtonText: "@lang('MAIN.BUTTON.CANCEL')",
            cancelButtonClass: "btn btn-light",
        }).then((confirm) => {
            if(confirm.value){
                $(this).closest('.element').remove();
                calculateKors();
                calculateHours();
            }
        });
    });


    $(document).on('click','.kor-remove',function(){
        swal({
            title: "@lang('MAIN.MODAL.DESTROY.MESSAGE')",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "@lang('MAIN.BUTTON.DESTROY')",
            confirmButtonClass: "btn btn-warning",
            cancelButtonText: "@lang('MAIN.BUTTON.CANCEL')",
            cancelButtonClass: "btn btn-light",
        }).then((confirm) => {
            if(confirm.value){
                $(this).closest('tr').remove();
                calculateKors();
            }
        });

    });

    $(document).on('click','.element-toggle',function(){
        $(this).toggleClass('rotate-180');
        $(this).closest('.element').children('.element-container').toggle('fast');
    });


    $(document).on('click','.btn-add-children', function(){
        let element = $(this).closest('.element');
        if (element.length == 0) {
            element = $('#program');
            container = $('#program');
        } else {
            container = element.children('.element-container');
        }
        container.append( renderTemplate( element.attr('data-children') + '-empty', {} ) );
        sortableRun();
    });


    $(document).on('click','.btn-copy-element', function(){
        let element = $(this).closest('.element');
        element.clone().appendTo(element.parent());
        sortableRun();
        calculateHours();
    });


    $(document).on('click','.btn-edit-element', function(){
        currentElement = $(this).closest('.element');
        let title = currentElement.children('.card-header').children('.card-title').children('.element-name');
        let info = currentElement.children('.card-header').children('.card-title').children('.element-info');
        $('#element-info-wrap').empty()

        if (info.length == 0) {
            $('#element-info-wrap').append('<textarea id="element-title" class="editable"></textarea>')
            $('#element-title').val(title.html());
        } else {
            $('#element-info-wrap').append('<input id="element-title" class="form-control">')
            $('#element-title').val(title.text());
            $('#element-info-wrap').append('<textarea id="element-info" class="editable"></textarea>')
            $('#element-info').val(info.html());
        }

        editor.addElements('.editable')
        $('#modal-edit-element').modal('show');
    });

    $(document).on('click','.btn-edit-green', function(){
        currentElement = $(this).closest('.element');
        $('#modal-edit-green-form').empty()

        @if(in_array($model->type,['educational', 'standart']))
            $('#modal-edit-green-form').append('<label class="small text-muted font-italic col-form-label col-sm-3 mb-3">Кількість годин</label><div class="col-sm-9 mb-3"><input type="number" id="green-hours" class="form-control"></div>')
            $('#green-hours').val(currentElement.find('.element-hours').first().html())
        @endif

        @if(in_array($model->type,['curriculum']))
            $('#modal-edit-green-form').append('<label class="small text-muted font-italic col-form-label col-sm-3 mb-3">Дата</label><div class="col-sm-9 mb-3"><input type="date" id="green-date" class="form-control"></div>')
            $('#green-date').val( formatDate(currentElement.find('.element-date').first().html(),'input') )
        @endif

        $('#modal-edit-green-form').append('<label class="small text-muted font-italic col-form-label col-sm-12">Пропонований зміст навчального предмета / інтегрованого курсу</label><div class="col-sm-12"><textarea id="green-name" class="editable"></textarea></div>')
        $('#green-name').val(currentElement.find('.element-name').first().html())

        @if(in_array($model->type,['curriculum']))
            $('#modal-edit-green-form').append('<label class="small text-muted font-italic col-form-label col-sm-12 mt-3">Перелік матеріалів для використання</label><div class="col-sm-12 mb-3"><textarea id="green-materials" class="editable"></textarea></div>')
            $('#green-materials').val(currentElement.find('.element-materials').first().html())

            $('#modal-edit-green-form').append('<label class="small text-muted font-italic col-form-label col-sm-12 mt-3">Очікувані результати за змістом навчання</label><div class="col-sm-12 mb-3"><textarea id="green-custom-expected" class="editable"></textarea></div>')
            $('#green-custom-expected').val(currentElement.find('.element-custom-expected').first().html())
        @endif


        editor.addElements('.editable')
        $('#modal-edit-green').modal('show');
    });

    $(document).on('click', '#modal-edit-green-submit', function(){
        let name = currentElement.find('.element-name').first();
        name.empty().append( $('#green-name').val() )
        @if(in_array($model->type,['educational', 'standart']))
            let hours = currentElement.find('.element-hours').first();
            hours.empty().append( $('#green-hours').val() )
            calculateHours()
        @endif
        @if(in_array($model->type,['curriculum']))
            let date = currentElement.find('.element-date').first();
            date.empty().append( formatDate($('#green-date').val(), 'html') )
            let materials = currentElement.find('.element-materials').first();
            materials.empty().append( $('#green-materials').val() )
            let customExpected = currentElement.find('.element-custom-expected').first();
            customExpected.empty().append( $('#green-custom-expected').val() )
        @endif
    })

    const formatDate = (value, type) => {
        if (value.length < 10) return ''
        if (type === 'html') {
            return value.substring(8,10) + '.' + value.substring(5,7) + '.' + value.substring(0,4)
        } else {
            return value.substring(6,10) + '-' + value.substring(3,5) + '-' + value.substring(0,2)
        }
    }

    $(document).on('click','.element-undo',function(){
        let element = $(this).closest('.line')
        let href = $(this).attr('data-href')
        swal({
            title: "@lang('MAIN.MODAL.UNDO.MESSAGE')",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "@lang('MAIN.BUTTON.UNDO')",
            confirmButtonClass: "btn btn-warning",
            cancelButtonText: "@lang('MAIN.BUTTON.CANCEL')",
            cancelButtonClass: "btn btn-light",
        }).then((confirm) => {
            if(confirm.value){
                $.ajax({
                    url: href,
                }).done((data, status, xhr) => {
                    element.replaceWith(data.elementHTML)
                }).fail((xhr) => {
                    // let data = xhr.responseJSON;
                    swal({
                        // title: data.message,
                        title: 'Помилка 404',
                        html: 'Батьківську навчальну програму не знайдено. Повернення неможливе.',
                        type: 'error',
                        confirmButtonClass: 'btn btn-primary',
                    })
                }).always((xhr, type, status) => {
                    sortableRun();
                    calculateKors();
                    calculateHours();
                })
            }
        });

    });


    $(document).on('click','#modal-edit-element-submit', function(){
        let title = currentElement.children('.card-header').children('.card-title').children('.element-name');
        title.empty().append( $('#element-title').val() );
        let info = currentElement.children('.card-header').children('.card-title').children('.element-info');
        if (info.length != 0) {
            info.empty().append( $('#element-info').val() );
        }
    });

    $(document).on('click','.btn-add-kors', function(){
        currentElement = $(this).closest('.element');
        $(".kor-checkBox:checked").prop('checked',false);
        calculateKors();
        $('.change-kors tbody').each(function( index ) { sortTable($(this)) })
        $('#modal-add-kors').modal('show');
    });

    $(document).on('change','.kor-checkBox', function(){
        let frequencyCount = $(this).closest('tr').children('.kor-frequency');
        let value = parseInt( frequencyCount.text() );
        frequencyCount.text( $(this).prop('checked') == true ? value+1 : value-1 );
    })

    $(document).on('click','#modal-add-kors-submit', function(){
        let container = currentElement.find('tbody');
        $(".kor-checkBox:checked").each(function( index ) {
            let galuz = $(this).closest('tr').attr('data-branch');
            let owner = $(this).closest('tr').attr('data-owner');
            let kod = $(this).closest('tr').find('.kor-kod').text();
            let name = $(this).closest('tr').find('.kor-name').text();
            container.append( renderTemplate( 'kor-empty', {galuz: galuz, owner: owner, code: kod, name: name} ) );
            sortTable(container)
            $(this).prop('checked',false);
        });
        calculateKors();
    });


    $('.change-kors tbody').each(function( index ) { sortTable($(this)) })

    function sortTable(container){
        var $tbody = container;
        $tbody.find('tr').sort(function (a, b) {
            var tda = $(a).find('td:eq(' + 0 + ')').text(); // Use your wished column index
            var tdb = $(b).find('td:eq(' + 0 + ')').text(); // Use your wished column index
            // if a < b return 1
            return tda > tdb ? 1
                   // else if a > b return -1
                   : tda < tdb ? -1
                   // else they are equal - return 0    
                   : 0;
        }).appendTo($tbody);
    }


    function assemblyProgram() {
        let program = new Object();
        $("#program .line").each(function( index ) {
            let line = new Object();
            line.name_line = $( this ).children('.card-header').children('.card-title').children('.element-name').text();
            line.info_line = $( this ).children('.card-header').children('.card-title').children('.element-info').html();
            let problems = new Object();
            current_problems = $( this ).children('.problems').children('.problem');
            current_problems.each(function( index2 ){
                let problem = new Object();
                problem.name_problem = $( this ).children('.card-header').children('.card-title').children('.element-name').html();
                @if (in_array($model->type,['educational', 'standart']))
                    problem.hours_problem = $(this).find('.element-hours').html()
                @endif
                @if (in_array($model->type,['curriculum']))
                    problem.date_problem = $(this).find('.element-date').html()
                    problem.materials_problem = $(this).find('.element-materials').html()
                    problem.custom_expected_problem = $(this).find('.element-custom-expected').html()
                @endif
                let opises = new Object();
                current_opises = $( this ).children('.opises').children('.opis');
                current_opises.each(function( index3 ){
                    let opis = new Object();
                    opis.name_opis = $( this ).children('.card-header').children('.card-title').children('.element-name').html();
                    let kors = new Object();
                    current_kors = $( this ).find('.kor');
                    current_kors.each(function( index4 ){
                        let kor = new Object();
                        kor.galuz = $( this ).children('.kor-kod').data('galuz');
                        kor.owner = $( this ).children('.kor-kod').data('owner');
                        kor.kod = $( this ).children('.kor-kod').text();
                        kor.name = $( this ).children('.kor-name').text();
                        kor.tool = $( this ).children('.kor-tool').children('select').val();
                        kors[ index4 ] = kor;
                    });
                    opis.kors = kors;
                    opises[ index3 ] = opis;
                });
                problem.opises = opises;
                problems[ index2 ] = problem;
            });
            line.problems = problems;
            program[ index ] = line;
        });
        return program;
    }

    $(document).on('click','.save-program',function(){
        let form = $('#program-form');
        let formData = new FormData( document.forms.namedItem("fileinfo") );
        formData.append('program', JSON.stringify( assemblyProgram() ) );

        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: formData,
            processData: false,
            contentType: false,
        }).done((data, status, xhr) => {
            swal({
                title: data.message,
                type: 'success',
                showCancelButton: true,
                confirmButtonText: "Вийти до переліку",
                confirmButtonClass: 'btn btn-primary',
                cancelButtonText: "Продовжити редагування",
                cancelButtonClass: 'btn btn-light',
            }).then((confirm) => {
                if(confirm.value){
                    window.location.href = data.exitURL;
                }else{
                    @if( \App\Models\Option::getOption('programAutosave') )
                        mytime = {{ \App\Models\Option::getOption('programAutosaveInterval',0) }};
                        make_save = true;
                    @endif
                }
            });
            form.find('fieldset').attr('disabled', true);
        }).fail((xhr) => {
            let data = xhr.responseJSON;
            swal({
                title: data.message,
                type: 'error',
                confirmButtonClass: 'btn btn-primary',
            })                    
        }).always((xhr, type, status) => {
            let response = xhr.responseJSON || status.responseJSON,
                errors = response.errors || [];
            form.find('.field').each((i, el) => {
                let field = $(el);
                field.removeClass('is-invalid');
                container = field.closest('.form-group'),
                    elem = $('<label class="message"></label>');
                container.find('label.message').remove();
                if(errors[field.attr('name')]){
                    field.addClass('is-invalid');
                    field.find('input').addClass('is-invalid');
                    errors[field.attr('name')].forEach((msg) => {
                        elem.clone().addClass('validation-invalid-label').html(msg).appendTo(container);
                    });
                }
            });
        });
    });

    $(document).on('change','.search-panel select',function(){
        showKors();
    });

    function showKors() {
        let flag = true;
        let using = 0;
        let branch = 0;
        $( '.change-kors .kor' ).each(function( index ){
            flag = true;
            element = $(this);
            using = parseInt( $('#selectUsing').val() );
            branch = parseInt( $('#selectBranch').val() );
            if ( (using == 1 && element.attr('data-frequency') == "0") ||
                 (using == 2 && element.attr('data-frequency') != "0"))
            {
                flag = false;
            }
            console.log(branch + ' - ' + element.attr('data-owner') + ' - ' + parseInt(element.attr('data-branch')) );
            if ( (branch == 0 && element.attr('data-owner') != "1") || (branch == -1 && element.attr('data-owner') == "1") || ( (branch > 0 && parseInt(element.attr('data-branch')) != branch)) ){
                flag = false;
            }
            if (flag){
                $(this).show();
            } else {
                $(this).hide();
            }
        });
    }

    function calculateKors() {
        let branchData = {};
        let data1 = [];
        let data2 = [];
        @foreach ($model->branches as $key=>$branch)
            branchData[{{$branch->id}}] = {'key':{{$key}}, 'name':'{{$branch->name}}', 'present':{{$branch->specificCount($model->cycle_id)}}, 'used':0, 'list':{}},
        @endforeach
        $( '#program .kor' ).each(function( index ){
            let code = $(this).children('.kor-kod').text();
            let branch = $(this).children('.kor-kod').attr('data-galuz');
            if (typeof branchData[branch] != 'undefined' ){
                branchData[branch].list[code] = (typeof branchData[branch].list[code] === 'undefined') ? 1 : branchData[branch].list[code] + 1;
            }
        });
        $('.kor').attr('data-frequency',0);
        $('.kor').children('.kor-frequency').text(0);
        $.each(branchData, function( index, value ) {
            branchData[index].used = countProperties(branchData[index].list);
            data1[branchData[index].key] = branchData[index].used;
            barChartData.datasets[ branchData[index].key ].data = branchData[index].present > 0 ? [round10(100 / branchData[index].present * branchData[index].used)] : [0];
            $.each(branchData[index].list, function( index2, value2 ) {
                $('[data-kod="' + index2 + '"]').attr('data-frequency',value2);
                $('[data-kod="' + index2 + '"]').children('.kor-frequency').text(value2);
            });
        });
        doughnutChartData.datasets[0].data = data1;
        window.myBar1.update();
        window.myBar2.update();
    }

    function calculateHours() {
        let hours = 0
        $( '.element-hours' ).each(function( index ){
            hours += parseInt($(this).html())
        });
        @if(in_array($model->type,['educational', 'standart']))
        $('#hours').val(hours)
        @endif
    }

    function countProperties(obj) {
        var count = 0;
        for(var prop in obj) {
            if(obj.hasOwnProperty(prop))
                ++count;
        }
        return count;
    }

    function round10(argument) {
        return Math.round( argument * 10 ) / 10;
    }
            window.chartColors = {
                @foreach ($model->branches as $key=>$branch)
                color{{$key}} : '{{$branch->color}}',
                @endforeach
            };
            var color = Chart.helpers.color;

            var doughnutChartData = {
            datasets: [{
                data: [1,2],
                backgroundColor: [
                    window.chartColors.color0,
                    window.chartColors.color1,
                    window.chartColors.color2,
                    window.chartColors.color3,
                    window.chartColors.color4,
                    window.chartColors.color5,
                    window.chartColors.color6,
                    window.chartColors.color7,
                    window.chartColors.color8,
                    window.chartColors.color9
                ],
                label: 'Dataset 0'
            }],
            labels: [
                @foreach ($model->branches as $key=>$branch)
                    "{{$branch->name}}",
                @endforeach
                ]
            };

            var barChartData = {
                datasets: [
                    @foreach ($model->branches as $key=>$branch)
                    {
                        label: '{{$branch->name}}',
                        backgroundColor: color(window.chartColors.color{{$key}}).alpha(0.9).rgbString(),
                        borderColor: color(window.chartColors.color{{$key}}).rgbString(),
                        borderWidth: 1,
                        data: [ {{$key}} ]
                    },
                    @endforeach
                ]

            };

            var ctx1 = document.getElementById("canvas-doughnut").getContext("2d");
            window.myBar1 = new Chart(ctx1, {
                type: 'doughnut',
                data: doughnutChartData,
                options: {
                    responsive: true,
                    legend: {
                        position: 'top',
                        display: false,
                    },
                    title: {
                        display: true,
                        text: 'Відповідно до використаних КОРів (кількість)'
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            });


            var ctx2 = document.getElementById("canvas-bar").getContext("2d");
            window.myBar2 = new Chart(ctx2, {
                type: 'bar',
                data: barChartData,
                options: {
                    responsive: true,
                    legend: {
                        position: 'top',
                        display: false,
                    },
                    title: {
                        display: true,
                        text: 'Відповідно до існуючих КОРів (%)'
                    }
                }
            });

    $('#explanatory').on('change',function(e){
        // var fileName = $(this).val()
        var fileName = e.target.files[0].name
        $(this).next('.custom-file-label').html(fileName)
    })

    $(document).on('click','.kor-edit, .kor-name',function(){
        let element = $(this).closest('.kor').find('.kor-name')
        swal({
            title: 'Конкретизація очікуваного результату',
            // type: "warning",
            input: 'textarea',
            inputPlaceholder: 'Type your message here',
            inputClass: 'form-control',
            inputValue: element.text(),
            showCancelButton: true,
            confirmButtonText: "Замінити",
            confirmButtonClass: "btn btn-success",
            cancelButtonText: "Скасувати",
            cancelButtonClass: "btn btn-light",
        }).then((confirm) => {
            if(confirm.value){
                element.empty().text(confirm.value)
            }
        });
    })

    calculateKors();
    calculateHours();
</script>
@endsection

@section('modals')
    <div id="modal-edit-element" class="modal" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <h6 class="modal-title">@lang('MAIN.MODAL.ELEMENT.TITLE')</h6>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div id="element-info-wrap"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">@lang('MAIN.BUTTON.CANCEL')</button>
                        <button type="button" class="btn bg-primary" id="modal-edit-element-submit" data-dismiss="modal">@lang('MAIN.BUTTON.CHANGE')</button>
                    </div>
            </div>
        </div>
    </div>

    <div id="modal-edit-green" class="modal" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                    <div class="modal-header bg-teal">
                        <h6 class="modal-title">@lang('MAIN.MODAL.ELEMENT.TITLE')</h6>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="row" id="modal-edit-green-form">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">@lang('MAIN.BUTTON.CANCEL')</button>
                        <button type="button" class="btn bg-teal" id="modal-edit-green-submit" data-dismiss="modal">@lang('MAIN.BUTTON.CHANGE')</button>
                    </div>
            </div>
        </div>
    </div>

    <div id="modal-add-kors" class="modal" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <h6 class="modal-title">@lang('MAIN.MODAL.EXPECTS.TITLE')</h6>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body p-0">
                        <div class="top-menu p-3 bg-light">
                            <div class="row search-panel">
                                <label for="" class="small text-muted font-italic col-form-label col-sm-2">@lang('MAIN.COLUMN.BRANCH')</label>
                                <div class="col-sm-4">
                                    <select class="form-control" id="selectBranch"  onchange="selectBranch2.value=this.value;">
                                        <option value="0">@lang('MAIN.PLACEHOLDER.ALL-BRANCHES')</option>
                                        @foreach($model->branches as $key => $branch)
                                            <option value="{{$branch->id}}">{{$branch->name}}</option>
                                        @endforeach
                                        @if(isset($team))
                                            <option value="-1">@lang('MAIN.MENU.TEAM-SPECIFICS')</option>
                                        @endif
                                    </select>
                                </div>
                                <label for="" class="small text-muted font-italic col-form-label col-sm-2">@lang('MAIN.COLUMN.USING')</label>
                                <div class="col-sm-4">
                                    <select class="form-control" id="selectUsing"  onchange="selectUsing2.value=this.value;">
                                        <option value="0">@lang('MAIN.PLACEHOLDER.ALL')</option>
                                        <option value="1">@lang('MAIN.PLACEHOLDER.USING')</option>
                                        <option value="2">@lang('MAIN.PLACEHOLDER.NOT-USING')</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-scroll-body">
                            <table class="change-kors sticked table table-striped">
                                <thead>
                                    <tr>
                                        <th class="bg-dark text-center pl-4 pr-4">@lang('MAIN.COLUMN.MARKING')</th>
                                        <th class="bg-dark text-center">@lang('MAIN.COLUMN.NAME')</th>
                                        <th class="bg-dark text-center">@lang('MAIN.COLUMN.COUNT')</th>
                                        <th class="bg-dark text-center">@lang('MAIN.COLUMN.ACTIONS')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($model->branches as $branch)
                                        @foreach($branch->expects()->where('cycle_id','=',$model->cycle_id)->get() as $expect)
                                            @foreach($expect->specifics as $specific)
                                                <tr class="kor" data-kod="{{$specific->marking}}" data-frequency="0" data-branch="{{$branch->id}}" data-owner="1">
                                                    <td class="kor-kod">{{$specific->marking}}</td>
                                                    <td class="kor-name">{{$specific->name}}</td>
                                                    <td class="text-center kor-frequency" >0</td>
                                                    <td class="text-center pl-4 pr-2">
                                                        <div class="custom-control custom-checkbox">
                                                          <input type="checkbox" class="kor-checkBox custom-control-input" id="{{$specific->id}}">
                                                          <label class="custom-control-label" for="{{$specific->id}}">&nbsp;</label>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endforeach
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer pt-3 bg-light">
                        <button type="button" class="btn btn-link" data-dismiss="modal">@lang('MAIN.BUTTON.CANCEL')</button>
                        <button type="button" class="btn bg-primary" id="modal-add-kors-submit" data-dismiss="modal">@lang('MAIN.BUTTON.APPEND')</button>
                    </div>
            </div>
        </div>
    </div>
@endsection

@section('templates')
    <template id="line-empty">
        <div class="element pb-1 line" data-children="problem">
            <div class="card-header header-elements-inline bg-primary">
                <div class="card-title"><span class="element-name">@lang('MAIN.TITLE.NEW-LINE')</span><br><span class="element-info"></span></div>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item ml-2 element-toggle {{--rotate-180--}}" data-action="toggle" title="@lang('MAIN.HINT.TOGGLE')"><i class="icon-arrow-down5"></i></a>
                        <a class="list-icons-item ml-2 btn-edit-element" data-action="" title="@lang('MAIN.HINT.EDIT')"><i class="icon-pencil7"></i></a>
                        <a class="list-icons-item ml-2 btn-add-children" data-action="" title="@lang('MAIN.HINT.ADD-PROBLEM')"><i class="icon-googleplus5"></i></a>
                        <a class="list-icons-item ml-2 btn-copy-element" data-action="" title="@lang('MAIN.HINT.COPY')"><i class="icon-gallery"></i></a>
                        <a class="list-icons-item ml-2 element-remove" data-action="" title="@lang('MAIN.HINT.DELETE')"><i class="icon-cross3"></i></a>
                    </div>
                </div>
            </div>
            <div class="element-container pt-2 pr-0 pb-1 pl-5 problems" {{--style="display: none;"--}}></div>
        </div>
    </template>

    <template id="problem-empty">
        <div class="element pb-1 problem" data-children="opis">
            <div class="card-header header-elements-inline bg-teal">
                <div class="card-title">
                    @if (in_array($model->type,['educational', 'standart']))
                        <div class="font-italic mb-2">Kількість годин: <span class="element-hours">0</span></div>
                    @endif
                    @if (in_array($model->type,['curriculum']))
                        <div class="font-italic mb-2">Дата: <span class="element-date"></span></div>
                    @endif
                    <span class="element-name">@lang('MAIN.TITLE.NEW-PROBLEM')</span>
                    @if (in_array($model->type,['curriculum']))
                        <div class="element-materials">
                            <div class="font-italic mb-2">Перелік матеріалів для використання</div>
                        </div>
                        <div class="element-custom-expected">
                            <div class="font-italic mb-2">Очікувані результати за змістом навчання</div>
                        </div>
                    @endif
                </div>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item ml-2 element-toggle {{--rotate-180--}}" data-action="toggle" title="@lang('MAIN.HINT.TOGGLE')"><i class="icon-arrow-down5"></i></a>
                        <a class="list-icons-item ml-2 btn-edit-green" data-action="delete"><i class="icon-pencil7" title="@lang('MAIN.HINT.EDIT')"></i></a>
                        <a class="list-icons-item ml-2 btn-add-children" data-action="" title="@lang('MAIN.HINT.ADD-OPIS')"><i class="icon-googleplus5"></i></a>
                        <a class="list-icons-item ml-2 btn-copy-element" data-action="" title="@lang('MAIN.HINT.COPY')"><i class="icon-gallery"></i></a>
                        <a class="list-icons-item ml-2 element-remove" data-action="delete" title="@lang('MAIN.HINT.DELETE')"><i class="icon-cross3"></i></a>
                    </div>
                </div>
            </div>
            <div class="element-container pt-2 pr-0 pb-1 pl-5 opises" {{--style="display: none;"--}}></div>
        </div>
    </template>

    <template id="opis-empty">
        <div class="element pb-1 opis">
            <div class="card-header header-elements-inline bg-warning">
                <div class="card-title"><span class="element-name">@lang('MAIN.TITLE.NEW-OPIS')</span></div>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item ml-2 element-toggle {{--rotate-180--}}" data-action="toggle" title="@lang('MAIN.HINT.TOGGLE')"><i class="icon-arrow-down5"></i></a>
                        <a class="list-icons-item ml-2 btn-edit-element" data-action="delete" title="@lang('MAIN.HINT.EDIT')"><i class="icon-pencil7"></i></a>
                        <a class="list-icons-item ml-2 btn-add-kors" data-action="" title="@lang('MAIN.HINT.ADD-KORS')"><i class="icon-googleplus5"></i></a>
                        <a class="list-icons-item ml-2 btn-copy-element" data-action="" title="@lang('MAIN.HINT.COPY')"><i class="icon-gallery"></i></a>
                        <a class="list-icons-item ml-2 element-remove" data-action="delete" title="@lang('MAIN.HINT.DELETE')"><i class="icon-cross3"></i></a>
                    </div>
                </div>
            </div>
            <div class="element-container p-0 kors">
                <table class="table table-striped text-dark w-100">
                    <thead class="bg-dark">
                        <tr>
                            <th class="text-center pl-5 pr-5">Код</th>
                            <th class="text-center">Очікуваний результат</th>
                            <th class="text-center pl-5 pr-5">Інструмент</th>
                            <th class="text-center"></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </template>

    <template id="kor-empty">
        <tr class="kor">
            <td class="kor-kod" data-galuz="{galuz}" data-owner="{owner}">{code}</td>
            <td class="kor-name">{name}</td>
            <td class="kor-tool">
                <select class="form-control">
                @foreach ($tools as $tool)
                    <option value="{{$tool->name_tool}}">{{$tool->name_tool}}</option>
                @endforeach
                </select>
            </td>
            <td>
                <div class="btn-group">
                    <button type="button" class="btn btn-sm btn-icon btn-light kor-edit"><i class="icon-pencil7" title="@lang('MAIN.HINT.EDIT')"></i></button>
                    <button type="button" class="btn btn-sm btn-icon btn-danger kor-remove" title="@lang('MAIN.HINT.DELETE')"><i class="icon-cross3"></i></button>
                </div>
            </td>
        </tr>
    </template>
@endsection
