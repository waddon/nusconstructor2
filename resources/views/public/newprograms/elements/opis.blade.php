<div class="element pb-1 opis">
  <div class="card-header header-elements-inline bg-warning">
    <div class="card-title flex-grow-1"><span class="element-name">{!! $opis->name_opis !!}</span></div>
    <div class="header-elements">
      <div class="list-icons">
        <a class="list-icons-item ml-2 element-toggle rotate-180" data-action="toggle" title="@lang('MAIN.HINT.TOGGLE')">
          <i class="icon-arrow-down5"></i>
        </a>
        <a class="list-icons-item ml-2 btn-edit-element" data-action="delete"  title="@lang('MAIN.HINT.EDIT')">
          <i class="icon-pencil7"></i>
        </a>
        @if (in_array($type,['model', 'standart']) || !($condition = \App\Models\Option::getOption('programTypeRestrictions')))
          <a class="list-icons-item ml-2 btn-add-kors" data-action="" title="@lang('MAIN.HINT.ADD-KORS')">
            <i class="icon-googleplus5"></i>
          </a>
        @endif
        <a class="list-icons-item ml-2 btn-copy-element" data-action="" title="@lang('MAIN.HINT.COPY')">
          <i class="icon-gallery"></i>
        </a>
        <a class="list-icons-item ml-2 element-remove" data-action="delete" title="@lang('MAIN.HINT.DELETE')">
          <i class="icon-cross3"></i>
        </a>
      </div>
    </div>
  </div>
  <div class="element-container p-0 kors" style="display: none;">
    <table class="table table-striped text-dark w-100">
      <thead class="bg-dark">
        <tr>
          <th class="text-center pl-5 pr-5">Код</th>
          <th class="text-center">Очікуваний результат</th>
          <th class="text-center pl-5 pr-5">Інструмент</th>
          <th class="text-center"></th>
        </tr>
      </thead>
      <tbody>
        @foreach ($opis->kors as $kor)
          @include('public.newprograms.elements.kor', ['type'=> $type, 'kor' => $kor])
        @endforeach
      </tbody>
    </table>
  </div>
</div>