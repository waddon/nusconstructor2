<div
  class="element pb-1 line"
  data-children="problem" 
  @if (isset($this->meta->layout) && $this->meta->layout)
    data-grade_id="{{ $line->grade_id }}
    data-period_element_id="{{ $line->period_element_id }}
  @endif
>
  <div
    class="card-header header-elements-inline bg-primary"
    @if (isset($this->meta->layout) && $this->meta->layout)
      style="cursor:auto;"
    @endif
  >
    <div class="card-title flex-grow-1">
      <span class="element-name">{{ $line->name_line }}</span><br>
      <span class="element-info">{!! $line->info_line !!}</span>
    </div>

    <div class="header-elements">
      <div class="list-icons">
        <a class="list-icons-item ml-2 element-toggle rotate-180" data-action="toggle" title="@lang('MAIN.HINT.TOGGLE')">
          <i class="icon-arrow-down5"></i>
        </a>
        <a class="list-icons-item ml-2 btn-edit-element" data-action="" title="@lang('MAIN.HINT.EDIT')">
          <i class="icon-pencil7"></i>
        </a>
        @if (in_array($type,['model', 'standart']) || !($condition = \App\Models\Option::getOption('programTypeRestrictions')))
          <a class="list-icons-item ml-2 btn-add-children" data-action="" title="@lang('MAIN.HINT.ADD-PROBLEM')">
            <i class="icon-googleplus5"></i>
          </a>
        @endif
        @if ((!isset($this->meta->layout) || $this->meta->layout==0) && (in_array($type,['model', 'standart']) || !($condition = \App\Models\Option::getOption('programTypeRestrictions'))))
          <a class="list-icons-item ml-2 btn-copy-element" data-action="" title="@lang('MAIN.HINT.COPY')">
            <i class="icon-gallery"></i>
          </a>
          <a class="list-icons-item ml-2 element-remove" data-action="" title="@lang('MAIN.HINT.DELETE')">
            <i class="icon-cross3"></i>
          </a>
        @endif
        @if (!in_array($type,['model', 'standart']))
          <a
            class="list-icons-item ml-2 element-undo"
            data-action=""
            data-href="{{ route('api.newprogram.elementHTML', [
              'parentId' => $parentId,
              'elementSerial' => $lineKey,
              'childType' => $type
            ]) }}"
            title="@lang('MAIN.HINT.UNDO')"
          >
            <i class="icon-undo"></i>
          </a>
        @endif
      </div>
    </div>
  </div>
  <div class="element-container pt-2 pr-0 pb-1 pl-5 problems" style="display: none;">
    @foreach ($line->problems as $problem)
      @include('public.newprograms.elements.problem', ['type'=> $type, 'problem' => $problem])
    @endforeach
  </div>
</div>