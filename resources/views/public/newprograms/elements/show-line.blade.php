<div class="element pb-1 line" data-children="problem">
  <div class="card-header header-elements-inline bg-primary">
    <div class="card-title flex-grow-1">
      <span class="element-name">{{ $line->name_line }}</span><br>
      <span class="element-info">{!! $line->info_line !!}</span>
    </div>
    <div class="header-elements">
      <div class="list-icons">
        <a class="list-icons-item ml-2 element-toggle rotate-180" data-action="toggle" title="@lang('MAIN.HINT.TOGGLE')">
          <i class="icon-arrow-down5"></i>
        </a>
      </div>
    </div>
  </div>
  <div class="element-container pt-2 pr-0 pb-1 pl-5 problems" style="display: none;">
    @foreach ($line->problems as $problem)
      @include('public.newprograms.elements.show-problem', ['type'=> $type, 'problem' => $problem])
    @endforeach
  </div>
</div>