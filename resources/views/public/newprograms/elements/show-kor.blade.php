<tr class="kor">
  <td class="kor-kod" data-galuz="{{ $kor->galuz }}" data-owner="{{ $kor->owner }}">{{ $kor->kod }}</td>
  <td class="kor-name">{{ $kor->name }}</td>
  <td class="kor-tool">{{ $kor->tool }}</td>
  <td>
    <a href="#" class="btn-show-materials" data-toggle="modal" data-target="#modal-show-materials" data-kod="{{ $kor->kod }}"><i class="icon-images2 icon-2x"></i></a>
  </td>
</tr>
