<tr class="kor">
  <td class="kor-kod" data-galuz="{{ $kor->galuz }}" data-owner="{{ $kor->owner }}">{{ $kor->kod }}</td>
  <td class="kor-name">{{ $kor->name }}</td>
  <td class="kor-tool">
    <select class="form-control">
      @foreach ($tools as $tool)
        <option {{ $tool->name_tool == $kor->tool ? 'selected' : '' }}>{{ $tool->name_tool }}</option>
      @endforeach
    </select>
  </td>
  <td>
    <div class="btn-group">
      <button type="button" class="btn btn-sm btn-icon btn-light kor-edit"><i class="icon-pencil7" title="@lang('MAIN.HINT.EDIT')"></i></button>
      <button type="button" class="btn btn-sm btn-icon btn-danger kor-remove"><i class="icon-cross3" title="@lang('MAIN.HINT.DELETE')"></i></button>
    </div>
  </td>
</tr>
