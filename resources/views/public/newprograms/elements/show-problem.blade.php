<div class="element pb-1 problem" data-children="opis">
  <div class="card-header header-elements-inline bg-teal">
    <div class="card-title flex-grow-1">
      @if (in_array($type,['educational', 'standart']))
        <div class="font-italic mb-2">Kількість годин: <span class="element-hours">{{ $problem->hours_problem ?? 0 }}</span></div>
      @endif
      @if (in_array($type,['curriculum']))
        <div class="font-italic mb-2">Дата: <span class="element-date">{{ $problem->date_problem ?? '' }}</span></div>
      @endif
      <span class="element-name">
        {!! $problem->name_problem !!}
      </span>
      @if (in_array($type,['curriculum']))
        <div class="mt-3">
          <div class="font-italic mb-2">Перелік матеріалів для використання</div>
          <div class="element-materials">{!! $problem->materials_problem ?? '' !!}</div>
        </div>
        <div class="mt-3">
          <div class="font-italic mb-2">Очікувані результати за змістом навчання</div>
          <div class="element-custom-expected">{!! $problem->custom_expected_problem ?? '' !!}</div>
        </div>
      @endif
    </div>
    <div class="header-elements">
      <div class="list-icons">
        <a class="list-icons-item ml-2 element-toggle rotate-180" data-action="toggle" title="@lang('MAIN.HINT.TOGGLE')">
          <i class="icon-arrow-down5"></i>
        </a>
      </div>
    </div>
  </div>
  <div class="element-container pt-2 pr-0 pb-1 pl-5 opises" style="display: none;">
    @foreach ($problem->opises as $opis)
      @include('public.newprograms.elements.show-opis', ['type'=> $type, 'opis' => $opis])
    @endforeach
  </div>
</div>