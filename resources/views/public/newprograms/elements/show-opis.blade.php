<div class="element pb-1 opis">
  <div class="card-header header-elements-inline bg-warning">
    <div class="card-title flex-grow-1"><span class="element-name">{!! $opis->name_opis !!}</span></div>
    <div class="header-elements">
      <div class="list-icons">
        <a class="list-icons-item ml-2 element-toggle rotate-180" data-action="toggle" title="@lang('MAIN.HINT.TOGGLE')">
          <i class="icon-arrow-down5"></i>
        </a>
      </div>
    </div>
  </div>
  <div class="element-container p-0 kors" style="display: none;">
    <table class="table table-striped text-dark w-100">
      <thead class="bg-dark">
        <tr>
          <th class="text-center pl-5 pr-5">Код</th>
          <th class="text-center">Очікуваний результат</th>
          <th class="text-center pl-5 pr-5">Інструмент</th>
          <th class="text-center">Матеріали</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($opis->kors as $kor)
          @include('public.newprograms.elements.show-kor', ['type'=> $type, 'kor' => $kor])
        @endforeach
      </tbody>
    </table>
  </div>
</div>