@extends('layouts.app')

@section('content')
<style type="text/css">
.element-container{
    border-left: 1px solid #ddd;
    border-bottom: 1px solid #ddd;
}
.buttons{
    padding: 15px 15px 15px 30px;
}
.element .card-header .header-elements i{
    font-size: 24px;
}
.element.line>.element-container{ background: #e4e4e4; }
.element.problem>.element-container{ background: #eaeaea; }
.element.opis>.element-container {background: #f1f1f1;}
.line>.card-header, .problem>.card-header, .opis>.card-header {
    cursor: auto;
}
</style>

<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">

        <div class="card mb-3 program-constructor">
                <!-- Search panel -->
                    <div class="search-panel">
                        <h2>Перегляд програми "{{$model->name}}"</h2>
                        <div class="text-right">
                            <div class="btn-group">

                                <button type="button" class="btn btn-primary btn-icon open-all" title="@lang('MAIN.HINT.TOGGLE-OPEN')"><i class="icon-menu-open"></i></button>
                                <button type="button" class="btn btn-primary btn-icon close-all" title="@lang('MAIN.HINT.TOGGLE-CLOSE')"><i class="icon-menu-close"></i></button>
                                <a href="{{route('loadprogram', [ $model->key ])}}" class="btn btn-danger btn-icon" title="@lang('MAIN.HINT.DOWNLOAD')" target="_blank"><i class="icon-file-pdf"></i></a>
                                @if(!$model->public || in_array($model->type, ['educational', 'curriculum']))
                                <a href="{{route('docExportProgram', [ $model->key ])}}" class="btn btn-success btn-icon" title="@lang('MAIN.HINT.DOWNLOAD')" target="_blank"><i class="icon-file-word"></i></a>
                                @endif
                                @if(isset($model->previousUrl))
                                    <a href="{{ $model->previousUrl }}" class="btn btn-danger" title="@lang('MAIN.HINT.EXIT')"><i class="icon-esc"></i></a>
                                @endif
                            </div>
                            
                        </div>
                    </div>
                <!-- /Search panel -->

                <!-- Tabs -->
                        <ul class="nav nav-tabs nav-tabs-highlight">
                            <li class="nav-item"><a href="#tab-constructor" class="nav-link active" data-toggle="tab"><i class="icon-tree6 mr-2"></i> @lang('MAIN.TAB.PROGRAM')</a></li>
                            <li class="nav-item"><a href="#tab-supervisor" class="nav-link" data-toggle="tab"><i class="icon-pie-chart3 mr-2"></i> @lang('MAIN.TAB.STATISTIC')</a></li>
                            <li class="nav-item"><a href="#tab-explanatory" class="nav-link" data-toggle="tab"><i class="icon-file-text2 mr-2"></i> @lang('MAIN.TAB.EXPLANATORY-NOTE')</a></li>
                            <li class="nav-item"><a href="#tab-additional" class="nav-link" data-toggle="tab"><i class="icon-insert-template mr-2"></i> @lang('MAIN.TAB.ADDITIONAL')</a></li>
                        </ul>

                        <div class="tab-content">
                            <!-- Constructor Table -->
                                <div class="tab-pane fade show active" id="tab-constructor">

                                    {{--<div class="card-body">
                                        <div class="form-group row">
                                            {{ Form::label('name', __('MAIN.COLUMN.NAME'), ['class'=>'small text-muted font-italic col-lg-2 col-form-label']) }}
                                            <div class="col-lg-6">
                                                {{ Form::text('name', $model->name, array('class' => 'field form-control')) }}
                                            </div>
                                            {{ Form::label('hours', __('MAIN.COLUMN.HOURS'), ['class'=>'small text-muted font-italic col-lg-2 col-form-label']) }}
                                            <div class="col-lg-2">
                                                {{ Form::text('hours', isset($model->meta->hours) ? $model->meta->hours : 0, array('class' => 'field form-control', 'onkeyup' => 'this.value = this.value.replace(/[^\d]/g,"");')) }}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            {{ Form::label('branches', __('MAIN.COLUMN.BRANCHES'), ['class'=>'small text-muted font-italic col-lg-2 col-form-label']) }}
                                            <div class="col-lg-6">{{$model->branches->implode('name', ', ')}}</div>
                                            {{ Form::label('cycle', __('MAIN.COLUMN.CYCLE'), ['class'=>'small text-muted font-italic col-lg-2 col-form-label']) }}
                                            <div class="col-lg-2">{{$model->cycle->name}}</div>
                                        </div>
                                    </div>--}}
                                    <div id="program" class="card-body program p-3" data-children="line">
                                        @if (is_object($model->content))
                                            @foreach ($model->content as $lineKey => $line)
                                                @include('public.newprograms.elements.show-line', [
                                                    'type' => $model->type,
                                                    'parentId' => $model->parent_id ?? 0,
                                                    'line' => $line,
                                                    'lineKey' => $lineKey,
                                                ])
                                            @endforeach
                                        @endif                                        
                                        {{--{!!$model->getViewTree()!!}--}}
                                    </div>
                                </div>
                            <!-- /Constructor Table -->
                            <!-- Supervisor Table -->
                                <div class="tab-pane fade" id="tab-supervisor">
                                    <h4 class="text-center">@lang('MAIN.TITLE.PROGRAM-CHART')</h4>
                                    <div class="p-3">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div style="width: 100%;">
                                                    <canvas id="canvas-doughnut" width="300" height="250"></canvas>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div style="width: 100%;">
                                                    <canvas id="canvas-bar" width="300" height="250"></canvas>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                @foreach ($model->branches as $branch)
                                                    <h5 class="count-kors branch{{$branch->id}}" data-count-kors="{{$branch->count_kors or '0'}}" style="color: {{$branch->color}};"><i class="icon-square mr-3"></i> {{$branch->name}}</h5>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-3" style="border-top: 1px solid #dddddd;">
                                        <div class="row search-panel mb-3">
                                            <label for="" class="small text-muted font-italic col-form-label col-sm-2">@lang('MAIN.COLUMN.BRANCH')</label>
                                            <div class="col-sm-4">
                                                <select class="form-control" id="selectBranch2">
                                                    <option value="0">@lang('MAIN.PLACEHOLDER.ALL-BRANCHES')</option>
                                                    @foreach($model->branches as $key => $branch)
                                                        <option value="{{$branch->id}}">{{$branch->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <label for="" class="small text-muted font-italic col-form-label col-sm-2">@lang('MAIN.COLUMN.USING')</label>
                                            <div class="col-sm-4">
                                                <select class="form-control" id="selectUsing2">
                                                    <option value="0">@lang('MAIN.PLACEHOLDER.ALL')</option>
                                                    <option value="1">@lang('MAIN.PLACEHOLDER.USING')</option>
                                                    <option value="2">@lang('MAIN.PLACEHOLDER.NOT-USING')</option>
                                                </select>
                                            </div>
                                        </div>
                                        <table class="change-kors table table-striped">
                                            <thead>
                                                <tr>
                                                    <th class="bg-dark text-center pl-4 pr-4">@lang('MAIN.COLUMN.MARKING')</th>
                                                    <th class="bg-dark text-center">@lang('MAIN.COLUMN.NAME')</th>
                                                    <th class="bg-dark text-center">@lang('MAIN.COLUMN.COUNT')</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($model->branches as $branch)
                                                    @foreach($branch->expects()->where('cycle_id','=',$model->cycle_id)->get() as $expect)
                                                        @foreach($expect->specifics as $specific)
                                                            <tr class="kor" data-kod="{{$specific->marking}}" data-frequency="0" data-branch="{{$branch->id}}" data-owner="1">
                                                                <td class="kor-kod">{{$specific->marking}}</td>
                                                                <td class="kor-name">{{$specific->name}}</td>
                                                                <td class="text-center kor-frequency" >0</td>
                                                            </tr>
                                                        @endforeach
                                                    @endforeach
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            <!-- /Supervisor Table -->
                            <!-- Explanatory Note -->
                                <div class="tab-pane fade px-3 pb-3" id="tab-explanatory">
                                    <h4 class="text-center">@lang('MAIN.TITLE.EXPLANATORY-NOTE')</h4>
                                    @if (isset($model->meta->explanatory->path))
                                        <div class="p-3">
                                            <div class="embed-responsive embed-responsive-16by9 mb-3">
                                                <iframe class="embed-responsive-item" frameborder="0" src="{{ $model->getExplanatoryPath() }}"></iframe>
                                            </div>
                                            <div class="text-center">
                                                <a href="{{\Storage::url($model->meta->explanatory->path)}}" download>Вивантажити</a>
                                            </div>
                                        </div>
                                    @else
                                        <p class="p-5 text-center">@lang('MAIN.LABEL.UNDEFINED')</p>
                                    @endif
                                </div>
                            <!-- /Explanatory Note -->
                            <!-- Additional -->
                                <div class="tab-pane fade px-3 pb-3" id="tab-additional">
                                    @if(isset($model->meta->isTitle) && $model->meta->isTitle && isset($model->meta->title))
                                        <div class="mt-3 p-3" style="border: 1px solid #ccc;">{!! $model->meta->title !!}</div>
                                    @endif
                                    @if(isset($model->meta->isDescription) && $model->meta->isDescription && isset($model->meta->description))
                                        <div class="mt-3 p-3" style="border: 1px solid #ccc;">{!! $model->meta->description !!}</div>
                                    @endif
                                    @if(isset($model->meta->isProvisions) && $model->meta->isProvisions && isset($model->meta->provisions))
                                        <div class="mt-3 p-3" style="border: 1px solid #ccc;">{!! $model->meta->provisions !!}</div>
                                    @endif
                                </div>
                            <!-- /Additional -->
                        </div>
                <!-- /Tabs -->

        </div>

    </div>

</div>
@endsection


@section('modals')
<div id="modal-show-materials" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h6 class="modal-title">@lang('MAIN.MODAL.SHOW-MATERIALS.TITLE')</h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn bg-primary" data-dismiss="modal">@lang('MAIN.BUTTON.CLOSE')</button>
                </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
<script src="/js/chart/Chart.min.js" type="text/javascript"></script>
<script src="{{ asset('/js/jquery_ui/interactions.min.js') }}"></script>
<script type="text/javascript">
    var currentElement;

    $(document).on('click','.open-all',function(){
        $( ".element" ).each(function( index ) {
            $(this).removeClass('mb-1');
            $(this).children('.card-header').find('.element-toggle').removeClass('rotate-180');
            $(this).children('.element-container').show();
        });
    });
    $(document).on('click','.close-all',function(){
        $( ".element" ).each(function( index ) {
            $(this).addClass('mb-1');
            $(this).children('.card-header').find('.element-toggle').addClass('rotate-180');
            $(this).children('.element-container').hide();
        });
    });


    $(document).on('click','.element-toggle',function(){
        $(this).toggleClass('rotate-180');
        $(this).closest('.element').children('.element-container').toggle('fast');
    });


    $(document).on('click','.btn-show-materials',function(){
        $('#modal-show-materials .modal-body').empty();
        let kod = $(this).attr('data-kod');
        $.ajax({
            url: '{{route("typmaterials.getList",$model->id)}}',
            type: 'POST',
            dataType : "json",
            data: {
                _token: "{{ csrf_token() }}",
                kod: kod,
            },
        }).done((data, status, xhr) => {
            $('#modal-show-materials .modal-body').html( '<h2 class="text-center">' + $(this).attr('data-kod') + '</h2>' );
            $('#modal-show-materials .modal-body').html( typeof data.data === 'undefined' ? "Детальний опис" : data.data );
        });
    });


    $(document).on('change','.search-panel select',function(){
        showKors();
    });

    function showKors() {
        let flag = true;
        let using = 0;
        let branch = 0;
        $( '.change-kors .kor' ).each(function( index ){
            flag = true;
            element = $(this);
            using = parseInt( $('#selectUsing2').val() );
            branch = parseInt( $('#selectBranch2').val() );
            if ( (using == 1 && element.attr('data-frequency') == "0") ||
                 (using == 2 && element.attr('data-frequency') != "0"))
            {
                flag = false;
            }
            // console.log(branch + ' - ' + element.attr('data-owner') + ' - ' + parseInt(element.attr('data-branch')) );
            if ( (branch == 0 && element.attr('data-owner') != "1") || (branch == -1 && element.attr('data-owner') == "1") || ( (branch > 0 && parseInt(element.attr('data-branch')) != branch)) ){
                flag = false;
            }
            if (flag){
                $(this).show();
            } else {
                $(this).hide();
            }
        });
    }

    function calculateKors() {
        let branchData = {};
        let data1 = [];
        let data2 = [];
        @foreach ($model->branches as $key=>$branch)
            branchData[{{$branch->id}}] = {'key':{{$key}}, 'name':'{{$branch->name}}', 'present':{{$branch->specificCount($model->cycle_id)}}, 'used':0, 'list':{}},
        @endforeach
        $( '#program .kor' ).each(function( index ){
            let code = $(this).children('.kor-kod').text();
            let branch = $(this).children('.kor-kod').attr('data-galuz');
            if (typeof branchData[branch] != 'undefined' ){
                branchData[branch].list[code] = (typeof branchData[branch].list[code] === 'undefined') ? 1 : branchData[branch].list[code] + 1;
            }
        });
        $('.kor').attr('data-frequency',0);
        $('.kor').children('.kor-frequency').text(0);
        $.each(branchData, function( index, value ) {
            branchData[index].used = countProperties(branchData[index].list);
            data1[branchData[index].key] = branchData[index].used;
            barChartData.datasets[ branchData[index].key ].data = branchData[index].present > 0 ? [round10(100 / branchData[index].present * branchData[index].used)] : [0];
            $.each(branchData[index].list, function( index2, value2 ) {
                $('[data-kod="' + index2 + '"]').attr('data-frequency',value2);
                $('[data-kod="' + index2 + '"]').children('.kor-frequency').text(value2);
            });
        });
        doughnutChartData.datasets[0].data = data1;
        window.myBar1.update();
        window.myBar2.update();
    }

    function countProperties(obj) {
        var count = 0;
        for(var prop in obj) {
            if(obj.hasOwnProperty(prop))
                ++count;
        }
        return count;
    }

    function round10(argument) {
        return Math.round( argument * 10 ) / 10;
    }
            window.chartColors = {
                @foreach ($model->branches as $key=>$branch)
                color{{$key}} : '{{$branch->color}}',
                @endforeach
            };
            var color = Chart.helpers.color;

            var doughnutChartData = {
            datasets: [{
                data: [1,2],
                backgroundColor: [
                    window.chartColors.color0,
                    window.chartColors.color1,
                    window.chartColors.color2,
                    window.chartColors.color3,
                    window.chartColors.color4,
                    window.chartColors.color5,
                    window.chartColors.color6,
                    window.chartColors.color7,
                    window.chartColors.color8,
                    window.chartColors.color9
                ],
                label: 'Dataset 0'
            }],
            labels: [
                @foreach ($model->branches as $key=>$branch)
                    "{{$branch->name}}",
                @endforeach
                ]
            };

            var barChartData = {
                datasets: [
                    @foreach ($model->branches as $key=>$branch)
                    {
                        label: '{{$branch->name}}',
                        backgroundColor: color(window.chartColors.color{{$key}}).alpha(0.9).rgbString(),
                        borderColor: color(window.chartColors.color{{$key}}).rgbString(),
                        borderWidth: 1,
                        data: [ {{$key}} ]
                    },
                    @endforeach
                ]

            };

            var ctx1 = document.getElementById("canvas-doughnut").getContext("2d");
            window.myBar1 = new Chart(ctx1, {
                type: 'doughnut',
                data: doughnutChartData,
                options: {
                    responsive: true,
                    legend: {
                        position: 'top',
                        display: false,
                    },
                    title: {
                        display: true,
                        text: 'Відповідно до використаних КОРів (кількість)'
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            });


            var ctx2 = document.getElementById("canvas-bar").getContext("2d");
            window.myBar2 = new Chart(ctx2, {
                type: 'bar',
                data: barChartData,
                options: {
                    responsive: true,
                    legend: {
                        position: 'top',
                        display: false,
                    },
                    title: {
                        display: true,
                        text: 'Відповідно до існуючих КОРів (%)'
                    }
                }
            });

    $('.change-kors tbody').each(function( index ) { sortTable($(this)) })

    function sortTable(container){
        var $tbody = container;
        $tbody.find('tr').sort(function (a, b) {
            var tda = $(a).find('td:eq(' + 0 + ')').text(); // Use your wished column index
            var tdb = $(b).find('td:eq(' + 0 + ')').text(); // Use your wished column index
            // if a < b return 1
            return tda > tdb ? 1
                   // else if a > b return -1
                   : tda < tdb ? -1
                   // else they are equal - return 0    
                   : 0;
        }).appendTo($tbody);
    }

    calculateKors();

</script>
@endsection
