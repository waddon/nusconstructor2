@extends('layouts.app')

@section('content')
<style>
	p {
		text-indent: 1.5em;
	}
</style>
    <div class="card">
        <div class="card-header header-elements-inline alpha-1">
            <h1 class="card-title">{{$model->name_page}}</h1>
        </div>
        <div class="card-body">
            {!! $model->content_page !!}
        </div>
    </div>
@endsection
