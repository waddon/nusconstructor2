@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header header-elements-inline alpha-1">
            <h1 class="card-title">@LANG('MAIN.TITLE.NOTE')</h1>
        </div>
        <div class="card-body">
            <div class="search-panel text-right">
                <div class="btn-group justify-content-center">
                    <a href="#" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">@lang('MAIN.BUTTON.CYCLE')</a>
                    <div class="dropdown-menu">
                            @foreach($cycles as $key => $cycle)
                                <a href="#" class="dropdown-item element-cycle {{ $cycle->id == 1 ? 'active' : ''}}" data-key="{{$cycle->id}}">{{$cycle->name}}</a>
                            @endforeach
                    </div>
                </div>
            </div>
            <div class="ajax-data mt-3">
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    var current_cycle = 1;
    getData();

    $('div.search-panel').on('click','.element-cycle',function(e){
        e.preventDefault();
        current_cycle = $(this).attr('data-key');
        $('.element-cycle').removeClass('active');
        $(this).addClass('active');
        getData();
    });
    
    function getData() {
        $.ajax({
            url: '{{route("getCycle")}}',
            type: 'GET',
            data: { "cycle_id" : current_cycle},
            dataType : "json",
        }).done((data, status, xhr) => {
            $(".ajax-data").empty();
            $(".ajax-data").append(data.cycle.content);
        });
    }
</script>
@endsection
