@extends('layouts.app')

@section('content')
            <div class="card">
                <div class="card-header header-elements-inline alpha-1">
                    <h1 class="card-title">@LANG('MAIN.TITLE.GLOSSARY')</h1>
                </div>
                <div class="card-body">
                    <div class="text-center"><h4>
                        @foreach (range(chr(0xC0), chr(0xDF)) as $key => $char1)
                            <a href="{{route('glossary','cyr'.$key)}}" class="{{ (isset($character) && iconv('CP1251', 'UTF-8', $char1)===$character) ? 'active' : '' }}">{{iconv('CP1251', 'UTF-8', $char1)}}</a>
                        @endforeach
                    </h4></div>
                    <div class="text-center"><h4>
                        @foreach (range('A', 'Z') as $char2)
                            <a href="{{route('glossary',$char2)}}" class="{{ (isset($character) && $char2 === $character) ? 'active' : ''}}">{{$char2}}</a>
                        @endforeach
                    </h4></div>
                    <div class="text-center mb-3"><h4>
                        @foreach (range('0', '9') as $char3)
                            <a href="{{route('glossary',$char3)}}" class="{{ (isset($character) && $char3 === $character) ? 'active' : ''}}">{{$char3}}</a>
                        @endforeach
                    </h4></div>
                    <h1 class="text-center">{{$character}}</h1>
                </div>
                <div class="card-body">
                    @foreach($glossaries as $glossary)
                        <div class="mb-3"}"><strong>{{$glossary->name_post}}</strong> {{ $glossary->excerpt_post }}<br>
                            <a href="{{route('postGetContent', $glossary->id_post)}}" class="list-icons-item text-success-600 btn-ajax-data" data-toggle="modal" data-target="#modal-ajax-data" data-title="{{$glossary->name_post}}" data-color="bg-success" data-method="POST">@lang('MAIN.BUTTON.DETAIL')</a>
                        </div>
                    @endforeach
                </div>
            </div>
@endsection
