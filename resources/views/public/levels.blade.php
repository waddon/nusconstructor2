@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header header-elements-inline alpha-1">
            <h1 class="card-title">@LANG('MAIN.TITLE.LEVELS')</h1>
        </div>
        <div class="card-body">
            @foreach($models as $model)
                <div class="card">
                    <div class="card-header bg-primary">
                        <h6 class="card-title">{{$model->name}}</h6>
                    </div>
                    <div class="card-body bg-light">
                        @foreach($model->cycles as $cycle)
                            <div class="card">
                                <div class="card-header bg-teal">
                                    <h6 class="card-title">{{$cycle->name}}</h6>
                                </div>
                                <div class="card-body">
                                    @foreach($cycle->grades as $grade)
                                        <div class="card">
                                            <div class="card-header bg-warning">
                                                <h6 class="card-title">{{$grade->name}}</h6>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
