@extends('layouts.app')

@section('content')
<style type="text/css">
    table#plan, table.branches{table-layout: fixed;}
    table#plan>thead>tr>th:last-child{width: 140px;}
    table.branches>tbody>tr>td:last-child{width: 50px;}
    table.branches input[type=number]{text-align: center;}
</style>

{{ Form::model($model, [ 'url' => route('tnewplans.update', [ $team->id, $model->id ]) , 'method' => 'PUT', 'id' => 'plan-form', 'name' => 'fileinfo' ]) }}

<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">
        <div class="card mb-3 program-constructor">
                <!-- Search panel -->
                    <div class="search-panel">
                        <div class="text-right">
                            <div class="btn-group">

                                <button type="button" class="btn btn-success save-plan" title="@lang('MAIN.HINT.SAVE')"><i class="icon-floppy-disk"></i></button>
                                <a href="{{ $model->previousUrl }}" class="btn btn-danger" title="@lang('MAIN.HINT.EXIT')"><i class="icon-esc"></i></a>
                            </div>
                            
                        </div>
                    </div>
                <!-- /Search panel -->

                <!-- Tabs -->
                        <ul class="nav nav-tabs nav-tabs-highlight">
                            <li class="nav-item"><a href="#tab-constructor" class="nav-link active" data-toggle="tab"><i class="icon-cube3 mr-2"></i> @lang('MAIN.TAB.CONSTRUCTOR')</a></li>
                        </ul>

                        <div class="tab-content">
                            <!-- Constructor Table -->
                                <div class="tab-pane fade show active" id="tab-constructor">

                                    <div class="card-body">
                                        <div class="form-group row">
                                            {{ Form::label('name', __('MAIN.COLUMN.NAME'), ['class'=>'small text-muted font-italic col-lg-2 col-form-label']) }}
                                            <div class="col-lg-6">
                                                {{ Form::text('name', $model->name, array('class' => 'field form-control')) }}
                                            </div>
                                            {{ Form::label('cycle', __('MAIN.COLUMN.CYCLE'), ['class'=>'small text-muted font-italic col-lg-2 col-form-label']) }}
                                            <div class="col-lg-2">{{$model->cycle->name}}</div>
                                        </div>
                                    </div>
                                    <div class="card-body program p-3" data-children="line">
                                        <table id="plan" class="table table-striped w-100">
                                            <thead class="bg-dark">
                                                <tr>
                                                    <th class="text-center">@lang('MAIN.COLUMN.SUBJECT')</th>
                                                    <th>
                                                        <table class="branches w-100">
                                                            <tr>
                                                                <td class="text-center">@lang('MAIN.COLUMN.BRANCH')</td>
                                                                @foreach($model->cycle->grades as $grade)
                                                                <td class="text-center">{{$grade->name}}</td>
                                                                @endforeach
                                                                <td class="text-center"></td>
                                                            </tr>
                                                        </table>
                                                    </th>
                                                    <th class="text-center">@lang('MAIN.COLUMN.ACTIONS')</th>
                                                </tr>
                                            </thead>
                                            <tbody class="plan-container">
                                                @foreach($model->content as $subject)
                                                    <tr class="subject">
                                                        <td><input type="text" class="form-control name_subject" value="{{$subject->name_subject}}"></td>
                                                        <td class="p-0">
                                                            <table class="branches table-striped w-100">
                                                                <tbody class="branch-container">
                                                                    @foreach($subject->galuzes as $galuz)
                                                                        <tr class="branch">
                                                                            <td class="p-1">
                                                                                <select class="form-control">
                                                                                    @foreach($branches as $branch)
                                                                                        <option value="{{$branch->id}}" {{$branch->id==$galuz->galuz ? 'selected' : ''}}>{{$branch->name}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </td>
                                                                            @foreach($galuz->classes as $grade)
                                                                                <td class="p-1"><input type="number" class="form-control" value="{{$grade->hours}}" data-class="{{$grade->class}}"></td>
                                                                            @endforeach
                                                                            <td class="text-center p-1"><button type="button" class="btn btn-danger btn-remove pt-1 pb-1 pl-2 pr-2"><i class="icon-cross2"></i></button></td>
                                                                        </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                        <td class="text-center">
                                                            <div class="btn-group">
                                                                <button type="button" class="btn btn-success btn-add-branch"><i class="icon-plus3"></i></button>
                                                                <button type="button" class="btn btn-danger btn-remove"><i class="icon-cross2"></i></button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="p-3">
                                        <button type="button" class="btn btn-light btn-add-subject"><i class="icon-googleplus5 icon-2x mr-2"></i>@lang(
                                        'MAIN.BUTTON.ADD-SUBJECT')</button>
                                    </div>
                                </div>
                            <!-- /Constructor Table -->
                        </div>
                <!-- /Tabs -->

        </div>

    </div>

</div>


{{ Form::close() }}
@endsection

@section('scripts')
<script src="{{ asset('/js/jquery_ui/interactions.min.js') }}"></script>
<script type="text/javascript">
    // sortableRun();

    // function sortableRun() {
    //     $('.program').sortable({
    //         // connectWith: '.element-container',
    //         handle: ".card-header",
    //         tolerance: 'pointer',
    //         opacity: 0.6,
    //         placeholder: 'sortable-placeholder',
    //         start: function(e, ui){
    //             ui.placeholder.height(ui.item.outerHeight());
    //         }
    //     }).disableSelection();

    //     $('.problems').sortable({
    //         connectWith: '.problems',
    //         handle: ".card-header",
    //         tolerance: 'pointer',
    //         opacity: 0.6,
    //         placeholder: 'sortable-placeholder',
    //         start: function(e, ui){
    //             ui.placeholder.height(ui.item.outerHeight());
    //         }
    //     }).disableSelection();

    //     $('.opises').sortable({
    //         connectWith: '.opises',
    //         handle: ".card-header",
    //         tolerance: 'pointer',
    //         opacity: 0.6,
    //         placeholder: 'sortable-placeholder',
    //         start: function(e, ui){
    //             ui.placeholder.height(ui.item.outerHeight());
    //         }
    //     }).disableSelection();
    // }

    $(document).on('click','.btn-add-subject', function(){
        $('.plan-container').append( renderTemplate( 'subject-empty', {} ) );
        // sortableRun();
    });

    $(document).on('click','.btn-add-branch', function(){
        $(this).closest('.subject').find('.branch-container').append( renderTemplate( 'branch-empty', {} ) );
        // sortableRun();
    });

    $(document).on('click','.btn-remove',function(){
        swal({
            title: "@lang('MAIN.MODAL.DESTROY.MESSAGE')",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "@lang('MAIN.BUTTON.DESTROY')",
            confirmButtonClass: "btn btn-warning",
            cancelButtonText: "@lang('MAIN.BUTTON.CANCEL')",
            cancelButtonClass: "btn btn-light",
        }).then((confirm) => {
            if(confirm.value){
                $(this).closest('tr').remove();
            }
        });
    });

    function assemblyPlan() {
        let plan = new Object();
        $(".subject").each(function( index ) {
            let subject = {};
            subject.type_subject = 2;
            subject.name_subject = $(this).find('.name_subject').val();
            subject.program = 0;
            subject.name_program = '';
            let branches = {};
            $(this).find('.branch').each(function( index2 ) {
                let branch = {};
                branch.galuz = $(this).find('select').first().val();
                let grades = {}
                $(this).find('input').each(function( index3 ) {
                    let grade = {}
                    grade.class = $(this).attr('data-class');
                    grade.hours = $(this).val();
                    grades[index3] = grade;
                });
                branch.classes = grades;
                branches[index2] = branch;
            });
            subject.galuzes = branches;
            plan[index]  =  subject;
        });

        return plan;
    };

    $(document).on('click','.save-plan',function(){
        let form = $('#plan-form');
        let formData = new FormData( document.forms.namedItem("fileinfo") );
        formData.append('plan', JSON.stringify( assemblyPlan() ) );

        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: formData,
            processData: false,
            contentType: false,
        }).done((data, status, xhr) => {
            swal({
                title: data.message,
                type: 'success',
                showCancelButton: true,
                confirmButtonText: "Вийти до переліку",
                confirmButtonClass: 'btn btn-primary',
                cancelButtonText: "Продовжити редагування",
                cancelButtonClass: 'btn btn-light',
            }).then((confirm) => {
                if(confirm.value){
                    window.location.href = data.exitURL;
                }
            });
            form.find('fieldset').attr('disabled', true);
        }).fail((xhr) => {
            let data = xhr.responseJSON;
            swal({
                title: data.message,
                type: 'error',
                confirmButtonClass: 'btn btn-primary',
            })                    
        });
    });

</script>
@endsection

@section('modals')
@endsection

@section('templates')
<template id="subject-empty">
    <tr class="subject">
        <td><input type="text" class="form-control name_subject"></td>
        <td class="p-0">
            <table class="branches table-striped w-100">
                <tbody class="branch-container"></tbody>
            </table>
        </td>
        <td class="text-center">
            <div class="btn-group">
                <button type="button" class="btn btn-success btn-add-branch"><i class="icon-plus3"></i></button>
                <button type="button" class="btn btn-danger btn-remove"><i class="icon-cross2"></i></button>
            </div>
        </td>
    </tr>
</template>

<template id="branch-empty">
    <tr class="branch">
        <td class="p-1">
            <select class="form-control">
                @foreach($branches as $branch)
                    <option value="{{$branch->id}}">{{$branch->name}}</option>
                @endforeach
            </select>
        </td>
        @foreach($model->cycle->grades as $grade)
            <td class="p-1"><input type="number" class="form-control" data-class="{{$grade->id}}"></td>
        @endforeach
        <td class="text-center p-1"><button type="button" class="btn btn-danger btn-remove pt-1 pb-1 pl-2 pr-2"><i class="icon-cross2"></i></button></td>
    </tr>
</template>
@endsection
