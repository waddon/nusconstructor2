@extends('layouts.app')

@section('content')
<style type="text/css">
    table#plan, table.branches{table-layout: fixed;}
    table#plan>thead>tr>th:first-child{width: 35%;}
    table.branches>tbody>tr>td:first-child{width: 50%;}
</style>

<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">

        <div class="card mb-3 program-constructor">
                <!-- Search panel -->
                    <div class="search-panel">
                        <h2>Перегляд плану "{{$model->name}}"</h2>
                        <div class="text-right">
                            <div class="btn-group">

                                <a href="{{route('loadplan', [ $model->key ])}}" class="btn btn-danger" title="@lang('MAIN.HINT.DOWNLOAD')" target="_blank"><i class="icon-file-pdf"></i></a>

                                @if(isset($model->previousUrl))
                                    <a href="{{ $model->previousUrl }}" class="btn btn-danger" title="@lang('MAIN.HINT.EXIT')"><i class="icon-esc"></i></a>
                                @endif
                            </div>
                            
                        </div>
                    </div>
                <!-- /Search panel -->

                <!-- Tabs -->
                        <ul class="nav nav-tabs nav-tabs-highlight">
                            <li class="nav-item"><a href="#tab-constructor" class="nav-link active" data-toggle="tab"><i class="icon-eye mr-2"></i> @lang('MAIN.TAB.SHOW')</a></li>
                        </ul>

                        <div class="tab-content">
                            <!-- Constructor Table -->
                                <div class="tab-pane fade show active" id="tab-constructor">
                                    <table id="plan" class="table table-striped w-100 mb-3">
                                        <thead class="bg-dark">
                                            <tr>
                                                <th class="text-center">@lang('MAIN.COLUMN.SUBJECT')</th>
                                                <th>
                                                    <table class="branches w-100">
                                                        <tr>
                                                            <td class="text-center">@lang('MAIN.COLUMN.BRANCH')</td>
                                                            @foreach($model->cycle->grades as $grade)
                                                            <td class="text-center">{{$grade->name}}</td>
                                                            @endforeach
                                                        </tr>
                                                    </table>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody class="plan-container">
                                            @foreach($model->content as $subject)
                                                <tr class="subject">
                                                    <td><h4 class="mb-0">{{$subject->name_subject}}</h4></td>
                                                    <td class="">
                                                        <table class="branches table-striped w-100">
                                                            <tbody class="branch-container">
                                                                @foreach($subject->galuzes as $galuz)
                                                                    <tr class="branch">
                                                                        <td class="">
                                                                            {{$branches->find($galuz->galuz)->name ?? ''}}
                                                                        </td>
                                                                        @foreach($galuz->classes as $grade)
                                                                            <td class="text-center">{{$grade->hours}}</td>
                                                                        @endforeach
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            <!-- /Constructor Table -->
                        </div>
                <!-- /Tabs -->

        </div>

    </div>

</div>


@endsection
