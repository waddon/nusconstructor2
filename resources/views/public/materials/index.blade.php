@extends('layouts.app')

@section('content')
<div class="card mb-3">

    <!-- Search panel -->
        <div class="search-panel">
            <div class="text-right">
                <div class="btn-group">
                    <button type="button" class="btn btn-primary datatableRefresh" title="@lang('MAIN.HINT.REFRESH')"><i class="icon-sync"></i></button>
                </div>                
                
            </div>
        </div>
    <!-- /Search panel -->

    <!-- Tabs -->
            <ul class="nav nav-tabs nav-tabs-highlight">
                <li class="nav-item"><a href="#tab-active" class="nav-link active" data-toggle="tab"><i class="icon-menu7 mr-2"></i> @lang('MAIN.TAB.ACTIVE')</a></li>
            </ul>

            <div class="tab-content">
                <!-- Active Table -->
                    <div class="tab-pane fade show active" id="tab-active">
                        <div class="table-responsive">
                            <table class="table table-bordered table-styled table-striped mb-3 w-100 datatable" id="datatableActive">
                                <thead class="bg-indigo">
                                    <tr>
                                        <th>@lang('MAIN.COLUMN.ID')</th>
                                        <th>@lang('MAIN.COLUMN.NAME')</th>
                                        <th>@lang('MAIN.COLUMN.SPECIFICS')</th>
                                        <th>@lang('MAIN.COLUMN.URL')</th>
                                        <th>@lang('MAIN.COLUMN.INFO')</th>
                                        <th>@lang('MAIN.COLUMN.ACTIONS')</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                <!-- /Active Table -->
            </div>
    <!-- /Tabs -->

</div>
@endsection

@section('scripts')
<script src="/js/datatables/datatables.min.js"></script>
<script src="/js/select2/select2.min.js"></script>
<script>
    var language = {"url": "/js/datatables/languages/ukrainian.json"};
    var pageLength = {{ isset(auth()->user()->options->datatablePageLength) ? auth()->user()->options->datatablePageLength : 10 }};
    var current_cycle = 0;
    var current_branch = 0;
    var columns = [
            {"data":"id"},
            {"data":"name"},
            {"data":"specific","searchable":false,"orderable":false},
            {"data":"url","searchable":false,"orderable":false},
            {"data":"info","searchable":false,"orderable":false},
            {"data":"actions","searchable":false,"orderable":false}
        ];
    var columnDefs = [
            { "targets": 0, "className": "text-center", },
            { "targets": 2, "className": "text-center", },
            { "targets": 4, "className": "text-center", },
        ];


    var tableActive = $("#datatableActive").DataTable({
        autoWidth : true,
        responsive: true,
        processing: true,
        serverSide: true,
        pageLength: pageLength,
        bSortCellsTop: true,
        order: [[ 0, "desc" ]],
        ajax: {
            url: "{{route('typmaterials.data')}}",
            dataType: "json",
            type: "POST",
            data: function ( d ) {
                d._token = "{{ csrf_token() }}";
                d.cycle_id = current_cycle;
                d.branch_id = current_branch;
                d.table = "active";
            }
        },
        columns:columns,
        columnDefs: columnDefs,
        language: language,
    });

</script>
@endsection


@section('modals')
<div id="modal-copy-team-program" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h6 class="modal-title">@lang('MAIN.MODAL.COPY-TEAM.TITLE')</h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form id="modal-copy-team-program-form" action="#" method="POST">
                        @csrf
                        <select class="field form-control select-remote-data" data-fouc id="team_id" name="team_id">
                            <option value="0" selected>@lang('MAIN.PLACEHOLDER.SELECT-TEAM')</option>
                        </select>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">@lang('MAIN.BUTTON.CANCEL')</button>
                    <button type="button" class="btn bg-primary" id="modal-copy-team-program-submit" data-dismiss="modal">@lang('MAIN.BUTTON.COPY')</button>
                </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
        $(document).on('click','.btn-copy-team-program',function () {
            $('#modal-copy-team-program-form').attr('action', $(this).attr('href'));
        })

        $('#modal-copy-team-program-submit').click(function(e){
            let form = $('#modal-copy-team-program-form');
            e.preventDefault();
            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                dataType : "json",
                data: form.serialize(),
            }).done((data, status, xhr) => {
                swal({
                    title: data.message,
                    type: 'success',
                    confirmButtonClass: 'btn btn-primary',
                })
            }).fail((xhr) => {
                let data = xhr.responseJSON;
                swal({
                    title: data.message,
                    type: 'error',
                    confirmButtonClass: 'btn btn-primary',
                })
            });
        });



    $('#team_id').select2({
        ajax: {
            url: "{{route('teams-select2')}}",
            type: 'get',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term,
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        minimumInputLength: 0,
        templateResult: formatRepo, // omitted for brevity, see the source of this page
        templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
    });
    
    function formatRepo (repo) {
        if (repo.loading) return repo.name;
        var markup = '<div class="select2-result-repository clearfix">';
                //markup += '<div class="select2-result-repository__avatar"><img src="' + repo.avatar + '"></div>';
                //markup += '<div class="select2-result-repository__meta">';
                    markup += '<div class="select2-result-repository__title">' + repo.name + '</div>';
                    markup += '<div class="select2-result-repository__description"></div>';
                    markup += '<div class="select2-result-repository__statistics">';
                        markup += '<div class="select2-result-repository__forks">@lang("MAIN.COLUMN.LEADER"): ' + repo.leader + '</div>';
                        markup += '<div class="select2-result-repository__stargazers">@lang("MAIN.COLUMN.TTYPE"): ' + repo.type + '</div>';
                        markup += '<div class="select2-result-repository__watchers">@lang("MAIN.COLUMN.USERS"): ' + repo.cusers + '</div>';
                    markup += '</div>';
                //markup += '</div>';
            markup += '</div>';
        return markup;
    }

    function formatRepoSelection (repo) {
        return repo.name || repo.text;
    }
});
</script>
@endsection