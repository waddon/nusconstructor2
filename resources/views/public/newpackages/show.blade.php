@extends('layouts.app')

@section('content')

<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">

        <div class="card mb-3 program-constructor">
                <!-- Search panel -->
                    <div class="search-panel">
                        <h2 class="text-center mb-5">{{$model->name}}</h2>
                    </div>
                <!-- /Search panel -->

                <!-- Tabs -->
                        <ul class="nav nav-tabs nav-tabs-highlight">
                            <li class="nav-item"><a href="#tab-explanatory" class="nav-link active" data-toggle="tab"><i class="icon-file-text2 mr-2"></i> @lang('MAIN.TAB.EXPLANATORY-NOTE')</a></li>
                            <li class="nav-item"><a href="#tab-programs" class="nav-link" data-toggle="tab"><i class="icon-list mr-2"></i> @lang('MAIN.TAB.PROGRAMS')</a></li>
                            <li class="nav-item"><a href="#tab-supervisor" class="nav-link" data-toggle="tab"><i class="icon-pie-chart3 mr-2"></i> @lang('MAIN.TAB.STATISTIC')</a></li>
                        </ul>

                        <div class="tab-content">
                            <!-- Explanatory Note -->
                                <div class="tab-pane fade show active p-3" id="tab-explanatory">
                                    {!! $model->content !!}
                                </div>
                            <!-- /Explanatory Note -->
                            <!-- Programs -->
                                <div class="tab-pane fade p-3" id="tab-programs">
                                    <table id="plan" class="table table-striped w-100">
                                        <thead class="bg-dark">
                                            <tr>
                                                <th class="text-center">@lang('MAIN.COLUMN.SUBJECT')</th>
                                                <th></th>
                                                <th class="text-center">@lang('MAIN.COLUMN.PROGRAM')</th>
                                            </tr>
                                        </thead>
                                        <tbody class="plan-container">
                                            @if(isset($model->newplan->content) && $model->newplan->content)
                                                @foreach($model->newplan->content as $key => $subject)
                                                    <tr class="subject">
                                                        <td>{{$subject->name_subject}}</td>
                                                        <td class="p-0"></td>
                                                        <td class="text-center">
                                                            {!! $model->getProgramLink(isset($model->meta->programs[$key]) ? $model->meta->programs[$key] : 0) !!}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            <!-- /Programs -->
                            <!-- Supervisor -->
                                <div class="tab-pane fade" id="tab-supervisor">
                                    <div class="p-3">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div style="width: 100%;">
                                                    <canvas id="canvas-doughnut" width="300" height="250"></canvas>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div style="width: 100%;">
                                                    <canvas id="canvas-bar" width="300" height="250"></canvas>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                @foreach ($branches as $branch)
                                                    <h5 class="count-kors branch{{$branch->id}}" data-count-kors="{{$branch->count_kors or '0'}}" style="color: {{$branch->color}};"><i class="icon-square mr-3"></i> {{$branch->name}}</h5>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>

                                    <div class="p-3" style="border-top: 1px solid #dddddd;">
                                        <div class="row search-panel mb-3">
                                            <label for="" class="small text-muted font-italic col-form-label col-sm-2">@lang('MAIN.COLUMN.BRANCH')</label>
                                            <div class="col-sm-4">
                                                <select class="form-control" id="selectBranch2">
                                                    <option value="0">@lang('MAIN.PLACEHOLDER.ALL-BRANCHES')</option>
                                                    @foreach($branches as $key => $branch)
                                                        <option value="{{$branch->id}}">{{$branch->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <label for="" class="small text-muted font-italic col-form-label col-sm-2">@lang('MAIN.COLUMN.USING')</label>
                                            <div class="col-sm-4">
                                                <select class="form-control" id="selectUsing2">
                                                    <option value="0">@lang('MAIN.PLACEHOLDER.ALL')</option>
                                                    <option value="1">@lang('MAIN.PLACEHOLDER.USING')</option>
                                                    <option value="2">@lang('MAIN.PLACEHOLDER.NOT-USING')</option>
                                                </select>
                                            </div>
                                        </div>
                                        <table class="change-kors table table-striped">
                                            <thead>
                                                <tr>
                                                    <th class="bg-dark text-center pl-4 pr-4">@lang('MAIN.COLUMN.MARKING')</th>
                                                    <th class="bg-dark text-center">@lang('MAIN.COLUMN.NAME')</th>
                                                    <th class="bg-dark text-center">@lang('MAIN.COLUMN.COUNT')</th>
                                                </tr>
                                            </thead>
                                            <tbody id="program">
                                                @foreach($object as $branch)
                                                    @foreach($branch->specifics as $specific)
                                                    <tr class="kor" data-kod="{{$specific->marking}}" data-frequency="{{$specific->frequency}}" data-branch="{{$branch->id}}" data-owner="1">
                                                        <td class="kor-kod">{{$specific->marking}}</td>
                                                        <td class="kor-name">{{$specific->name}}</td>
                                                        <td class="text-center kor-frequency">{{$specific->frequency}}</td>
                                                    </tr>
                                                    @endforeach
                                                @endforeach                               
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            <!-- /Supervisor -->
                        </div>
                <!-- /Tabs -->

        </div>

    </div>

</div>

@endsection

@section('scripts')
<script src="/js/chart/Chart.min.js" type="text/javascript"></script>
<script src="{{ asset('/js/jquery_ui/interactions.min.js') }}"></script>
<script type="text/javascript">
    var currentElement;

    $(document).on('change','.search-panel select',function(){
        showKors();
    });

    function showKors() {
        let flag = true;
        let using = 0;
        let branch = 0;
        $( '.change-kors .kor' ).each(function( index ){
            flag = true;
            element = $(this);
            using = parseInt( $('#selectUsing2').val() );
            branch = parseInt( $('#selectBranch2').val() );
            if ( (using == 1 && element.attr('data-frequency') == "0") ||
                 (using == 2 && element.attr('data-frequency') != "0"))
            {
                flag = false;
            }
            // console.log(branch + ' - ' + element.attr('data-owner') + ' - ' + parseInt(element.attr('data-branch')) );
            if ( (branch == 0 && element.attr('data-owner') != "1") || (branch == -1 && element.attr('data-owner') == "1") || ( (branch > 0 && parseInt(element.attr('data-branch')) != branch)) ){
                flag = false;
            }
            if (flag){
                $(this).show();
            } else {
                $(this).hide();
            }
        });
    }

    function calculateKors() {
        let branchData = {};
        let data1 = [];
        let data2 = [];
        @foreach ($branches as $key=>$branch)
            branchData[{{$branch->id}}] = {'key':{{$key}}, 'name':'{{$branch->name}}', 'present':{{$branch->specificCount($model->newplan->cycle_id)}}, 'used':0, 'list':{}},
        @endforeach
        $( '#program .kor' ).each(function( index ){
            let code = $(this).attr('data-kod');
            let branch = $(this).attr('data-branch');
            if (typeof branchData[branch] != 'undefined' ){
                branchData[branch].used = (typeof branchData[branch].used === 'undefined') ? parseInt($(this).attr('data-frequency')) : branchData[branch].used + parseInt($(this).attr('data-frequency'));
            }
        });
        $.each(branchData, function( index, value ) {
            data1[branchData[index].key] = branchData[index].used;
            barChartData.datasets[ branchData[index].key ].data = branchData[index].present > 0 ? [round10(100 / branchData[index].present * branchData[index].used)] : [0];
        });
        doughnutChartData.datasets[0].data = data1;
        window.myBar1.update();
        window.myBar2.update();
    }

    function countProperties(obj) {
        var count = 0;
        for(var prop in obj) {
            if(obj.hasOwnProperty(prop))
                ++count;
        }
        return count;
    }

    function round10(argument) {
        return Math.round( argument * 10 ) / 10;
    }
            window.chartColors = {
                @foreach ($branches as $key=>$branch)
                color{{$key}} : '{{$branch->color}}',
                @endforeach
            };
            var color = Chart.helpers.color;

            var doughnutChartData = {
            datasets: [{
                data: [1,2],
                backgroundColor: [
                    window.chartColors.color0,
                    window.chartColors.color1,
                    window.chartColors.color2,
                    window.chartColors.color3,
                    window.chartColors.color4,
                    window.chartColors.color5,
                    window.chartColors.color6,
                    window.chartColors.color7,
                    window.chartColors.color8,
                    window.chartColors.color9
                ],
                label: 'Dataset 0'
            }],
            labels: [
                @foreach ($branches as $key=>$branch)
                    "{{$branch->name}}",
                @endforeach
                ]
            };

            var barChartData = {
                datasets: [
                    @foreach ($branches as $key=>$branch)
                    {
                        label: '{{$branch->name}}',
                        backgroundColor: color(window.chartColors.color{{$key}}).alpha(0.9).rgbString(),
                        borderColor: color(window.chartColors.color{{$key}}).rgbString(),
                        borderWidth: 1,
                        data: [ {{$key}} ]
                    },
                    @endforeach
                ]

            };

            var ctx1 = document.getElementById("canvas-doughnut").getContext("2d");
            window.myBar1 = new Chart(ctx1, {
                type: 'doughnut',
                data: doughnutChartData,
                options: {
                    responsive: true,
                    legend: {
                        position: 'top',
                        display: false,
                    },
                    title: {
                        display: true,
                        text: 'Відповідно до використаних КОРів (кількість)'
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            });


            var ctx2 = document.getElementById("canvas-bar").getContext("2d");
            window.myBar2 = new Chart(ctx2, {
                type: 'bar',
                data: barChartData,
                options: {
                    responsive: true,
                    legend: {
                        position: 'top',
                        display: false,
                    },
                    title: {
                        display: true,
                        text: 'Відповідно до існуючих КОРів (%)'
                    }
                }
            });

    calculateKors();

</script>
@endsection
