@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header header-elements-inline alpha-1">
            <h1 class="card-title">@LANG('MAIN.TITLE.EXPECTS')</h1>
        </div>
        <div class="card-body">
            <div class="search-panel text-right">
                <div class="btn-group justify-content-center">
                    <a href="#" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">@lang('MAIN.BUTTON.BRANCH')</a>
                    <div class="dropdown-menu">
                            @foreach($branches as $key => $branch)
                                <a href="#" class="dropdown-item element-branch {{ $branch->id == 1 ? 'active' : ''}}" data-key="{{$branch->id}}">{{$branch->name}}</a>
                            @endforeach
                    </div>
                </div>
                <div class="btn-group justify-content-center">
                    <a href="#" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">@lang('MAIN.BUTTON.CYCLE')</a>
                    <div class="dropdown-menu">
                            @foreach($cycles as $key => $cycle)
                                <a href="#" class="dropdown-item element-cycle {{ $cycle->id == 1 ? 'active' : ''}}" data-key="{{$cycle->id}}">{{$cycle->name}}</a>
                            @endforeach
                    </div>
                </div>
            </div>
            <div class="ajax-data">
                <table class="table table-styled table-striped mt-3">
                    <thead class="bg-dark">
                        <tr>
                            <th>#</th>
                            <th>Загальні результати навчання здобувачів освіти</th>
                            <th>Обов’язкові результати навчання здобувачів освіти</th>
                        </tr>
                    </thead>
                    <tbody id="view-expects"></tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    var current_branch = 1;
    var current_cycle = 1;
    getData();

    $('div.search-panel').on('click','.element-branch',function(e){
        e.preventDefault();
        current_branch = $(this).attr('data-key');
        $('.element-branch').removeClass('active');
        $(this).addClass('active');
        getData();
    });

    $('div.search-panel').on('click','.element-cycle',function(e){
        e.preventDefault();
        current_cycle = $(this).attr('data-key');
        $('.element-cycle').removeClass('active');
        $(this).addClass('active');
        getData();
    });
    
    function getData() {
        $.ajax({
            url: '{{route("getDataExpects")}}',
            type: 'GET',
            data: { "branch_id" : current_branch, "cycle_id" : current_cycle},
            dataType : "json",
        }).done((data, status, xhr) => {
            // $('.ajax-data').html( typeof data.data === 'undefined' ? "Помилка отримання" : data.data );
            // console.log(data.models);
            $("#view-expects").empty();
            temp = '';
            data.models.forEach(function(item, i, result) {
                if (item.group != temp) {
                    temp = item.group;
                    $("#view-expects").append('<tr><td colspan="3" style="font-weight: bold;">'+item.group+'</td></tr>');
                }
                $("#view-expects").append('<tr><td>'+item.marking+'</td><td>'+item.skillName+'</td><td>'+item.name+'</td></tr>');
            });
        });
    }
</script>
@endsection
