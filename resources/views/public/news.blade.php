@extends('layouts.app')

@section('content')
            <div class="card">
                <div class="card-header header-elements-inline alpha-1">
                    <h1 class="card-title">@LANG('MAIN.TITLE.NEWS')</h1>
                </div>
                <div class="card-body">
                	@foreach($models as $model)
                        <div class="mb-3"}"><h3><strong>{{ $model->created_at->format('d.m.Y') }}</strong> - {{ $model->name_post }}</h3>
                            <a href="{{route('postGetContent', $model->id_post)}}" class="list-icons-item text-success-600 btn-ajax-data" data-toggle="modal" data-target="#modal-ajax-data" data-title="{{$model->name_post}}" data-color="bg-success" data-method="POST">@lang('MAIN.BUTTON.DETAIL')</a>
                        </div>
                	@endforeach
	                {{ $models->links() }}
                </div>
            </div>
@endsection
