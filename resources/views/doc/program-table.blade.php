<html>
<head>
  <meta charset="utf-8">
  <style>
    .program {
      width: 100%;
      border: 1px solid #222;
      border-collapse: collapse;
      font-family: arial;
    }
    .program th {
      background: #ccf;
    }
    .program td {
      vertical-align: top;
    }
    .program th, .program td {
      padding: 10px;
      border: 1px solid #222;
    }
  </style>
</head>
<body>

  {{-- Титульна сторінка --}}
  @if(isset($model->meta->isTitle) && $model->meta->isTitle && isset($model->meta->title))
    <div style="font-family: arial;">{!! $model->meta->title !!}</div>
    <br style="page-break-before: always">
  @endif

  {{-- Вступна частина (Пояснювальна записка) --}}
  @if(isset($model->meta->isDescription) && $model->meta->isDescription && isset($model->meta->description))
    <div style="font-family: arial;">{!! $model->meta->description !!}</div>
    <br style="page-break-before: always">
  @endif

  {{-- Основна таблиця --}}
  <h2 style="text-align: center;font-family: arial;">{{ $model->cycle->grades->implode('name', ', ') }}</h2>
  <table class="program">
    <thead>
      <tr>
        @php($colspan = count($model->getOutputTableHead()))
        @foreach($model->getOutputTableHead() as $cell)
          <th>{!! $cell !!}</th>
        @endforeach
      </tr>
    </thead>
    <tbody>
        @foreach($model->getOutputTableBody() as $row)
          <tr>
            @foreach($row as $cell)
              <td {{ count($row) == 1 ? 'colspan="' . $colspan . '"' : '' }}>{!! $cell !!}</td>
            @endforeach
          </tr>
        @endforeach
    </tbody>
  </table>

  {{-- Прикінцева частина --}}
  @if(isset($model->meta->isProvisions) && $model->meta->isProvisions && isset($model->meta->provisions))
    <br style="page-break-before: always">
    <div style="font-family: arial;">{!! $model->meta->provisions !!}</div>
  @endif

</body>
</html>