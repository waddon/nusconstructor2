@extends('layouts.empty')

@section('content')
    <div class="text-center mb-3">
        <h1 class="error-title">503</h1>
        <h5>@lang('MAIN.ERROR.503')</h5>
    </div>
@endsection
