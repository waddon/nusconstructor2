@extends('layouts.guest')

@section('content')
    <div class="text-center mb-3">
        <h1 class="error-title">419</h1>
        <h5>@lang('MAIN.ERROR.419')</h5>
    </div>
    <div class="row">
        <div class="col-xl-4 offset-xl-4 col-md-8 offset-md-2">
            <div class="row">
                <div class="col-sm-6">
                    <a href="{{route('home')}}" class="btn btn-primary btn-block"><i class="icon-home2 mr-2"></i>@lang('MAIN.BUTTON.HOME')</a>
                </div>
                <div class="col-sm-6">
                    <button href="#" class="btn btn-light btn-block mt-3 mt-sm-0" onclick="window.history.back();"><i class="icon-arrow-left8 mr-2"></i> @lang('MAIN.BUTTON.BACK')</button>
                </div>
            </div>
        </div>
    </div>
@endsection
