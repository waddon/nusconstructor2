@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header header-elements-inline alpha-1">
            <h1 class="card-title">@lang('MAIN.TITLE.MY-TEAMS')</h1>
        </div>
        <div class="card-body">
            @livewire('user.teams')
       </div>
    </div>
@endsection

@section('scripts')
<script src="/js/select2/select2.min.js"></script>
@endsection

@section('modals')
<div id="modal-apply" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h6 class="modal-title">@lang('MAIN.MODAL.APPLY.TITLE')</h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <p><i>У переліку відображено перші 10 команд. Для взужування кола або пошуку необхідної команди почніть вводити назву.</i></p>
                    <form id="modal-apply-form" action="{{ route('apply') }}" method="POST">
                        @csrf
                        <select class="field form-control select-remote-data" data-fouc id="team_id" name="team_id">
                            <option value="0" selected>@lang('MAIN.PLACEHOLDER.SELECT-TEAM')</option>
                        </select>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">@lang('MAIN.BUTTON.CANCEL')</button>
                    <button type="button" class="btn bg-primary" id="modal-apply-submit" data-dismiss="modal">@lang('MAIN.BUTTON.APPLY')</button>
                </div>
        </div>
    </div>
</div>

<script type="text/javascript">

$(document).ready(function(){

        $('#modal-apply-submit').click(function(e){
            let form = $('#modal-apply-form');
            e.preventDefault();
            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                dataType : "json",
                data: form.serialize(),
            }).done((data, status, xhr) => {
                window.livewire.emit('refresh');
            }).fail((xhr) => {
                let data = xhr.responseJSON;
                swal({
                    title: data.message,
                    type: 'error',
                    confirmButtonClass: 'btn btn-primary',
                })
            });
        });

    $('#team_id').select2({
        ajax: {
            url: "{{route('teams-all-select2')}}",
            type: 'get',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term,
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        minimumInputLength: 0,
        templateResult: formatRepo, // omitted for brevity, see the source of this page
        templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
    });
    
    function formatRepo (repo) {
        if (repo.loading) return repo.name;
        var markup = '<div class="select2-result-repository clearfix">';
                //markup += '<div class="select2-result-repository__avatar"><img src="' + repo.avatar + '"></div>';
                //markup += '<div class="select2-result-repository__meta">';
                    markup += '<div class="select2-result-repository__title">' + repo.name + '</div>';
                    markup += '<div class="select2-result-repository__description"></div>';
                    markup += '<div class="select2-result-repository__statistics">';
                        markup += '<div class="select2-result-repository__forks">@lang("MAIN.COLUMN.LEADER"): ' + repo.leader + '</div>';
                        markup += '<div class="select2-result-repository__stargazers">@lang("MAIN.COLUMN.TTYPE"): ' + repo.type + '</div>';
                        markup += '<div class="select2-result-repository__watchers">@lang("MAIN.COLUMN.USERS"): ' + repo.cusers + '</div>';
                    markup += '</div>';
                //markup += '</div>';
            markup += '</div>';
        return markup;
    }

    function formatRepoSelection (repo) {
        return repo.name || repo.text;
    }   
});
</script>
@endsection
