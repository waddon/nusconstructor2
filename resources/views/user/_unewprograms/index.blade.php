@extends('layouts.app')

@section('content')
<div class="card mb-3">

    <!-- Search panel -->
        <div class="search-panel">
            <div class="text-right">
                <div class="btn-group">
                    <div class="btn-group justify-content-center">
                        <a href="#" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">@lang('MAIN.BUTTON.CIKL')</a>
                        <div class="dropdown-menu">
                                @foreach($cycles as $key => $cycle)
                                    <a href="#" class="dropdown-item element-cycle {{!$key ? 'active' : ''}}" data-key="{{$key}}">{{$cycle}}</a>
                                @endforeach
                        </div>
                    </div>
                    <div class="btn-group justify-content-center">
                        <a href="#" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">@lang('MAIN.BUTTON.GALUZ')</a>
                        <div class="dropdown-menu">
                                @foreach($branches2 as $key => $branch)
                                    <a href="#" class="dropdown-item element-branch {{!$key ? 'active' : ''}}" data-key="{{$key}}">{{$branch}}</a>
                                @endforeach
                        </div>
                    </div>
                    <a class="btn btn-success btn-create-program" href="{{route('unewprograms.create')}}" title="@lang('MAIN.HINT.CREATE')"  title="'.__('MAIN.HINT.RESTORE').'" data-toggle="modal" data-target="#modal-create-program" data-title="'.__('MAIN.MODAL.RESTORE.TITLE').'" data-text="'.__('MAIN.MODAL.RESTORE.MESSAGE').'" data-color="bg-success" data-method="POST"><i class="icon-file-plus"></i></a>

                    <button type="button" class="btn btn-primary datatableRefresh" title="@lang('Refresh')"><i class="icon-sync"></i></button>
                </div>                
                
            </div>
        </div>
    <!-- /Search panel -->

    <!-- Tabs -->
            <ul class="nav nav-tabs nav-tabs-highlight">
                <li class="nav-item"><a href="#tab-active" class="nav-link active" data-toggle="tab"><i class="icon-menu7 mr-2"></i> @lang('MAIN.TAB.ACTIVE')</a></li>
                <li class="nav-item"><a href="#tab-trash" class="nav-link" data-toggle="tab"><i class="icon-trash mr-2"></i> @lang('MAIN.TAB.TRASH')</a></li>
            </ul>

            <div class="tab-content">
                <!-- Active Table -->
                    <div class="tab-pane fade show active" id="tab-active">
                        <div class="table-responsive">
                            <table class="table table-bordered table-styled table-striped mb-3 w-100 datatable" id="datatableActive">
                                <thead class="bg-indigo">
                                    <tr>
                                        <th>@lang('MAIN.COLUMN.ID')</th>
                                        <th>@lang('MAIN.COLUMN.NAME')</th>
                                        <th>@lang('MAIN.COLUMN.CYCLE')</th>
                                        {{--<th>@lang('MAIN.COLUMN.PERIOD')</th>--}}
                                        <th>@lang('MAIN.COLUMN.BRANCHES')</th>
                                        {{--<th>@lang('MAIN.COLUMN.USERS')</th>--}}
                                        <th>@lang('MAIN.COLUMN.TEAMS')</th>
                                        <th>@lang('MAIN.COLUMN.INFO')</th>
                                        <th>@lang('MAIN.COLUMN.ACTIONS')</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                <!-- /Active Table -->
                <!-- Trash Table -->
                    <div class="tab-pane fade show" id="tab-trash">
                        <div class="table-responsive">
                            <table class="table table-bordered table-styled table-striped mb-3 w-100 datatable" id="datatableTrash">
                                <thead class="bg-indigo">
                                    <tr>
                                        <th>@lang('MAIN.COLUMN.ID')</th>
                                        <th>@lang('MAIN.COLUMN.NAME')</th>
                                        <th>@lang('MAIN.COLUMN.CIKL')</th>
                                        {{--<th>@lang('MAIN.COLUMN.PERIOD')</th>--}}
                                        <th>@lang('MAIN.COLUMN.GALUZES')</th>
                                        {{--<th>@lang('MAIN.COLUMN.USERS')</th>--}}
                                        <th>@lang('MAIN.COLUMN.TEAMS')</th>
                                        <th>@lang('MAIN.COLUMN.INFO')</th>
                                        <th>@lang('MAIN.COLUMN.ACTIONS')</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                <!-- /Trash Table -->
            </div>
    <!-- /Tabs -->

</div>
@endsection

@section('scripts')
<script src="/js/datatables/datatables.min.js"></script>
<script src="/js/select2/select2.min.js"></script>
<script>
    var language = {"url": "/js/datatables/languages/ukrainian.json"};
    var pageLength = {{ isset(auth()->user()->options->datatablePageLength) ? auth()->user()->options->datatablePageLength : 10 }};
    var current_cycle = 0;
    var current_branch = 0;
    var columns = [
            {"data":"id"},
            {"data":"name"},
            {"data":"cikl","searchable":false,"orderable":false},
            {{--{"data":"period","searchable":false,"orderable":false},--}}
            {"data":"galuzes","searchable":false,"orderable":false},
            {{--{"data":"users","searchable":false,"orderable":false},--}}
            {"data":"teams","searchable":false,"orderable":false},
            {"data":"info","searchable":false,"orderable":false},
            {"data":"actions","searchable":false,"orderable":false}
        ];
    var columnDefs = [
            { "targets": 0, "className": "text-center", },
            { "targets": 2, "className": "text-center", },
            { "targets": 3, "className": "text-center", },
            { "targets": 4, "className": "text-center", },
            { "targets": 6, "className": "text-center", },
            {{--{ "targets": 6, "className": "text-center", },--}}
            {{--{ "targets": 7, "className": "text-center", },--}}
        ];


    var tableActive = $("#datatableActive").DataTable({
        autoWidth : true,
        responsive: true,
        processing: true,
        serverSide: true,
        pageLength: pageLength,
        bSortCellsTop: true,
        order: [[ 0, "desc" ]],
        ajax: {
            url: "{{route('unewprograms.data')}}",
            dataType: "json",
            type: "POST",
            data: function ( d ) {
                d._token = "{{ csrf_token() }}";
                d.cycle_id = current_cycle;
                d.branch_id = current_branch;
                d.table = "active";
            }
        },
        columns:columns,
        columnDefs: columnDefs,
        language: language,
    });
    var tableTrash = $("#datatableTrash").DataTable({
        autoWidth : true,
        responsive: true,
        processing: true,
        serverSide: true,
        pageLength: pageLength,
        bSortCellsTop: true,
        order: [[ 0, "desc" ]],
        ajax: {
            url: "{{route('unewprograms.data')}}",
            dataType: "json",
            type: "POST",
            data: function ( d ) {
                d._token = "{{ csrf_token() }}";
                d.cycle_id = current_cycle;
                d.branch_id = current_branch;
                d.table = "trash";
            }
        },
        columns:columns,
        columnDefs: columnDefs,
        language: language,
    });


    $('div.search-panel').on('click','.element-cycle',function(e){
        e.preventDefault();
        current_cycle = $(this).attr('data-key');
        $('.element-cycle').removeClass('active');
        $(this).addClass('active');
        $('.datatableRefresh').click();
    });

    $('div.search-panel').on('click','.element-branch',function(e){
        e.preventDefault();
        current_branch = $(this).attr('data-key');
        $('.element-branch').removeClass('active');
        $(this).addClass('active');
        $('.datatableRefresh').click();
    });
</script>
@endsection


@section('modals')
<div id="modal-create-program" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h6 class="modal-title">@lang('MAIN.MODAL.CREATE-PROGRAM.TITLE')</h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" id="remove-modal-body">
                    <form id="modal-create-program-form" action="{{ route('unewprograms.store') }}" method="POST">
                        @csrf
                        <div class="form-group row">
                            {{ Form::label('cycle_id', __('MAIN.COLUMN.CYCLE'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                            <div class="col-md-8">
                                {{ Form::select('cycle_id', $cycles2, null, array('class' => 'field form-control')) }}
                            </div>
                        </div>
                        {{--<div class="form-group row">
                            {{ Form::label('period_id', __('MAIN.COLUMN.PERIOD'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                            <div class="col-md-8">
                                {{ Form::select('period_id', $periods, null, array('class' => 'field form-control')) }}
                            </div>
                        </div>--}}
                        <div class="form-group row">
                            {{ Form::label('branches', __('MAIN.COLUMN.BRANCHES'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                            <div class="col-md-8">
                                @foreach ($branches as $branch)
                                    <div class="field custom-control custom-checkbox" name="branches">
                                        {{ Form::checkbox('branches[]',  $branch->id , null, ['id' => $branch->name, 'class' => 'custom-control-input']) }}
                                        {{ Form::label($branch->name, ucfirst($branch->name), ['class' => 'custom-control-label']) }}
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">@lang('MAIN.BUTTON.CANCEL')</button>
                    <button type="button" class="btn bg-primary" id="modal-create-program-submit">@lang('MAIN.BUTTON.CREATE')</button>
                </div>
        </div>
    </div>
</div>

<div id="modal-share-program" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h6 class="modal-title">@lang('MAIN.MODAL.SHARE.TITLE')</h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form id="modal-share-program-form" action="{{ route('unewprograms.store') }}" method="POST">
                        @csrf
                        <select class="field form-control select-remote-data" data-fouc id="team_id" name="team_id">
                            <option value="0" selected>@lang('MAIN.PLACEHOLDER.SELECT-TEAM')</option>
                        </select>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">@lang('MAIN.BUTTON.CANCEL')</button>
                    <button type="button" class="btn bg-primary" id="modal-share-program-submit" data-dismiss="modal">@lang('MAIN.BUTTON.SHARE')</button>
                </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
        $('#modal-create-program-submit').click(function(e){
            let form = $('#modal-create-program-form');
            e.preventDefault();
            $.ajax({
                url: $('#modal-create-program-form').attr('action'),
                type: 'POST',
                dataType : "json",
                data: $('#modal-create-program-form').serialize(),
            }).done((data, status, xhr) => {
                console.log(data.relocateURL);
                window.location.href = data.relocateURL;
            }).fail((xhr) => {
                let data = xhr.responseJSON;
                swal({
                    title: data.message,
                    type: 'error',
                    confirmButtonClass: 'btn btn-primary',
                })
            }).always((xhr, type, status) => {
                let response = xhr.responseJSON || status.responseJSON,
                    errors = response.errors || [];
                form.find('.field').each((i, el) => {
                    let field = $(el);
                    field.removeClass('is-invalid');
                    field.find('input').removeClass('is-invalid');
                    container = field.closest('.form-group'),
                        elem = $('<label class="message">');
                    container.find('label.message').remove();
                    if(errors[field.attr('name')]){
                        field.addClass('is-invalid');
                        field.find('input').addClass('is-invalid');
                        errors[field.attr('name')].forEach((msg) => {
                            elem.clone().addClass('validation-invalid-label').html(msg).appendTo(container);
                        });
                    }
                });
            });
        });

        $(document).on('click','.btn-share-program',function () {
            $('#modal-share-program-form').attr('action', $(this).attr('href'));
        })

        $('#modal-share-program-submit').click(function(e){
            let form = $('#modal-share-program-form');
            e.preventDefault();
            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                dataType : "json",
                data: form.serialize(),
            }).done((data, status, xhr) => {
                $('.datatableRefresh').click();
            }).fail((xhr) => {
                let data = xhr.responseJSON;
                swal({
                    title: data.message,
                    type: 'error',
                    confirmButtonClass: 'btn btn-primary',
                })
            });
        });

    $('#team_id').select2({
        ajax: {
            url: "{{route('teams-select2')}}",
            type: 'get',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term,
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        minimumInputLength: 0,
        templateResult: formatRepo, // omitted for brevity, see the source of this page
        templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
    });
    
    function formatRepo (repo) {
        if (repo.loading) return repo.name;
        var markup = '<div class="select2-result-repository clearfix">';
                //markup += '<div class="select2-result-repository__avatar"><img src="' + repo.avatar + '"></div>';
                //markup += '<div class="select2-result-repository__meta">';
                    markup += '<div class="select2-result-repository__title">' + repo.name + '</div>';
                    markup += '<div class="select2-result-repository__description"></div>';
                    markup += '<div class="select2-result-repository__statistics">';
                        markup += '<div class="select2-result-repository__forks">@lang("MAIN.COLUMN.LEADER"): ' + repo.leader + '</div>';
                        markup += '<div class="select2-result-repository__stargazers">@lang("MAIN.COLUMN.TTYPE"): ' + repo.type + '</div>';
                        markup += '<div class="select2-result-repository__watchers">@lang("MAIN.COLUMN.USERS"): ' + repo.cusers + '</div>';
                    markup += '</div>';
                //markup += '</div>';
            markup += '</div>';
        return markup;
    }

    function formatRepoSelection (repo) {
        return repo.name || repo.text;
    }

});
</script>
@endsection