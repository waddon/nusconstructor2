@extends('layouts.app')

@section('content')
<div class="d-flex align-items-start flex-column flex-md-row">
    <div class="order-2 order-md-1 w-100">

        <!-- Account settings -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Личные данные</h5>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <form action="{{route('user.update')}}" method="POST" class="ajax-form">
                        @csrf
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        {{ Form::label('email', __('MAIN.COLUMN.EMAIL'), ['class'=>'small text-muted font-italic col-form-label']) }}
                                        {{ Form::text('email', auth()->user()->email, array('class' => 'field form-control')) }}
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        {{ Form::label('name', __('MAIN.COLUMN.NAME'), ['class'=>'small text-muted font-italic col-form-label']) }}
                                        {{ Form::text('name', auth()->user()->name, array('class' => 'field form-control')) }}
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        {{ Form::label('second_name', __('MAIN.COLUMN.SECOND-NAME'), ['class'=>'small text-muted font-italic col-form-label']) }}
                                        {{ Form::text('second_name', auth()->user()->second_name, array('class' => 'field form-control')) }}
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        {{ Form::label('patronymic', __('MAIN.COLUMN.PATRONYMIC'), ['class'=>'small text-muted font-italic col-form-label']) }}
                                        {{ Form::text('patronymic', auth()->user()->patronymic, array('class' => 'field form-control')) }}
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        {{ Form::label('password', __('MAIN.COLUMN.NEW-PASSWORD'), ['class'=>'small text-muted font-italic col-form-label']) }}
                                        {{ Form::password('password', array('class' => 'field form-control')) }}
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        {{ Form::label('password_confirmation', __('MAIN.COLUMN.REPEAT-PASSWORD'), ['class'=>'small text-muted font-italic col-form-label']) }}
                                        {{ Form::password('password_confirmation', array('class' => 'field form-control')) }}
                                    </div>
                                </div>
                            
                        </div>

                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">@lang('MAIN.BUTTON.SAVE')</button>
                        </div>
                    </form>
                </div>
            </div>
        <!-- /account settings -->

    </div>

    <!-- Right sidebar component -->
        <div class="sidebar sidebar-light bg-transparent sidebar-component sidebar-component-right wmin-300 border-0 shadow-0 order-1 order-md-2 sidebar-expand-md">

            <!-- Sidebar content -->
            <div class="sidebar-content">

                <!-- User card -->
                <div class="card">
                    <div class="card-body text-center">
                        <div class="card-img-actions d-inline-block mb-3">
                            <img class="img-fluid rounded-circle" src="{{ auth()->user()->getAvatarPath() ?? '' }}" style="width:170px;height:170px" alt="" id="profile-avatar">
                            <div class="card-img-actions-overlay card-img rounded-circle">
                                <label for="loadavatar" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round mb-0">
                                    <i class="icon-plus3"></i>
                                </label>
                                <input type="file" name="loadavatar" id="loadavatar">
                            </div>
                        </div>

                        <h6 class="font-weight-semibold mb-0">{{ auth()->user()->combineName() }}</h6>
                        <span class="d-block text-muted">{{ auth()->user()->roles->implode('name',', ') }}</span>
                    </div>
                    <div class="card-body">
                        <p>
                            <span class="small text-muted font-italic">@lang('MAIN.COLUMN.CREATED')</span>
                            <span class="float-right">{{ auth()->user()->created_at->format('d.m.Y') }}</span>
                        </p>
                        <p>
                            <span class="small text-muted font-italic">@lang('MAIN.COLUMN.UPDATED')</span>
                            <span class="float-right">{{ auth()->user()->created_at->format('d.m.Y') }}</span>
                        </p>
                    </div>
                </div>
                <!-- /user card -->

            </div>
            <!-- /sidebar content -->

        </div>
    <!-- /right sidebar component -->

</div>
@endsection

@section('scripts')
<script src="/js/cropper/cropper.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        var urlCreator = window.URL || window.webkitURL;
        var cropper;
        var image = $('#image');

        $(document).on('change','#loadavatar',function() {
            files = this.files;
            readURL(this);
            setTimeout(shower, 100);
            document.getElementById("loadavatar").value = "";            
        });

        $(".modal").on("shown.bs.modal", function() {
            image.cropper('destroy');
            cropper = image.cropper({
                aspectRatio: 1 / 1,
                done: function (data) {
                    console.log(data);
                }
            });
        })

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#image').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        function shower() {
            $('#modal-crop').modal('show');
        }

        $('#modal-crop-submit').click(function(){
            image.cropper('getCroppedCanvas',{
                width: 200,
                height: 200,
            }).toBlob(function(blob){
                var formData = new FormData();

                formData.append('croppedImage', blob);
                formData.append('_token', '{{ csrf_token() }}');
                $.ajax('{{route("user.change-avatar")}}', {
                    method: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(){
                        $('#main-avatar').attr('src', urlCreator.createObjectURL(blob));
                        $('#profile-avatar').attr('src', urlCreator.createObjectURL(blob));
                    },
                    error: function(){
                        console.log('error');
                    }
                });
            });

        });

    });
</script>
@endsection

@section('modals')
<div id="modal-crop" class="modal" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h6 class="modal-title">@lang('MAIN.MODAL.CROP.TITLE')</h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="image-cropper-container">
                        <img src="/img/bg-50.jpg" alt="" class="crop-not-resizable1" id="image">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">@lang('MAIN.BUTTON.CANCEL')</button>
                    <button type="button" class="btn bg-primary" id="modal-crop-submit" data-dismiss="modal">@lang('MAIN.BUTTON.UPLOAD')</button>
                </div>
        </div>
    </div>
</div>

@endsection
