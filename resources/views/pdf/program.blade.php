<!DOCTYPE html>
<html lang="ua">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{$title ?? 'Document'}}</title>
    <style>
        body{font-family: arial;}
        h2{margin-bottom: 60px;}
        .text-left{text-align: left;}
        .text-center{text-align: center;}
        .text-right{text-align: right;}
        .text-justify{text-align: justify;}
        table.columns3{border-spacing: 0;table-layout: fixed;width: 100%;margin-bottom: 60px;}
        table.columns3 td{width: 33%;vertical-align: top;}
        .element-header{padding:15px;}
        .element-container{padding-left: 30px;}
        .line-header{background-color: #2196f3; color: #ffffff;}
        .problem-header{background-color: #009688; color: #ffffff;}
        .opis-header{background-color: #ff5722; color: #ffffff;}
        table.kors{border-spacing: 0;table-layout: fixed;width: 100%;font-size: 13px;}
        table.kors th{background-color: #324148; color: #ffffff;font-weight: normal;}
        table.kors th.col1{width:120px;}
        table.kors th.col3{width:120px;}
        table.kors th, table.kors td{padding: 5px;}
        .odd {background-color: rgba(0, 0, 0, 0.02);}
        .mt-5 {margin-top: 5px;}
    </style>
</head>
<body>
    {{--{!!$model->getReplacedContent($attr)!!}--}}
    <h2 class="text-center">{{ $model->name ?? ''}}</h2>
    <table class="columns3">
        <tr>
            <td>@lang('MAIN.COLUMN.CYCLE'): {{$model->cycle->name ?? ''}}</td>
            <td>@lang('MAIN.COLUMN.HOURS'): {{$model->meta->hours ?? ''}}</td>
            <td>@lang('MAIN.COLUMN.BRANCHES'): {{$model->branches->implode('name', ', ')}}</td>
        </tr>
    </table>

    <div class="program">
        @if(isset($model->content) && $model->content)
            @foreach($model->content as $line)
                <div class="element line mt-5">
                    <div class="element-header line-header">
                        {{$line->name_line}}
                    </div>
                    <div class="element-container">
                        @if($line->info_line)
                            <div class="mt-5">{!!$line->info_line!!}</div>
                        @endif
                        @foreach($line->problems as $problem)
                            <div class="element problem mt-5">
                                <div class="element-header problem-header">
                                    {!!$problem->name_problem!!}
                                </div>
                                <div class="element-container">
                                    @foreach($problem->opises as $opis)
                                        <div class="element opis mt-5">
                                            <div class="element-header opis-header">
                                                {!!$opis->name_opis!!}
                                            </div>
                                            <div class="">
                                                <table class="kors table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th class="col1">Код</th>
                                                            <th class="col2">Очікувані результати навчання</th>
                                                            <th class="col3">Інструмент оцінювання</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($opis->kors as $key => $kor)
                                                        <tr class="{{ ($key % 2) ? 'odd' : ''}}">
                                                            <td>{{$kor->kod}}</td>
                                                            <td>{{$kor->name}}</td>
                                                            <td>{{$kor->tool}}</td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endforeach
        @endif
    </div>
</body>
</html>