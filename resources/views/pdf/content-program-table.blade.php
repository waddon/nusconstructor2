<style>
  .program {
    width: 100%;
    border: 1px solid #222;
    border-collapse: collapse;
    font-family: arial;
  }
  .program th {
    background: #ccf;
  }
  .program td {
    vertical-align: top;
  }
  .program th, .program td {
    padding: 10px;
    border: 1px solid #222;
  }
</style>

  <h2 style="text-align: center;font-family: arial;">{{ $model->cycle->grades->implode('name', ', ') }}</h2>
  <table class="program">
    <thead>
      <tr>
        @php($colspan = count($model->getOutputTableHead()))
        @foreach($model->getOutputTableHead() as $cell)
          <th>{!! $cell !!}</th>
        @endforeach
      </tr>
    </thead>
    <tbody>
        @foreach($model->getOutputTableBody() as $row)
          <tr>
            @foreach($row as $cell)
              <td colspan="{{ count($row) === 1 ? $colspan : 1 }}">{!! $cell !!}</td>
            @endforeach
          </tr>
        @endforeach
    </tbody>
  </table>
