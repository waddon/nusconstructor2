<!DOCTYPE html>
<html lang="ua">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{$title ?? 'Document'}}</title>
    <style>
        body{font-family: arial;}
        h2{margin-bottom: 60px;}
        .text-left{text-align: left;}
        .text-center{text-align: center;}
        .text-right{text-align: right;}
        .text-justify{text-align: justify;}

        .bg-dark{background-color: #324148;}
        .text-white{color: #ffffff;}
        .odd {background-color: rgba(0, 0, 0, 0.02);}
        .mt-5 {margin-top: 5px;}
        .w-100{width: 100%;}
        .table-bordered th, .table-bordered td {
            border: 1px solid #ddd;
        }
        .badge-primary {
            color: #fff;
            background-color: #2196F3;
        }
        .badge {
            display: inline-block;
            padding: 0.3125rem 0.375rem;
            font-size: 75%;
            font-weight: 500;
            line-height: 1;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: 0.125rem;
        }
.bg-primary {
    background-color: #2196F3 !important;
}

.table th, .table td {
    padding: 0.75rem 1.25rem;
    vertical-align: top;
    border-top: 1px solid #ddd;
}        
    </style>
</head>
<body>
    <h2 class="text-center">{{ $model->name ?? ''}}</h2>
    <h4 class="text-center">{{$model->cycle->name ?? ''}}</h4>

    {!! $model->showClusters(true) !!}
    <br><br>
    {!! $model->showUsed() !!}

</body>
</html>