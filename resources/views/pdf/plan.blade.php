<!DOCTYPE html>
<html lang="ua">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{$title ?? 'Document'}}</title>
    <style>
        body{font-family: arial;}
        h2{margin-bottom: 60px;}
        .text-left{text-align: left;}
        .text-center{text-align: center;}
        .text-right{text-align: right;}
        .text-justify{text-align: justify;}

        table#plan, table.branches{table-layout: fixed;}
        table#plan th.col35{width: 35%;}
        table#plan td.col50{width: 50%;}

/*        .table th, .table td {
            padding: 0.75rem 1.25rem;
        }
*/        .bg-dark{background-color: #324148;}
        .text-white{color: #ffffff;}
        .odd {background-color: rgba(0, 0, 0, 0.02);}
        .mt-5 {margin-top: 5px;}
        .w-100{width: 100%;}
    </style>
</head>
<body>
    <h2 class="text-center">{{ $model->name ?? ''}}</h2>
    <h4 class="text-center">{{$model->cycle->name ?? ''}}</h4>

    <table id="plan" class="table table-striped w-100 mb-3">
        <thead class="bg-dark">
            <tr class="bg-dark">
                <th class="text-center text-white col35">@lang('MAIN.COLUMN.SUBJECT')</th>
                <th>
                    <table class="branches w-100">
                        <tr>
                            <td class="text-center text-white col50">@lang('MAIN.COLUMN.BRANCH')</td>
                            @foreach($model->cycle->grades as $grade)
                            <td class="text-center text-white">{{$grade->name}}</td>
                            @endforeach
                        </tr>
                    </table>
                </th>
            </tr>
        </thead>
        <tbody class="plan-container">
            @foreach($model->content as $key => $subject)
                <tr class="subject {{ ($key % 2) ? 'odd' : ''}}">
                    <td><h4 class="mb-0">{{$subject->name_subject}}</h4></td>
                    <td class="">
                        <table class="branches w-100">
                            <tbody class="branch-container">
                                @foreach($subject->galuzes as $key2 => $galuz)
                                    <tr class="branch {{ ($key2 % 2) ? '' : 'odd'}}">
                                        <td class="col50">
                                            {{$branches->find($galuz->galuz)->name ?? ''}}
                                        </td>
                                        @foreach($galuz->classes as $grade)
                                            <td class="text-center">{{$grade->hours}}</td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

</body>
</html>