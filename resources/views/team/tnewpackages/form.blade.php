@extends('layouts.app')

@section('content')

{{ Form::model($model, [ 'url' => $model->id ? route('tnewpackages.update', [$team->id, $model->id]) : route('tnewpackages.store', $team->id), 'method' => $model->id ? 'PUT' : 'POST', 'class' => 'ajax-form' ]) }}

<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">
        <div class="card mb-3">
            <div class="card-header bg-transparent header-elements-inline">
                <h5 class="card-title">@lang('MAIN.TITLE.PARAMETERS')</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    {{ Form::label('name', __('MAIN.COLUMN.NAME'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                    <div class="col-md-8">
                        {{ Form::text('name', $model->name, array('class' => 'field form-control')) }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="small text-muted font-italic col-form-label">@lang('MAIN.COLUMN.PROGRAMS')</label>
                    <table id="plan" class="table table-striped w-100">
                        <thead class="bg-dark">
                            <tr>
                                <th class="text-center">@lang('MAIN.COLUMN.SUBJECT')</th>
                                <th></th>
                                <th class="text-center">@lang('MAIN.COLUMN.PROGRAM')</th>
                            </tr>
                        </thead>
                        <tbody class="plan-container">
                            @if(isset($model->newplan->content) && $model->newplan->content)
                                @foreach($model->newplan->content as $key => $subject)
                                    <tr class="subject">
                                        <td>{{$subject->name_subject}}</td>
                                        <td class="p-0"></td>
                                        <td class="text-center">
                                            {{ Form::select('programs['.$key.']', $team->approvedNewprograms->pluck('name','id')->prepend(__('MAIN.PLACEHOLDER.SELECT-PROGRAM'),0), isset($model->meta->programs[$key]) ? $model->meta->programs[$key] : 0, array('class' => 'field form-control')) }}
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>

                <div class="form-group">
                    {{ Form::label('content', __('MAIN.COLUMN.EXPLANATORY-NOTE'), ['class'=>'small text-muted font-italic col-form-label']) }}
                    {{ Form::textarea('content', $model->content, ['class' => 'editor-full field form-control', 'id' => 'editor']) }}
                </div>

            </div>
        </div>

    </div>

    <!-- Right sidebar component -->
    <div class="sidebar-mobile-component w-100 w-md-auto order-1 order-md-2">
        <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
            <div class="sidebar-content">
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">@lang('MAIN.TITLE.NAVIGATION')</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="btn-group w-100">
                            <button type="submit" id="submit" class="btn btn-success w-50" title="@lang('MAIN.HINT.SAVE')"><i class="icon-floppy-disk"></i></button>
                            <a href="{{ route('tnewpackages.index', $team->id) }}" class="btn btn-danger w-50" title="@lang('MAIN.HINT.EXIT')"><i class="icon-esc"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /right sidebar component -->

</div>

{{ Form::close() }}
@endsection

@section('scripts')
<script src="/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('.editor-full').each(function(e){
        CKEDITOR.replace(this.id, {
            height: 400,
            extraPlugins: 'forms'
        });
    });    
});    
</script>
@endsection

