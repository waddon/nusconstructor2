<style>
    thead {
        position: sticky;
        top: 0;
        background: blue;
        z-index: 10;
    }
</style>
<div class="card mb-3">
    <div class="card-header bg-primary" {{--style="background: {{$branch->color}};"--}}><h6 class="card-title">{{$line->name}}</h6></div>
    <div class="table-scrollable">
        <table class="table table-striped">
            <thead class="bg-primary" {{--style="background: {{$branch->color}};"--}}>
                <tr>
                    <th rowspan="2" class="text-center">@lang('MAIN.COLUMN.SPECIFIC')</th>
                    @foreach($model->cycle->grades as $grade)
                        <th class="text-center" colspan="{{$model->period->metaElementCount()}}">{{$grade->name ?? ''}}</th>
                    @endforeach
                </tr>
                <tr>
                    @foreach($model->cycle->grades as $grade)
                        @foreach($model->period->meta as $key => $element)
                            <th class="rotate"><div>{{$element ?? ''}}</div></th>
                        @endforeach
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @foreach($line->specificsWithCycle( $model->cycle_id )->get() as $specific)
                    <tr>
                        <th>{{$specific->name}} <span class="badge text-white" style="background: {{$branch->color}}">[{{$specific->marking}}]</span></th>
                        @foreach($model->cycle->grades as $grade)
                            @foreach($model->period->meta as $key => $element)
                                <td>
                                    <div class="field custom-control custom-checkbox" name="branches">
                                        <input type="checkbox" class="custom-control-input layout-element" id="{{$specific->id.'-'.$grade->id.'-'.$key}}" data-grade="{{$grade->id}}" data-period="{{$key}}" data-specific="{{$specific->id}}" {{ (isset($model->content->{$grade->id}->{$key}->{$specific->id}) && $model->content->{$grade->id}->{$key}->{$specific->id}) ? 'checked' : ''}}>
                                        <label for="{{$specific->id.'-'.$grade->id.'-'.$key}}" class="custom-control-label"></label>
                                        {{--{{ Form::checkbox('layout['.$specific->id.']['.$grade->id.']['.$key.']',  $specific->id.'-'.$grade->id.'-'.$key , null, ['id' => $specific->id.'-'.$grade->id.'-'.$key, 'class' => 'custom-control-input layout-element', 'data-grade' => $grade->id, 'data-period' => $key, 'data-specific' => $specific->id]) }}
                                        {{ Form::label($specific->id.'-'.$grade->id.'-'.$key, ' ', ['class' => 'custom-control-label']) }}--}}
                                    </div>
                                </td>
                            @endforeach
                        @endforeach
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
