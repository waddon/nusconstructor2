@extends('layouts.app')

@section('content')
<style type="text/css">
    .rotate {
        vertical-align:bottom;
        text-align:center;    }
    .rotate div {
       -ms-writing-mode: tb-rl;
       -webkit-writing-mode:vertical-rl;
       writing-mode: vertical-rl;
       transform:rotate(180deg);
       white-space: nowrap;
    }
    .table td, .table th {
      padding: 0.5em;
    }
    .table .custom-checkbox{
        top: -13px;
    }
    /*.sticked {
        max-width: 100%;
        overflow-x: scroll;
    }
    .sticked tbody th {
        position: sticky;
        left: 0;
        z-index: 1;
        background: #f5f5f5;
    }
    */

</style>

{{ Form::model($model, [ 'url' => route('tlayouts.update', [ $team->id, $model->id ]), 'method' => 'PUT', 'id' => 'layout-form', 'name' => 'fileinfo' ]) }}
<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">
        <div class="card mb-3 layout-loader">
            <!-- Search panel -->
                <h2 class="p-3 pb-0">Створення шаблону програми</h2>
                <div class="search-panel">
                    <div class="text-right">
                        <div class="btn-group">
                            <a href="{{ $model->previousUrl }}" class="btn btn-danger" title="@lang('MAIN.HINT.EXIT')"><i class="icon-esc"></i></a>
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <h2 class="mb-3">@lang('MAIN.TITLE.CONNECTING')</h2>
                    {{--<img class="mb-5" src="/img/loader2.gif" style="width: 50%;height: 50%;">--}}
                    <div id="fountainG" class="mb-5">
                        <div id="fountainG_1" class="fountainG"></div>
                        <div id="fountainG_2" class="fountainG"></div>
                        <div id="fountainG_3" class="fountainG"></div>
                        <div id="fountainG_4" class="fountainG"></div>
                        <div id="fountainG_5" class="fountainG"></div>
                        <div id="fountainG_6" class="fountainG"></div>
                        <div id="fountainG_7" class="fountainG"></div>
                        <div id="fountainG_8" class="fountainG"></div>
                    </div>
                </div>
        </div>

        <div class="card mb-3 layout-constructor" style="display: none;">
                <h2 class="p-3 pb-0">Створення шаблону програми</h2>
                <!-- Search panel -->
                    <div class="search-panel">
                        <div class="text-right">
                            <div class="btn-group">
                                <button type="button" class="btn btn-success save-layout" title="@lang('MAIN.HINT.SAVE')"><i class="icon-floppy-disk"></i></button>
                                <a href="{{ route('tlayouts.index',$team->id) }}" class="btn btn-danger" title="@lang('MAIN.BUTTON.EXIT')"><i class="icon-esc"></i></a>
                            </div>
                            
                        </div>
                    </div>
                <!-- /Search panel -->

                <!-- Tabs -->
                        <ul class="nav nav-tabs nav-tabs-highlight">
                            <li class="nav-item"><a href="#tab-constructor" class="nav-link active" data-toggle="tab"><i class="icon-cube3 mr-2"></i> @lang('MAIN.TAB.CONSTRUCTOR')</a></li>
                        </ul>

                        <div class="tab-content">
                            <!-- Constructor Table -->
                                <div class="tab-pane fade p-3 show active" id="tab-constructor">
                                    <div class="card-body">
                                        <div class="form-group row">
                                            {{ Form::label('name', __('MAIN.COLUMN.NAME'), ['class'=>'small text-muted font-italic col-lg-2 col-form-label']) }}
                                            <div class="col-lg-6">
                                                {{ Form::text('name', $model->name, array('class' => 'field form-control')) }}
                                            </div>
                                            {{ Form::label('period', __('MAIN.COLUMN.PERIOD'), ['class'=>'small text-muted font-italic col-lg-2 col-form-label']) }}
                                            <div class="col-lg-2">{{$model->period->name}}</div>
                                        </div>
                                        <div class="form-group row">
                                            {{ Form::label('branches', __('MAIN.COLUMN.BRANCHES'), ['class'=>'small text-muted font-italic col-lg-2 col-form-label']) }}
                                            <div class="col-lg-6">{{$model->branches->implode('name', ', ')}}</div>
                                            {{ Form::label('cycle', __('MAIN.COLUMN.CYCLE'), ['class'=>'small text-muted font-italic col-lg-2 col-form-label']) }}
                                            <div class="col-lg-2">{{$model->cycle->name}}</div>
                                        </div>
                                    </div>
                                    <h2 class="text-center mb-3">Послідовність очікуваних результатів навчання в межах змістових ліній</h2>
                                    <form id="layout-form">
                                    @foreach($model->branches as $branch)
                                        <h3 class="text-center">{{$branch->name}}</h3>
                                            @foreach($branch->lines as $line)
                                                @if( $line->specificsWithCycle($model->cycle_id)->count() > 0)
                                                    @include('team.tlayouts.line')
                                                @endif
                                            @endforeach
                                    @endforeach
                                    </form>
                                </div>
                            <!-- /Constructor Table -->
                        </div>
                <!-- /Tabs -->
        </div>

    </div>


</div>

@endsection

@section('scripts')
<!--script src="/js/autobahn-old-master/autobahn.min.js" type="text/javascript"></script-->
<script type="text/javascript">

    const channel = 'layout-{{$model->id}}'
    const user = Date.now()
    let socket

    function websocketConnect() {
        socket = new WebSocket("{{ env('FORCE_HTTPS',false) ? 'wss' : 'ws' }}://{{ \App\Models\Option::getOption('socketLink','localhost') }}:{{ env('FORCE_HTTPS',false) ? '443' : '8080' }}");

        socket.onopen = function(e) {
            console.log(`[open] Connection established`);
            socket.send(`{"commandType":"connect", "user":"${user}", "channel":"${channel}"}`)
            $(".layout-constructor").show()
            $(".layout-loader").hide()
        };

        socket.onmessage = function(event) {
            // console.log(`[message] Данные получены с сервера: ${event.data}`);
            let json = JSON.parse(event.data)
            if (json.commandType === 'interruption' && user !== parseInt(json.user)) {
                $(".layout-loader").empty();
                $(".layout-constructor").empty();
                quit();
            }
        };

        socket.onclose = function(event) {
            if (event.wasClean) {
                console.log(`[close] Connection closed clearly, code=${event.code} reason=${event.reason}`)
            } else {
                $(".layout-constructor").hide();
                $(".layout-loader").show();
                console.log('[close] Connection terminated');
                setTimeout(function() {
                    websocketConnect();
                }, 10000);
            }
        };

    }
    websocketConnect()

    function quit() {
        swal({
            title: 'Редагування було перехоплено, або елемент було видалено из системи',
            type: 'warning',
            confirmButtonText: "Вийти",
            confirmButtonClass: 'btn btn-primary',
        }).then((confirm) => {
            window.location.href = "{{$model->previousUrl}}";
        });
    }

    function assemblyLayout() {
        let object = {};
        $( ".layout-element" ).each(function( index ) {
            if (typeof object[$(this).attr('data-grade')] === 'undefined') {object[$(this).attr('data-grade')] = {};}
            if (typeof object[$(this).attr('data-grade')][$(this).attr('data-period')] === 'undefined') {object[$(this).attr('data-grade')][$(this).attr('data-period')] = {};}
            object[$(this).attr('data-grade')][$(this).attr('data-period')][$(this).attr('data-specific')] = $(this).prop('checked');
        });
        return object;
    }

    $(document).on('click','.save-layout',function(){
        let form = $('#layout-form');
        let formData = new FormData( document.forms.namedItem("fileinfo") );
        formData.append('layout', JSON.stringify( assemblyLayout() ) );

        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: formData,
            processData: false,
            contentType: false,
        }).done((data, status, xhr) => {
            swal({
                title: data.message,
                type: 'success',
                showCancelButton: true,
                confirmButtonText: "Вийти до переліку",
                confirmButtonClass: 'btn btn-primary',
                cancelButtonText: "Продовжити редагування",
                cancelButtonClass: 'btn btn-light',
            }).then((confirm) => {
                if(confirm.value){
                    window.location.href = data.exitURL;
                }
            });
            form.find('fieldset').attr('disabled', true);
        }).fail((xhr) => {
            let data = xhr.responseJSON;
            swal({
                title: data.message,
                type: 'error',
                confirmButtonClass: 'btn btn-primary',
            })                    
        });
    });

    function collect_data() {
        let result = {}
        $('.layout-element').each(function(){
            if ($(this).prop('checked')){
                grade = $(this).attr('data-grade');
                period = $(this).attr('data-period');
                specific = $(this).attr('data-specific');

                if (typeof result[grade] === 'undefined') {result[grade] = {};}
                if (typeof result[grade][period] === 'undefined') {result[grade][period] = [];}
                result[grade][period].push(specific);
            }
        })
        return result;
    }
</script>
@endsection
