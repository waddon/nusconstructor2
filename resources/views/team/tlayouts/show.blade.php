@extends('layouts.app')

@section('content')
<style type="text/css">
    table#plan, table.branches{table-layout: fixed;}
    table#plan>thead>tr>th:first-child{width: 35%;}
    table.branches>tbody>tr>td:first-child{width: 50%;}
</style>

<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">

        <div class="card mb-3 program-constructor">
                <!-- Search panel -->
                    <div class="search-panel">
                        <h2>Перегляд шаблону "{{$model->name}}" у вигляді тематичних кластерів</h2>
                        <div class="text-right">
                            <div class="btn-group">

                                <a href="{{route('loadlayout', [ $model->key ])}}" class="btn btn-danger" title="@lang('MAIN.HINT.DOWNLOAD')" target="_blank"><i class="icon-file-pdf"></i></a>

                                @if(isset($model->previousUrl))
                                    <a href="{{ $model->previousUrl }}" class="btn btn-danger" title="@lang('MAIN.HINT.EXIT')"><i class="icon-esc"></i></a>
                                @endif
                            </div>
                            
                        </div>
                    </div>
                <!-- /Search panel -->

                {!! $model->showClusters() !!}
                <br><br>
                {!! $model->showUsed() !!}
        </div>

    </div>

</div>


@endsection
