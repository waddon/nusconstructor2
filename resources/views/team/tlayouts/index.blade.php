@extends('layouts.app')

@section('content')
<div class="card mb-3">

    <!-- Search panel -->
        <div class="search-panel">
            <div class="text-right">
                <div class="btn-group">
                    <div class="btn-group justify-content-center">
                        <a href="#" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">@lang('MAIN.BUTTON.CIKL')</a>
                        <div class="dropdown-menu">
                                @foreach($cycles as $key => $cycle)
                                    <a href="#" class="dropdown-item element-cycle {{!$key ? 'active' : ''}}" data-key="{{$key}}">{{$cycle}}</a>
                                @endforeach
                        </div>
                    </div>
                    <div class="btn-group justify-content-center">
                        <a href="#" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">@lang('MAIN.BUTTON.PERIOD')</a>
                        <div class="dropdown-menu">
                                @foreach($periods as $key => $period)
                                    <a href="#" class="dropdown-item element-period {{!$key ? 'active' : ''}}" data-key="{{$key}}">{{$period}}</a>
                                @endforeach
                        </div>
                    </div>
                    <div class="btn-group justify-content-center">
                        <a href="#" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">@lang('MAIN.BUTTON.GALUZ')</a>
                        <div class="dropdown-menu">
                                @foreach($branches2 as $key => $branch)
                                    <a href="#" class="dropdown-item element-branch {{!$key ? 'active' : ''}}" data-key="{{$key}}">{{$branch}}</a>
                                @endforeach
                        </div>
                    </div>
                    <a class="btn btn-success btn-create-layout" href="{{route('tlayouts.create', $team->id)}}" title="@lang('MAIN.HINT.CREATE')"  title="'.__('MAIN.HINT.RESTORE').'" data-toggle="modal" data-target="#modal-create-layout" data-title="'.__('MAIN.MODAL.RESTORE.TITLE').'" data-text="'.__('MAIN.MODAL.RESTORE.MESSAGE').'" data-color="bg-success" data-method="POST"><i class="icon-file-plus"></i></a>

                    <button type="button" class="btn btn-primary datatableRefresh" title="@lang('Refresh')"><i class="icon-sync"></i></button>
                </div>                
                
            </div>
        </div>
    <!-- /Search panel -->

    <!-- Tabs -->
            <ul class="nav nav-tabs nav-tabs-highlight">
                <li class="nav-item"><a href="#tab-active" class="nav-link active" data-toggle="tab"><i class="icon-hammer-wrench mr-2"></i> @lang('MAIN.TAB.DEVELOPING')</a></li>
                <li class="nav-item"><a href="#tab-approving" class="nav-link" data-toggle="tab"><i class="icon-library2 mr-2"></i> @lang('MAIN.TAB.APPROVING')</a></li>
                <li class="nav-item"><a href="#tab-approved" class="nav-link" data-toggle="tab"><i class="icon-checkmark mr-2"></i> @lang('MAIN.TAB.APPROVED')</a></li>
                <li class="nav-item"><a href="#tab-trash" class="nav-link" data-toggle="tab"><i class="icon-trash mr-2"></i> @lang('MAIN.TAB.TRASH')</a></li>
            </ul>

            <div class="tab-content">
                <!-- Active Table -->
                    <div class="tab-pane fade show active" id="tab-active">
                        <div class="table-responsive">
                            <table class="table table-bordered table-styled table-striped mb-3 w-100 datatable" id="datatableActive">
                                <thead class="bg-indigo">
                                    <tr>
                                        <th>@lang('MAIN.COLUMN.ID')</th>
                                        <th>@lang('MAIN.COLUMN.NAME')</th>
                                        <th>@lang('MAIN.COLUMN.CYCLE')</th>
                                        <th>@lang('MAIN.COLUMN.PERIOD')</th>
                                        <th>@lang('MAIN.COLUMN.BRANCHES')</th>
                                        <th>@lang('MAIN.COLUMN.EDITORS')</th>
                                        <th>@lang('MAIN.COLUMN.INFO')</th>
                                        <th>@lang('MAIN.COLUMN.ACTIONS')</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                <!-- /Active Table -->
                <!-- Approving Table -->
                    <div class="tab-pane fade" id="tab-approving">
                        <div class="table-responsive">
                            <table class="table table-bordered table-styled table-striped mb-3 w-100 datatable" id="datatableApproving">
                                <thead class="bg-indigo">
                                    <tr>
                                        <th>@lang('MAIN.COLUMN.ID')</th>
                                        <th>@lang('MAIN.COLUMN.NAME')</th>
                                        <th>@lang('MAIN.COLUMN.CYCLE')</th>
                                        <th>@lang('MAIN.COLUMN.PERIOD')</th>
                                        <th>@lang('MAIN.COLUMN.BRANCHES')</th>
                                        <th>@lang('MAIN.COLUMN.EDITORS')</th>
                                        <th>@lang('MAIN.COLUMN.INFO')</th>
                                        <th>@lang('MAIN.COLUMN.ACTIONS')</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                <!-- /Approving Table -->
                <!-- Approved Table -->
                    <div class="tab-pane fade" id="tab-approved">
                        <div class="table-responsive">
                            <table class="table table-bordered table-styled table-striped mb-3 w-100 datatable" id="datatableApproved">
                                <thead class="bg-indigo">
                                    <tr>
                                        <th>@lang('MAIN.COLUMN.ID')</th>
                                        <th>@lang('MAIN.COLUMN.NAME')</th>
                                        <th>@lang('MAIN.COLUMN.CYCLE')</th>
                                        <th>@lang('MAIN.COLUMN.PERIOD')</th>
                                        <th>@lang('MAIN.COLUMN.BRANCHES')</th>
                                        <th>@lang('MAIN.COLUMN.EDITORS')</th>
                                        <th>@lang('MAIN.COLUMN.INFO')</th>
                                        <th>@lang('MAIN.COLUMN.ACTIONS')</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                <!-- /Approved Table -->
                <!-- Trash Table -->
                    <div class="tab-pane fade show" id="tab-trash">
                        <div class="table-responsive">
                            <table class="table table-bordered table-styled table-striped mb-3 w-100 datatable" id="datatableTrash">
                                <thead class="bg-indigo">
                                    <tr>
                                        <th>@lang('MAIN.COLUMN.ID')</th>
                                        <th>@lang('MAIN.COLUMN.NAME')</th>
                                        <th>@lang('MAIN.COLUMN.CIKL')</th>
                                        <th>@lang('MAIN.COLUMN.PERIOD')</th>
                                        <th>@lang('MAIN.COLUMN.GALUZES')</th>
                                        <th>@lang('MAIN.COLUMN.EDITORS')</th>
                                        <th>@lang('MAIN.COLUMN.INFO')</th>
                                        <th>@lang('MAIN.COLUMN.ACTIONS')</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                <!-- /Trash Table -->
            </div>
    <!-- /Tabs -->

</div>
@endsection

@section('scripts')
<script src="/js/datatables/datatables.min.js"></script>
<script src="/js/select2/select2.min.js"></script>
<script>
    var language = {"url": "/js/datatables/languages/ukrainian.json"};
    var pageLength = {{ isset(auth()->user()->options->datatablePageLength) ? auth()->user()->options->datatablePageLength : 10 }};
    var current_cycle = 0;
    var current_period = 0;
    var current_branch = 0;
    var columns = [
            {"data":"id"},
            {"data":"name"},
            {"data":"cikl","searchable":false,"orderable":false},
            {"data":"period","searchable":false,"orderable":false},
            {"data":"galuzes","searchable":false,"orderable":false},
            {"data":"users","searchable":false,"orderable":false},
            {"data":"info","searchable":false,"orderable":false},
            {"data":"actions","searchable":false,"orderable":false}
        ];
    var columnDefs = [
            { "targets": 0, "className": "text-center", },
            { "targets": 2, "className": "text-center", },
            { "targets": 3, "className": "text-center", },
            { "targets": 4, "className": "text-center", },
            { "targets": 5, "className": "text-center", },
            { "targets": 6, "className": "text-center", },
            { "targets": 7, "className": "text-center", },
        ];


    var tableActive = $("#datatableActive").DataTable({
        autoWidth : true,
        responsive: true,
        processing: true,
        serverSide: true,
        pageLength: pageLength,
        bSortCellsTop: true,
        order: [[ 0, "desc" ]],
        ajax: {
            url: "{{route('tlayouts.data',$team->id)}}",
            dataType: "json",
            type: "POST",
            data: function ( d ) {
                d._token = "{{ csrf_token() }}";
                d.cycle_id = current_cycle;
                d.period_id = current_period;
                d.branch_id = current_branch;
                d.table = "active";
            }
        },
        columns:columns,
        columnDefs: columnDefs,
        language: language,
    });

    var tableApproving = $("#datatableApproving").DataTable({
        autoWidth : true,
        responsive: true,
        processing: true,
        serverSide: true,
        pageLength: pageLength,
        bSortCellsTop: true,
        order: [[ 0, "desc" ]],
        ajax: {
            url: "{{route('tlayouts.data',$team->id)}}",
            dataType: "json",
            type: "POST",
            data: function ( d ) {
                d._token = "{{ csrf_token() }}";
                d.cycle_id = current_cycle;
                d.period_id = current_period;
                d.branch_id = current_branch;
                d.status = "Approving";
                d.table = "active";
            }
        },
        columns:columns,
        columnDefs: columnDefs,
        language: language,
    });

    var tableApproved = $("#datatableApproved").DataTable({
        autoWidth : true,
        responsive: true,
        processing: true,
        serverSide: true,
        pageLength: pageLength,
        bSortCellsTop: true,
        order: [[ 0, "desc" ]],
        ajax: {
            url: "{{route('tlayouts.data',$team->id)}}",
            dataType: "json",
            type: "POST",
            data: function ( d ) {
                d._token = "{{ csrf_token() }}";
                d.cycle_id = current_cycle;
                d.period_id = current_period;
                d.branch_id = current_branch;
                d.status = "Approved";
                d.table = "active";
            }
        },
        columns:columns,
        columnDefs: columnDefs,
        language: language,
    });

    var tableTrash = $("#datatableTrash").DataTable({
        autoWidth : true,
        responsive: true,
        processing: true,
        serverSide: true,
        pageLength: pageLength,
        bSortCellsTop: true,
        order: [[ 0, "desc" ]],
        ajax: {
            url: "{{route('tlayouts.data',$team->id)}}",
            dataType: "json",
            type: "POST",
            data: function ( d ) {
                d._token = "{{ csrf_token() }}";
                d.cycle_id = current_cycle;
                d.period_id = current_period;
                d.branch_id = current_branch;
                d.table = "trash";
            }
        },
        columns:columns,
        columnDefs: columnDefs,
        language: language,
    });


    $('div.search-panel').on('click','.element-cycle',function(e){
        e.preventDefault();
        current_cycle = $(this).attr('data-key');
        $('.element-cycle').removeClass('active');
        $(this).addClass('active');
        $('.datatableRefresh').click();
    });

    $('div.search-panel').on('click','.element-period',function(e){
        e.preventDefault();
        current_period = $(this).attr('data-key');
        $('.element-period').removeClass('active');
        $(this).addClass('active');
        $('.datatableRefresh').click();
    });

    $('div.search-panel').on('click','.element-branch',function(e){
        e.preventDefault();
        current_branch = $(this).attr('data-key');
        $('.element-branch').removeClass('active');
        $(this).addClass('active');
        $('.datatableRefresh').click();
    });
</script>
@endsection


@section('modals')
<div id="modal-create-layout" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h6 class="modal-title">@lang('MAIN.MODAL.CREATE-LAYOUT.TITLE')</h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" id="remove-modal-body">
                    <form id="modal-create-layout-form" action="{{ route('tlayouts.store',$team->id) }}" method="POST">
                        @csrf
                        <div class="form-group row">
                            {{ Form::label('cycle_id', __('MAIN.COLUMN.CYCLE'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                            <div class="col-md-8">
                                {{ Form::select('cycle_id', $cycles2, null, array('class' => 'field form-control')) }}
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('period_id', __('MAIN.COLUMN.PERIOD'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                            <div class="col-md-8">
                                {{ Form::select('period_id', $periods2, null, array('class' => 'field form-control')) }}
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('branches', __('MAIN.COLUMN.BRANCHES'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                            <div class="col-md-8">
                                @foreach ($branches as $branch)
                                    <div class="field custom-control custom-checkbox" name="branches">
                                        {{ Form::checkbox('branches[]',  $branch->id , null, ['id' => $branch->name, 'class' => 'custom-control-input']) }}
                                        {{ Form::label($branch->name, ucfirst($branch->name), ['class' => 'custom-control-label']) }}
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">@lang('MAIN.BUTTON.CANCEL')</button>
                    <button type="button" class="btn bg-primary" id="modal-create-layout-submit">@lang('MAIN.BUTTON.CREATE')</button>
                </div>
        </div>
    </div>
</div>

<div id="modal-share-layout" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h6 class="modal-title">@lang('MAIN.MODAL.SHARE-LAYOUT.TITLE')</h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form id="modal-share-layout-form" action="#" method="POST">
                        @csrf
                        @foreach($team->users as $user)
                            <div class="field custom-control custom-checkbox" name="branches">
                                {{ Form::checkbox('editors[]',  $user->id , null, ['id' => 'editor-'.$user->id, 'class' => 'custom-control-input editors']) }}
                                {{ Form::label('editor-'.$user->id, $user->combineName(), ['class' => 'custom-control-label']) }}
                            </div>
                        @endforeach
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">@lang('MAIN.BUTTON.CANCEL')</button>
                    <button type="button" class="btn bg-primary" id="modal-share-layout-submit" data-dismiss="modal">@lang('MAIN.BUTTON.SHARE')</button>
                </div>
        </div>
    </div>
</div>

<div id="modal-send-approve-layout" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h6 class="modal-title">@lang('MAIN.MODAL.SEND-APPROVE.TITLE')</h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    @lang('MAIN.MODAL.SEND-APPROVE.MESSAGE')
                    <form id="modal-send-approve-layout-form" action="#" method="POST">
                        @csrf
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">@lang('MAIN.BUTTON.CANCEL')</button>
                    <button type="button" class="btn bg-primary" id="modal-send-approve-layout-submit" data-dismiss="modal">@lang('MAIN.BUTTON.SEND')</button>
                </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
        $('#modal-create-layout-submit').click(function(e){
            let form = $('#modal-create-layout-form');
            e.preventDefault();
            $.ajax({
                url: $('#modal-create-layout-form').attr('action'),
                type: 'POST',
                dataType : "json",
                data: $('#modal-create-layout-form').serialize(),
            }).done((data, status, xhr) => {
                console.log(data.relocateURL);
                window.location.href = data.relocateURL;
            }).fail((xhr) => {
                let data = xhr.responseJSON;
                swal({
                    title: data.message,
                    type: 'error',
                    confirmButtonClass: 'btn btn-primary',
                })
            }).always((xhr, type, status) => {
                let response = xhr.responseJSON || status.responseJSON,
                    errors = response.errors || [];
                form.find('.field').each((i, el) => {
                    let field = $(el);
                    field.removeClass('is-invalid');
                    field.find('input').removeClass('is-invalid');
                    container = field.closest('.form-group'),
                        elem = $('<label class="message">');
                    container.find('label.message').remove();
                    if(errors[field.attr('name')]){
                        field.addClass('is-invalid');
                        field.find('input').addClass('is-invalid');
                        errors[field.attr('name')].forEach((msg) => {
                            elem.clone().addClass('validation-invalid-label').html(msg).appendTo(container);
                        });
                    }
                });
            });
        });

        $(document).on('click','.btn-share-layout',function () {
            let editors = JSON.parse( $(this).attr('data-editors') );
            $('.editors').prop( "checked", false );
            $.each( editors, function( key, value ) {
                $('#editor-'+value).prop( "checked", true );
            });
            $('#modal-share-layout-form').attr('action', $(this).attr('href'));
        })

        $('#modal-share-layout-submit').click(function(e){
            let form = $('#modal-share-layout-form');
            console.log(form.serialize());
            e.preventDefault();
            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                dataType : "json",
                data: form.serialize(),
            }).done((data, status, xhr) => {
                console.log(data);
                $('.datatableRefresh').click();
            }).fail((xhr) => {
                let data = xhr.responseJSON;
                swal({
                    title: data.message,
                    type: 'error',
                    confirmButtonClass: 'btn btn-primary',
                })
            });
        });

        $(document).on('click','.btn-send-approve-layout',function () {
            $('#modal-send-approve-layout-form').attr('action', $(this).attr('href'));
        })

        $('#modal-send-approve-layout-submit').click(function(e){
            let form = $('#modal-send-approve-layout-form');
            console.log(form.serialize());
            e.preventDefault();
            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                dataType : "json",
                data: form.serialize(),
            }).done((data, status, xhr) => {
                console.log(data);
                $('.datatableRefresh').click();
            }).fail((xhr) => {
                let data = xhr.responseJSON;
                swal({
                    title: data.message,
                    type: 'error',
                    confirmButtonClass: 'btn btn-primary',
                })
            });
        });


});
</script>
@endsection