@extends('layouts.app')

@section('content')

{{ Form::model($model, [ 'url' => $model->id ? route('tmaterials.update', [$team->id, $model->id]) : route('tmaterials.store', $team->id), 'method' => $model->id ? 'PUT' : 'POST', 'class' => 'ajax-form' ]) }}

<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">
        <div class="card mb-3">
            <div class="card-header bg-transparent header-elements-inline">
                <h5 class="card-title">@lang('MAIN.TITLE.PARAMETERS')</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    {{ Form::label('name', __('MAIN.COLUMN.NAME'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                    <div class="col-md-8">
                        {{ Form::text('name', $model->name, array('class' => 'field form-control')) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('url', __('MAIN.COLUMN.URL'), ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                    <div class="col-md-8">
                        {{ Form::text('url', $model->url, array('class' => 'field form-control')) }}
                    </div>
                </div>

                <div class="form-group">
                            <div class="row search-panel">
                                <label for="" class="small text-muted font-italic col-form-label col-sm-1">@lang('MAIN.COLUMN.CYCLE')</label>
                                <div class="col-sm-3">
                                    <select class="form-control" id="selectCycle">
                                        @foreach($cycles as $key => $cycle)
                                            <option value="{{$cycle->id}}">{{$cycle->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="" class="small text-muted font-italic col-form-label col-sm-1">@lang('MAIN.COLUMN.BRANCH')</label>
                                <div class="col-sm-3">
                                    <select class="form-control" id="selectBranch">
                                        @foreach($branches as $key => $branch)
                                            <option value="{{$branch->id}}">{{$branch->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="" class="small text-muted font-italic col-form-label col-sm-1">@lang('MAIN.COLUMN.USING')</label>
                                <div class="col-sm-3">
                                    <select class="form-control" id="selectUsing">
                                        <option value="0">@lang('MAIN.PLACEHOLDER.ALL')</option>
                                        <option value="1">@lang('MAIN.PLACEHOLDER.USING')</option>
                                        <option value="2">@lang('MAIN.PLACEHOLDER.NOT-USING')</option>
                                    </select>
                                </div>
                            </div>

                            <table class="change-kors sticked table table-striped">
                                <thead>
                                    <tr>
                                        <th class="bg-dark text-center pl-4 pr-4">@lang('MAIN.COLUMN.MARKING')</th>
                                        <th class="bg-dark text-center">@lang('MAIN.COLUMN.NAME')</th>
                                        <th class="bg-dark text-center">@lang('MAIN.COLUMN.ACTIONS')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($specifics as $specific)
                                        <tr class="kor" data-kod="{{$specific->marking}}" data-branch="{{$specific->expect->branch_id}}" data-cycle="{{$specific->expect->cycle_id}}" data-frequency="0" style="display: none;">
                                            <td class="kor-kod">{{$specific->marking}}</td>
                                            <td class="kor-name">{{$specific->name}}</td>
                                            <td class="text-center pl-4 pr-2">

                                                <div class="field custom-control custom-checkbox mb-1" name="rorgans">
                                                    {{ Form::checkbox('specifics[]',  $specific->id , null, ['id' => $specific->id, 'class' => 'custom-control-input' ]) }}
                                                    {{ Form::label($specific->id, '&nbsp;', ['class' => 'custom-control-label']) }}
                                                </div>

                                                {{--<div class="custom-control custom-checkbox">
                                                  <input type="checkbox" class="kor-checkBox custom-control-input" id="{{$specific->id}}">
                                                  <label class="custom-control-label" for="{{$specific->id}}">&nbsp;</label>
                                                </div>--}}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                </div>
            </div>
        </div>

    </div>

    <!-- Right sidebar component -->
    <div class="sidebar-mobile-component w-100 w-md-auto order-1 order-md-2">
        <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
            <div class="sidebar-content">
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">@lang('MAIN.TITLE.NAVIGATION')</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="btn-group w-100">
                            <button type="submit" id="submit" class="btn btn-success w-50" title="@lang('MAIN.HINT.SAVE')"><i class="icon-floppy-disk"></i></button>
                            <a href="{{ route('tmaterials.index', $team->id) }}" class="btn btn-danger w-50" title="@lang('MAIN.HINT.EXIT')"><i class="icon-esc"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /right sidebar component -->

</div>

{{ Form::close() }}
@endsection

@section('scripts')
<script type="text/javascript">
    showKors();
    $(document).on('change','.search-panel select',function(){
        showKors();
    });

    function showKors() {
        let flag = true;
        let using = 0;
        let branch = 0;
        let cycle = 0;
        $( '.change-kors .kor' ).each(function( index ){
            flag = true;
            element = $(this);
            using = parseInt( $('#selectUsing').val() );
            branch = parseInt( $('#selectBranch').val() );
            cycle = parseInt( $('#selectCycle').val() );
            if ( (using == 1 && element.find('input').first().prop('checked') == false) ||
                 (using == 2 && element.find('input').first().prop('checked') == true))
            {
                flag = false;
            }
            // console.log(branch + ' - ' + element.attr('data-owner') + ' - ' + parseInt(element.attr('data-branch')) );
            if ( (branch > 0 && parseInt(element.attr('data-branch')) != branch) || (cycle > 0 && parseInt(element.attr('data-cycle')) != cycle) ){
                flag = false;
            }
            if (flag){
                $(this).show();
            } else {
                $(this).hide();
            }
        });
    }
</script>
@endsection
