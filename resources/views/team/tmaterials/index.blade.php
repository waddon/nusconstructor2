@extends('layouts.app')

@section('content')
<div class="card mb-3">

    <!-- Search panel -->
        <div class="search-panel">
            <div class="text-right">
                <div class="btn-group">
                    {{--<div class="btn-group justify-content-center">
                        <a href="#" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">@lang('MAIN.BUTTON.CIKL')</a>
                        <div class="dropdown-menu">
                                @foreach($cycles as $key => $cycle)
                                    <a href="#" class="dropdown-item element-cycle {{!$key ? 'active' : ''}}" data-key="{{$key}}">{{$cycle}}</a>
                                @endforeach
                        </div>
                    </div>--}}
                    <a class="btn btn-success btn-create-plan" href="{{route('tmaterials.create', $team->id)}}" title="@lang('MAIN.HINT.CREATE')"  title="'.__('MAIN.HINT.RESTORE').'"><i class="icon-file-plus"></i></a>

                    <button type="button" class="btn btn-primary datatableRefresh" title="@lang('Refresh')"><i class="icon-sync"></i></button>
                </div>                
                
            </div>
        </div>
    <!-- /Search panel -->

    <!-- Tabs -->
            <ul class="nav nav-tabs nav-tabs-highlight">
                <li class="nav-item"><a href="#tab-active" class="nav-link active" data-toggle="tab"><i class="icon-hammer-wrench mr-2"></i> @lang('MAIN.TAB.DEVELOPING')</a></li>
                <li class="nav-item"><a href="#tab-approving" class="nav-link" data-toggle="tab"><i class="icon-library2 mr-2"></i> @lang('MAIN.TAB.APPROVING')</a></li>
                <li class="nav-item"><a href="#tab-approved" class="nav-link" data-toggle="tab"><i class="icon-checkmark mr-2"></i> @lang('MAIN.TAB.APPROVED')</a></li>
                <li class="nav-item"><a href="#tab-trash" class="nav-link" data-toggle="tab"><i class="icon-trash mr-2"></i> @lang('MAIN.TAB.TRASH')</a></li>
            </ul>

            <div class="tab-content">
                <!-- Active Table -->
                    <div class="tab-pane fade show active" id="tab-active">
                        <div class="table-responsive">
                            <table class="table table-bordered table-styled table-striped mb-3 w-100 datatable" id="datatableActive">
                                <thead class="bg-indigo">
                                    <tr>
                                        <th>@lang('MAIN.COLUMN.ID')</th>
                                        <th>@lang('MAIN.COLUMN.NAME')</th>
                                        <th>@lang('MAIN.COLUMN.SPECIFICS')</th>
                                        <th>@lang('MAIN.COLUMN.URL')</th>
                                        <th>@lang('MAIN.COLUMN.INFO')</th>
                                        <th>@lang('MAIN.COLUMN.ACTIONS')</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                <!-- /Active Table -->
                <!-- Approving Table -->
                    <div class="tab-pane fade" id="tab-approving">
                        <div class="table-responsive">
                            <table class="table table-bordered table-styled table-striped mb-3 w-100 datatable" id="datatableApproving">
                                <thead class="bg-indigo">
                                    <tr>
                                        <th>@lang('MAIN.COLUMN.ID')</th>
                                        <th>@lang('MAIN.COLUMN.NAME')</th>
                                        <th>@lang('MAIN.COLUMN.SPECIFICS')</th>
                                        <th>@lang('MAIN.COLUMN.URL')</th>
                                        <th>@lang('MAIN.COLUMN.INFO')</th>
                                        <th>@lang('MAIN.COLUMN.ACTIONS')</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                <!-- /Approving Table -->
                <!-- Approved Table -->
                    <div class="tab-pane fade" id="tab-approved">
                        <div class="table-responsive">
                            <table class="table table-bordered table-styled table-striped mb-3 w-100 datatable" id="datatableApproved">
                                <thead class="bg-indigo">
                                    <tr>
                                        <th>@lang('MAIN.COLUMN.ID')</th>
                                        <th>@lang('MAIN.COLUMN.NAME')</th>
                                        <th>@lang('MAIN.COLUMN.SPECIFICS')</th>
                                        <th>@lang('MAIN.COLUMN.URL')</th>
                                        <th>@lang('MAIN.COLUMN.INFO')</th>
                                        <th>@lang('MAIN.COLUMN.ACTIONS')</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                <!-- /Approved Table -->
                <!-- Trash Table -->
                    <div class="tab-pane fade show" id="tab-trash">
                        <div class="table-responsive">
                            <table class="table table-bordered table-styled table-striped mb-3 w-100 datatable" id="datatableTrash">
                                <thead class="bg-indigo">
                                    <tr>
                                        <th>@lang('MAIN.COLUMN.ID')</th>
                                        <th>@lang('MAIN.COLUMN.NAME')</th>
                                        <th>@lang('MAIN.COLUMN.SPECIFICS')</th>
                                        <th>@lang('MAIN.COLUMN.URL')</th>
                                        <th>@lang('MAIN.COLUMN.INFO')</th>
                                        <th>@lang('MAIN.COLUMN.ACTIONS')</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                <!-- /Trash Table -->
            </div>
    <!-- /Tabs -->

</div>
@endsection

@section('scripts')
<script src="/js/datatables/datatables.min.js"></script>
<script src="/js/select2/select2.min.js"></script>
<script>
    var language = {"url": "/js/datatables/languages/ukrainian.json"};
    var pageLength = {{ isset(auth()->user()->options->datatablePageLength) ? auth()->user()->options->datatablePageLength : 10 }};
    var current_cycle = 0;
    var current_branch = 0;
    var columns = [
            {"data":"id"},
            {"data":"name"},
            {"data":"specific","searchable":false,"orderable":false},
            {"data":"url"},
            {"data":"info","searchable":false,"orderable":false},
            {"data":"actions","searchable":false,"orderable":false}
        ];
    var columnDefs = [
            { "targets": 0, "className": "text-center", },
            { "targets": 2, "className": "text-center", },
            { "targets": 4, "className": "text-center", },
        ];


    var tableActive = $("#datatableActive").DataTable({
        autoWidth : true,
        responsive: true,
        processing: true,
        serverSide: true,
        pageLength: pageLength,
        bSortCellsTop: true,
        order: [[ 0, "desc" ]],
        ajax: {
            url: "{{route('tmaterials.data',$team->id)}}",
            dataType: "json",
            type: "POST",
            data: function ( d ) {
                d._token = "{{ csrf_token() }}";
                d.cycle_id = current_cycle;
                d.branch_id = current_branch;
                d.table = "active";
            }
        },
        columns:columns,
        columnDefs: columnDefs,
        language: language,
    });

    var tableApproving = $("#datatableApproving").DataTable({
        autoWidth : true,
        responsive: true,
        processing: true,
        serverSide: true,
        pageLength: pageLength,
        bSortCellsTop: true,
        order: [[ 0, "desc" ]],
        ajax: {
            url: "{{route('tmaterials.data',$team->id)}}",
            dataType: "json",
            type: "POST",
            data: function ( d ) {
                d._token = "{{ csrf_token() }}";
                d.cycle_id = current_cycle;
                d.branch_id = current_branch;
                d.status = "Approving";
                d.table = "active";
            }
        },
        columns:columns,
        columnDefs: columnDefs,
        language: language,
    });

    var tableApproved = $("#datatableApproved").DataTable({
        autoWidth : true,
        responsive: true,
        processing: true,
        serverSide: true,
        pageLength: pageLength,
        bSortCellsTop: true,
        order: [[ 0, "desc" ]],
        ajax: {
            url: "{{route('tmaterials.data',$team->id)}}",
            dataType: "json",
            type: "POST",
            data: function ( d ) {
                d._token = "{{ csrf_token() }}";
                d.cycle_id = current_cycle;
                d.branch_id = current_branch;
                d.status = "Approved";
                d.table = "active";
            }
        },
        columns:columns,
        columnDefs: columnDefs,
        language: language,
    });

    var tableTrash = $("#datatableTrash").DataTable({
        autoWidth : true,
        responsive: true,
        processing: true,
        serverSide: true,
        pageLength: pageLength,
        bSortCellsTop: true,
        order: [[ 0, "desc" ]],
        ajax: {
            url: "{{route('tmaterials.data',$team->id)}}",
            dataType: "json",
            type: "POST",
            data: function ( d ) {
                d._token = "{{ csrf_token() }}";
                d.cycle_id = current_cycle;
                d.branch_id = current_branch;
                d.table = "trash";
            }
        },
        columns:columns,
        columnDefs: columnDefs,
        language: language,
    });


    // $('div.search-panel').on('click','.element-cycle',function(e){
    //     e.preventDefault();
    //     current_cycle = $(this).attr('data-key');
    //     $('.element-cycle').removeClass('active');
    //     $(this).addClass('active');
    //     $('.datatableRefresh').click();
    // });

</script>
@endsection


@section('modals')
<div id="modal-send-approve-program" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h6 class="modal-title">@lang('MAIN.MODAL.SEND-APPROVE.TITLE')</h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    @lang('MAIN.MODAL.SEND-APPROVE.MESSAGE')
                    <form id="modal-send-approve-program-form" action="#" method="POST">
                        @csrf
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">@lang('MAIN.BUTTON.CANCEL')</button>
                    <button type="button" class="btn bg-primary" id="modal-send-approve-program-submit" data-dismiss="modal">@lang('MAIN.BUTTON.SEND')</button>
                </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){

        $(document).on('click','.btn-send-approve-program',function () {
            $('#modal-send-approve-program-form').attr('action', $(this).attr('href'));
        })

        $('#modal-send-approve-program-submit').click(function(e){
            let form = $('#modal-send-approve-program-form');
            console.log(form.serialize());
            e.preventDefault();
            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                dataType : "json",
                data: form.serialize(),
            }).done((data, status, xhr) => {
                console.log(data);
                $('.datatableRefresh').click();
            }).fail((xhr) => {
                let data = xhr.responseJSON;
                swal({
                    title: data.message,
                    type: 'error',
                    confirmButtonClass: 'btn btn-primary',
                })
            });
        });

});
</script>
@endsection