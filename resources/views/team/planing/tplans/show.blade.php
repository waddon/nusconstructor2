@extends('layouts.app')

@section('content')

{{ Form::model($model, [ 'url' => route('tplans.update', [ $model->id ]) , 'method' => 'PUT', 'class' => 'ajax-form' ]) }}

<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">
        <div class="card mb-3 program-constructor">
            <div class="card-header d-flex justify-content-between align-items-start">
                <h2>Навчальний план закладу освіти (перегляд)</h2>
                <a class="btn btn-icon btn-primary" href="{{ route('tplans.export', $model->id) }}" title="@lang('MAIN.HINT.DOWNLOAD')"><i class="icon-file-word"></i></a>
            </div>
            <div class="card-body">
                <h3 class="text-center">{{$model->name}}</h3>
                @if(!$model->tload->validate() || !$model->validate())
                    <h3 class="text-center text-danger-600">@lang('MAIN.HINT.ERROR-EXISTS')</h3>
                @endif
                <table class="table table-bordered table-striped table-fixed w-100 mb-3" id="load-table">
                    <thead class="bg-dark">
                        <tr>
                            <th>Курс</th>
                            <th>Предмет</th>
                            <th style="width: 15%;">Індекс галузі</th>
                            @foreach($model->tload->cycle->grades as $grade)
                                <th style="width: 10%;">{{ $grade->name }}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($cbranches as $cbranch)
                            @php($componentCount = $model->getComponents(1, $model->tload->loadtemplate->languagetype_id, $model->tload->cycle_id, $cbranch->id )->count())
                            @if( $componentCount > 0 )
                                @foreach( $model->getComponents(1, $model->tload->loadtemplate->languagetype_id, $model->tload->cycle_id, $cbranch->id) as $key => $component)
                                    <tr>
                                        @if(!$key)  
                                        <td rowspan="{{$componentCount}}" class="align-top value" data-cbranch="{{$cbranch->id}}"
                                            @foreach($model->tload->cycle->grades as $grade)
                                                data-grade{{$grade->id}}="{{$model->tload->meta->loads->{$grade->id}->{$cbranch->id}->value ?? 0 }}"
                                            @endforeach
                                        >
                                            @foreach( $model->getComponents(2, $model->tload->loadtemplate->languagetype_id, $model->tload->cycle_id, $cbranch->id ) as $key2 => $component2)

                                                @if(isset($model->meta->components) && in_array($component2->id, $model->meta->components))
                                                    <p class="mb-0 {{ $key2 ? 'mt-2' : '' }}">{{$component2->name}}</p>
                                                @endif
                                            @endforeach
                                        </td>
                                        @endif
                                        <td>{{$component->name}}</td>
                                        <td>{{ $cbranch->marking }}</td>
                                        @foreach($model->tload->cycle->grades as $grade)
                                            <td>{{ $model->meta->details->{$component->id}->{$grade->id} ?? 0 }}</td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            @endif

                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="3">Усього</th>
                            @foreach($model->tload->cycle->grades as $grade)
                                <th>{{ $model->getCalculatedData($grade->id) }}</th>
                            @endforeach
                        </tr>
                        <tr>
                            <th colspan="3">@lang('MAIN.LABEL.ADDITIONAL-WEEK')</th>
                            @foreach($model->tload->cycle->grades as $grade)
                                <th>{{ $model->tload->loadtemplate->getCalculatedData($grade->id, 'additional', 'weekly') }}</th>
                            @endforeach
                        </tr>
                        <tr>
                            <th colspan="3">@lang('MAIN.LABEL.FINANCED-WEEK')</th>
                            @foreach($model->tload->cycle->grades as $grade)
                                <th>{{ $model->tload->loadtemplate->getCalculatedData($grade->id, 'financed', 'weekly') }}</th>
                            @endforeach
                        </tr>
                        <tr>
                            <th colspan="3">@lang('MAIN.LABEL.ADMISSIBLE-WEEK')</th>
                            @foreach($model->tload->cycle->grades as $grade)
                                <th>{{ $model->tload->loadtemplate->getCalculatedData($grade->id, 'admissible', 'weekly') }}</th>
                            @endforeach
                        </tr>
                    </tfoot>
                </table>

                <table class="table table-bordered table-striped table-fixed w-100 mb-3">
                    <thead class="bg-dark">
                        <tr>
                            <th>Міжгалузеві інтегровані курси</th>
                            @foreach($model->tload->cycle->grades as $grade)
                                <th style="width: 10%;">{{ $grade->name }}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach( $model->getComponents(3, $model->tload->loadtemplate->languagetype_id, $model->tload->cycle_id ) as $component3)
                            @if(isset($model->meta->components) && in_array($component3->id, $model->meta->components))
                            <tr>
                                <td>{{$component3->name}}</td>
                                @foreach($model->tload->cycle->grades as $grade)
                                    <td>{{ $model->meta->details->{$component3->id}->{$grade->id} ?? 0 }}</td>
                                @endforeach
                            </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>

                <div class="mb-3" id="errorMessages"></div>
                <div class="text-right">
                    <div class="btn-group">
                        <a class="btn btn-primary" href="{{ $model->tload->parentRoute() }}">Вийти</a>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>


{{ Form::close() }}
@endsection
