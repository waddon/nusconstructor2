@extends('layouts.app')

@section('content')
<style>
    .green-outline {
        outline: 2px solid #9c6;
    }
    .table-fixed input.form-control:focus {
        box-shadow: none;
    }
    .table-fixed input.form-control {
        background: transparent;
    }
    .table-fixed td.error {
        position: relative;
        background: rgba(255, 0, 0, .3);
    }
    .table-fixed td.error .badge{
        position: absolute;
        top: 5px;
        right: 5px;
    }
    .custom-control-right .custom-control-label:before, .custom-control-right .custom-control-label:after {
        top: calc(50% - 0.6125rem);
    }    
    .table-fixed {
        table-layout:fixed;
    }
    .custom-checkbox .custom-control-input:checked ~ .custom-control-label::before {
        /*background-color: #455A64;*/
        background: linear-gradient(180deg, #A3BF41 0%, #81A506 100%);
    }
</style>

{{ Form::model($model, [ 'url' => route('tplans.update', [ $model->id ]) , 'method' => 'PUT', 'class' => 'ajax-form' ]) }}

<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">
        <div class="card mb-3 program-constructor">
            <div class="card-header">
                <h2>Навчальний план закладу освіти</h2>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    {{ Form::label('name', __('MAIN.COLUMN.NAME'), ['class'=>'small text-muted font-italic col-md-2 col-form-label']) }}
                    <div class="col-md-10">
                        {{ Form::text('name', $model->name, array('class' => 'field form-control')) }}
                    </div>
                </div>
                <table class="table table-bordered table-striped table-fixed w-100 mb-3" id="load-table">
                    <thead class="bg-dark">
                        <tr>
                            <th>Курс</th>
                            <th>Предмет</th>
                            <th style="width: 15%;">Індекс галузі</th>
                            @foreach($model->tload->cycle->grades as $grade)
                                <th style="width: 10%;">{{ $grade->name }}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($cbranches as $cbranch)
                            @php($componentCount = $model->getComponents(1, $model->tload->loadtemplate->languagetype_id, $model->tload->cycle_id, $cbranch->id )->count())
                            @if( $componentCount > 0 )
                                @foreach( $model->getComponents(1, $model->tload->loadtemplate->languagetype_id, $model->tload->cycle_id, $cbranch->id) as $key => $component)
                                    <tr>
                                        @if(!$key)
                                        <td rowspan="{{$componentCount}}" class="align-top value" data-cbranch="{{$cbranch->id}}"
                                            @foreach($model->tload->cycle->grades as $grade)
                                                data-grade{{$grade->id}}="{{$model->tload->meta->loads->{$grade->id}->{$cbranch->id}->value ?? 0 }}"
                                            @endforeach
                                        >
                                            @foreach( $model->getComponents(2, $model->tload->loadtemplate->languagetype_id, $model->tload->cycle_id, $cbranch->id ) as $component2)
                                                <div class="custom-control custom-control-right custom-checkbox">
                                                    <input 
                                                        id="c{{$component2->id}}"
                                                        class="custom-control-input"
                                                        name="components[]"
                                                        type="checkbox"
                                                        value="{{$component2->id}}"
                                                        {{ (isset($model->meta->components) && in_array($component2->id, $model->meta->components)) ? 'checked' : ''}}
                                                    >
                                                    <label for="c{{$component2->id}}" class="custom-control-label position-static">
                                                        {{$component2->name}}
                                                        <br><small class="text-muted" style="line-height: 1;">{{$component2->restricted->implode('name', ', ')}}</small>
                                                    </label>
                                                </div>
                                            @endforeach
                                        </td>
                                        @endif
                                        <td>
                                            <div class="custom-control custom-control-right custom-checkbox">
                                                <input 
                                                    id="c{{$component->id}}"
                                                    class="custom-control-input"
                                                    name="components[]"
                                                    type="checkbox"
                                                    value="{{$component->id}}"
                                                    {{ (isset($model->meta->components) && in_array($component->id, $model->meta->components)) ? 'checked' : ''}}
                                                >
                                                <label for="c{{$component->id}}" class="custom-control-label position-static">
                                                    {{$component->name}}
                                                </label>
                                            </div>
                                        </td>
                                        <td>{{-- $cbranch->marking --}}{{$component->getMarking()}}</td>
                                        @foreach($model->tload->cycle->grades as $grade)
                                            <td class="p-0">
                                                @if($component->canEdit($grade->id))
                                                <input
                                                    type="number"
                                                    class="w-100 border-0 p-3 form-control detailInput checkable"
                                                    name="details[{{$component->id}}][{{ $grade->id }}]"
                                                    value="{{ $model->meta->details->{$component->id}->{$grade->id} ?? 0 }}"
                                                    step="0.5"
                                                    data-cbranch="{{$cbranch->id}}"
                                                    data-grade="{{$grade->id}}"
                                                    data-min-value="{{$model->tload->loadtemplate->getRestrictionValue($component->id,$grade->id,'minimum')}}"
                                                    data-max-value="{{$model->tload->loadtemplate->getRestrictionValue($component->id,$grade->id,'maximum')}}"
                                                >
                                                @endif
                                            </td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            @endif

                        @endforeach
                        <tr>
                            <td colspan="3"><strong>Всього обов’язкової частини</strong></td>
                            @foreach($model->tload->cycle->grades as $grade)
                                <td class="text-center"><strong class="obligatory-{{$grade->id}}"></strong></td>
                            @endforeach
                        </tr>
                    </tbody>
                </table>

                <table class="table table-bordered table-striped table-fixed w-100 mb-3" id="intersectoral-table">
                    <thead class="bg-dark">
                        <tr>
                            <th>Міжгалузеві інтегровані курси</th>
                            @foreach($model->tload->cycle->grades as $grade)
                                <th style="width: 10%;">{{ $grade->name }}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach( $model->getComponents(3, $model->tload->loadtemplate->languagetype_id, $model->tload->cycle_id ) as $component3)
                            <tr>
                                <td>
                                    <div class="custom-control custom-control-right custom-checkbox">
                                        <input 
                                            id="c{{$component3->id}}"
                                            class="custom-control-input"
                                            name="components[]"
                                            type="checkbox"
                                            value="{{$component3->id}}"
                                            {{ (isset($model->meta->components) && in_array($component3->id, $model->meta->components)) ? 'checked' : ''}}
                                        >
                                        <label for="c{{$component3->id}}" class="custom-control-label position-static">
                                            {{$component3->name}}
                                            <br><small class="text-muted" style="line-height: 1;">{{$component3->cbranches->implode('name', ', ')}}</small>
                                        </label>
                                    </div>
                                </td>
                                @foreach($model->tload->cycle->grades as $grade)
                                    <td class="p-0">
                                        <input
                                            type="number"
                                            class="w-100 border-0 p-3 form-control"
                                            name="details[{{$component3->id}}][{{ $grade->id }}]"
                                            value="{{ $model->meta->details->{$component3->id}->{$grade->id} ?? 0 }}"
                                            step="0.5"
                                            data-grade="{{$grade->id}}"                                            
                                        >
                                    </td>
                                @endforeach
                            </tr>
                        @endforeach
                        <tr>
                            <td><strong>Загальний тижневий обсяг</strong></td>
                            @foreach($model->tload->cycle->grades as $grade)
                                <td class="text-center"><strong class="total-{{$grade->id}}"></strong></td>
                            @endforeach
                        </tr>
                    </tbody>
                </table>

                <div class="mb-3" id="errorMessages"></div>
                <div class="text-right">
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary" id="check">Перевірити ведення</button>
                        <button type="submit" class="btn btn-success">Зберегти</button>
                        <a class="btn btn-primary" href="{{ $model->tload->parentRoute() }}">Вийти</a>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>


{{ Form::close() }}
@endsection

@section('scripts')
<script src="{{ asset('/js/jquery_ui/interactions.min.js') }}"></script>
<script type="text/javascript">
$(document).ready( () => {
    var errors = []
    var errorIndex = 1
    var symbol = '<i class="icon-notification2 mr-3 icon-2x text-danger"></i>'

    $('#check').click( () => checkLoad())

    function checkLoad() {
        errors = []
        errorIndex = 1
        let result = {}
        $('#load-table td').removeClass('error')
        $('#load-table .badge').remove()
        $('#errorMessages').empty()

        $('#load-table .value').each(function( index ) {
            result[$(this).attr('data-cbranch')] = {}
            @foreach($model->tload->cycle->grades as $grade)
                result[$(this).attr('data-cbranch')][{{$grade->id}}] = {
                    'value' : parseFloat($(this).attr('data-grade{{$grade->id}}')),
                    'sumDetails' : 0,
                }
            @endforeach
        })

        $('#load-table .detailInput').each(function( index ) {
            let value = parseFloat($(this).val())
            result[$(this).attr('data-cbranch')][$(this).attr('data-grade')]['sumDetails'] += value
        })

        $.each(result, function(index, value){
            $.each(value, function(index2, value2){
                let value = value2['value']
                let sumDetails = value2['sumDetails']
                if( value != sumDetails ) {
                    $error = $(`[data-cbranch=${index}][data-grade=${index2}]`)
                    $error.parent().addClass('error');                    
                    $error.parent().append(`<span class="badge badge-light badge-pill">${errorIndex}</span>`)
                    errors.push({'id': errorIndex, 'message': `<p class="d-flex align-items-center">${symbol}<span><strong>${errorIndex}.</strong> Сума значеннь галузі <strong>${sumDetails}</strong> повинна дорівнювати значенню Обсягу тижневого навантаження закладу освіти, а саме <strong>${value}</strong></span></p>`})
                    errorIndex++;
                }
            })
        })
        checkCheckables()
        showErrorMessages()
    }


    function checkCheckables() {
        $('.checkable').each(function( index ) {
            let value = parseFloat($(this).val())
            let minValue = parseFloat($(this).attr('data-min-value'))
            let maxValue = parseFloat($(this).attr('data-max-value'))
            if ((minValue > 0 && value < minValue) || (maxValue > 0 && value > maxValue)) {
                $(this).parent().addClass('error');
                $(this).parent().append(`<span class="badge badge-light badge-pill">${errorIndex}</span>`)
                let message = (minValue > 0 && value < minValue)
                    ? `Значення повинно бути не менш  ніж <strong>${minValue}</strong>. Наразі воно дорівнює <strong>${value}</strong>`
                    : `Значення не повинно перевищювати <strong>${maxValue}</strong>. Наразі воно дорівнює <strong>${value}</strong>`
                errors.push({'id': errorIndex, 'message': `<p class="d-flex align-items-center">${symbol}<span><strong>${errorIndex}.</strong> ${message}</span></p>`})
                // errors.push({'id': errorIndex, 'message': `<p class="d-flex align-items-center">${symbol}<span><strong>${errorIndex}.</strong> Значення повинно знаходитися в межах <strong>${minValue}</strong> та <strong>${maxValue}</strong>. Наразі воно дорівнює <strong>${value}</strong></span></p>`})
                errorIndex++;
            }
            console.log(errors)
        })
    }

    function showErrorMessages() {
        $('#errorMessages').empty()
        $.each(errors, function(index, value){
            $('#errorMessages').append(value.message)
        })
    }


    $('.table-fixed').on('focus','input.form-control', function(){
        $(this).closest('td').addClass('green-outline')
    })
    $('.table-fixed').on('blur','input.form-control', function(){
        $(this).closest('td').removeClass('green-outline')
    })

    $(document).on('change','#load-table input.form-control, #intersectoral-table input.form-control', () => {
        calculate()
        // checkLoad()
    })

    calculate()
    // checkLoad()

    function calculate() {
        let data = {}
        @foreach($model->tload->cycle->grades as $grade)
            data[{{$grade->id}}] = {'obligatory' : 0, 'total' : 0}
        @endforeach
        $('#load-table input.form-control').each(function( index ) {
            let element = data[ parseFloat($(this).attr('data-grade')) ]
            element.obligatory += parseFloat($(this).val())
            element.total += parseFloat($(this).val())
        })
        $('#intersectoral-table input.form-control').each(function( index ) {
            let element = data[ parseFloat($(this).attr('data-grade')) ]
            element.total += parseFloat($(this).val())
        })
        $.each(data, function( index, value ) {
            $('.obligatory-' + index).text(value.obligatory)
            $('.total-' + index).text(value.total)
        })
    }

})

</script>
@endsection

