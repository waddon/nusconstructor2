@extends('layouts.app')

@section('content')

<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">
        <div class="card mb-3 program-constructor">
            <div class="card-header d-flex justify-content-between align-items-start">
                <h2>Річний навчальний план закладу освіти (перегляд)</h2>
                <a class="btn btn-icon btn-primary" href="{{ route('yplans.export', $model->id) }}" title="@lang('MAIN.HINT.DOWNLOAD')"><i class="icon-file-word"></i></a>
            </div>
            <div class="card-body">
                <h3 class="text-center">{{$model->name}}</h3>
                @if(!$model->tplan->tload->validate() || !$model->tplan->validate() || !$model->validate())
                    <h3 class="text-center text-danger-600">@lang('MAIN.HINT.ERROR-EXISTS')</h3>
                @endif

                <table class="table table-bordered table-striped w-100 mb-3">
                    <thead class="bg-dark">
                        <tr>
                            <th class="text-center">Освітні галузі</th>
                            <th class="text-center">Освітні компоненти</th>
                            <th class="text-center">Кількість годин на тиждень</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data->sectoral as $cbranch)
                            <tr>
                                <td rowspan="{{ max(count($cbranch['components']), 1) }}"><strong>{{$cbranch['name']}}</strong></td>
                                @if(count($cbranch['components']) == 0)
                                    <td></td><td></td>
                                @else
                                    @foreach($cbranch['components'] as $key => $component)
                                        <td>{{$component->name}}</td><td>{{$component->value}}</td>
                                        @if( array_key_last($cbranch['components']) != $key )
                                            </tr><tr>
                                        @endif
                                    @endforeach
                                @endif
                            </tr>
                        @endforeach

                        <tr>
                            <td colspan="2"><strong>Разом</strong></td>
                            <td><strong>{{$data->sumSectoral }}</strong></td>
                        </tr>
                        <tr>
                            <td colspan="3"><strong>Міжгалузеві інтегровані курси</strong></td>
                        </tr>
                        @foreach($data->intersectoral as $component)
                            <tr>
                                <td colspan="2">{{$component->name}}</td>
                                <td>{{$component->value}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="2"><strong>@lang('MAIN.LABEL.ADMISSIBLE-WEEK')</strong></th>
                            <th><strong>{{ $model->tplan->tload->loadtemplate->getCalculatedData($model->grade_id, 'admissible', 'weekly') }}</strong></th>
                        </tr>
                        <tr>
                            <th colspan="2"><strong>Всього</strong></th>
                            <th><strong>{{ $data->sumSectoral + $data->sumIntersectoral }}</strong></th>
                        </tr>
                    </tfoot>
                </table>


                <h3 class="text-center">Перелік модельних навчальних програм </h3>
                <table class="table table-bordered table-striped w-100 mb-3">
                    <thead class="bg-dark">
                        <tr>
                            <th class="text-center">Освітній компонент</th>
                            <th class="text-center">Назва програми</th>
                            <th class="text-center">Клас</th>
                            <th class="text-center">Коли і ким погоджено, джерело</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($model->meta->usedComponents) && (is_array($model->meta->usedComponents) || is_object($model->meta->usedComponents)))
                            @foreach($model->meta->usedComponents as $component)
                                <tr>
                                    <td>{{$component->name}}</td>
                                    <td>{{$component->cprogram_name}}</td>
                                    <td>{{$model->grade->name}}</td>
                                    <td></td>
                                </tr>
                            @endforeach
                        @endif
                        @if(isset($model->meta->additionalComponentList) && (is_array($model->meta->additionalComponentList) || is_object($model->meta->additionalComponentList)))
                            @foreach($model->meta->additionalComponentList as $component)
                                <tr>
                                    <td>{{$component->name}}</td>
                                    <td>{{$component->cprogram_name}}</td>
                                    <td>{{$model->grade->name}}</td>
                                    <td></td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>

                <div class="text-right">
                    <div class="btn-group">
                        <a class="btn btn-primary" href="{{ $model->tplan->tload->parentRoute() }}">Вийти</a>
                    </div>
                </div>

            </div>
        </div>

    </div>

</div>

@endsection
