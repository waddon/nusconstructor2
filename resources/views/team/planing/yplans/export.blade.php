<html>
<head><meta charset="utf-8"></head>
<body>
    <style>
        .text-center{text-align: center;}
        .text-left{text-align: left;}
        .table{border-top: 1px solid #ddd;border-left: 1px solid #ddd;width:100%;}
        .table th, .table td{border-bottom: 1px solid #ddd;border-right: 1px solid #ddd;}
        .bg-light {background: #fafafa;}
    </style>
    <h2 class="text-center">Річний навчальний план закладу освіти</h2>
    <h3 class="text-center">{{$model->name}}</h3>

    @if(!$model->tplan->tload->validate() || !$model->tplan->validate() || !$model->validate())
        <h3 class="text-center" style="color: #e53935;">@lang('MAIN.HINT.ERROR-EXISTS')</h3>
    @endif

    @php($data = $model->getForShow())

                <table class="table">
                    <thead class="bg-dark">
                        <tr>
                            <th class="text-center">Освітні галузі</th>
                            <th class="text-center">Освітні компоненти</th>
                            <th class="text-center">Кількість годин на тиждень</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data->sectoral as $cbranch)
                            <tr>
                                <td rowspan="{{ max(count($cbranch['components']), 1) }}"><strong>{{$cbranch['name']}}</strong></td>
                                @if(count($cbranch['components']) == 0)
                                    <td></td><td></td>
                                @else
                                    @foreach($cbranch['components'] as $key => $component)
                                        <td>{{$component->name}}</td><td class="text-center">{{$component->value}}</td>
                                        @if( array_key_last($cbranch['components']) != $key )
                                            </tr><tr>
                                        @endif
                                    @endforeach
                                @endif
                            </tr>
                        @endforeach

                        <tr>
                            <td colspan="2"><strong>Разом</strong></td>
                            <td class="text-center"><strong>{{$data->sumSectoral }}</strong></td>
                        </tr>
                        <tr>
                            <td colspan="3"><strong>Міжгалузеві інтегровані курси</strong></td>
                        </tr>
                        @foreach($data->intersectoral as $component)
                            <tr>
                                <td colspan="2">{{$component->name}}</td>
                                <td class="text-center">{{$component->value}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="2" class="text-left"><strong>@lang('MAIN.LABEL.ADMISSIBLE-WEEK')</strong></th>
                            <th><strong>{{ $model->tplan->tload->loadtemplate->getCalculatedData($model->grade_id, 'admissible', 'weekly') }}</strong></th>
                        </tr>
                        <tr>
                            <th colspan="2" class="text-left"><strong>Всього</strong></th>
                            <th><strong>{{ $data->sumSectoral + $data->sumIntersectoral }}</strong></th>
                        </tr>
                    </tfoot>
                </table>


                <h3 class="text-center">Перелік модельних навчальних програм </h3>
                <table class="table">
                    <thead class="bg-dark">
                        <tr>
                            <th class="text-center">Освітній компонент</th>
                            <th class="text-center">Назва програми</th>
                            <th class="text-center">Клас</th>
                            <th class="text-center">Коли і ким погоджено, джерело</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($model->meta->usedComponents) && (is_array($model->meta->usedComponents) || is_object($model->meta->usedComponents)))
                            @foreach($model->meta->usedComponents as $component)
                                <tr>
                                    <td>{{$component->name}}</td>
                                    <td>{{$component->cprogram_name}}</td>
                                    <td class="text-center">{{$model->grade->name}}</td>
                                    <td></td>
                                </tr>
                            @endforeach
                        @endif
                        @if(isset($model->meta->additionalComponentList) && (is_array($model->meta->additionalComponentList) || is_object($model->meta->additionalComponentList)))
                            @foreach($model->meta->additionalComponentList as $component)
                                <tr>
                                    <td>{{$component->name}}</td>
                                    <td>{{$component->cprogram_name}}</td>
                                    <td class="text-center">{{$model->grade->name}}</td>
                                    <td></td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>


</body>
</html>