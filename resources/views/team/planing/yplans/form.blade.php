@extends('layouts.app')

@section('content')
<style>
    .green-outline {
        outline: 2px solid #9c6;
    }
    .table-fixed input.form-control:focus {
        box-shadow: none;
    }
    .table-fixed input.form-control {
        background: transparent;
    }
    .table-fixed td.error {
        position: relative;
        background: rgba(255, 0, 0, .3);
    }
    .table-fixed td.error .badge{
        position: absolute;
        top: 5px;
        right: 5px;
    }
    .custom-control-right .custom-control-label:before, .custom-control-right .custom-control-label:after {
        top: calc(50% - 0.6125rem);
    }    
    .table-fixed {
        table-layout:fixed;
    }
    .custom-checkbox .custom-control-input:checked ~ .custom-control-label::before {
        /*background-color: #455A64;*/
        background: linear-gradient(180deg, #A3BF41 0%, #81A506 100%);
    }
    .text-wrap {
        white-space: pre-wrap;
    }
    .table-fixed .table-fixed tr:nth-of-type(odd) {
        background: transparent;
    }
    .table-fixed .table-fixed td {
        /*border: 0px;*/
    }
    .table-fixed .table-fixed tr:last-child td {
        border-bottom: 0px;
    }
    .table-fixed .table-fixed td {
        border-right: 0px;
    }
    .table-fixed .table-fixed td:first-child {
        border-left: 0px;
    }
    .component:not(.visible) {
        display: none;
    }

    [data-branch=intersectoral] table td:first-child, [data-branch=additional] table td:first-child{
        width: 67%;
    }
</style>

{{ Form::model($model, [ 'url' => route('yplans.update', [ $model->id ]) , 'method' => 'PUT', 'class' => 'ajax-form' ]) }}

<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">
        <div class="card mb-3 program-constructor">
            <div class="card-header">
                <h2>Річний навчальний план закладу освіти</h2>
            </div>
            <div class="card-body d-none">
                <div class="form-group row">
                    {{ Form::label('name', __('MAIN.COLUMN.NAME'), ['class'=>'small text-muted font-italic col-md-2 col-form-label']) }}
                    <div class="col-md-10">
                        {{ Form::text('name', $model->name, array('class' => 'field form-control')) }}
                    </div>
                    <input type="hidden" name="componentList">
                </div>
                <table class="table table-bordered table-striped table-fixed w-100 mb-3" id="load-table">
                    <thead class="bg-dark">
                        <tr>
                            <th>Освітня галузь</th>
                            <th>Предмети / курси</th>
                            <th>{{ $model->grade->name }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($cbranches as $cbranch)
                            <tr class="cbranch" data-branch="{{ $cbranch->id}}"
                                data-min="{{ $model->tplan->tload->loadtemplate->meta->loads->{$model->grade_id}->{$cbranch->id}->minimum ?? '' }}"
                                data-max="{{ $model->tplan->tload->loadtemplate->meta->loads->{$model->grade_id}->{$cbranch->id}->maximum ?? '' }}"
                            >
                                <td class="p-2" style="vertical-align: top;">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span>{{ $cbranch->name }}</span>
                                        <span class="btn btn-success btn-add-component p-1 text-wrap" data-toggle="modal" data-target="#modal-select-components" style="flex: 0 0 100px;line-height: 10px;"><small><small>@lang('MAIN.BUTTON.ADD-COMPONENT')</small></small></span>
                                    </div>
                                </td>
                                <td class="p-0" colspan="2">
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="2"><strong>Разом:</strong></td>
                            <td><strong id="sumLoads">0</strong></td>
                        </tr>
                    </tfoot>
                </table>

                <table class="table table-bordered table-striped table-fixed w-100 mb-3" id="load-table2">
                    <thead class="bg-dark">
                        <tr>
                            <th colspan="2">Міжгалузеві інтегровані курси</th>
                            <!--th>{{ $model->grade->name }}</th-->
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="cbranch" data-branch="intersectoral">
                            <td class="p-0" colspan="2"></td>
                        </tr>
                        <tr data-branch="additional">
                            <td class="p-0" colspan="2">
                                <table class="table table-fixed w-100 bg-transparent">
                                    <tbody></tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr data-branch="intersectoral">
                            <th colspan="2">
                                <div class="btn-group">
                                    <span class="btn btn-success btn-add-component" data-toggle="modal" data-target="#modal-select-components">@lang('MAIN.BUTTON.ADD-COMPONENT')</span>
                                    <span class="btn btn-primary btn-add-additional-component">@lang('MAIN.BUTTON.ADD-ADDITIONAL-COMPONENT')</span>
                                </div>
                            </th>
                        </tr>
                        <tr>                            
                            <td class="p-0" colspan="2">
                                <table class="table table-fixed w-100 bg-transparent">
                                    <tbody>
                                        <tr>
                                            <td class="px-3 py-2 font-weight-bold" style="width: 67%">Усього:</td>
                                            <td class="p-0" style="width: 33%"><input class="w-100 border-0 p-3 form-control font-weight-bold checkable" type="number" id="total" data-min-value="0" data-max-value="{{ $model->tplan->tload->loadtemplate->getCalculatedData($model->grade_id, 'admissible') }}" readonly></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tfoot>
                </table>

                <div class="mb-3" id="errorMessages"></div>

                <div class="text-right">
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary" id="check">Перевірити ведення</button>
                        <button type="submit" class="btn btn-success">Зберегти</button>
                        <a class="btn btn-primary" href="{{ $model->tplan->tload->parentRoute() }}">Вийти</a>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

<template id="additional-component">
    <tr class="additional-component" data-component="{id}" data-cprogram-id="{programId}">
        <td class="p-2">
            <div class="d-flex justify-content-between align-items-center">
                <span style="flex: 1 1 1px;"><input type="text" class="w-100 border-0 form-control detailTitle" data-branch="additional" placeholder="@lang('MAIN.PLACEHOLDER.COMPONENT-NAME')" value="{componentName}" name="additionalComponentList[{id}][name]"><div style="line-height: 10px;"><a href="{programUrl}" target="_blank" class="cprogram-url"><small class="text-muted cprogram-name">{programName}</small></a></div></span>
                <span class="btn btn-outline-success btn-add-cprogram2 px-1" data-toggle="modal" data-target="#modal-select-cprogram2" style="flex: 0 0 100px;line-height: 10px;" data-branch="additional" data-component="{id}"><small><small>Обрати програму</small></small></span>
            </div>
        </td>
        <td class="p-0"><input type="number" class="w-100 border-0 p-3 form-control detailInput" value="{loadValue}" step="0.5" data-branch="additional" name="additionalComponentList[{id}][value]"><input type="hidden" value="{programId}" name="additionalComponentList[{id}][cprogram_id]" class="cprogram_id"><input type="hidden" value="{programUrl}" name="additionalComponentList[{id}][cprogram_url]" class="cprogram_url"><input type="hidden" value="{programName}" name="additionalComponentList[{id}][cprogram_name]" class="cprogram_name"></td>
    </tr>
</template>

{{ Form::close() }}
@endsection

@section('scripts')
<script src="{{ asset('/js/jquery_ui/interactions.min.js') }}"></script>
<script type="text/javascript">

const renderTemplate = window.renderTemplate = function renderTemplate(name, data) {
    var template = document.getElementById(name).innerHTML;
 
    for (var property in data) {
        if (data.hasOwnProperty(property)) {
            var search = new RegExp('{' + property + '}', 'g');
            template = template.replace(search, data[property]);
        }
    }
    return template;
}

$(document).ready( () => {
    let componentList = {}
    let cprogramList = {}
    let yplanData = {}

    var errors = []
    var errorIndex = 1
    var symbol = '<i class="icon-notification2 mr-3 icon-2x text-danger"></i>'

    $('#check').click( () => {
        errors = []
        errorIndex = 1

        // let errorIndex = 1
        // let symbol = '<i class="icon-notification2 mr-3 icon-2x text-danger"></i>'
        let result = {}
        $('#load-table td').removeClass('error')
        $('#load-table .badge').remove()
        $('#errorMessages').empty()

        $('.cbranch').each(function( index ) {
            let min = parseFloat( $(this).attr('data-min') )
            let max = parseFloat( $(this).attr('data-max') )
            let val = 0;
            let $element = $(this)
            let $container = $(this).find('td[colspan=2]')
            $element.find('input').each(function( index2 ) {
                val += parseFloat($(this).val())
            })
            if (val < min || val > max) {
                $container.addClass('error');
                $container.append(`<span class="badge badge-light badge-pill">${errorIndex}</span>`)
                errors.push({'id': errorIndex, 'message': `<p class="d-flex align-items-center">${symbol}<span><strong>${errorIndex}.</strong> Значення <strong>${val}</strong> знаходиться за межами дозволеного діапазону між <strong>${min}</strong> та <strong>${max}</strong></span></p>`})

                // $('#errorMessages').append(`<p class="d-flex align-items-center">${symbol}<span><strong>${errorIndex}.</strong> Значення <strong>${val}</strong> знаходиться за межами дозволеного діапазону між <strong>${min}</strong> та <strong>${max}</strong></span></p>`)
                errorIndex++;
            }
            // console.log(val)
            //$(this).find('td[colspan=2]').addClass('error')
        })
        checkCheckables()
        showErrorMessages()

        // console.log(result);
    })

    function checkCheckables() {
        $('.checkable').each(function( index ) {
            let value = parseFloat($(this).val())
            let minValue = parseFloat($(this).attr('data-min-value'))
            let maxValue = parseFloat($(this).attr('data-max-value'))
            if ((minValue > 0 && value < minValue) || (maxValue > 0 && value > maxValue)) {
                $(this).parent().addClass('error');
                $(this).parent().append(`<span class="badge badge-light badge-pill">${errorIndex}</span>`)
                let message = (minValue > 0 && value < minValue)
                    ? `Значення повинно бути не менш  ніж <strong>${minValue}</strong>. Наразі воно дорівнює <strong>${value}</strong>`
                    : `Значення не повинно перевищювати <strong>${maxValue}</strong>. Наразі воно дорівнює <strong>${value}</strong>`
                errors.push({'id': errorIndex, 'message': `<p class="d-flex align-items-center">${symbol}<span><strong>${errorIndex}.</strong> ${message}</span></p>`})
                errorIndex++;
            }
            console.log(errors)
        })
    }

    function showErrorMessages() {
        $('#errorMessages').empty()
        $.each(errors, function(index, value){
            $('#errorMessages').append(value.message)
        })
    }


    $('.table-fixed').on('focus','input.form-control', function(){
        $(this).closest('td').addClass('green-outline')
    })
    $('.table-fixed').on('blur','input.form-control', function(){
        $(this).closest('td').removeClass('green-outline')
    })


    getData()
    function getData(){
        $.ajax({
            url: '{{route("api.yplan.data", $model->id)}}',
            dataType : "json",
        }).done(function(data) {
            componentList = data.componentList
            additionalComponentList = data.additionalComponentList
            cprogramList = data.cprogramList
            //yplanData = data.yplanData
            console.log(data)
            drawComponents()
            updateComponentList()
        })
    }

    function drawComponents() {
        $.each(componentList, function(index, value){
            let $container = $(`tr[data-branch=${index}]`).find('td[colspan=2]')
            $container.empty().html('<table class="table table-fixed w-100 bg-transparent"><tbody></tbody></table>')
            let $containerTable = $container.find('tbody')
            $.each(value, function(index2, value2){
                let visibleClass = componentList[index][index2]['checked'] ? 'visible' : ''
                $url = typeof(cprogramList[index2][value2.cprogram_id]) !== 'undefined' ? cprogramList[index2][value2.cprogram_id]['url'] : '#'
                $containerTable.append(
                    `<tr class="component ${visibleClass}" data-component="${value2.id}" data-cprogram-id="${value2.cprogram_id}">
                        <td class="p-2">
                            <div class="d-flex justify-content-between align-items-center">
                                <span>${value2.name}<div style="line-height: 10px;"><a href="${$url}" target="_blank" class="cprogram-url"><small class="text-muted cprogram-name">${value2.cprogram_name}</small></a></div></span>
                                <span class="btn btn-outline-success btn-add-cprogram px-1" data-toggle="modal" data-target="#modal-select-cprogram" style="flex: 0 0 100px;line-height: 10px;" data-component="${value2.id}" data-branch="${index}"><small><small>@lang('MAIN.BUTTON.ADD-PROGRAM')</small></small></span>
                            </div>
                        </td>
                        <td class="p-0">
                            <input type="number" class="w-100 border-0 p-3 form-control detailInput" value="${componentList[index][index2]['value']}" step="0.5" data-component="${value2.id}" data-branch="${index}">
                        </td>
                    </tr>`
                )

            })
        })
        let $container = $('[data-branch=additional] tbody')
        let i = 1
        $.each(additionalComponentList, function(index, value){
            $container.append( renderTemplate('additional-component', {
                id: i,
                componentName: '',
                programUrl: '',
                programId: value.cprogram_id,
                programName: '',
                loadValue: value.value})
            )
            $element = $container.find(`.additional-component[data-component=${i}]`)
            $element.find('.detailTitle').val(value.name ?? '')
            $element.find('.cprogram_name').val(value.cprogram_name)
            $element.find('.cprogram-name').text(value.cprogram_name)
            $element.find('.cprogram_url').val(value.cprogram_url)
            i++
        })
        $('.card-body.d-none').removeClass('d-none')
    }


    $('.btn-add-component').click(function(e){
        let contentArea = $('#modal-select-components').find('.modal-body')
        let cbranch = parseInt($(this).closest('tr').attr('data-branch'))
        cbranch = cbranch ? cbranch : 'intersectoral'
        contentArea.empty()
        $.each(componentList[ cbranch ], function(index, value){
            let checkvalue = value.checked ? 'checked' : ''
            contentArea.append(`
                <div class="custom-control custom-checkbox mb-3">
                    <input id="c${value.id}" class="custom-control-input select-component-input" type="checkbox" data-component="${value.id}" data-branch="${cbranch}" ${checkvalue}>
                    <label for="c${value.id}" class="custom-control-label">${value.name}</label>
                </div>
            `)
        })
        checkActiveComponents()
    })


    $('.select-components-submit').click(function(){
        $('.select-component-input').each(function( index ) {
            let componentId = parseInt($(this).attr('data-component'))
            let cbranch = parseInt($(this).attr('data-branch'))
            cbranch = cbranch ? cbranch : 'intersectoral'
            if ($(this).prop('checked')) {
                componentList[ cbranch ][ componentId ]['checked'] = true
                $(`.component[data-component=${componentId}]`).addClass('visible')
            } else {
                componentList[ cbranch ][ componentId ]['checked'] = false
                $(`.component[data-component=${componentId}]`).removeClass('visible')
                $(`.component[data-component=${componentId}]`).find('input').val(0)
            }
        })
        updateComponentList()
    })

    $(document).on('change','.detailInput', function(){
        if (typeof $(this).attr('data-component') != 'undefined') {
            componentList[ $(this).attr('data-branch') ][ $(this).attr('data-component') ]['value'] = $(this).val()
        }
        updateComponentList()
    })

    $(document).on('change','.select-component-input', function(){
        checkActiveComponents()
    })

    function updateComponentList() {
        $('input[name=componentList]').val( JSON.stringify(componentList) )
        let sumLoads = 0;
        let total = 0;
        $('#load-table .component.visible').each(function( index ){
            sumLoads += parseFloat($(this).find('.detailInput').val())
            total += parseFloat($(this).find('.detailInput').val())
        })
        $('#load-table2 .component.visible').each(function( index ){
            total += parseFloat($(this).find('.detailInput').val())
        })
        $('#load-table2 .additional-component').each(function( index ){
            total += parseFloat($(this).find('.detailInput').val())
        })        
        $('#sumLoads').text(sumLoads)
        $('#total').val(total)
    }

    function checkActiveComponents() {
        $('.select-component-input').removeAttr('disabled')
        $('.select-component-input').each(function( index ) {
            let componentId = parseInt($(this).attr('data-component'))
            let cbranch = parseInt($(this).attr('data-branch'))
            cbranch = cbranch ? cbranch : 'intersectoral'
            if ($(this).prop('checked')) {
                // componentList[ cbranch ][ componentId ]['checked'] = true
                // console.log(componentList[ cbranch ][ componentId ]['restricted'])
                $.each(componentList[ cbranch ][ componentId ]['restricted'], function(index2, value){
                    $(`.select-component-input[data-component=${index2}]`).attr('disabled','disabled')
                })
            }
        })
    }

    $(document).on('click', '.btn-add-cprogram', function(e) {
        let contentArea = $('#modal-select-cprogram').find('.modal-body')
        // let component = parseInt($(this).closest('tr').attr('data-component'))
        let component = $(this).attr('data-component')
            // component = component ? component : 'additional'
            console.log(component)
        $('#modal-select-cprogram').attr('data-component', component)
        $('#modal-select-cprogram').attr('data-cbranch', $(this).attr('data-branch'))
        let cprogram_id = parseInt($(this).closest('tr').attr('data-cprogram-id'))
        contentArea.empty()
        $.each(cprogramList[ component ], function(index, value){
            let checkvalue = value.id == cprogram_id ? 'checked' : ''
            contentArea.append(`
                <div class="custom-control custom-radio mb-3">
                    <input id="ch${value.id}" class="custom-control-input select-cprogram-input" type="radio" value="${value.id}" ${checkvalue} name="select-cprograms">
                    <label for="ch${value.id}" class="custom-control-label" data-url="${value.url}">${value.name} (авт. ${value.authors})</label>
                </div>
            `)
        })
    })

    $(document).on('click', '.btn-add-cprogram2', function(e) {
        let contentArea = $('#modal-select-cprogram2').find('.modal-body')
        let component = $(this).attr('data-component')
        $('#modal-select-cprogram2').attr('data-component', component)
        $('#modal-select-cprogram2').attr('data-cbranch', $(this).attr('data-branch'))
        let cprogram_id = parseInt($(this).closest('tr').attr('data-cprogram-id'))
        contentArea.empty()
        $.each(cprogramList[ 'additional' ], function(index, value){
            let checkvalue = value.id == cprogram_id ? 'checked' : ''
            contentArea.append(`
                <div class="custom-control custom-radio mb-3">
                    <input id="c${value.id}" class="custom-control-input select-cprogram-input" type="radio" value="${value.id}" ${checkvalue} name="select-cprograms">
                    <label for="c${value.id}" class="custom-control-label" data-url="${value.url}">${value.name} (авт. ${value.authors})</label>
                </div>
            `)
        })
    })


    $(document).on('click','.btn-add-additional-component', function(e){
        $('[data-branch=additional] tbody').append( renderTemplate('additional-component', {
            id: $('.additional-component').length + 1,
            componentName:'',
            programUrl:'#',
            programId:0,
            programName:'Програма не обрана',
            loadValue:0})
        )
    })

    $('.select-cprogram-submit').click(function(){
        console.log($('#modal-select-cprogram').attr('data-cbranch'))
        let cbranch = parseInt($('#modal-select-cprogram').attr('data-cbranch'))
        cbranch = cbranch ? cbranch : 'intersectoral'
        let componentId =  parseInt($('#modal-select-cprogram').attr('data-component'))
        let radio = $('[name=select-cprograms]:checked')
        let cprogramName = radio.siblings('.custom-control-label').text()
        let cprogramUrl = radio.siblings('.custom-control-label').attr('data-url')
        componentList[ cbranch ][ componentId ][ 'cprogram_id' ] = parseInt(radio.val())
        componentList[ cbranch ][ componentId ][ 'cprogram_name' ] = cprogramName
        $(`.component[data-component=${componentId}]`).attr('data-cprogram-id',parseInt(radio.val()))
        $(`.component[data-component=${componentId}]`).find('.cprogram-name').html(cprogramName)
        $(`.component[data-component=${componentId}]`).find('.cprogram-url').attr('href',cprogramUrl)
        
        updateComponentList()
    })


    $('.select-cprogram2-submit').click(function(){
        let radio = $('[name=select-cprograms]:checked')
        let componentId =  parseInt($('#modal-select-cprogram2').attr('data-component'))
        let cprogramName = radio.siblings('.custom-control-label').text()
        let cprogramUrl = radio.siblings('.custom-control-label').attr('data-url')
        $(`.additional-component[data-component=${componentId}]`).attr('data-cprogram-id',parseInt(radio.val()))
        $(`.additional-component[data-component=${componentId}]`).find('.cprogram_id').val(parseInt(radio.val()))
        $(`.additional-component[data-component=${componentId}]`).find('.cprogram_name').val(cprogramName)
        $(`.additional-component[data-component=${componentId}]`).find('.cprogram-name').html(cprogramName)
        $(`.additional-component[data-component=${componentId}]`).find('.cprogram_url').val(cprogramUrl)
        
    })

})

</script>
@endsection

@section('modals')
<div id="modal-select-components" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header bg-success">
                    <h6 class="modal-title">@lang('MAIN.MODAL.SELECT-COMPONENTS.TITLE')</h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">@lang('MAIN.BUTTON.CANCEL')</button>
                    <button type="button" class="btn bg-success modal-submit select-components-submit" data-dismiss="modal">@lang('MAIN.BUTTON.SAVE')</button>
                </div>
        </div>
    </div>
</div>

<div id="modal-select-cprogram" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header bg-success">
                    <h6 class="modal-title">@lang('MAIN.MODAL.SELECT-PROGRAM.TITLE')</h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">@lang('MAIN.BUTTON.CANCEL')</button>
                    <button type="button" class="btn bg-success modal-submit select-cprogram-submit" data-dismiss="modal">@lang('MAIN.BUTTON.SAVE')</button>
                </div>
        </div>
    </div>
</div>

<div id="modal-select-cprogram2" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header bg-success">
                    <h6 class="modal-title">@lang('MAIN.MODAL.SELECT-PROGRAM.TITLE') 2</h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">@lang('MAIN.BUTTON.CANCEL')</button>
                    <button type="button" class="btn bg-success modal-submit select-cprogram2-submit" data-dismiss="modal">@lang('MAIN.BUTTON.SAVE')</button>
                </div>
        </div>
    </div>
</div>
@endsection
