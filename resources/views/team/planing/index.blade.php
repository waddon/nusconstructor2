@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header header-elements-inline">
            <h1 class="card-title">@lang('MAIN.TITLE.PLANING')</h1>
        </div>
        {{--<div class="card-body" style="background: #eee;">
            <h2 class="text-center">{{ $cperiod->name }}</h2>
        </div>--}}
    <!-- Tabs -->
            <ul class="nav nav-tabs nav-tabs-highlight">
                <li class="nav-item"><a href="#tab-loads" class="nav-link active" data-toggle="tab">@lang('MAIN.TAB.LOADS')</a></li>
                <li class="nav-item"><a href="#tab-plans" class="nav-link" data-toggle="tab">@lang('MAIN.TAB.PLANS')</a></li>
                <li class="nav-item"><a href="#tab-year-plans" class="nav-link" data-toggle="tab">@lang('MAIN.TAB.YEAR-PLANS')</a></li>
            </ul>

            <div class="tab-content">
                <!-- Loads Table -->
                    <div class="tab-pane pb-3 fade show active" id="tab-loads">
                        <!-- Search panel -->
                            <div class="search-panel pt-0 mb-3">
                                <div class="text-right">
                                    <div class="btn-group">
                                        <a class="btn btn-success btn-create-tload" href="#{{--route('tnewprograms.create', $team->id)--}}" title="@lang('MAIN.HINT.CREATE')" data-toggle="modal" data-target="#modal-create-tload"><i class="icon-file-plus"></i></a>
                                        <button type="button" class="btn btn-primary datatableRefresh" title="@lang('Refresh')"><i class="icon-sync"></i></button>
                                    </div>
                                </div>
                            </div>
                        <!-- /Search panel -->
                        <!-- Active Table -->
                            <div class="table-responsive">
                                <table class="table table-bordered table-styled table-striped mb-3 w-100 datatable" id="datatableLoads">
                                    <thead class="bg-indigo">
                                        <tr>
                                            <th>@lang('MAIN.COLUMN.ID')</th>
                                            <th>@lang('MAIN.COLUMN.NAME')</th>
                                            <th>@lang('MAIN.COLUMN.CYCLE')</th>
                                            <th>@lang('MAIN.COLUMN.VALIDATE')</th>
                                            <th>@lang('MAIN.COLUMN.ACTIONS')</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        <!-- /Active Table -->
                    </div>
                <!-- /Loads Table -->

                <!-- Plans Table -->
                    <div class="tab-pane pb-3 fade" id="tab-plans">
                        <!-- Search panel -->
                            <div class="search-panel pt-0 mb-3">
                                <div class="text-right">
                                    <div class="btn-group">
                                        <a class="btn btn-success btn-create-tload" href="#" title="@lang('MAIN.HINT.CREATE')" data-toggle="modal" data-target="#modal-create-tplan"><i class="icon-file-plus"></i></a>
                                        <button type="button" class="btn btn-primary datatableRefresh" title="@lang('Refresh')"><i class="icon-sync"></i></button>
                                    </div>
                                </div>
                            </div>
                        <!-- /Search panel -->
                        <!-- Active Table -->
                            <div class="table-responsive">
                                <table class="table table-bordered table-styled table-striped mb-3 w-100 datatable" id="datatableTplans">
                                    <thead class="bg-indigo">
                                        <tr>
                                            <th>@lang('MAIN.COLUMN.ID')</th>
                                            <th>@lang('MAIN.COLUMN.NAME')</th>
                                            <th>@lang('MAIN.COLUMN.CYCLE')</th>
                                            <th>@lang('MAIN.COLUMN.VALIDATE')</th>
                                            <th>@lang('MAIN.COLUMN.ACTIONS')</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        <!-- /Active Table -->
                    </div>
                <!-- /Plans Table -->

                <!-- Year Plans Table -->
                    <div class="tab-pane pb-3 fade" id="tab-year-plans">
                        <!-- Search panel -->
                            <div class="search-panel pt-0 mb-3">
                                <div class="text-right">
                                    <div class="btn-group">
                                        <a class="btn btn-success btn-create-tload" href="#" title="@lang('MAIN.HINT.CREATE')" data-toggle="modal" data-target="#modal-create-yplan"><i class="icon-file-plus"></i></a>
                                        <button type="button" class="btn btn-primary datatableRefresh" title="@lang('Refresh')"><i class="icon-sync"></i></button>
                                    </div>
                                </div>
                            </div>
                        <!-- /Search panel -->
                        <!-- Active Table -->
                            <div class="table-responsive">
                                <table class="table table-bordered table-styled table-striped mb-3 w-100 datatable" id="datatableYplans">
                                    <thead class="bg-indigo">
                                        <tr>
                                            <th>@lang('MAIN.COLUMN.ID')</th>
                                            <th>@lang('MAIN.COLUMN.NAME')</th>
                                            <th>@lang('MAIN.COLUMN.GRADE')</th>
                                            <th>@lang('MAIN.COLUMN.VALIDATE')</th>
                                            <th>@lang('MAIN.COLUMN.ACTIONS')</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        <!-- /Active Table -->
                    </div>
                <!-- /Year Plans Table -->
            </div>
    <!-- /Tabs -->
    </div>
@endsection


@section('scripts')
<script src="/js/datatables/datatables.min.js"></script>
<script src="/js/select2/select2.min.js"></script>
<script>
    var loadtemplates
    var language = {"url": "/js/datatables/languages/ukrainian.json"};
    var pageLength = {{ isset(auth()->user()->options->datatablePageLength) ? auth()->user()->options->datatablePageLength : 10 }};
    var columns = [
            {"data":"id"},
            {"data":"name"},
            {"data":"cycle","searchable":false,"orderable":false},
            {"data":"info","searchable":false,"orderable":false},
            {"data":"actions","searchable":false,"orderable":false}
        ];
    var columnDefs = [
            { "targets": 0, "className": "text-center", },
            { "targets": 2, "className": "text-center", },
            { "targets": 3, "className": "text-center", },
            { "targets": 4, "className": "text-center text-nowrap", },
        ];


    var tableLoads = $("#datatableLoads").DataTable({
        autoWidth : true,
        responsive: true,
        processing: true,
        serverSide: true,
        pageLength: pageLength,
        bSortCellsTop: true,
        order: [[ 0, "desc" ]],
        ajax: {
            url: "{{route('tloads.data')}}",
            dataType: "json",
            type: "POST",
            data: function ( d ) {
                d._token = "{{ csrf_token() }}";
                d.cperiod_id = {{ $cperiod->id }};
                d.team_id = {{ $model->id }};
            }
        },
        columns:columns,
        columnDefs: columnDefs,
        language: language,
    });

    var tableTplans = $("#datatableTplans").DataTable({
        autoWidth : true,
        responsive: true,
        processing: true,
        serverSide: true,
        pageLength: pageLength,
        bSortCellsTop: true,
        order: [[ 0, "desc" ]],
        ajax: {
            url: "{{route('tplans.data')}}",
            dataType: "json",
            type: "POST",
            data: function ( d ) {
                d._token = "{{ csrf_token() }}";
                d.cperiod_id = {{ $cperiod->id }};
                d.team_id = {{ $model->id }};
            }
        },
        columns:columns,
        columnDefs: columnDefs,
        language: language,
    });

    var tableYplans = $("#datatableYplans").DataTable({
        autoWidth : true,
        responsive: true,
        processing: true,
        serverSide: true,
        pageLength: pageLength,
        bSortCellsTop: true,
        order: [[ 0, "desc" ]],
        ajax: {
            url: "{{route('yplans.data')}}",
            dataType: "json",
            type: "POST",
            data: function ( d ) {
                d._token = "{{ csrf_token() }}";
                d.cperiod_id = {{ $cperiod->id }};
                d.team_id = {{ $model->id }};
            }
        },
        columns:columns,
        columnDefs: columnDefs,
        language: language,
    });

</script>
@endsection


@section('modals')
<div id="modal-create-tload" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h6 class="modal-title">@lang('MAIN.MODAL.CREATE-TLOAD.TITLE')</h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('tloads.store', $model->id) }}" id="modal-create-tload-form">
                        @csrf
                        <input type="hidden" name="cperiod_id" value="{{ $cperiod->id }}">
                        <input type="hidden" name="team_id" value="{{ $model->id }}">
                        <div class="form-group row">
                            <select class="form-control field select" name="loadtemplate_id" id="loadtemplate_id" data-placeholder="@lang('MAIN.PLACEHOLDER.SELECT-LAYOUT')">
                                <option></option>
                            </select>
                        </div>
                        <div class="form-group row">
                            <select class="form-control field select" name="cycle_id" id="cycle_id" data-placeholder="@lang('MAIN.PLACEHOLDER.SELECT-CYCLE')">
                                <option></option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">@lang('MAIN.BUTTON.CANCEL')</button>
                    <button type="button" class="btn bg-primary modal-submit">@lang('MAIN.BUTTON.CREATE')</button>
                </div>
        </div>
    </div>
</div>

<div id="modal-create-tplan" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h6 class="modal-title">@lang('MAIN.MODAL.CREATE-TPLAN.TITLE')</h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('tplans.store',$model->id) }}">
                        @csrf
                        <div class="form-group row">
                            <select class="form-control field select" name="tload_id" id="tload_id" data-placeholder="@lang('MAIN.PLACEHOLDER.SELECT-TLOAD')">
                                <option></option>
                                @foreach($model->getValidTloads($cperiod->id) as $tload)
                                    <option value="{{$tload->id}}">[{{$tload->cycle->name}}] - {{$tload->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">@lang('MAIN.BUTTON.CANCEL')</button>
                    <button type="button" class="btn bg-primary modal-submit">@lang('MAIN.BUTTON.CREATE')</button>
                </div>
        </div>
    </div>
</div>

<div id="modal-create-yplan" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h6 class="modal-title">@lang('MAIN.MODAL.CREATE-YPLAN.TITLE')</h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('yplans.store',$model->id) }}">
                        @csrf
                        <div class="form-group row">
                            <select class="form-control field select" name="tplan_id" id="tplan_id" data-placeholder="@lang('MAIN.PLACEHOLDER.SELECT-TPLAN')">
                                <option></option>
                            </select>
                        </div>
                        <div class="form-group row">
                            <select class="form-control field select" name="grade_id" id="grade_id" data-placeholder="@lang('MAIN.PLACEHOLDER.SELECT-GRADE')">
                                <option></option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">@lang('MAIN.BUTTON.CANCEL')</button>
                    <button type="button" class="btn bg-primary modal-submit">@lang('MAIN.BUTTON.CREATE')</button>
                </div>
        </div>
    </div>
</div>


<script>
    $(document).ready( () => {
        $('.select').select2({
            minimumResultsForSearch: Infinity
        })

        getLoadtemplates()
        getTplans()
        function getLoadtemplates(){
            $.ajax({
                url: '{{route("api.loadtemplates.list", $cperiod->id)}}',
                dataType : "json",
            }).done(function(data) {
                for (const [key, value] of Object.entries(data)) {
                    //console.log(`${key}: ${value}`);
                    $('#loadtemplate_id').append(`<option value="${key}">${value.name}</option>`);
                }
                loadtemplates = data
            })
        }
        function getTplans(){
            $.ajax({
                url: '{{route("api.tplans.list", [$model->id, "Team", $cperiod->id])}}',
                dataType : "json",
            }).done(function(data) {
                console.log(data);
                for (const [key, value] of Object.entries(data)) {
                    $('#tplan_id').append(`<option value="${key}">${value.name}</option>`);
                }
                tplans = data
            })
        }

    $('#loadtemplate_id').on('change',function () {
        $('#cycle_id').empty().html('<option></option>')
        console.log(loadtemplates[$(this).val()]['cycles'])
        for (const [key, value] of Object.entries(loadtemplates[$(this).val()]['cycles'])) {
            $('#cycle_id').append(`<option value="${key}">${value}</option>`);
        }
    })

    $('#tplan_id').on('change',function () {
        $('#grade_id').empty().html('<option></option>')
        console.log(tplans[$(this).val()]['grades'])
        for (const [key, value] of Object.entries(tplans[$(this).val()]['grades'])) {
            $('#grade_id').append(`<option value="${key}">${value}</option>`);
        }
    })

        $('.modal-submit').click( function(e) {
            let form = $(this).closest('.modal-content').find('form').first();
            e.preventDefault();
            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                dataType : "json",
                data: form.serialize(),
            }).done((data, status, xhr) => {
                console.log(data.relocateURL);
                window.location.href = data.relocateURL;
            }).fail((xhr) => {
                let data = xhr.responseJSON;
                swal({
                    title: data.message,
                    type: 'error',
                    confirmButtonClass: 'btn btn-primary',
                })
            }).always((xhr, type, status) => {
                let response = xhr.responseJSON || status.responseJSON,
                    errors = response.errors || [];
                form.find('.field').each((i, el) => {
                    let field = $(el);
                    field.removeClass('is-invalid');
                    field.find('input').removeClass('is-invalid');
                    container = field.closest('.form-group'),
                        elem = $('<label class="message">');
                    container.find('label.message').remove();
                    if(errors[field.attr('name')]){
                        field.addClass('is-invalid');
                        field.find('input').addClass('is-invalid');
                        errors[field.attr('name')].forEach((msg) => {
                            elem.clone().addClass('validation-invalid-label').html(msg).appendTo(container);
                        });
                    }
                });
            });
        });

    })
</script>
@endsection
