@extends('layouts.app')

@section('content')
<style>
    #load-table input.form-control {
        background: transparent;
    }
    #load-table td.error {
        position: relative;
        background: rgba(255, 0, 0, .3);
    }
    #load-table td.error .badge{
        position: absolute;
        top: 5px;
        right: 5px;
    }
    #load-table input.form-control:focus {
        outline: 2px solid #9c6;
    }
</style>

{{ Form::model($model, [ 'url' => route('tloads.update', [ $model->id ]) , 'method' => 'PUT', 'class' => 'ajax-form' ]) }}

<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">
        <div class="card mb-3 program-constructor">
            <div class="card-header">
                <h2>Обсяг тижневого навантаження закладу освіти</h2>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    {{ Form::label('name', __('MAIN.COLUMN.NAME'), ['class'=>'small text-muted font-italic col-md-2 col-form-label']) }}
                    <div class="col-md-10">
                        {{ Form::text('name', $model->name, array('class' => 'field form-control')) }}
                    </div>
                </div>
                <table class="table table-bordered table-striped w-100 mb-3" id="load-table">
                    <thead>
                        <tr>
                            <th>Назва освітньої галузі</th>
                            @foreach($model->cycle->grades as $grade)
                                <th>{{ $grade->name }}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($cbranches as $cbranch)
                            <tr>
                                <td>{{ $cbranch->name }}</td>
                                @foreach($model->cycle->grades as $grade)
                                    <td class="p-0">
                                        <input
                                            type="number"
                                            class="w-100 border-0 p-3 form-control"
                                            name="loads[{{ $grade->id }}][{{$cbranch->id}}][value]"
                                            value="{{ $model->meta->loads->{$grade->id}->{$cbranch->id}->value ?? 0 }}"
                                            step="0.5"
                                            data-rec="{{ $model->loadtemplate->meta->loads->{$grade->id}->{$cbranch->id}->recommended ?? '' }}"
                                            data-min="{{ $model->loadtemplate->meta->loads->{$grade->id}->{$cbranch->id}->minimum ?? '' }}"
                                            data-max="{{ $model->loadtemplate->meta->loads->{$grade->id}->{$cbranch->id}->maximum ?? '' }}"
                                            data-grade="{{ $grade->id }}"
                                        >
                                    </td>
                                @endforeach                                
                            </tr>
                        @endforeach
                        <tr>
                            <td><strong>Усього</strong></td>
                            @foreach($model->cycle->grades as $grade)
                                <td class="text-center"><strong class="sum-{{$grade->id}}"></strong></td>
                            @endforeach
                        </tr>
                        <tr>
                            <td><strong>Резерв навчальних годин</strong></td>
                            @foreach($model->cycle->grades as $grade)
                                <td class="text-center"><strong class="reserv-{{$grade->id}}"></strong></td>
                            @endforeach
                        </tr>
                    </tbody>
                </table>
                <div class="mb-3" id="errorMessages"></div>
                <div class="text-right">
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary" id="setRecommended">Рекомендовані</button>
                        <button type="button" class="btn btn-primary" id="chechLoad">Перевірити ведення</button>
                        <button type="submit" class="btn btn-success">Зберегти</button>
                        <a class="btn btn-primary" href="{{ $model->parentRoute() }}">Вийти</a>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>


{{ Form::close() }}
@endsection

@section('scripts')
<script src="{{ asset('/js/jquery_ui/interactions.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready( () => {
        $('#chechLoad').click( () => checkLoad())

        function checkLoad() {
            let errorIndex = 1
            let symbol = '<i class="icon-notification2 mr-3 icon-2x text-danger"></i>'
            $('#load-table td').removeClass('error')
            $('#load-table .badge').remove()
            $('#errorMessages').empty()
            $('#load-table input.form-control').each(function( index ) {
                //console.log( index + ": " + $( this ).val() );
                let min = parseFloat( $(this).attr('data-min') )
                let max = parseFloat( $(this).attr('data-max') )
                let val = $(this).val()
                if (val < min || val > max) {
                    $(this).parent().addClass('error');
                    $(this).parent().append(`<span class="badge badge-light badge-pill">${errorIndex}</span>`)
                    $('#errorMessages').append(`<p class="d-flex align-items-center">${symbol}<span><strong>${errorIndex}.</strong> Значення <strong>${val}</strong> знаходиться за межами дозволеного діапазону між <strong>${min}</strong> та <strong>${max}</strong></span></p>`)
                    errorIndex++;
                }
            })
        }

        $('#setRecommended').click( () => {
            $('#load-table input.form-control').each(function( index ) {
                $(this).val( $(this).attr('data-rec') )
                calculate()
            })
        })

        $(document).on('change','#load-table input.form-control', () => {
            calculate()
            // checkLoad()
        })
    
        calculate()
        // checkLoad()
    
        function calculate() {
            let data = {}
            @foreach($model->cycle->grades as $grade)
                data[{{$grade->id}}] = {'sum' : 0, 'reserv' : 0}
            @endforeach
            $('#load-table input.form-control').each(function( index ) {
                let element = data[ parseFloat($(this).attr('data-grade')) ]
                element.sum += parseFloat($(this).val())
                temp = parseFloat($(this).attr('data-rec')) - parseFloat($(this).val());
                // element.reserv += temp > 0 ? temp : 0;
                element.reserv += temp;
            })
            $.each(data, function( index, value ) {
                $('.sum-' + index).text(value.sum)
                $('.reserv-' + index).text(value.reserv)
            })
        }
    })
</script>
@endsection

