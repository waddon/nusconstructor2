@extends('layouts.app')

@section('content')
<style>
    #load-table input.form-control {
        background: transparent;
    }
    #load-table td.error {
        position: relative;
        background: rgba(255, 0, 0, .3);
    }
    #load-table td.error .badge{
        position: absolute;
        top: 5px;
        right: 5px;
    }
    #load-table input.form-control:focus {
        outline: 2px solid #9c6;
    }
</style>

<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">
        <div class="card mb-3 program-constructor">
            <div class="card-header d-flex justify-content-between align-items-start">
                <h2>Обсяг тижневого навантаження закладу освіти (перегляд)</h2>
                <a class="btn btn-icon btn-primary" href="{{ route('tloads.export', $model->id) }}" title="@lang('MAIN.HINT.DOWNLOAD')"><i class="icon-file-word"></i></a>
            </div>
            <div class="card-body">
                <h3 class="text-center">{{$model->name}}</h3>
                @if(!$model->validate())
                    <h3 class="text-center text-danger-600">@lang('MAIN.HINT.ERROR-EXISTS')</h3>
                @endif
                <table class="table table-bordered table-striped w-100 mb-3" id="load-table">
                    <thead>
                        <tr>
                            <th class="px-1 text-center" rowspan="2">Назва освітньої галузі</th>
                            <th class="px-1 text-center" rowspan="2">Навчальне навантаження</th>
                            @foreach($model->cycle->grades as $key => $grade)
                                <th colspan="3" class="text-center {{ !($key & 1) ? 'bg-light' : ''}}">{{ $grade->name }}</th>
                            @endforeach
                        </tr>
                        <tr>
                            @foreach($model->cycle->grades as $key => $grade)
                                <th class="px-1 text-center {{ !($key & 1) ? 'bg-light' : ''}}">@lang('MAIN.LABEL.REC')</th>
                                <th class="px-1 text-center {{ !($key & 1) ? 'bg-light' : ''}}">@lang('MAIN.LABEL.MIN')</th>
                                <th class="px-1 text-center {{ !($key & 1) ? 'bg-light' : ''}}">@lang('MAIN.LABEL.MAX')</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($cbranches as $cbranch)
                        <tr>
                            <td rowspan="2">{{ $cbranch->name }}</td>
                            <td class="text-center">на тиждень</td>
                                @foreach($model->cycle->grades as $key => $grade)
                                    <td class="text-center">
                                        {{ $model->meta->loads->{$grade->id}->{$cbranch->id}->value ?? 0}}
                                    </td>
                                    <td class="text-center">
                                        {{ $model->loadtemplate->meta->loads->{$grade->id}->{$cbranch->id}->minimum ?? ''}}
                                    </td>
                                    <td class="text-center">
                                        {{ $model->loadtemplate->meta->loads->{$grade->id}->{$cbranch->id}->maximum ?? ''}}
                                    </td>
                                @endforeach
                        </tr>
                        <tr>
                            <td class="text-center">на рік</td>
                                @foreach($model->cycle->grades as $key => $grade)
                                    <td class="text-center">
                                        {{ ($model->meta->loads->{$grade->id}->{$cbranch->id}->value ?? 0) * 35 }}
                                    </td>
                                    <td class="text-center">
                                        {{ ($model->loadtemplate->meta->loads->{$grade->id}->{$cbranch->id}->minimum ?? 0) * 35 }}
                                    </td>
                                    <td class="text-center">
                                        {{ ($model->loadtemplate->meta->loads->{$grade->id}->{$cbranch->id}->maximum ?? 0) * 35 }}
                                    </td>
                                @endforeach
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="mb-3" id="errorMessages"></div>
                <div class="text-right">
                    <div class="btn-group">
                        <a class="btn btn-primary" href="{{ $model->parentRoute() }}">Вийти</a>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

@endsection
