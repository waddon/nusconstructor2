<html>
<head><meta charset="utf-8"></head>
<body>
    <style>
        .text-center{text-align: center;}
        .table{border-top: 1px solid #ddd;border-left: 1px solid #ddd;width:100%;}
        .table th, .table td{border-bottom: 1px solid #ddd;border-right: 1px solid #ddd;}
        .bg-light {background: #fafafa;}
    </style>
    <h2 class="text-center">Обсяг тижневого навантаження закладу освіти</h2>
    <h3 class="text-center">{{$model->name}}</h3>

    @if(!$model->validate())
        <h3 class="text-center" style="color: #e53935;">@lang('MAIN.HINT.ERROR-EXISTS')</h3>
    @endif

    <table cellspacing="5" class="table">
        <thead>
            <tr>
                <th class="px-1 text-center" rowspan="2">Назва освітньої галузі</th>
                <th class="px-1 text-center" rowspan="2">Навчальне навантаження</th>
                @foreach($model->cycle->grades as $key => $grade)
                    <th colspan="3" class="text-center {{ !($key & 1) ? 'bg-light' : ''}}">{{ $grade->name }}</th>
                @endforeach
            </tr>
            <tr>
                @foreach($model->cycle->grades as $key => $grade)
                    <th class="px-1 text-center {{ !($key & 1) ? 'bg-light' : ''}}">@lang('MAIN.LABEL.REC')</th>
                    <th class="px-1 text-center {{ !($key & 1) ? 'bg-light' : ''}}">@lang('MAIN.LABEL.MIN')</th>
                    <th class="px-1 text-center {{ !($key & 1) ? 'bg-light' : ''}}">@lang('MAIN.LABEL.MAX')</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach($cbranches as $cbranch)
            <tr>
                <td rowspan="2">{{ $cbranch->name }}</td>
                <td class="text-center">на тиждень</td>
                    @foreach($model->cycle->grades as $key => $grade)
                        <td class="text-center">
                            {{ $model->meta->loads->{$grade->id}->{$cbranch->id}->value ?? 0}}
                        </td>
                        <td class="text-center">
                            {{ $model->loadtemplate->meta->loads->{$grade->id}->{$cbranch->id}->minimum ?? ''}}
                        </td>
                        <td class="text-center">
                            {{ $model->loadtemplate->meta->loads->{$grade->id}->{$cbranch->id}->maximum ?? ''}}
                        </td>
                    @endforeach
            </tr>
            <tr>
                <td class="text-center">на рік</td>
                    @foreach($model->cycle->grades as $key => $grade)
                        <td class="text-center">
                            {{ ($model->meta->loads->{$grade->id}->{$cbranch->id}->value ?? 0) * 35 }}
                        </td>
                        <td class="text-center">
                            {{ ($model->loadtemplate->meta->loads->{$grade->id}->{$cbranch->id}->minimum ?? 0) * 35 }}
                        </td>
                        <td class="text-center">
                            {{ ($model->loadtemplate->meta->loads->{$grade->id}->{$cbranch->id}->maximum ?? 0) * 35 }}
                        </td>
                    @endforeach
            </tr>
            @endforeach
        </tbody>
    </table>

</body>
</html>