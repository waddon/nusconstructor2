@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header header-elements-inline alpha-1">
            <h1 class="card-title">@lang('MAIN.TITLE.TEAM-INFO')</h1>
        </div>
        <div class="card-body">
            <h4>@lang('MAIN.TITLE.GENERAL-INFORMATION')</h4>
            <table class="table table-bordered table-styled table-striped mb-3 w-100">
                <thead class="bg-indigo">
                    <tr>
                        <th>@lang('MAIN.COLUMN.PARAMETER')</th>
                        <th>@lang('MAIN.COLUMN.VALUE')</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>@lang('MAIN.COLUMN.NAME')</td>
                        <td>{{$model->name}}</td>
                    </tr>
                    <tr>
                        <td>@lang('MAIN.COLUMN.TTYPE')</td>
                        <td>{{$model->ttype->name}}</td>
                    </tr>
                    @foreach($model->institutions as $institution)
                        <tr>
                            <td>@lang('MAIN.COLUMN.INSTITUTION')</td>
                            <td>{{$institution->meta->institution_name ?? ''}}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td>@lang('MAIN.COLUMN.USERS')</td>
                        <td>{{$model->users->count()}}</td>
                    </tr>
                </tbody>
            </table>
            <h4>@lang('MAIN.TITLE.USERS')</h4>
            <table class="table table-bordered table-styled table-striped mb-3 w-100 datatable" id="datatableActive">
                <thead class="bg-indigo">
                    <tr>
                        <th>@lang('MAIN.COLUMN.ID')</th>
                        <th>@lang('MAIN.COLUMN.IMAGE')</th>
                        <th>@lang('MAIN.COLUMN.NAME')</th>
                        <th>@lang('MAIN.COLUMN.EMAIL')</th>
                        <th>@lang('MAIN.COLUMN.TEAMROLE')</th>
                        <th>@lang('MAIN.COLUMN.ACTIONS')</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($model->users as $user)
                        <tr>
                            <td class="text-center">{{$user->id}}</td>
                            <td class="text-center"><img src="{{$user->getAvatarPath()}}" class="rounded-circle shadow-1" style="object-fit: cover;" width="80" height="80" alt=""></td>
                            <td>{{$user->combineName()}}</td>
                            <td>{{$user->email}}</td>
                            <td class="text-center">{{$user->pivot->teamrole->name}}</td>
                            <td class="text-center">
                                @if($model->isLeader(auth()->user()->id))
                                    @if($user->pivot->teamrole->id==3)
                                        <a class="btn-decision" href="{{route('uteam.approve',['team_id'=>$model->id, 'user_id'=>$user->id])}}" title="@lang('MAIN.HINT.APPROVE')"><i class="icon-check text-success icon-2x"></i></a>
                                    @endif
                                    @if(in_array($user->pivot->teamrole->id,[2,3]))
                                        <a class="btn-decision" href="{{route('uteam.remove',['team_id'=>$model->id, 'user_id'=>$user->id])}}" title="@lang('MAIN.HINT.DELETE')"><i class="icon-x text-danger icon-2x"></i></a>
                                    @endif
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
        $('.btn-decision').click(function(e){
            e.preventDefault();
            $.ajax({
                url: $(this).attr('href'),
                type: 'POST',
                dataType : "json",
                data: {"_token":"{{ csrf_token() }}"},
            }).done((data, status, xhr) => {
                location.href=location.href;
            }).fail((xhr) => {
                let data = xhr.responseJSON;
                swal({
                    title: data.message,
                    type: 'error',
                    confirmButtonClass: 'btn btn-primary',
                })
            });
        });
</script>        
@endsection