    <div class="navbar navbar-expand-md navbar-nus navbar-static">
        <div class="navbar-brand">
            <a href="{{route('home')}}" class="d-inline-block">
                <img src="/img/logo.png" alt="">
            </a>
        </div>

        <div class="d-md-none">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
                <i class="icon-tree5"></i>
            </button>
            <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
                <i class="icon-paragraph-justify3"></i>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="navbar-mobile">
            {{--<ul class="navbar-nav">
                <li class="nav-item">
                    <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                        <i class="icon-paragraph-justify3"></i>
                    </a>
                </li>
            </ul>--}}

            <ul class="navbar-nav ml-md-auto">

                <li class="nav-item">
                    <div class="form-search">
                        <form class="position-relative" action="{{route('search')}}" method="GET">
                            @csrf
                            <input type="text" class="form-control bg-light" placeholder="@lang('MAIN.PLACEHOLDER.SEARCH')" name="search_text" value="{{ $old_value ?? ''}}">
                            <button class="btn btn-link position-absolute text-green"><i class="icon-search4"></i></button>
                        </form>
                    </div>
                </li>

                @auth
                <li class="nav-item">
                    <a href="#" class="btn btn-light w-100" data-toggle="modal" data-target="#modal-exit">@lang('MAIN.BUTTON.EXIT')</a>
                </li>
                @endauth
            </ul>
        </div>
    </div>
