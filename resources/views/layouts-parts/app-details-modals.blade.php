<div id="modal-details" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h6 class="modal-title">Заповнити реквізити</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="remove-modal-body">
                <select name="detail-list" id="detail-list" class="form-control">
                    <option value="" hidden></option>
                    @foreach($details as $key => $detail)
                        <option value="{{$detail->id}}" data-person="{{$detail->data->person ?? ''}}" data-basis="{{$detail->data->basis ?? ''}}" data-edrpou="{{$detail->data->edrpou ?? ''}}" data-mfo="{{$detail->data->mfo ?? ''}}" data-bank="{{$detail->data->bank ?? ''}}" data-account="{{$detail->data->account ?? ''}}" data-address_legal="{{$detail->data->address_legal ?? ''}}" data-address_actual="{{$detail->data->address_actual ?? ''}}" data-phone="{{$detail->data->phone ?? ''}}" data-email="{{$detail->data->email ?? ''}}">{{$detail->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">@lang('Cancel')</button>
                <button type="button" class="btn bg-primary" id="details-btn-ok" data-dismiss="modal">@lang('Fill')</button>
            </div>
        </div>
    </div>
</div>

<script>
$('#details-btn-ok').on('click',function(e){
    e.preventDefault();
    $('input#details_name').val( $('#detail-list option:selected').text() );
    $('input#details_person').val( $('#detail-list option:selected').attr('data-person') );
    $('input#details_basis').val( $('#detail-list option:selected').attr('data-basis') );
    $('input#details_edrpou').val( $('#detail-list option:selected').attr('data-edrpou') );
    $('input#details_mfo').val( $('#detail-list option:selected').attr('data-mfo') );
    $('input#details_bank').val( $('#detail-list option:selected').attr('data-bank') );
    $('input#details_account').val( $('#detail-list option:selected').attr('data-account') );
    $('input#details_address_legal').val( $('#detail-list option:selected').attr('data-address_legal') );
    $('input#details_address_actual').val( $('#detail-list option:selected').attr('data-address_actual') );
    $('input#details_phone').val( $('#detail-list option:selected').attr('data-phone') );
    $('input#details_email').val( $('#detail-list option:selected').attr('data-email') );
});
</script>