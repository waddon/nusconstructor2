<div class="card card-sidebar-mobile">
    <ul class="nav nav-sidebar" data-nav-type="accordion">

        <li class="nav-item bt-1"><a href="{{route('home')}}" class="nav-link {{ in_array(\Route::currentRouteName(),['home']) ? 'active' : '' }}"><i class="icon-home2"></i><span>Головна панель</span></a></li>

        <li class="nav-item nav-item-submenu">
            <a href="#" class="nav-link"><i class="icon-star-empty3"></i><span>@lang('MAIN.TITLE.STANDART')</span></a>
            <ul class="nav nav-group-sub" data-submenu-title="@lang('MAIN.TITLE.STANDART')">

                <li class="nav-item"><a href="{{route('page','description1')}}" class="nav-link {!! url()->current() == route('page','description1') ? 'active' : '' !!}"><span class="ml-2">Початкова освіта</span></a></li>
                <li class="nav-item"><a href="{{route('page','description2')}}" class="nav-link {!! url()->current() == route('page','description2') ? 'active' : '' !!}"><span class="ml-2">Базова середня освіта</span></a></li>

                <li class="nav-item"><a href="{{route('levels')}}" class="nav-link {{ in_array(\Route::currentRouteName(),['levels']) ? 'active' : '' }}"><span class="ml-2">@lang('MAIN.MENU.LEVELS')</span></a></li>
                <li class="nav-item"><a href="{{route('branches')}}" class="nav-link {{ in_array(\Route::currentRouteName(),['branches']) ? 'active' : '' }}"><span class="ml-2">@lang('MAIN.MENU.BRANCHES')</span></a></li>
                {{--<li class="nav-item"><a href="{{route('expects')}}" class="nav-link {{ in_array(\Route::currentRouteName(),['expects']) ? 'active' : '' }}"><span class="ml-2">@lang('MAIN.MENU.EXPECTS')</span></a></li>--}}

                <li class="nav-item"><a href="{{route('page','appraisal')}}" class="nav-link {!! url()->current() == route('page','appraisal') ? 'active' : '' !!}"><span class="ml-2">@lang('MAIN.MENU.APPRAISAL')</span></a></li>
                <li class="nav-item"><a href="{{route('materials')}}" class="nav-link {{ in_array(\Route::currentRouteName(),['materials']) ? 'active' : '' }}"><span class="ml-2">@lang('MAIN.MENU.MATERIALS')</span></a></li>
            </ul>
        </li>

        <li class="nav-item nav-item-submenu">
            <a href="#" class="nav-link"><i class="icon-table2"></i><span>@lang('MAIN.MENU.BASIC-PLANS')</span></a>
            <ul class="nav nav-group-sub" data-submenu-title="@lang('MAIN.MENU.BASIC-PLANS')">
                <li class="nav-item"><a href="{{route('page','basicplans')}}" class="nav-link {!! url()->current() == route('page','basicplans') ? 'active' : '' !!}"><span class="ml-2">@lang('MAIN.MENU.BASIC-PLANS')</span></a></li>
                <li class="nav-item"><a href="{{route('page','basicplans_5-9')}}" class="nav-link {!! url()->current() == route('page','basicplans_5-9') ? 'active' : '' !!}"><span class="ml-2">Базовий навчальний план базової середньої освіти</span></a></li>
            </ul>
        </li>

        <li class="nav-item"><a href="{{route('specifics')}}" class="nav-link {{ in_array(\Route::currentRouteName(),['specifics']) ? 'active' : '' }}"><i class="icon-checkbox-checked"></i><span>@lang('MAIN.MENU.EXPECTS')</span></a></li>

        {{--<li class="nav-item nav-item-submenu">
            <a href="#" class="nav-link"><i class="icon-file-eye"></i><span>@lang('MAIN.MENU.EDUCATIONAL-PROGRAMS')</span></a>
            <ul class="nav nav-group-sub" data-submenu-title="@lang('MAIN.MENU.EDUCATIONAL-PROGRAMS')">
                <li class="nav-item"><a href="{{route('notes')}}" class="nav-link {{ in_array(\Route::currentRouteName(),['notes']) ? 'active' : '' }}"><span class="ml-2">@lang('MAIN.MENU.EXPLANATORY-NOTE')</span></a></li>
                <li class="nav-item"><a href="{{route('specifics')}}" class="nav-link {{ in_array(\Route::currentRouteName(),['specifics']) ? 'active' : '' }}"><span class="ml-2">@lang('MAIN.MENU.SPECIFICS')</span></a></li>
                <li class="nav-item"><a href="{{route('page','program_5-9')}}" class="nav-link {!! url()->current() == route('page','program_5-9') ? 'active' : '' !!}"><span class="ml-2">Типова освітня програма для 5-9 класів</span></a></li>
            </ul>
        </li>--}}

        {{-- Типовые документы --}}
            <li class="nav-item nav-item-submenu">
                <a href="#" class="nav-link"><i class="icon-archive"></i><span>@lang('MAIN.MENU.TYPICAL-PROGRAMS')</span></a>
                <ul class="nav nav-group-sub" data-submenu-title="@lang('MAIN.MENU.TYPICAL-DOCUMENTS')">
                    <li class="nav-item"><a href="{{route('typnewpackages.index')}}" class="nav-link {!! (in_array( url()->current(), [ route('typnewpackages.index')]) || in_array(\Route::currentRouteName(),['typnewpackages.show']) ) ? 'active' : '' !!}"><span class="ml-2">@lang('MAIN.MENU.TYPICAL-PACKAGES')</span></a></li>
                    <li class="nav-item"><a href="{{route('typnewplans.index')}}" class="nav-link {!! (in_array( url()->current(), [ route('typnewplans.index')]) || in_array(\Route::currentRouteName(),['typnewplans.show']) ) ? 'active' : '' !!}"><span class="ml-2">@lang('MAIN.MENU.TYPICAL-PLANS')</span></a></li>
                    {{--<li class="nav-item"><a href="{{route('typnewprograms.index')}}" class="nav-link {!! (in_array( url()->current(), [ route('typnewprograms.index')]) || in_array(\Route::currentRouteName(),['typnewprograms.show']) ) ? 'active' : '' !!}"><span class="ml-2">@lang('MAIN.MENU.TYPICAL-PROGRAMS')</span></a></li>--}}
                    <li class="nav-item"><a href="{{route('typmaterials.index')}}" class="nav-link {!! (in_array( url()->current(), [ route('typmaterials.index')]) || in_array(\Route::currentRouteName(),['typmaterials.show']) ) ? 'active' : '' !!}"><span class="ml-2">@lang('MAIN.MENU.TYPICAL-MATERIALS')</span></a></li>
                </ul>
            </li>

        {{-- Публичные документы --}}
            <li class="nav-item nav-item-submenu">
                <a href="#" class="nav-link"><i class="icon-file-eye"></i><span>@lang('MAIN.MENU.PUBLIC-DOCUMENTS')</span></a>
                <ul class="nav nav-group-sub" data-submenu-title="@lang('MAIN.MENU.PUBLIC-DOCUMENTS')">
                  <li class="nav-item">
                    <a href="{{ route('publicDocuments', ['model']) }}" class="nav-link {!! url()->current() == route('publicDocuments',['model']) ? 'active' : '' !!}">@lang('MAIN.MENU.PROGRAM-PUBLIC.MODEL')</a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('publicDocuments', ['standart']) }}" class="nav-link {!! url()->current() == route('publicDocuments',['standart']) ? 'active' : '' !!}">@lang('MAIN.MENU.PROGRAM-PUBLIC.STANDART')</a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('publicDocuments', ['educational']) }}" class="nav-link {!! url()->current() == route('publicDocuments',['educational']) ? 'active' : '' !!}">@lang('MAIN.MENU.PROGRAM-PUBLIC.EDUCATIONAL')</a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('publicDocuments', ['curriculum']) }}" class="nav-link {!! url()->current() == route('publicDocuments',['curriculum']) ? 'active' : '' !!}">@lang('MAIN.MENU.PROGRAM-PUBLIC.CURRICULUM')</a>
                  </li>
                </ul>
            </li>

        {{--<li class="nav-item nav-item-submenu">
            <a href="#" class="nav-link"><i class="icon-copy3"></i><span>Заклад освіти</span></a>
            <ul class="nav nav-group-sub" data-submenu-title="Заклад освіти">
                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><span class="ml-2">Діючі документи</span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Діючі документи">
                            <li class="nav-item"><a href="#" class="nav-link"><span></span><div>Навчальні плани</div></a></li>
                            <li class="nav-item"><a href="#" class="nav-link"><span></span><div>Навчальні програми</div></a></li>
                        </ul>
                </li>
                <li class="nav-item"><a href="#" class="nav-link"><i class="icon-library2"></i><span class="ml-2">Очікувані результати навчання закзаду освіти</span></a></li>
            </ul>
        </li>--}}

        {{--<li class="nav-item nav-item-submenu">
            <a href="#" class="nav-link"><i class="icon-calendar"></i><span>Конструктор</span></a>
            <ul class="nav nav-group-sub" data-submenu-title="Конструктор">
                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><span class="ml-2">Навчальні плани</span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Навчальні плани">
                            <li class="nav-item"><a href="#" class="nav-link"><span></span><div>Базові навчальні плани</div></a></li>
                            <li class="nav-item"><a href="#" class="nav-link"><span></span><div>Типові навчальні плани</div></a></li>
                            <li class="nav-item"><a href="#" class="nav-link"><span></span><div>Навчальні плани закладу освіти</div></a></li>
                        </ul>
                </li>
                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><span class="ml-2">Навчальні програми</span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Навчальні програми">
                            <li class="nav-item"><a href="#" class="nav-link"><span></span><div>Типові навчальні програми</div></a></li>
                            <li class="nav-item"><a href="#" class="nav-link"><span></span><div>Навчальні програми закладу освіти</div></a></li>
                        </ul>
                </li>
            </ul>
        </li>--}}

        {{--<li class="nav-item nav-item-submenu">
            <a href="#" class="nav-link"><i class="icon-hammer2"></i><span>Документи</span></a>
            <ul class="nav nav-group-sub" data-submenu-title="Документи">
                <li class="nav-item"><a href="#" class="nav-link"><span class="ml-2">Закони</span></a></li>
                <li class="nav-item"><a href="#" class="nav-link"><span class="ml-2">Положення</span></a></li>
                <li class="nav-item"><a href="#" class="nav-link"><span class="ml-2">Методичні рекомендації</span></a></li>
            </ul>
        </li>--}}


    </ul>
</div>
