<div class="card card-sidebar-mobile">
    <ul class="nav nav-sidebar" data-nav-type="accordion">

        <li class="nav-item">
            <a href="{{route('dashboard')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['dashboard']) ? 'active' : '' !!}"><i class="icon-equalizer3"></i><span>@lang('MAIN.MENU.DASHBOARD')</span></a>
        </li>

        {{-- Пользователи и Команды --}}
        @if(Gate::check('admin') || Gate::check('users') || Gate::check('teams'))
            <li class="nav-item nav-item-submenu">
                <a href="#" class="nav-link"><i class="icon-users4"></i> <span>@lang('MAIN.MENU.TEAMS') & @lang('MAIN.MENU.USERS')</span></a>

                <ul class="nav nav-group-sub" data-submenu-title="@lang('MAIN.MENU.TEAMS') & @lang('MAIN.MENU.USERS')">
                    <li class="nav-item"><a href="{{route('users.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['users.index','users.create','users.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.USERS')</a></li>
                    <li class="nav-item"><a href="{{route('teams.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['teams.index','teams.create','teams.show','teams.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.TEAMS')</a></li>
                </ul>
            </li>
        @endif

        {{-- Стандарт новый --}}
        @if(Gate::check('admin') || Gate::check('branches') || Gate::check('rivens') || Gate::check('cikls') || Gate::check('notes') || Gate::check('classes') || Gate::check('galuzes') || Gate::check('zors') || Gate::check('zmists'))
            <li class="nav-item nav-item-submenu">
                <a href="#" class="nav-link"><i class="icon-star-empty3"></i> <span>@lang('MAIN.MENU.STANDART')</span></a>
                <ul class="nav nav-group-sub" data-submenu-title="@lang('MAIN.MENU.STANDART')">
                    <li class="nav-item"><a href="{{route('levels.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['levels.index','levels.create','levels.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.LEVELS')</a></li>
                    <li class="nav-item"><a href="{{route('cycles.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['cycles.index','cycles.create','cycles.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.CYCLES')</a></li>
                    <li class="nav-item"><a href="{{route('grades.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['grades.index','grades.create','grades.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.GRADES')</a></li>
                    <li class="nav-item"><a href="{{route('branches.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['branches.index','branches.create','branches.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.BRANCHES')</a></li>
                    <li class="nav-item"><a href="{{route('lines.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['lines.index','lines.create','lines.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.LINES')</a></li>
                    <li class="nav-item"><a href="{{route('expects.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['expects.index','expects.create','expects.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.EXPECTS')</a></li>
                    <li class="nav-item"><a href="{{route('specifics.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['specifics.index','specifics.create','specifics.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.SPECIFICS')</a></li>
                    <li class="nav-item"><a href="{{route('skills.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['skills.index','skills.create','skills.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.SKILLS')</a></li>
                    <li class="nav-item"><a href="{{route('descriptions.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['descriptions.index','descriptions.create','descriptions.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.DESCRIPTIONS')</a></li>
                </ul>
            </li>
        @endif

        {{-- Кабинет рецензента/модератора --}}
        @if(Gate::check('admin') || Gate::check('approve'))
            <li class="nav-item nav-item-submenu">
                <a href="#" class="nav-link"><i class="icon-cube3"></i> <span>@lang('MAIN.MENU.REVIEWER-OFFICE')</span></a>
                <ul class="nav nav-group-sub" data-submenu-title="@lang('MAIN.MENU.REVIEWER-OFFICE')">
                    <li class="nav-item"><a href="{{route('newpackages.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['newpackages.index','newpackages.create','newpackages.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.PACKAGES')</a></li>
                    <li class="nav-item"><a href="{{route('newplans.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['newplans.index','newplans.create','newplans.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.PLANS')</a></li>
                    {{--<li class="nav-item"><a href="{{route('newprograms.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['newprograms.index','newprograms.create','newprograms.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.PROGRAMS')</a></li>--}}

                    <li class="nav-item">
                        <a href="{{ route('newprograms.list', 'model') }}" class="nav-link {!! url()->current() == route('newprograms.list','model') ? 'active' : '' !!}">@lang('MAIN.MENU.PROGRAM.MODEL')</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('newprograms.list', 'educational') }}" class="nav-link {!! url()->current() == route('newprograms.list','educational') ? 'active' : '' !!}">@lang('MAIN.MENU.PROGRAM.EDUCATIONAL')</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('newprograms.list', 'standart') }}" class="nav-link {!! url()->current() == route('newprograms.list','standart') ? 'active' : '' !!}">@lang('MAIN.MENU.PROGRAM.STANDART')</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('newprograms.list', 'curriculum') }}" class="nav-link {!! url()->current() == route('newprograms.list','curriculum') ? 'active' : '' !!}">@lang('MAIN.MENU.PROGRAM.CURRICULUM')</a>
                    </li>

                    <li class="nav-item"><a href="{{route('materials.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['materials.index','materials.create','materials.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.MATERIALS')</a></li>
                    <li class="nav-item"><a href="{{route('layouts.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['layouts.index','layouts.create','layouts.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.LAYOUTS')</a></li>
                </ul>
            </li>
        @endif

        {{-- Планирование --}}
        @if(Gate::check('admin') || Gate::check('planing'))
            <li class="nav-item nav-item-submenu">
                <a href="#" class="nav-link"><i class="icon-calendar"></i> <span>@lang('MAIN.MENU.PLANING')</span></a>
                <ul class="nav nav-group-sub" data-submenu-title="@lang('MAIN.MENU.PLANING')">
                    <li class="nav-item"><a href="{{route('languagetypes.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['languagetypes.index','languagetypes.create','languagetypes.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.LANGUAGETYPES')</a></li>
                    <li class="nav-item"><a href="{{route('ctypes.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['ctypes.index','ctypes.create','ctypes.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.CTYPES')</a></li>
                    <li class="nav-item"><a href="{{route('cbranches.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['cbranches.index','cbranches.create','cbranches.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.CBRANCHES')</a></li>
                    <li class="nav-item"><a href="{{route('cauthors.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['cauthors.index','cauthors.create','cauthors.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.CAUTHORS')</a></li>
                    <li class="nav-item"><a href="{{route('cperiods.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['cperiods.index','cperiods.create','cperiods.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.CPERIODS')</a></li>
                    <li class="nav-item"><a href="{{route('components.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['components.index','components.create','components.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.COMPONENTS')</a></li>
                    <li class="nav-item"><a href="{{route('cprograms.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['cprograms.index','cprograms.create','cprograms.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.CPROGRAMS')</a></li>
                    <li class="nav-item"><a href="{{route('loadtemplates.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['loadtemplates.index','loadtemplates.create','loadtemplates.show','loadtemplates.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.LOADTEMPLATES')</a></li>
                </ul>
            </li>
        @endif

        {{-- Наполнение сайта --}}
        @if(Gate::check('admin') || Gate::check('posts') || Gate::check('pages'))
            <li class="nav-item nav-item-submenu">
                <a href="#" class="nav-link"><i class="icon-display"></i> <span>@lang('MAIN.MENU.CONTENT')</span></a>
                <ul class="nav nav-group-sub" data-submenu-title="@lang('MAIN.MENU.CONTENT')">
                    <li class="nav-item"><a href="{{route('pages.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['pages.index','pages.create','pages.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.PAGES')</a></li>
                    <li class="nav-item"><a href="{{route('posts.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['posts.index','posts.create','posts.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.POSTS')</a></li>
                </ul>
            </li>
        @endif

        {{-- Справочники --}}
        @if(Gate::check('admin') || Gate::check('vocabularies'))
            <li class="nav-item nav-item-submenu">
                <a href="#" class="nav-link"><i class="icon-copy"></i> <span>@lang('MAIN.MENU.VOCABULARIES')</span></a>

                <ul class="nav nav-group-sub" data-submenu-title="@lang('MAIN.MENU.VOCABULARIES')">
                    <li class="nav-item"><a href="{{route('roles.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['roles.index','roles.create','roles.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.ROLES')</a></li>
                    <li class="nav-item"><a href="{{route('permissions.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['permissions.index','permissions.create','permissions.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.PERMISSIONS')</a></li>
                    <li class="nav-item"><a href="{{route('institutions.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['institutions.index','institutions.show']) ? 'active' : '' !!}">@lang('MAIN.MENU.INSTITUTIONS')</a></li>
                    <li class="nav-item"><a href="{{route('creates.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['creates.index','creates.create','creates.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.CREATES')</a></li>
                    <li class="nav-item"><a href="{{route('tools.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['tools.index','tools.create','tools.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.TOOLS')</a></li>
                    <li class="nav-item"><a href="{{route('statuses.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['statuses.index','statuses.create','statuses.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.STATUSES')</a></li>
                    <li class="nav-item"><a href="{{route('stypes.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['stypes.index','stypes.create','stypes.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.STYPES')</a></li>
                    <li class="nav-item"><a href="{{route('ptypes.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['ptypes.index','ptypes.create','ptypes.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.PTYPES')</a></li>
                    <li class="nav-item"><a href="{{route('ttypes.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['ttypes.index','ttypes.create','ttypes.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.TTYPES')</a></li>
                    <li class="nav-item"><a href="{{route('teamroles.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['teamroles.index','teamroles.create','teamroles.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.TEAMROLES')</a></li>
                    <li class="nav-item"><a href="{{route('periods.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['periods.index','periods.create','periods.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.PERIODS')</a></li>
                    <li class="nav-item"><a href="{{route('cats.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['cats.index','cats.create','cats.edit']) ? 'active' : '' !!}">@lang('MAIN.MENU.CATEGORIES')</a></li>

                </ul>
            </li>
        @endif

        {{-- Настройки и опции --}}
        @if(Gate::check('admin') || Gate::check('options'))
            <li class="nav-item">
                <a href="{{route('options')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['options']) ? 'active' : '' !!}"><i class="icon-cog"></i><span>@lang('MAIN.MENU.OPTIONS')</span></a>
            </li>
        @endif
        @if(Gate::check('admin') || Gate::check('seeds'))
            <li class="nav-item">
                <a href="{{route('seeds')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['seeds']) ? 'active' : '' !!}">
                    <i class="icon-database-refresh"></i><span>@lang('MAIN.MENU.SEEDS')</span>
                </a>
            </li>
        @endif

    </ul>
</div>
