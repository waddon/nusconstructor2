			<div class="navbar navbar-expand-lg navbar-light">
				<div class="text-center d-lg-none w-100">
					<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
						<i class="icon-unfold mr-2"></i> @lang('MAIN.BUTTON.FOOTER')
					</button>
				</div>

				<div class="navbar-collapse collapse w-100" id="navbar-footer">
					<div class="w-100">
						<div class="d-flex justify-content-center flex-wrap flex-lg-nowrap flex-xl-nowrap">
							<div class="text-center text-lg-left mr-lg-auto p-2"><img src="/img/EU.png" class="mr-3"><img src="/img/logo2.png"></div>
							<div class="text-center p-2">Проект здійснюється за фінансування Європейського Союзу<br>та Міністерства закордонних справ Фінляндії</div>
							<div class="text-center ml-lg-auto p-2"><img src="/img/lt-2.png"></div>
						</div>
						<div class="text-center p-2 bt-1 small">Цей веб-сайт був підготовлений за фінансової допомоги Європейського Союзу та Міністерства закордонних справ Фінляндії. Висловлені в цьому документі погляди жодним чином не можуть сприймати офіційну думку Європейського Союзу чи Міністерства закордонних справ Фінляндії</div>
					</div>
				</div>
			</div>