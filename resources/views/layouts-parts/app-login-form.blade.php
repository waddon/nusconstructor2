<div class="sidebar-user-nus">
    <div class="sidebar-user-nus-body">
        <div class="card">
            <div class="card-body text-center">
                <h6 class="card-title">@lang('MAIN.TITLE.AUTHORIZATION')</h6>
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <input id="email" type="email" class="form-control @if(isset($errors)) @error('email') is-invalid @enderror @endif mb-3" name="email" value="{{ old('email') }}" required autocomplete="email" Placeholder="@lang('MAIN.PLACEHOLDER.EMAIL')" autofocus>
                    @if(isset($errors)) @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror @endif
                    <input id="password" type="password" class="form-control @if(isset($errors)) @error('password') is-invalid @enderror @endif mb-3" name="password" required autocomplete="current-password" Placeholder="@lang('MAIN.PLACEHOLDER.PASSWORD')">
                    @if(isset($errors)) @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror @endif
                    <div class="custom-control custom-checkbox mb-3">
                        <input type="checkbox" class="custom-control-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label class="custom-control-label" for="remember">@lang('MAIN.BUTTON.REMEMBER-ME')</label>
                    </div>
                    <button type="submit" class="btn btn-primary form-control mb-3">@lang('MAIN.BUTTON.LOGIN')</button>
                    <div class="text-center">
                        @if (Route::has('password.request'))
                            <a href="{{ route('password.request') }}">
                                @lang('MAIN.BUTTON.FORGOT-PASSWORD')
                            </a>
                        @endif/
                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">
                                @lang('MAIN.BUTTON.REGISTER')
                            </a>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
