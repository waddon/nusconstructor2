<div class="sidebar-user-nus">
  <div class="sidebar-user-nus-body">
    <div class="card-body text-center">
      <a href="{{route('profile')}}">
        <img src="{{ auth()->user()->getAvatarPath() }}" class="img-fluid rounded-circle shadow-1 mb-3" width="80" height="80" alt="" id="main-avatar">
      </a>
      <h6 class="mb-0 text-dakr">{{ auth()->user()->combineName() ?? '' }}</h6>
    </div>
    <div class="sidebar-user-material-footer">
      <a href="#user-nav" class="d-flex justify-content-between align-items-center text-shadow-dark dropdown-toggle" data-toggle="collapse"><span>@lang('MAIN.MENU.ACCOUNT')</span></a>
    </div>
  </div>

  <div class="collapse {!! in_array(\Route::currentRouteName(),['profile','uteams','unewprograms.index', 'unewprograms.create', 'unewprograms.show', 'unewprograms.edit', 'unewprograms.list', 'uplaning.index', 'planing.index', 'uteam.info', 'tnewprograms.index', 'tnewprograms.create', 'tnewprograms.show', 'tnewprograms.edit', 'tnewprograms.list', 'tlayouts.index', 'tlayouts.edit', 'tmaterials.index', 'tmaterials.create', 'tmaterials.edit', 'tspecifics.index', 'tspecifics.create', 'tspecifics.edit', 'tnewpackages.index', 'tnewpackages.create', 'tnewpackages.edit']) ? 'show' : '' !!}" id="user-nav">
    <ul class="nav nav-sidebar" data-nav-type="accordion">
      <li class="nav-item">
        <a href="{{route('profile')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['profile']) ? 'active' : '' !!}">
          <i class="icon-user"></i>
          <span>@lang('MAIN.MENU.PROFILE')</span>
        </a>
      </li>

      <li class="nav-item nav-item-submenu">
        <a href="#" class="nav-link"><i class="icon-stack-text"></i><span>@lang('MAIN.MENU.SELF-DOCUMENTS')</span></a>
        <ul class="nav nav-group-sub" data-submenu-title="@lang('MAIN.MENU.SELF-DOCUMENTS')">
          <li class="nav-item">
            <a href="{{ route('unewprograms.list', 'model') }}" class="nav-link {!! url()->current() == route('unewprograms.list','model') ? 'active' : '' !!}">@lang('MAIN.MENU.PROGRAM.MODEL')</a>
          </li>
          <li class="nav-item">
            <a href="{{ route('unewprograms.list', 'educational') }}" class="nav-link {!! url()->current() == route('unewprograms.list','educational') ? 'active' : '' !!}">@lang('MAIN.MENU.PROGRAM.EDUCATIONAL')</a>
          </li>
          <li class="nav-item">
            <a href="{{ route('unewprograms.list', 'standart') }}" class="nav-link {!! url()->current() == route('unewprograms.list','standart') ? 'active' : '' !!}">@lang('MAIN.MENU.PROGRAM.STANDART')</a>
          </li>
          <li class="nav-item">
            <a href="{{ route('unewprograms.list', 'curriculum') }}" class="nav-link {!! url()->current() == route('unewprograms.list','curriculum') ? 'active' : '' !!}">@lang('MAIN.MENU.PROGRAM.CURRICULUM')</a>
          </li>

          {{--<li class="nav-item">
            <a href="{{route('unewprograms.index')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['unewprograms.index', 'unewprograms.create', 'unewprograms.show', 'unewprograms.edit']) ? 'active' : '' !!}">
              <i class="icon-graduation"></i><span>@lang('MAIN.MENU.PROGRAMS')</span>
            </a>
          </li>--}}

          {{-- Планрование с разбивкой по годам обучения --}}
          {{--<li class="nav-item nav-item-submenu">
            <a href="#" class="nav-link"><span>@lang('MAIN.MENU.PLANING')</span></a>
            <ul class="nav nav-group-sub" data-submenu-title="@lang('MAIN.MENU.PLANING')">
              @foreach(auth()->user()->getActivePlaningPeriods() as $cperiod)
              <li class="nav-item">
                <a href="{{route('uplaning.index',[$cperiod->id])}}" class="nav-link  {!! in_array( url()->current(), [ route('uplaning.index',[$cperiod->id]) ]) ? 'active' : '' !!}">{{ $cperiod->name }}</a>
              </li>
              @endforeach
            </ul>
          </li>--}}
          {{-- Планрование без разбивки по годам обучения --}}
          <li class="nav-item">
            <a href="{{ route('uplaning.index') }}" class="nav-link {!! url()->current() == route('uplaning.index') ? 'active' : '' !!}">@lang('MAIN.MENU.PLANING')</a>
          </li>

        </ul>
      </li>

      <li class="nav-item">
        <a href="{{route('uteams')}}" class="nav-link {!! in_array(\Route::currentRouteName(),['uteams']) ? 'active' : '' !!}">
          <i class="icon-users4"></i>
          <span>@lang('MAIN.MENU.TEAMS')</span>
        </a>
      </li>

      <li class="nav-item nav-item-submenu">
        <a href="#" class="nav-link"><i class="icon-stack-text"></i><span>@lang('MAIN.MENU.TEAM-DOCUMENTS')</span></a>
        <ul class="nav nav-group-sub" data-submenu-title="@lang('MAIN.MENU.TEAM-DOCUMENTS')">
          @foreach(auth()->user()->teams as $team)
          <li class="nav-item nav-item-submenu">
            <a href="#" class="nav-link"><span>{{$team->name}}</span></a>
            <ul class="nav nav-group-sub" data-submenu-title="{{$team->name}}">
                <li class="nav-item">
                  <a href="{{route('uteam.info',[$team->id])}}" class="nav-link {!! url()->current() == route('uteam.info',[$team->id]) ? 'active' : '' !!}">
                    <span>@lang('MAIN.MENU.TEAM-ABOUT')</span>
                    @if($team->candidateCount()>0)
                      <span class="badge bg-blue-400 align-self-center ml-auto">{{$team->candidateCount()}}</span>
                    @endif
                  </a>
                </li>
                @if( $team->isActiveMember( auth()->user()->id ) )
                  @if($team->ttype->institution)
                    <li class="nav-item"><a href="{{route('tspecifics.index',$team->id)}}" class="nav-link {!! in_array( url()->current(), [ route('tspecifics.index',$team->id), route('tspecifics.create',$team->id)]) ? 'active' : '' !!}"><span>@lang('MAIN.MENU.TEAM-SPECIFICS')</span></a></li>
                    <li class="nav-item"><a href="{{route('tnewpackages.index',$team->id)}}" class="nav-link {!! in_array( url()->current(), [ route('tnewpackages.index',$team->id), route('tnewpackages.create',$team->id)]) ? 'active' : '' !!}"><span>@lang('MAIN.MENU.TEAM-PACKAGES')</span></a></li>
                    {{--<li class="nav-item"><a href="{{route('tnewplans.index',$team->id)}}" class="nav-link {!! in_array( url()->current(), [ route('tnewplans.index',$team->id), route('tnewplans.create',$team->id)]) ? 'active' : '' !!}"><span>@lang('MAIN.MENU.TEAM-PLANS')</span></a></li>--}}
                  @else
                    <li class="nav-item"><a href="{{route('tlayouts.index',$team->id)}}" class="nav-link {!! in_array( url()->current(), [ route('tlayouts.index',$team->id), route('tlayouts.create',$team->id)]) ? 'active' : '' !!}"><span>@lang('MAIN.MENU.TEAM-LAYOUTS')</span></a></li>
                  @endif

                  <li class="nav-item">
                    <a href="{{ route('tnewprograms.list', [$team->id, 'model']) }}" class="nav-link {!! url()->current() == route('tnewprograms.list',[$team->id, 'model']) ? 'active' : '' !!}">@lang('MAIN.MENU.PROGRAM.MODEL')</a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('tnewprograms.list', [$team->id, 'educational']) }}" class="nav-link {!! url()->current() == route('tnewprograms.list',[$team->id, 'educational']) ? 'active' : '' !!}">@lang('MAIN.MENU.PROGRAM.EDUCATIONAL')</a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('tnewprograms.list', [$team->id, 'standart']) }}" class="nav-link {!! url()->current() == route('tnewprograms.list',[$team->id, 'standart']) ? 'active' : '' !!}">@lang('MAIN.MENU.PROGRAM.STANDART')</a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('tnewprograms.list', [$team->id, 'curriculum']) }}" class="nav-link {!! url()->current() == route('tnewprograms.list',[$team->id, 'curriculum']) ? 'active' : '' !!}">@lang('MAIN.MENU.PROGRAM.CURRICULUM')</a>
                  </li>

                @endif

                @if($team->isLeader(auth()->user()->id))
                  {{--<li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><span>@lang('MAIN.MENU.PLANING')</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="@lang('MAIN.MENU.PLANING')">
                      @foreach($team->getActivePlaningPeriods() as $cperiod)
                      <li class="nav-item">
                        <a href="{{route('planing.index',[$team->id, $cperiod->id])}}" class="nav-link  {!! in_array( url()->current(), [ route('planing.index',[$team->id, $cperiod->id]) ]) ? 'active' : '' !!}">{{ $cperiod->name }}</a>
                      </li>
                      @endforeach
                    </ul>
                  </li>--}}
                  <li class="nav-item">
                    <a href="{{route('planing.index',[$team->id])}}" class="nav-link {!! url()->current() == route('planing.index',[$team->id]) ? 'active' : '' !!}">@lang('MAIN.MENU.PLANING')</a>
                  </li>
                @endif

                @if( $team->isActiveMember( auth()->user()->id ) )
                  <li class="nav-item"><a href="{{route('tmaterials.index',$team->id)}}" class="nav-link {!! in_array( url()->current(), [ route('tmaterials.index',$team->id), route('tmaterials.create',$team->id)]) ? 'active' : '' !!}"><span>@lang('MAIN.MENU.TEAM-MATERIALS')</span></a></li>                
                @endif
            </ul>
          </li>
          @endforeach
        </ul>
      </li>

      <li class="nav-item">
        <a href="#" class="nav-link" data-toggle="modal" data-target="#modal-exit">
          <i class="icon-switch2"></i>
          <span>@lang('MAIN.MENU.LOGOUT')</span>
        </a>
      </li>
    </ul>
  </div>
</div>
