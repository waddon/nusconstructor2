<div id="modal-edebo" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h6 class="modal-title">@lang('School select by EDEBO')</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <select class="form-control select-remote-data" data-fouc>
                    <option value="0" selected>@lang('Select School')</option>
                </select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">@lang('Cancel')</button>
                <button type="button" class="btn bg-primary btn-school-append-edebo" data-dismiss="modal">@lang('Select')</button>
            </div>
        </div>
    </div>
</div>

<div id="modal-placement" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h6 class="modal-title">@lang('School select by Placement')</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="remove-modal-body">
                <div class="form-group">
                    <select name="region" id="region" class="form-control">
                        <option value="0" hidden>@lang('Select Region')</option>
                    </select>
                </div>
                <div class="form-group">
                    <select name="settlement" id="settlement" class="form-control">
                        <option value="0" hidden>@lang('Select Settlement')</option>
                    </select>
                </div>
                <div class="form-group">
                    <select name="school-list" id="school-list" class="form-control">
                        <option value="0" hidden>@lang('Select School')</option>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">@lang('Cancel')</button>
                <button type="button" class="btn bg-primary btn-school-append-placement" data-dismiss="modal">@lang('Select')</button>
            </div>
        </div>
    </div>
</div>

<script src="/js/select2/select2.min.js"></script>
<script>
    $('[name=user_type]').change(function(){
        if ($(this).val()=='other'){
            $('.school-block').hide();
        } else {
            $('.school-block').show();
        }
    });
    getRegions();
    $('.select-remote-data').select2({
        ajax: {
            url: "{{route('selectSchollsByEdebo')}}",
            type: 'get',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term,
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        minimumInputLength: 0,
        templateResult: formatRepo, // omitted for brevity, see the source of this page
        templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
    });

    function formatRepo (repo) {
        if (repo.loading) return repo.name;
        var markup = '<div class="select2-result-repository clearfix">' +
            '<div class="select2-result-repository__title">' + repo.short_name + '</div>';
        markup += '<div class="select2-result-repository__statistics">' +
            '<div class="select2-result-repository__forks">@lang("EDEBO"): ' + repo.id + ',</div>' +
            '<div class="select2-result-repository__stargazers">@lang("Address"): ' + repo.region_name + ', ' + repo.koatuu_name + ', ' + repo.address + ',</div>' +
            '</div></div>';
            return markup;
    }

    function formatRepoSelection (repo) {
        $('#modal-edebo').attr('data-edebo',repo.id);
        $('#modal-edebo').attr('data-name',repo.short_name);
        return repo.short_name || repo.text;
    }

    function getRegions() {
            $.ajax({
                url: "{{route('selectRegions')}}",
                type: 'GET',
                dataType : "json",
                // data: {"_token":"{{ csrf_token() }}"},
            }).done((data, status, xhr) => {
                $("#region").html('<option hidden>@lang("Select Region")</option>');
                $("#settlement").html('<option hidden>@lang("Select Settlement")</option>');
                $("#school-list").html('<option value="0" hidden>@lang("Select School")</option>');
                $.each(data, function( index, value ) {
                    $("#region").append('<option>'+value.region_name+'</option>');
                });
            }).fail((xhr) => {
                let data = xhr.responseJSON;
                swal({
                    title: data.message,
                    type: 'error',
                    confirmButtonClass: 'btn btn-primary',
                })
            });
    }

    function getSettlements() {
            $.ajax({
                url: "{{route('selectSettlements')}}",
                type: 'GET',
                dataType : "json",
                data: {"region":function(){
                    return $("#region").val();
                }},
            }).done((data, status, xhr) => {
                $("#settlement").html('<option hidden>@lang("Select Settlement")</option>');
                $("#school-list").html('<option value="0" hidden>@lang("Select School")</option>');
                $.each(data, function( index, value ) {
                    $("#settlement").append('<option>'+value.koatuu_name+'</option>');
                });
            }).fail((xhr) => {
                let data = xhr.responseJSON;
                swal({
                    title: data.message,
                    type: 'error',
                    confirmButtonClass: 'btn btn-primary',
                })
            });
    }

    function getSchools() {
            $.ajax({
                url: "{{route('selectSchollsByPlacement')}}",
                type: 'GET',
                dataType : "json",
                data: {
                    "region":function(){
                        return $("#region").val();
                    },
                    "settlement":function(){
                        return $("#settlement").val();
                    }
                },
            }).done((data, status, xhr) => {
                $("#school-list").html('<option value="0" hidden>@lang("Select School")</option>');
                console.log(data);
                $.each(data, function( index, value ) {
                    $("#school-list").append('<option value="'+value.id+'">'+(value.short_name!='' ? value.short_name : value.name)+'</option>');
                });
            }).fail((xhr) => {
                let data = xhr.responseJSON;
                swal({
                    title: data.message,
                    type: 'error',
                    confirmButtonClass: 'btn btn-primary',
                })
            });
    }

    $(document).on('change','#region',function(){
        getSettlements();
    });

    $(document).on('change','#settlement',function(){
        getSchools();
    });

    $(document).on('click','.btn-school-append-edebo',function(){
        $('#school').val( $('#modal-edebo').attr('data-name') );
        $('#school_id').val( $('#modal-edebo').attr('data-edebo') );
    });

    $(document).on('click','.btn-school-append-placement',function(){
        let temt_text = $( "#school-list option:selected" ).text();
        $('#school').val( temt_text != '@lang("Select School")' ? temt_text : '' );
        $('#school_id').val( $('#school-list').val() );
    });
</script>
