            <div class="page-header page-header-light">

                <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                    <div class="d-flex">
                        {{--<div class="breadcrumb">--}}
                            {{--<span class="breadcrumb-item active"><i class="icon-home2 mr-2"></i>Головна</span>--}}
                            {{--@widget('appbreadcrumbs')--}}
                        {{--</div>--}}
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>

                    <div class="header-elements d-none">
                        <div class="breadcrumb justify-content-center">

                            @auth
                                @if(auth()->user()->can('admin') || auth()->user()->can('dashboard'))
                                    <a href="{{route('dashboard')}}" class="breadcrumb-elements-item"><i class="icon-equalizer3 mr-2"></i><span>@lang('MAIN.MENU.DASHBOARD')</span></a>
                                @endif
                            @endauth

                            <a href="{{route('glossary')}}" class="breadcrumb-elements-item"><i class="icon-text-color mr-2"></i><span>@lang('MAIN.MENU.GLOSSARY')</span></a>
                            <a href="{{route('news')}}" class="breadcrumb-elements-item"><i class="icon-newspaper mr-2"></i><span>@lang('MAIN.MENU.NEWS')</span></a>

                            <div class="breadcrumb-elements-item dropdown p-0">
                                <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-gear mr-2"></i>@lang('MAIN.MENU.DOCUMENTS')
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="{{route('laws')}}" class="dropdown-item"><i class="icon-hammer2"></i> @lang('MAIN.MENU.LAWS')</a>
                                    <a href="{{route('regulations')}}" class="dropdown-item"><i class="icon-stack-star"></i> @lang('MAIN.MENU.REGULATIONS')</a>
                                    <a href="{{route('guidelines')}}" class="dropdown-item"><i class="icon-stack-text"></i> @lang('MAIN.MENU.GUIDELINES')</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
