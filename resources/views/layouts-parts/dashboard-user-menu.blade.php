				<div class="sidebar-user-material">
					<div class="sidebar-user-material-body">
						<div class="card-body text-center">
							@auth
							<a href="{{route('profile')}}">
								<img src="{{ auth()->user()->getAvatarPath() }}" class="img-fluid rounded-circle shadow-1 mb-3" width="80" height="80" alt="" id="main-avatar">
							</a>
							<h6 class="mb-0 text-white text-shadow-dark">{{auth()->user()->name}}</h6>
							<span class="font-size-sm text-white text-shadow-dark">{{auth()->user()->roles->implode('name',', ')}}</span>
							@endauth
						</div>
					</div>
				</div>
