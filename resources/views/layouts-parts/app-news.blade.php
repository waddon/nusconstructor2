        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="mb-3">{{$element->name ?? ''}}</h5>
                    @if($element->data->thumbnail_id)
                        <div class="item-responsive item-16by9 mb-3">
                            <div class="item-responsive-image" style="background: url('{{$element->getThumbnail()}}');"></div>
                        </div>
                    @endif
                    <p class="item-excerption mb-3">{{$element->getExcerption() ?? ''}}</p>
                    <div class="d-flex justify-content-between align-items-center">
                        <span class="item-meta text-muted">{{$element->getDateCreation()}}</span>
                        <a href="{{route('showsinglenews',$element->slug)}}" class="btn bg-teal">Детальніше<i class="icon-arrow-right7 ml-2"></i></a>
                    </div>
                </div>
            </div>
        </div>
