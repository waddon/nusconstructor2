<style type="text/css">
div#modal-ajax-data img {
    max-width: 100%;
    height: auto;
}    
</style>
<div id="modal-ajax-data" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h6 class="modal-title"></h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">@lang('MAIN.BUTTON.CLOSE')</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).on('click','.btn-ajax-data',function(){
        $.ajax({
            url: $(this).attr('href'),
            type: 'GET',
            dataType : "json",
        }).done((data, status, xhr) => {
            $('#modal-ajax-data .modal-title').html( typeof data.title === 'undefined' ? "Детальний опис" : data.title );
            $('#modal-ajax-data .modal-body').html( typeof data.body === 'undefined' ? "Детальний опис" : data.body );
        });
    });
</script>
