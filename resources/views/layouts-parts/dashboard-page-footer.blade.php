			<div class="navbar navbar-expand-lg navbar-light">
				<div class="text-center d-lg-none w-100">
					<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
						<i class="icon-unfold mr-2"></i>
						Footer
					</button>
				</div>

				<div class="navbar-collapse collapse" id="navbar-footer">
					<span class="navbar-text">
						&copy; 2020. <a href="//activemedia.ua/" target="_blank">Activemedia</a> by <a href="//waddonator.ru/" target="_blank">Vadym Donets</a>
					</span>
					<span class="navbar-text" style="display:none;">{{\Route::currentRouteName()}}</span>
				</div>
			</div>
