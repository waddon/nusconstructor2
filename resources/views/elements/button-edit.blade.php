@if(($filterData['owner']=='user' && $model->belongToMe()) || ($filterData['owner']=='team' && $model->canEdit($filterData['id'])))
<a
	href="{{ route('unewprograms.edit', $modelId) }}"
	class="list-icons-item"
	title="@lang('MAIN.HINT.EDIT')"
><i class="icon-pencil7"></i></a>
@endif