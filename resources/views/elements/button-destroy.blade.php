<span 
  class="list-icons-item text-danger livewire-click" 
  title="@lang('MAIN.HINT.DESTROY')"
  wire:click="$set('modelId', '{{ $modelId }}')"
  data-element-type="confirm"
  data-title="@lang('MAIN.MODAL.DESTROY.TITLE')"
  data-text="@lang('MAIN.MODAL.DESTROY.MESSAGE')"
  data-btn-confirm="@lang('MAIN.BUTTON.DESTROY')"
  data-action="destroy"
  data-type="warning"
  data-btn-type="btn-danger"
  data-btn-cancel="@lang('MAIN.BUTTON.CANCEL')"
>
<i class="icon-x"></i>
</span>
