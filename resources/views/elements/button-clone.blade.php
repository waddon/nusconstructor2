<span 
  class="list-icons-item livewire-click" 
  title="@lang('MAIN.HINT.CLONE')"
  wire:click="$set('modelId', '{{ $modelId }}')"
  data-element-type="confirm"
  data-title="@lang('MAIN.MODAL.CLONE.TITLE')"
  data-text="@lang('MAIN.MODAL.CLONE.MESSAGE')"
  data-btn-confirm="@lang('MAIN.BUTTON.CLONE')"
  data-action="clone"
  data-type="question"
  data-btn-type="btn-success"
  data-btn-cancel="@lang('MAIN.BUTTON.CANCEL')"
>
<i class="icon-copy3"></i>
</span>
