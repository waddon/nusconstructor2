<span 
  class="list-icons-item text-danger livewire-click" 
  title="@lang('MAIN.HINT.DELETE')"
  wire:click="$set('modelId', '{{ $modelId }}')"
  data-element-type="confirm"
  data-title="@lang('MAIN.MODAL.DELETE.TITLE')"
  data-text="@lang('MAIN.MODAL.DELETE.MESSAGE')"
  data-btn-confirm="@lang('MAIN.BUTTON.DELETE')"
  data-action="delete"
  data-type="warning"
  data-btn-type="btn-danger"
  data-btn-cancel="@lang('MAIN.BUTTON.CANCEL')"
>
<i class="icon-bin"></i>
</span>
