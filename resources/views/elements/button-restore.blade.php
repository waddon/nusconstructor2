<span 
  class="list-icons-item text-success livewire-click" 
  title="@lang('MAIN.HINT.RESTORE')"
  wire:click="$set('modelId', '{{ $modelId }}')"
  data-element-type="confirm"
  data-title="@lang('MAIN.MODAL.RESTORE.TITLE')"
  data-text="@lang('MAIN.MODAL.RESTORE.MESSAGE')"
  data-btn-confirm="@lang('MAIN.BUTTON.RESTORE')"
  data-action="restore"
  data-type="question"
  data-btn-type="btn-success"
  data-btn-cancel="@lang('MAIN.BUTTON.CANCEL')"
>
<i class="icon-undo"></i>
</span>
