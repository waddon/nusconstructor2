@extends('layouts.app')

@section('content')
    <style>
        .programs {
            /*border-top: 1px solid #ddd;*/
        }
        .programs__search:after {
            content: "\E98E";
            font-family: "icomoon";
            font-size: 0.75rem;
            display: inline-block;
            position: absolute;
            top: 50%;
            right: 0;
            margin-top: -0.375rem;
            line-height: 1;
            opacity: 0.5;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }
        .programs__row, .programs__head {
            display: grid;
            grid-template-columns: 100px 2fr 100px 2fr 150px 200px 150px;
            align-items: center;
            border-bottom: 1px solid #ddd;
        }
        .programs__cell {
            padding: 0.75rem 1.25rem;
            /*border-right: 1px solid #ddd;*/
        }
        .programs__label {
            display: none;
            font-weight: bold;
        }
        .programs__pagination {
            display: flex;
            align-items: center;
        }
        @media (max-width: 1140px){
            .programs__head {display: none;}
            .programs__row, .programs__pagination {display: block;}
            .programs__label {display: inline;}
            .programs__cell:not(:last-child) {
                padding-bottom: 0;
            }
        }
    </style>
    <div class="card">
        <div class="card-header header-elements-inline alpha-1">
            <h1 class="card-title">{{ $title ?? 'undefined' }}</h1>
        </div>
        @livewire('common.program-list', ['filterData' => $filterData])
    </div>
@endsection

@section('scripts')
<script src="/js/select2/select2.min.js"></script>
<script>
$livewireContainer = document.querySelector('.livewire-container');
$livewireContainer.addEventListener('click', function (event) {
    let $element = event.target.closest('.livewire-click')
    if (!$element) return
    let elementType = $element.getAttribute('data-element-type') ?? 'link'

    if (elementType === 'confirm') {
        swal({
            title: $element.getAttribute('data-title') ? $element.getAttribute('data-title') : '',
            text: $element.getAttribute('data-text') ? $element.getAttribute('data-text') : '',
            type: $element.getAttribute('data-type') ? $element.getAttribute('data-type') : 'question',
            showCancelButton: true,
            confirmButtonText: $element.getAttribute('data-btn-confirm') ? $element.getAttribute('data-btn-confirm') : 'Confirm',
            confirmButtonClass: "btn " + ($element.getAttribute('data-btn-type') ? $element.getAttribute('data-btn-type') : 'bg-slate-700'),
            cancelButtonText: $element.getAttribute('data-btn-cancel') ? $element.getAttribute('data-btn-cancel') : 'Cancel',
            cancelButtonClass: "btn btn-light",
        }).then((confirm) => {
            if(confirm.value){
                window.livewire.emit('execAction', { action : $element.getAttribute('data-action') });
            }
        })
    }

    if (elementType === 'input') {
        value = $element.closest('tr')?.querySelector('.team-name').innerHTML ?? ''
        swal({
            title: $element.getAttribute('data-title') ? $element.getAttribute('data-title') : '',
            input: 'text',
            inputValue: value,
            inputClass: 'form-control',
            inputPlaceholder: `@lang('MAIN.PLACEHOLDER.TEAM-NAME')`,
            showCancelButton: true,
            confirmButtonText: $element.getAttribute('data-btn-confirm') ? $element.getAttribute('data-btn-confirm') : 'Confirm',
            confirmButtonClass: "btn " + ($element.getAttribute('data-btn-type') ? $element.getAttribute('data-btn-type') : 'bg-slate-700'),
            cancelButtonText: $element.getAttribute('data-btn-cancel') ? $element.getAttribute('data-btn-cancel') : 'Cancel',
            cancelButtonClass: "btn btn-light",
        }).then(function (result) {
            if (result.value) {
                window.livewire.emit('execAction', { action: $element.getAttribute('data-action'), value: result.value });
            }
        });        
    }

    if (elementType === 'link') {
        window.location.href = $element.getAttribute('href')
    }
    

}, false);    
</script>
@endsection