
//global.$ = global.jQuery = require('./core/jquery.min');
global.$ = global.jQuery = require('jquery');

require('./custom');
require('./core/bootstrap.bundle.min');
require('./core/blockui.min');
require('./core/ripple.min');
// const Cropper = require('./core/cropper.min');

const swal = window.swal = require('./core/sweet_alert.min');

require('./core/app');

