$(document).ready(function(){
    $('.nav-link.active').parents('.nav-item-submenu').addClass('nav-item-open');
    $('.nav-link.active').parents('.nav-group-sub').show();
    $('.card-sidebar-mobile').show();
    
    $('form.ajax-form').on('submit', function(e){
        e.preventDefault();
        if( typeof(CKEDITOR) !== "undefined" ){
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
        }
        save_ajax_form( $(this) );
    });

    function save_ajax_form(form){
        let data = {};
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: form.serialize(),
        }).done((data, status, xhr) => {
            swal({
                title: data.message,
                type: 'success',
                showCancelButton: true,
                confirmButtonText: "Вийти до переліку",
                confirmButtonClass: 'btn btn-primary',
                cancelButtonText: "Продовжити редагування",
                cancelButtonClass: 'btn btn-light',
            }).then((confirm) => {
                if(confirm.value){
                    window.location.href = data.exitURL;
                }else{
                    if (data.formAction){
                        form.attr('action',data.formAction);
                        form.prepend('<input name="_method" type="hidden" value="PUT">');
                    }
                }
            });
            form.find('fieldset').attr('disabled', true);
        }).fail((xhr) => {
            let data = xhr.responseJSON;
            swal({
                title: data.message,
                type: 'error',
                confirmButtonClass: 'btn btn-primary',
            })                    
        }).always((xhr, type, status) => {
            let response = xhr.responseJSON || status.responseJSON,
                errors = response.errors || [];
            form.find('.field').each((i, el) => {
                let field = $(el);
                field.removeClass('is-invalid');
                container = field.closest('.form-group'),
                    elem = $('<label class="message">');
                container.find('label.message').remove();
                if(errors[field.attr('name')]){
                    field.addClass('is-invalid');
                    field.find('input').addClass('is-invalid');
                    errors[field.attr('name')].forEach((msg) => {
                        elem.clone().addClass('validation-invalid-label').html(msg).appendTo(container);
                    });
                }
            });
        });
    };

});

$(document).on('click','.datatableRefresh',function(){
    $('.datatable').DataTable().ajax.reload(null, false);
})

window.renderTemplate = function renderTemplate(name, data) {
    var template = document.getElementById(name).innerHTML;
    for (var property in data) {
        if (data.hasOwnProperty(property)) {
            var search = new RegExp('{' + property + '}', 'g');
            template = template.replace(search, data[property]);
        }
    }
    return template;
}    


document.querySelector('.livewire-container')?.addEventListener('click', function (event) {
    let $element = event.target.closest('.livewire-click')
    if (!$element) return
    let elementType = $element.getAttribute('data-element-type') ?? 'link'

    if (elementType === 'confirm') {
        // event.preventDefault()
        swal({
            title: $element.getAttribute('data-title') ?? '',
            text: $element.getAttribute('data-text') ?? '',
            type: $element.getAttribute('data-type') ?? 'question',
            showCancelButton: true,
            confirmButtonText: $element.getAttribute('data-btn-confirm') ?? 'Confirm',
            confirmButtonClass: "btn " + ($element.getAttribute('data-btn-type') ?? 'bg-slate-700'),
            cancelButtonText: $element.getAttribute('data-btn-cancel') ?? 'Cancel',
            cancelButtonClass: "btn btn-light",
        }).then((confirm) => {
            if(confirm.value){
                window.livewire.emit('execAction', { action : $element.getAttribute('data-action') });
            }
        })
    }

    if (elementType === 'input') {
        // event.preventDefault()
        value = $element.closest('tr')?.querySelector('.team-name').innerHTML ?? ''
        swal({
            title: $element.getAttribute('data-title') ?? '',
            input: 'text',
            inputValue: value,
            inputClass: 'form-control',
            inputPlaceholder: $element.getAttribute('data-placeholder') ?? 'placeholder',
            showCancelButton: true,
            confirmButtonText: $element.getAttribute('data-btn-confirm') ?? 'Confirm',
            confirmButtonClass: "btn " + ($element.getAttribute('data-btn-type') ?? 'bg-slate-700'),
            cancelButtonText: $element.getAttribute('data-btn-cancel') ?? 'Cancel',
            cancelButtonClass: "btn btn-light",
        }).then(function (result) {
            if (result.value) {
                window.livewire.emit('execAction', { action: $element.getAttribute('data-action'), value: result.value });
            }
        });        
    }

    // if (elementType === 'link') {
    //     window.location.href = $element.getAttribute('href')
    // }
    

}, false);