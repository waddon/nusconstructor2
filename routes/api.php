<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'Api'], function(){
    Route::get('loadtemplates/{cperiod_id?}', ['as' => 'api.loadtemplates.list', 'uses' => 'ApiController@loadtemplates']);
    Route::get('tplans/{model_id?}/{model_type?}/{cperiod_id?}/', ['as' => 'api.tplans.list', 'uses' => 'ApiController@tplans']);
    Route::get('yplan/{yplan_id?}', ['as' => 'api.yplan.data', 'uses' => 'ApiController@yplanData']);

    Route::get('newprogram/element-HTML/{parentId}/{elementSerial}/{childType}', ['as' => 'api.newprogram.elementHTML', 'uses' => 'NewprogramController@getElementHTML']);
});