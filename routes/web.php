<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

# Dashboard
    Route::group(['prefix' => 'dashboard', 'namespace' => 'Dashboard', 'middleware' => ['auth', 'permission:dashboard']], function() {
        Route::get('/', ['as' => 'dashboard', 'uses' => 'AdminController@index']);

        Route::get('users/select2', ['as' => 'users.select2', 'uses' => 'UserController@select2']);
        Route::resource('users', 'UserController')->middleware('permission:users');
        Route::resource('teams', 'TeamController')->middleware('permission:teams');

        Route::resource('levels', 'LevelController')->middleware('permission:levels');
        Route::resource('cycles', 'CycleController')->middleware('permission:cycles');
        Route::resource('grades', 'GradeController')->middleware('permission:grades');
        Route::resource('branches', 'BranchController')->middleware('permission:branches');
        Route::resource('lines', 'LineController')->middleware('permission:lines');
        Route::resource('expects', 'ExpectController')->middleware('permission:expects');
        Route::resource('specifics', 'SpecificController')->middleware('permission:specifics');
        Route::resource('skills', 'SkillController')->middleware('permission:skills');
        Route::resource('descriptions', 'DescriptionController')->middleware('permission:descriptions');
        Route::resource('subjects',  'SubjectController')->middleware('permission:subjects');

        Route::resource('rivens', 'RivenController')->middleware('permission:rivens');
        Route::resource('cikls',  'CiklController')->middleware('permission:cikls');
        Route::resource('notes',  'NoteController')->middleware('permission:notes');
        Route::resource('classes',  'ClassController')->middleware('permission:classes');
        Route::resource('galuzes',  'GaluzController')->middleware('permission:galuzes');
        Route::resource('zors',  'ZorController')->middleware('permission:zors');
        Route::resource('zmists',  'ZmistController')->middleware('permission:zmists');
        Route::resource('kors',  'KorController')->middleware('permission:kors');
        Route::resource('umenies',  'UmenieController')->middleware('permission:umenies');
        Route::resource('galuznotes',  'GaluznoteController')->middleware('permission:galuznotes');
        Route::resource('_subjects',  '_SubjectController')->middleware('permission:_subjects');

        Route::post('/newprograms/changeStatus/{id}',     ['as'=>'newprograms.changeStatus',  'uses'=>'NewprogramController@changeStatus']);
        Route::get('/newprograms/list/{type}', ['as'=>'newprograms.list', 'uses'=>'NewprogramController@list']);
        Route::resource('newprograms', 'NewprogramController');
        Route::post('/newplans/changeStatus/{id}',     ['as'=>'newplans.changeStatus',  'uses'=>'NewplanController@changeStatus']);
        Route::resource('newplans', 'NewplanController');
        Route::post('/materials/changeStatus/{id}',     ['as'=>'materials.changeStatus',  'uses'=>'MaterialController@changeStatus']);
        Route::resource('materials', 'MaterialController');
        Route::post('/newpackages/changeStatus/{id}',     ['as'=>'newpackages.changeStatus',  'uses'=>'NewpackageController@changeStatus']);
        Route::resource('newpackages', 'NewpackageController');
        Route::post('/layouts/changeStatus/{id}',     ['as'=>'layouts.changeStatus',  'uses'=>'LayoutController@changeStatus']);
        Route::resource('layouts', 'LayoutController');

        Route::resource('pages', 'PageController')->middleware('permission:pages');
        Route::resource('posts', 'PostController')->middleware('permission:posts');

        Route::resource('roles', 'RoleController', ['except' => 'show'])->middleware('permission:vocabularies');
        Route::resource('permissions', 'PermissionController', ['except' => 'show'])->middleware('permission:vocabularies');
        Route::get('institutions/select2', ['as' => 'institutions.select2', 'uses' => 'InstitutionController@select2']);
        Route::resource('institutions', 'InstitutionController', ['only' => ['index', 'store', 'show', 'data']])->middleware('permission:institutions');
        Route::resource('creates', 'CreateController', ['except' => 'show'])->middleware('permission:creates');
        Route::resource('cats', 'CatController', ['except' => 'show'])->middleware('permission:cats');
        Route::resource('tools', 'ToolController', ['except' => 'show'])->middleware('permission:tools');
        Route::resource('statuses', 'StatusController', ['except' => 'show'])->middleware('permission:statuses');
        Route::resource('stypes', 'StypeController', ['except' => 'show'])->middleware('permission:stypes');
        Route::resource('ptypes', 'PtypeController', ['except' => 'show'])->middleware('permission:ptypes');

        Route::resource('ttypes', 'TtypeController', ['except' => 'show'])->middleware('permission:ttypes');
        Route::resource('teamroles', 'TeamroleController', ['except' => 'show'])->middleware('permission:teamroles');
        Route::resource('periods', 'PeriodController', ['except' => 'show'])->middleware('permission:periods');

        Route::match(['get', 'post'],'/options',  ['as'=>'options', 'uses'=>'AdminController@options'])->middleware('permission:options');
        Route::get('transfer', ['as' => 'transfer', 'uses' => 'AdminController@transfer'])->middleware('permission:transfer');
        Route::get('customseed', ['as' => 'customseed', 'uses' => 'AdminController@customseed'])->middleware('permission:customseed');

        # Модуль "Навчальний план"
        Route::resource('languagetypes', 'LanguagetypeController', ['except' => 'show'])->middleware('permission:languagetypes');
        Route::resource('ctypes', 'CtypeController', ['except' => 'show'])->middleware('permission:ctypes');
        Route::resource('cbranches', 'CbranchController', ['except' => 'show'])->middleware('permission:cbranches');
        Route::resource('components', 'ComponentController')->middleware('permission:components');
        Route::resource('cauthors', 'CauthorController', ['except' => 'show'])->middleware('permission:cauthors');
        Route::resource('cprograms', 'CprogramController')->middleware('permission:cprograms');
        Route::resource('cperiods', 'CperiodController', ['except' => 'show'])->middleware('permission:cperiods');
        // Route::resource('loadtypes', 'LoadtypeController', ['except' => 'show'])->middleware('permission:loadtypes');
        Route::resource('loadtemplates', 'LoadtemplateController')->middleware('permission:loadtemplates');

        # Посев данных
        Route::match(['get', 'post'],'/seeds', 'AdminController@seeds')->name('seeds')->middleware('permission:seeds');
        Route::get('/phpinfo', ['as' => 'phpinfo', 'uses' => 'AdminController@phpinfo']);

    });


# User
    Route::group(['middleware' => ['auth']], function() {
        Route::get('/profile',       ['as' => 'profile', 'uses' => 'UserController@profile']);
        Route::post('/user-change-avatar',  ['as' => 'user.change-avatar', 'uses'=>'UserController@userChangeAvatar']);
        Route::post('/user-update',  ['as' => 'user.update', 'uses'=>'UserController@user_update']);
        Route::get('/teams-select2', ['as' => 'teams-select2', 'uses' => 'UserController@teams_select2']);
        Route::get('/teams-all-select2', ['as' => 'teams-all-select2', 'uses' => 'UserController@teams_all_select2']);
        Route::get('/uteams',        ['as' => 'uteams', 'uses' => 'UserController@uteams']);

        Route::post('/apply',        ['as'=>'apply', 'uses'=>'UserController@apply']);

        Route::get('/uteam/{team_id}',    ['as' => 'uteam.info', 'uses' => 'TeamController@info']);
        Route::post('/approve/{team_id}/{user_id}', ['as'=>'uteam.approve', 'uses'=>'TeamController@approve']);
        Route::post('/remove/{team_id}/{user_id}', ['as'=>'uteam.remove', 'uses'=>'TeamController@remove']);
        Route::resource('/uteam/{team_id}/tspecifics', 'TspecificController')->middleware('teamActiveMember');

        # Программы команды
        Route::get( '/uteam/{team_id}/tnewprograms/catch/{ulayout}',['as'=>'tnewprograms.catch', 'uses'=>'TnewprogramController@catch']);
        Route::post('/uteam/{team_id}/tnewprograms/share/{ulayout}',['as'=>'tnewprograms.share', 'uses'=>'TnewprogramController@share']);
        Route::post('/uteam/{team_id}/tnewprograms/clone/{ulayout}',['as'=>'tnewprograms.clone', 'uses'=>'TnewprogramController@clone']);
        Route::post('/uteam/{team_id}/tnewprograms/send/{ulayout}', ['as'=>'tnewprograms.send',  'uses'=>'TnewprogramController@send']);
        Route::post('/uteam/{team_id}/tnewprograms/withdraw/{ulayout}', ['as'=>'tnewprograms.withdraw',  'uses'=>'TnewprogramController@withdraw']);
        Route::post( '/uteam/{team_id}/tnewprograms/createOnLayout',['as'=>'tnewprograms.createOnLayout','uses'=>'TnewprogramController@createOnLayout']);
        Route::get( '/uteam/{team_id}/getSpecifics/{id}',['as'=>'tnewprograms.getSpecifics', 'uses'=>'TnewprogramController@getSpecifics']);
        Route::get('/uteam/{team_id}/tnewprograms/list/{type}', ['as'=>'tnewprograms.list', 'uses'=>'TnewprogramController@list']);
        Route::resource('/uteam/{team_id}/tnewprograms', 'TnewprogramController')->middleware('teamActiveMember');

        # Планы команды
        Route::post('/uteam/{team_id}/tnewplans/send/{id}',     ['as'=>'tnewplans.send',  'uses'=>'TnewplanController@send']);
        Route::resource('/uteam/{team_id}/tnewplans', 'TnewplanController')->middleware('teamActiveMember');

        # Материалы команды
        Route::post('/uteam/{team_id}/tmaterials/send/{id}',     ['as'=>'tmaterials.send',  'uses'=>'TmaterialController@send']);
        Route::resource('/uteam/{team_id}/tmaterials', 'TmaterialController')->middleware('teamActiveMember');

        # Пакеты документов команды
        Route::post('/uteam/{team_id}/tnewpackages/send/{id}',     ['as'=>'tnewpackages.send',  'uses'=>'TnewpackageController@send']);
        Route::resource('/uteam/{team_id}/tnewpackages', 'TnewpackageController')->middleware('teamActiveMember');

        # Шаблоны команды
        Route::get( '/uteam/{team_id}/tlayouts/catch/{ulayout}',    ['as'=>'tlayouts.catch', 'uses'=>'TlayoutController@catch']);
        Route::post('/uteam/{team_id}/tlayouts/share/{ulayout}',    ['as'=>'tlayouts.share', 'uses'=>'TlayoutController@share']);
        Route::post('/uteam/{team_id}/tlayouts/clone/{ulayout}',    ['as'=>'tlayouts.clone', 'uses'=>'TlayoutController@clone']);
        Route::post('/uteam/{team_id}/tlayouts/send/{id}',     ['as'=>'tlayouts.send',  'uses'=>'TlayoutController@send']);
        Route::resource('/uteam/{team_id}/tlayouts', 'TlayoutController')->middleware('teamActiveMember');

        # Планирование команды
        // Route::get( '/uteam/{team_id}/planing/{cperiod_id}',    ['as'=>'planing.index', 'uses'=>'TeamController@planing'])->middleware('teamActiveMember');
        Route::get( '/uteam/{team_id}/planing',    ['as'=>'planing.index', 'uses'=>'TeamController@planing'])->middleware('teamActiveMember');
        //Route::get('/uteam/{team_id}/tloads/check/{id}',     ['as'=>'tloads.check',  'uses'=>'TloadController@check']);
        // Route::resource('/uteam/{team_id}/tloads', 'TloadController')->middleware('teamLeader');
        // Route::resource('/uteam/{team_id}/tplans', 'TplanController')->middleware('teamLeader');
        // Route::resource('/uteam/{team_id}/yplans', 'YplanController')->middleware('teamLeader');
        Route::resource('tloads', 'TloadController');
        Route::resource('tplans', 'TplanController');
        Route::resource('yplans', 'YplanController');

        # Программы пользователя
        Route::get( 'unewprograms/catch/{ulayout}', ['as'=>'unewprograms.catch', 'uses'=>'UnewprogramController@catch']);
        Route::post('unewprograms/share/{ulayout}', ['as'=>'unewprograms.share', 'uses'=>'UnewprogramController@share']);
        Route::post('unewprograms/send/{ulayout}', ['as'=>'unewprograms.send',  'uses'=>'UnewprogramController@send']);
        Route::post('unewprograms/withdraw/{ulayout}', ['as'=>'unewprograms.withdraw',  'uses'=>'UnewprogramController@withdraw']);
        Route::post('unewprograms/clone/{ulayout}', ['as'=>'unewprograms.clone',  'uses'=>'UnewprogramController@clone']);
        Route::get('unewprograms/list/{type}', ['as'=>'unewprograms.list', 'uses'=>'UnewprogramController@list']);
        Route::resource('unewprograms', 'UnewprogramController');

        # Планирование пользователя
        // Route::get( '/uplaning/{cperiod_id}',    ['as'=>'uplaning.index', 'uses'=>'UserController@planing']);
        Route::get( '/uplaning',    ['as'=>'uplaning.index', 'uses'=>'UserController@planing']);
    });


# Public
    Route::get('/', 'GeneralController@index')->name('home');
    Route::get('/glossary/{character?}', ['as' => 'glossary', 'uses' => 'GeneralController@glossary']);
    Route::get('/postGetContent/{id}',   ['as' => 'postGetContent', 'uses' => 'GeneralController@postGetContent']);
    Route::get('/news',     ['as' => 'news',     'uses' => 'GeneralController@news']);
    Route::get('/levels',   ['as' => 'levels',   'uses' => 'GeneralController@levels']);
    Route::get('/branches', ['as' => 'branches', 'uses' => 'GeneralController@branches']);
    Route::get('/expects',  ['as' => 'expects',  'uses' => 'GeneralController@expects']);
    Route::get('/getDataExpects',  ['as' => 'getDataExpects',  'uses' => 'GeneralController@getDataExpects']);
    Route::get('/materials',['as' => 'materials','uses' => 'GeneralController@materials']);
    Route::get('/notes',    ['as' => 'notes',    'uses' => 'GeneralController@notes']);
    Route::get('/getCycle', ['as' => 'getCycle', 'uses' => 'GeneralController@getCycle']);
    Route::get('/specifics',['as' => 'specifics','uses' => 'GeneralController@specifics']);
    Route::get('/getDataSpecifics',  ['as' => 'getDataSpecifics',  'uses' => 'GeneralController@getDataSpecifics']);

    Route::get('/laws',['as' => 'laws','uses' => 'GeneralController@laws']);
    Route::get('/regulations',['as' => 'regulations','uses' => 'GeneralController@regulations']);
    Route::get('/guidelines',['as' => 'guidelines','uses' => 'GeneralController@guidelines']);

    Route::match(['get', 'post'],'/search',  ['as'=>'search', 'uses'=>'GeneralController@search']);

    # Типичные документы
        Route::post('typnewplans/copyTeam/{id}',    ['as'=>'typnewplans.copyTeam', 'uses'=>'TypnewplanController@copyTeam']);
        Route::resource('typnewplans', 'TypnewplanController');
        Route::post('typnewprograms/copySelf/{id}',    ['as'=>'typnewprograms.copySelf', 'uses'=>'TypnewprogramController@copySelf']);
        Route::post('typnewprograms/copyTeam/{id}',    ['as'=>'typnewprograms.copyTeam', 'uses'=>'TypnewprogramController@copyTeam']);
        Route::get('public-documents/{type}', ['as'=>'publicDocuments', 'uses'=>'TypnewprogramController@list']);
        Route::resource('typnewprograms', 'TypnewprogramController');
        Route::post('typmaterials/copyTeam/{id}',    ['as'=>'typmaterials.copyTeam', 'uses'=>'TypmaterialController@copyTeam']);
        Route::post('typmaterials/getList/{program_id?}',    ['as'=>'typmaterials.getList', 'uses'=>'TypmaterialController@getList']);
        Route::resource('typmaterials', 'TypmaterialController');
        Route::post('typnewpackages/copyTeam/{id}',    ['as'=>'typnewpackages.copyTeam', 'uses'=>'TypnewpackageController@copyTeam']);
        Route::resource('typnewpackages', 'TypnewpackageController');

    # Все виды программ
    Route::post('/create-curriculum/{id}/{teamId?}', ['as'=>'createCurriculum', 'uses'=>'GeneralController@createCurriculum']);

    Route::get('/showprogram/{key?}', ['as'=>'showprogram', 'uses'=>'GeneralController@showprogram']);
    Route::get('/loadprogram/{key?}', ['as'=>'loadprogram', 'uses'=>'GeneralController@loadprogram']);
    Route::get('/doc-export-program/{key?}', ['as'=>'docExportProgram', 'uses'=>'GeneralController@docExportProgram']);
    Route::get('/showplan/{key?}', ['as'=>'showplan', 'uses'=>'GeneralController@showplan']);
    Route::get('/loadplan/{key?}', ['as'=>'loadplan', 'uses'=>'GeneralController@loadplan']);
    Route::get('/showpackage/{key?}', ['as'=>'showpackage', 'uses'=>'GeneralController@showpackage']);
    Route::get('/showlayout/{key?}', ['as'=>'showlayout', 'uses'=>'GeneralController@showlayout']);
    Route::get('/loadlayout/{key?}', ['as'=>'loadlayout', 'uses'=>'GeneralController@loadlayout']);

Route::get('/{slag}', ['as'=>'page', 'uses'=>'GeneralController@pages']);