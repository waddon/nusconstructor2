<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableComponents extends Migration
{
    const DB_CONNECTION     = 'mysql';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        # Языки обучения
        Schema::connection(self::DB_CONNECTION)->create('languagetypes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->timestamps();
        });


        # Типы компонентов
        Schema::connection(self::DB_CONNECTION)->create('ctypes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->timestamps();
        });

        # Галузи компонентов
        Schema::connection(self::DB_CONNECTION)->create('cbranches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('marking')->nullable();
            $table->timestamps();
        });

        # Компоненты
        Schema::connection(self::DB_CONNECTION)->create('components', function(Blueprint $table) {
            $table->increments('id');
            $table->text('name')->nullable();
            $table->integer('ctype_id')->unsigned()->nullable();
            $table->foreign('ctype_id')->references('id')->on('ctypes')->onDelete('set null');
            $table->softDeletes();
            $table->timestamps();
        });

        # Связь компонентов с галузями
        Schema::connection(self::DB_CONNECTION)->create('cbranch_component', function (Blueprint $table) {
            $table->integer('cbranch_id')->unsigned();
            $table->foreign('cbranch_id')->references('id')->on('cbranches')->onDelete('cascade');
            $table->integer('component_id')->unsigned();
            $table->foreign('component_id')->references('id')->on('components')->onDelete('cascade');
            $table->primary(['cbranch_id', 'component_id']);
        });

        # Связь компонентов с классами
        Schema::connection(self::DB_CONNECTION)->create('component_grade', function (Blueprint $table) {
            $table->integer('component_id')->unsigned();
            $table->foreign('component_id')->references('id')->on('components')->onDelete('cascade');
            $table->integer('grade_id')->unsigned();
            $table->foreign('grade_id')->references('id')->on('grades')->onDelete('cascade');
            $table->primary(['component_id', 'grade_id']);
        });

        # Связь компонентов с языками обучения
        Schema::connection(self::DB_CONNECTION)->create('component_languagetype', function (Blueprint $table) {
            $table->integer('component_id')->unsigned();
            $table->foreign('component_id')->references('id')->on('components')->onDelete('cascade');
            $table->integer('languagetype_id')->unsigned();
            $table->foreign('languagetype_id')->references('id')->on('languagetypes')->onDelete('cascade');
            $table->primary(['component_id', 'languagetype_id']);
        });

        # Связь компонентов друг с другом
        Schema::connection(self::DB_CONNECTION)->create('restricted_component', function (Blueprint $table) {
            $table->integer('component_id')->unsigned();
            $table->foreign('component_id')->references('id')->on('components')->onDelete('cascade');
            $table->integer('restricted_id')->unsigned();
            $table->foreign('restricted_id')->references('id')->on('components')->onDelete('cascade');
            $table->primary(['component_id', 'restricted_id']);
        });

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(self::DB_CONNECTION)->dropIfExists('restricted_component');
        Schema::connection(self::DB_CONNECTION)->dropIfExists('component_languagetype');
        Schema::connection(self::DB_CONNECTION)->dropIfExists('component_grade');
        Schema::connection(self::DB_CONNECTION)->dropIfExists('cbranch_component');
        Schema::connection(self::DB_CONNECTION)->dropIfExists('components');
        Schema::connection(self::DB_CONNECTION)->dropIfExists('cbranches');
        Schema::connection(self::DB_CONNECTION)->dropIfExists('ctypes');
        Schema::connection(self::DB_CONNECTION)->dropIfExists('languagetypes');
    }
}
