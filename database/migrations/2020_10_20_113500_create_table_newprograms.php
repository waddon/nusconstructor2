<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNewprograms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        # Периоды
        Schema::create('newprograms', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->integer('cycle_id')->unsigned();
            $table->foreign('cycle_id')->references('id')->on('cycles')->onDelete('cascade');
            $table->string('status')->default('Editing');
            $table->boolean('public')->default(0);
            $table->string('key')->nullable();
            $table->longtext('meta')->nullable();
            $table->longtext('content')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('newprogram_team', function (Blueprint $table) {
            $table->integer('newprogram_id')->unsigned();
            $table->foreign('newprogram_id')->references('id')->on('newprograms')->onDelete('cascade');
            $table->bigInteger('team_id')->unsigned();
            $table->foreign('team_id')->references('id')->on('teams')->onDelete('cascade');

            $table->longtext('meta')->nullable();
            $table->primary(['newprogram_id', 'team_id']);
        });

        Schema::create('newprogram_user', function (Blueprint $table) {
            $table->integer('newprogram_id')->unsigned();
            $table->foreign('newprogram_id')->references('id')->on('newprograms')->onDelete('cascade');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('table_users')->onDelete('cascade');
            $table->primary(['newprogram_id', 'user_id']);
        });

        Schema::create('branch_newprogram', function (Blueprint $table) {
            $table->integer('branch_id')->unsigned();
            $table->foreign('branch_id')->references('id')->on('branches')->onDelete('cascade');
            $table->integer('newprogram_id')->unsigned();
            $table->foreign('newprogram_id')->references('id')->on('newprograms')->onDelete('cascade');
            $table->primary(['branch_id', 'newprogram_id']);
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_newprogram');
        Schema::dropIfExists('newprogram_user');
        Schema::dropIfExists('newprogram_team');
        Schema::dropIfExists('newprograms');
    }
}
