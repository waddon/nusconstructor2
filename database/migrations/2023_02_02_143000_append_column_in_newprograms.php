<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AppendColumnInNewprograms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('newprograms', function (Blueprint $table) {
            $table->string('type')
                ->nullable()
                ->default('model')
                ->comment('Can take values: model, educational, curriculum');
            $table->integer('parent_id')
                ->unsigned()
                ->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        Schema::table('newprograms', function (Blueprint $table) {
            $table->dropColumn('parent_id');
            $table->dropColumn('type');
        });
    }
}
