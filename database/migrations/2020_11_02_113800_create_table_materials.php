<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMaterials extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        # Периоды
        Schema::create('materials', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('url')->nullable();
            $table->string('status')->default('Editing');
            $table->boolean('public')->default(0);
            $table->string('key')->nullable();
            $table->longtext('content')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('material_team', function (Blueprint $table) {
            $table->integer('material_id')->unsigned();
            $table->foreign('material_id')->references('id')->on('materials')->onDelete('cascade');
            $table->bigInteger('team_id')->unsigned();
            $table->foreign('team_id')->references('id')->on('teams')->onDelete('cascade');

            $table->primary(['material_id', 'team_id']);
        });

        Schema::create('material_specific', function (Blueprint $table) {
            $table->integer('material_id')->unsigned();
            $table->foreign('material_id')->references('id')->on('materials')->onDelete('cascade');
            $table->integer('specific_id')->unsigned();
            $table->foreign('specific_id')->references('id')->on('specifics')->onDelete('cascade');

            $table->primary(['material_id', 'specific_id']);
        });

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_specific');
        Schema::dropIfExists('material_team');
        Schema::dropIfExists('materials');
    }
}
