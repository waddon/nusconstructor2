<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCprograms extends Migration
{
    const DB_CONNECTION     = 'mysql';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        # Авторы
        Schema::connection(self::DB_CONNECTION)->create('cauthors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->timestamps();
        });

        # Программы
        Schema::connection(self::DB_CONNECTION)->create('cprograms', function(Blueprint $table) {
            $table->increments('id');
            $table->text('name')->nullable();
            $table->longtext('meta')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        # Связь компонентов с языками обучения
        Schema::connection(self::DB_CONNECTION)->create('cprogram_languagetype', function (Blueprint $table) {
            $table->integer('cprogram_id')->unsigned();
            $table->foreign('cprogram_id')->references('id')->on('cprograms')->onDelete('cascade');
            $table->integer('languagetype_id')->unsigned();
            $table->foreign('languagetype_id')->references('id')->on('languagetypes')->onDelete('cascade');
            $table->primary(['cprogram_id', 'languagetype_id']);
        });

        # Связь программ с авторами
        Schema::connection(self::DB_CONNECTION)->create('cauthor_cprogram', function (Blueprint $table) {
            $table->integer('cauthor_id')->unsigned();
            $table->foreign('cauthor_id')->references('id')->on('cauthors')->onDelete('cascade');
            $table->integer('cprogram_id')->unsigned();
            $table->foreign('cprogram_id')->references('id')->on('cprograms')->onDelete('cascade');
            $table->primary(['cauthor_id', 'cprogram_id']);
        });

        # Связь программ с классами
        Schema::connection(self::DB_CONNECTION)->create('cprogram_grade', function (Blueprint $table) {
            $table->integer('cprogram_id')->unsigned();
            $table->foreign('cprogram_id')->references('id')->on('cprograms')->onDelete('cascade');
            $table->integer('grade_id')->unsigned();
            // $table->foreign('grade_id')->references('id')->on('grades')->onDelete('cascade');
            $table->primary(['cprogram_id', 'grade_id']);
        });

        # Связь программ с компонентами
        Schema::connection(self::DB_CONNECTION)->create('component_cprogram', function (Blueprint $table) {
            $table->integer('component_id')->unsigned();
            $table->foreign('component_id')->references('id')->on('components')->onDelete('cascade');
            $table->integer('cprogram_id')->unsigned();
            $table->foreign('cprogram_id')->references('id')->on('cprograms')->onDelete('cascade');
            $table->primary(['component_id', 'cprogram_id']);
        });

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(self::DB_CONNECTION)->dropIfExists('component_cprogram');
        Schema::connection(self::DB_CONNECTION)->dropIfExists('cprogram_grade');
        Schema::connection(self::DB_CONNECTION)->dropIfExists('cauthor_cprogram');
        Schema::connection(self::DB_CONNECTION)->dropIfExists('cprogram_languagetype');
        Schema::connection(self::DB_CONNECTION)->dropIfExists('cprograms');
        Schema::connection(self::DB_CONNECTION)->dropIfExists('cauthors');
    }
}
