<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLayouts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        # Периоды
        Schema::create('layouts', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->integer('cycle_id')->unsigned();
            $table->foreign('cycle_id')->references('id')->on('cycles')->onDelete('cascade');
            $table->integer('period_id')->unsigned();
            $table->foreign('period_id')->references('id')->on('periods')->onDelete('cascade');
            $table->string('status')->default('Editing');
            $table->boolean('public')->default(0);
            $table->string('key')->nullable();
            $table->longtext('meta')->nullable();
            $table->longtext('content')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('layout_team', function (Blueprint $table) {
            $table->integer('layout_id')->unsigned();
            $table->foreign('layout_id')->references('id')->on('layouts')->onDelete('cascade');
            $table->bigInteger('team_id')->unsigned();
            $table->foreign('team_id')->references('id')->on('teams')->onDelete('cascade');

            $table->longtext('meta')->nullable();
            $table->primary(['layout_id', 'team_id']);
        });

        Schema::create('layout_user', function (Blueprint $table) {
            $table->integer('layout_id')->unsigned();
            $table->foreign('layout_id')->references('id')->on('layouts')->onDelete('cascade');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('table_users')->onDelete('cascade');
            $table->primary(['layout_id', 'user_id']);
        });

        Schema::create('branch_layout', function (Blueprint $table) {
            $table->integer('branch_id')->unsigned();
            $table->foreign('branch_id')->references('id')->on('branches')->onDelete('cascade');
            $table->integer('layout_id')->unsigned();
            $table->foreign('layout_id')->references('id')->on('layouts')->onDelete('cascade');
            $table->primary(['branch_id', 'layout_id']);
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_layout');
        Schema::dropIfExists('layout_user');
        Schema::dropIfExists('layout_team');
        Schema::dropIfExists('layouts');
    }
}
