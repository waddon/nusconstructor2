<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLevelsTable extends Migration
{

    const DB_CONNECTION = 'mysql';
    // const DB_CONNECTION = 'new';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(self::DB_CONNECTION)->create('levels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->timestamps();
        });

        Schema::connection(self::DB_CONNECTION)->create('cycles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('marking')->nullable();
            $table->integer('level_id')->unsigned();
            $table->foreign('level_id')->references('id')->on('levels')->onDelete('cascade');
            $table->longtext('content')->nullable();
            $table->timestamps();
        });

        Schema::connection(self::DB_CONNECTION)->create('grades', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->integer('cycle_id')->unsigned();
            $table->foreign('cycle_id')->references('id')->on('cycles')->onDelete('cascade');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(self::DB_CONNECTION)->dropIfExists('grades');
        Schema::connection(self::DB_CONNECTION)->dropIfExists('cycles');
        Schema::connection(self::DB_CONNECTION)->dropIfExists('levels');
    }
}
