<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AppendColumnsInTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('posts', function (Blueprint $table) {
            $table->timestamp('published_at')->useCurrent();
        });
        Schema::table('branches', function (Blueprint $table) {
            $table->integer('order')->unsigned()->default(0);
        });
        Schema::table('skills', function (Blueprint $table) {
            $table->integer('order')->unsigned()->default(0);
        });
        Schema::table('lines', function (Blueprint $table) {
            $table->integer('order')->unsigned()->default(0);
        });
        Schema::table('expects', function (Blueprint $table) {
            $table->integer('order')->unsigned()->default(0);
        });
        Schema::table('specifics', function (Blueprint $table) {
            $table->integer('order')->unsigned()->default(0);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        Schema::table('branches', function (Blueprint $table) {
            $table->dropColumn('published_at');
        });
        Schema::table('branches', function (Blueprint $table) {
            $table->dropColumn('order');
        });
        Schema::table('skills', function (Blueprint $table) {
            $table->dropColumn('order');
        });
        Schema::table('lines', function (Blueprint $table) {
            $table->dropColumn('order');
        });
        Schema::table('expects', function (Blueprint $table) {
            $table->dropColumn('order');
        });
        Schema::table('specifics', function (Blueprint $table) {
            $table->dropColumn('order');
        });
    }
}
