<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTplans extends Migration
{
    const DB_CONNECTION     = 'mysql';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        # Учебные планы учебного заведения
        Schema::connection(self::DB_CONNECTION)->create('tplans', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name')->nullable();

            $table->integer('tload_id')->unsigned()->nullable();
            $table->foreign('tload_id')->references('id')->on('tloads')->onDelete('cascade');

            $table->longtext('meta')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(self::DB_CONNECTION)->dropIfExists('tplans');
    }
}
