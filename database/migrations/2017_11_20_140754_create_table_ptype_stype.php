<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePtypeStype extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ptypes', function (Blueprint $table) {
            $table->bigIncrements('id_ptype');
            $table->string('name_ptype')->default('');
        });
        Schema::create('stypes', function (Blueprint $table) {
            $table->bigIncrements('id_stype');
            $table->string('name_stype')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ptypes');
        Schema::dropIfExists('stypes');
    }
}
