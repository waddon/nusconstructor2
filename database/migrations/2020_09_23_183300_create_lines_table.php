<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinesTable extends Migration
{

    const DB_CONNECTION = 'mysql';
    // const DB_CONNECTION = 'new';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(self::DB_CONNECTION)->create('lines', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('marking')->nullable();
            $table->integer('branch_id')->unsigned();
            $table->foreign('branch_id')->references('id')->on('branches')->onDelete('cascade');            
            // $table->integer('cycle_id')->unsigned();
            // $table->foreign('cycle_id')->references('id')->on('cycles')->onDelete('cascade');            
            $table->longtext('content')->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(self::DB_CONNECTION)->dropIfExists('lines');
    }
}
