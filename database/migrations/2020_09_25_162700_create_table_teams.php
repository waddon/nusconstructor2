<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTeams extends Migration
{

    const DB_CONNECTION = 'mysql';
    // const DB_CONNECTION = 'new';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        # Типы команд
        Schema::connection(self::DB_CONNECTION)->create('ttypes', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->boolean('institution')->default(0);
            $table->timestamps();
        });

        # Роли участника команды
        Schema::connection(self::DB_CONNECTION)->create('teamroles', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->timestamps();
        });

        # Команды
        Schema::connection(self::DB_CONNECTION)->create('teams', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->integer('ttype_id')->unsigned();
            $table->longtext('meta')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        # Связь между командами и пользователями
        Schema::connection(self::DB_CONNECTION)->create('team_user', function (Blueprint $table) {
            $table->bigInteger('team_id')->unsigned();
            $table->foreign('team_id')->references('id')->on('teams')->onDelete('cascade');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('table_users')->onDelete('cascade');
            $table->integer('teamrole_id')->unsigned();
            $table->longtext('meta')->nullable();
            $table->primary(['team_id', 'user_id']);
        });

        # Создание связи между разными соединениями
        // Schema::table('new_team_user', function (Blueprint $table) {
        //     $table->foreign('user_id')->references('id')->on('table_users')->onDelete('cascade');
        // });

        # Связь между командами и школами
        Schema::connection(self::DB_CONNECTION)->create('institution_team', function (Blueprint $table) {
            $table->bigInteger('institution_id')->unsigned();
            $table->foreign('institution_id')->references('id')->on('institutions')->onDelete('cascade');
            $table->bigInteger('team_id')->unsigned();
            $table->foreign('team_id')->references('id')->on('teams')->onDelete('cascade');
            $table->primary(['institution_id', 'team_id']);
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(self::DB_CONNECTION)->dropIfExists('institution_team');
        Schema::connection(self::DB_CONNECTION)->dropIfExists('team_user');
        Schema::connection(self::DB_CONNECTION)->dropIfExists('teams');
        Schema::connection(self::DB_CONNECTION)->dropIfExists('teamroles');
        Schema::connection(self::DB_CONNECTION)->dropIfExists('ttypes');
    }
}
