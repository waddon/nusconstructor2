<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableYplans extends Migration
{
    const DB_CONNECTION     = 'mysql';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        # Учебные планы учебного заведения
        Schema::connection(self::DB_CONNECTION)->create('yplans', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name')->nullable();

            $table->integer('tplan_id')->unsigned()->nullable();
            $table->foreign('tplan_id')->references('id')->on('tplans')->onDelete('cascade');

            $table->integer('grade_id')->unsigned()->nullable();

            $table->longtext('meta')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(self::DB_CONNECTION)->dropIfExists('yplans');
    }
}
