<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTloads extends Migration
{
    const DB_CONNECTION     = 'mysql';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        # Нагрузка учебного заведения
        Schema::connection(self::DB_CONNECTION)->create('tloads', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name')->nullable();
            // $table->bigInteger('team_id')->unsigned();
            // $table->foreign('team_id')->references('id')->on('teams')->onDelete('cascade');

            $table->integer('loadable_id')->nullable();
            $table->string('loadable_type')->nullable();

            $table->integer('cperiod_id')->unsigned()->nullable();
            $table->foreign('cperiod_id')->references('id')->on('cperiods')->onDelete('set null');

            $table->integer('loadtemplate_id')->unsigned()->nullable();
            $table->foreign('loadtemplate_id')->references('id')->on('loadtemplates')->onDelete('set null');

            $table->integer('cycle_id')->unsigned()->nullable();
            $table->foreign('cycle_id')->references('id')->on('cycles')->onDelete('cascade');

            $table->longtext('meta')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(self::DB_CONNECTION)->dropIfExists('tloads');
    }
}
