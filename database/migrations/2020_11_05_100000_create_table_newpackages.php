<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNewpackages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        # Периоды
        Schema::create('newpackages', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->integer('newplan_id')->unsigned();
            $table->foreign('newplan_id')->references('id')->on('newplans')->onDelete('cascade');
            $table->string('status')->default('Editing');
            $table->boolean('public')->default(0);
            $table->string('key')->nullable();
            $table->longtext('meta')->nullable();
            $table->longtext('content')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('newpackage_team', function (Blueprint $table) {
            $table->integer('newpackage_id')->unsigned();
            $table->foreign('newpackage_id')->references('id')->on('newpackages')->onDelete('cascade');
            $table->bigInteger('team_id')->unsigned();
            $table->foreign('team_id')->references('id')->on('teams')->onDelete('cascade');

            $table->primary(['newpackage_id', 'team_id']);
        });

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('newpackage_team');
        Schema::dropIfExists('newpackages');
    }
}
