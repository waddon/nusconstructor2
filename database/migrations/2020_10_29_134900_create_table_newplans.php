<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNewplans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        # Периоды
        Schema::create('newplans', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->integer('cycle_id')->unsigned();
            $table->foreign('cycle_id')->references('id')->on('cycles')->onDelete('cascade');
            $table->string('status')->default('Editing');
            $table->boolean('public')->default(0);
            $table->string('key')->nullable();
            $table->longtext('meta')->nullable();
            $table->longtext('content')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('newplan_team', function (Blueprint $table) {
            $table->integer('newplan_id')->unsigned();
            $table->foreign('newplan_id')->references('id')->on('newplans')->onDelete('cascade');
            $table->bigInteger('team_id')->unsigned();
            $table->foreign('team_id')->references('id')->on('teams')->onDelete('cascade');

            $table->primary(['newplan_id', 'team_id']);
        });

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('newplan_team');
        Schema::dropIfExists('newplans');
    }
}
