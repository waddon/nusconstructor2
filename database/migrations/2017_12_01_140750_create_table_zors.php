<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableZors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zors', function (Blueprint $table) {
            $table->bigIncrements('id_zor');
            $table->bigInteger('id_cikl')->default('1');
            $table->bigInteger('id_galuz')->default('1');
            $table->string('key_zor')->default('');
            $table->text('name_zor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zors');
    }
}
