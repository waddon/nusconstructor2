<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePlaning extends Migration
{
    const DB_CONNECTION     = 'mysql';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        # Периоды планирования
        Schema::connection(self::DB_CONNECTION)->create('cperiods', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->timestamps();
        });

        # Шаблоны нагрузок
        Schema::connection(self::DB_CONNECTION)->create('loadtemplates', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name')->nullable();
            $table->integer('languagetype_id')->unsigned()->nullable();
            $table->foreign('languagetype_id')->references('id')->on('languagetypes')->onDelete('set null');
            $table->longtext('meta')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        # Связь шаблонов нагрузок с периодами планирования
        Schema::connection(self::DB_CONNECTION)->create('cperiod_loadtemplate', function (Blueprint $table) {
            $table->integer('cperiod_id')->unsigned();
            $table->foreign('cperiod_id')->references('id')->on('cperiods')->onDelete('cascade');
            $table->integer('loadtemplate_id')->unsigned();
            $table->foreign('loadtemplate_id')->references('id')->on('loadtemplates')->onDelete('cascade');
            $table->primary(['cperiod_id', 'loadtemplate_id']);
        });

        # Связь шаблонов нагрузок с циклами
        Schema::connection(self::DB_CONNECTION)->create('cycle_loadtemplate', function (Blueprint $table) {
            $table->integer('cycle_id')->unsigned();
            $table->foreign('cycle_id')->references('id')->on('cycles')->onDelete('cascade');
            $table->integer('loadtemplate_id')->unsigned();
            $table->foreign('loadtemplate_id')->references('id')->on('loadtemplates')->onDelete('cascade');
            $table->primary(['cycle_id', 'loadtemplate_id']);
        });

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(self::DB_CONNECTION)->dropIfExists('cycle_loadtemplate');
        Schema::connection(self::DB_CONNECTION)->dropIfExists('cperiod_loadtemplate');
        Schema::connection(self::DB_CONNECTION)->dropIfExists('loadtemplates');
        Schema::connection(self::DB_CONNECTION)->dropIfExists('cperiods');
    }
}
