<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUmenies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('umenies', function (Blueprint $table) {
            $table->bigIncrements('id_umenie');
            $table->bigInteger('id_galuz')->default('1');
            $table->string('key_zor')->default('');
            $table->string('name_umenie',255)->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('umenies');
    }
}
