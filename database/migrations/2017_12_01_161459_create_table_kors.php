<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kors', function (Blueprint $table) {
            $table->bigIncrements('id_kor');
            $table->bigInteger('id_zor')->default('1');
            $table->bigInteger('id_zmist')->default('1');
            $table->string('key_kor')->default('');
            $table->text('name_kor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kors');
    }
}
