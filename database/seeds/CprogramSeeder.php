<?php

use Illuminate\Database\Seeder;
use App\Models\Cprogram;

class CprogramSeeder extends Seeder
{
    const DB_CONNECTION     = 'mysql';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection(self::DB_CONNECTION)->statement('SET FOREIGN_KEY_CHECKS=0;');
        Cprogram::truncate();
        DB::connection(self::DB_CONNECTION)->statement('SET FOREIGN_KEY_CHECKS=1;');

        # 1 Мовно-літературна
            $model = Cprogram::create([
                'name' => '"Українська  мова. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1ngp-IBriUmeHFzzN-7QxjTU14_c9BRE0/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1]);
            $model->components()->sync([1]);
            $model->cauthors()->sync([1,2]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Українська  мова. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1lzseYy2T0bg6zWrRD6iGppVDB58jXU1K/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1]);
            $model->components()->sync([1]);
            $model->cauthors()->sync([3,4,5,6,7]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Українська мова для класів з навчанням угорською мовою. 5-9 класи" для закладів загальної сеедньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1Sp8CoXN-oFmy072ofhNugmRc1_dxFnbF/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([2]);
            $model->components()->sync([1]);
            $model->cauthors()->sync([8,9,10,11]);
            $model->grades()->sync([5,6,7,8,9]);


            $model = Cprogram::create([
                'name' => '"Українська мова для класів з навчанням угорською мовою. 5-9 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1Sp8CoXN-oFmy072ofhNugmRc1_dxFnbF/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([2]);
            $model->components()->sync([1]);
            $model->cauthors()->sync([12,13,14,15,16,17,18,19,20,21,22,23,24]);
            $model->grades()->sync([5,6,7,8,9]);


            $model = Cprogram::create([
                'name' => '"Українська мова для класів з навчанням молдовською мовою. 5-9 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/15lUGdMBDupLDkLta3G4vckuRd6Iau5Hs/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([2]);
            $model->components()->sync([1]);
            $model->cauthors()->sync([25,26,27,28]);
            $model->grades()->sync([5,6,7,8,9]);


            $model = Cprogram::create([
                'name' => '"Українська мова для класів з навчанням румунською мовою. 5-9 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1rGgMbB639L2EwDuEIlBySxBoG6UXvEOn/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([2]);
            $model->components()->sync([1]);
            $model->cauthors()->sync([29,30,31,32,33]);
            $model->grades()->sync([5,6,7,8,9]);


            $model = Cprogram::create([
                'name' => '"Українська література. 5-6 класи"  для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1mZUOTHruXmNJXck7fmtL5bPReQvXwgFR/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1]);
            $model->components()->sync([2]);
            $model->cauthors()->sync([34,35,36]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Українська література. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1MRfi5sRENH_yVuxtZ_Lh0IyYvHPKekP9/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1]);
            $model->components()->sync([2]);
            $model->cauthors()->sync([37,38]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Українська література. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1lq10rWXaEqTuQXQSE3LWzZyT_rHV7qhj/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1]);
            $model->components()->sync([2]);
            $model->cauthors()->sync([39,40,41,42,43,44,45,46,47]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Зарубіжна література. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1VcmR4LmueemuTyXWO2wcaGO22ME-lIEh/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1]);
            $model->components()->sync([3]);
            $model->cauthors()->sync([48,49,50,51,52,53,54,55,56,57]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Зарубіжна література. 5-9 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1Mkx9cjfzSArRZXd1V56NvhoooTXsX0UP/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1]);
            $model->components()->sync([3]);
            $model->cauthors()->sync([48,49,50,51,52,53,54,55,56,57]);
            $model->grades()->sync([5,6,7,8,9]);


            $model = Cprogram::create([
                'name' => '"Зарубіжна література. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1Y9CLRfBt7i2SA6Mz5WhcTBWsXx4gH9KI/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1]);
            $model->components()->sync([3]);
            $model->cauthors()->sync([58]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Зарубіжна література. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1Cz1MGthFJsmBF_5Ol0UQ3UCBQIDXsih3/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1]);
            $model->components()->sync([3]);
            $model->cauthors()->sync([59,60,61]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Інтегрований курс літератур (української та зарубіжної). 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1RvBILkNJkXvlQyA2myBueofXvRL9uEsH/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1]);
            $model->components()->sync([32]);
            $model->cauthors()->sync([39,47]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Інтегрований мовно-літературний курс (українська мова, українська та зарубіжні літератури). 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1VrCTNOJ1mrqZ7H17DY7FPnPqfyv5olti/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1]);
            $model->components()->sync([31]);
            $model->cauthors()->sync([62,63,64,65,66,67,68,69]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Болгарська мова. 5-9 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/13JGTjFxlMy9FAw13iEEHodT7VZiaA1Mp/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([2]);
            $model->components()->sync([48]);
            $model->cauthors()->sync([70,71,72]);
            $model->grades()->sync([5,6,7,8,9]);


            $model = Cprogram::create([
                'name' => '"Гагаузька мова. 5-9 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1kyHUM1__UX5ua3d0u8bSfGRuUA-X22S0/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([2]);
            $model->components()->sync([48]);
            $model->cauthors()->sync([70,73]);
            $model->grades()->sync([5,6,7,8,9]);


            $model = Cprogram::create([
                'name' => '"Мова іврит. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1kjsllXNeWmLBxn69TqsaU11LFuJHz6QC/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([2]);
            $model->components()->sync([48]);
            $model->cauthors()->sync([74,75]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Польська мова. 5-9 класи" (з навчанням польською мовою) для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1SPS9Qy3IA9CkGcgT5iXOP9pZolyv0z5J/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([2]);
            $model->components()->sync([48]);
            $model->cauthors()->sync([76]);
            $model->grades()->sync([5,6,7,8,9]);


            $model = Cprogram::create([
                'name' => '"Польська мова. 5-9 класи (початок вивчення з 5-го класу)" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1qti2kkdbNtcPYIWzRSffMNyvgeaR_a5E/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([2]);
            $model->components()->sync([48]);
            $model->cauthors()->sync([77,78,79]);
            $model->grades()->sync([5,6,7,8,9]);


            $model = Cprogram::create([
                'name' => '"Польська мова. 5-9 класи (початок вивчення з 1-го класу)" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1JtPs7B-iQqQ-M8PgMjW0AIjwo74-M_T7/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([2]);
            $model->components()->sync([48]);
            $model->cauthors()->sync([80,81]);
            $model->grades()->sync([5,6,7,8,9]);


            $model = Cprogram::create([
                'name' => '"Російська мова. 5-9 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1BIIHpEbk6KA6r1_mtz3JFQ25JAdXBMhk/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([2]);
            $model->components()->sync([48]);
            $model->cauthors()->sync([82,83]);
            $model->grades()->sync([5,6,7,8,9]);


            $model = Cprogram::create([
                'name' => '"Румунська мова. 5-9 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1h5DaztGCONn15TcMeeQd4kuTN_BhFJPQ/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([2]);
            $model->components()->sync([48]);
            $model->cauthors()->sync([84,85,86,87]);
            $model->grades()->sync([5,6,7,8,9]);


            $model = Cprogram::create([
                'name' => '"Молдовська мова. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/15APFeNQwe1-HZpJKvuiQ6ySP9LGYyOws/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([2]);
            $model->components()->sync([48]);
            $model->cauthors()->sync([88,89,90]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Новогрецька мова. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1pqJRMvkUnjJ9N_MgFVrVgl19_gvXYI20/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([2]);
            $model->components()->sync([48]);
            $model->cauthors()->sync([91,92,93,94,95,96,97,98,99]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Словацька мова. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1XeUAF3nlZpMdiU_LLYbGc8z14TGrmpa2/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([2]);
            $model->components()->sync([48]);
            $model->cauthors()->sync([100,101]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Угорська мова. 5-9 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1k0P7XxqQaiovom3A1uzKXBABgjambjCE/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([2]);
            $model->components()->sync([48]);
            $model->cauthors()->sync([102,103,104,105,106,107]);
            $model->grades()->sync([5,6,7,8,9]);


            $model = Cprogram::create([
                'name' => '"Інтегрований курс кримськотатарської мови та літератури. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1Om7i5eyHzDSTN4BDrd6o0JjLS9a8hwna/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([2]);
            $model->components()->sync([52]);
            $model->cauthors()->sync([108,109]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Інтегрований курс молдовської та зарубіжної літератури. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1VqO6BCu2ZaoqJCxoavkYjsZJkax2fKoy/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([2]);
            $model->components()->sync([51]);
            $model->cauthors()->sync([88,89,90]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Інтегрований курс угорської та зарубіжної літератури. 5-9 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/17C-JqpR7UOg8JUO2Ep2RYouxzuM6d6i3/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([2]);
            $model->components()->sync([51]);
            $model->cauthors()->sync([102,103,104,110,105,107]);
            $model->grades()->sync([5,6,7,8,9]);


            $model = Cprogram::create([
                'name' => '"Інтегрований курс польської та зарубіжної літератури. 5-9 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/13YNy7lxnHS72xiGJDkJCMjaeTntXo8zL/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([2]);
            $model->components()->sync([51]);
            $model->cauthors()->sync([111]);
            $model->grades()->sync([5,6,7,8,9]);


            $model = Cprogram::create([
                'name' => '"Інтегрований курс румунської та зарубіжної літератури. 5-9 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1WxzundviodixTpChrBnXvjw7Rj7L6N3d/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([2]);
            $model->components()->sync([51]);
            $model->cauthors()->sync([84,112,113,114]);
            $model->grades()->sync([5,6,7,8,9]);


            $model = Cprogram::create([
                'name' => '"Іноземна мова. 5-9 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1QtBRLiTcnKDCcA5JDu0CBL8crUo0DbNv/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([4]);
            $model->cauthors()->sync([115,116,117,118,119]);
            $model->grades()->sync([5,6,7,8,9]);


            $model = Cprogram::create([
                'name' => '"Іноземна мова. 5-9 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1Lvr1Juvpo3CMswCPPtWQozxXDQpq_yH-/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([4]);
            $model->cauthors()->sync([120,121,122,123,124,125,126,93,94]);
            $model->grades()->sync([5,6,7,8,9]);


            $model = Cprogram::create([
                'name' => '"Друга іноземна мова. 5-9 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/197vT9KTM6SRSci04yqZY544t8LnIPnUI/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([5]);
            $model->cauthors()->sync([120,121,122,123,124,125,126,93,94,77,127,102]);
            $model->grades()->sync([5,6,7,8,9]);


        # Громадянська та історична


            $model = Cprogram::create([
                'name' => '"Вступ до історії України та громадянської освіти. 5 клас" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1QMarnWls-cXLwIhg0aAAL0OumCSCQxnz/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([20]);
            $model->cauthors()->sync([128,129,130,131,132,133]);
            $model->grades()->sync([5]);


            $model = Cprogram::create([
                'name' => '"Вступ до історії України та громадянської освіти. 5 клас" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1SOeCotX_2hZUeCA4fbc6QJ2IQqRrGC6N/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([20]);
            $model->cauthors()->sync([134,135]);
            $model->grades()->sync([5]);


            $model = Cprogram::create([
                'name' => '"Вступ до історії України та громадянської освіти. 5 клас" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1caWcH9a4b0su3NTlnkxGwF6hHOwYdtoe/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([20]);
            $model->cauthors()->sync([136,130,137,133]);
            $model->grades()->sync([5]);


            $model = Cprogram::create([
                'name' => '"Вступ до історії України та громадянської освіти. 5 клас" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1-fWUn49_3f4eeqNjXJMTd1T2vPT8EkEL/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([20]);
            $model->cauthors()->sync([130,138]);
            $model->grades()->sync([5]);


            $model = Cprogram::create([
                'name' => '"Досліджуємо історію і суспільство. 5-6 класи (інтегрований курс) для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/10rxKQ9U_OmKP8JFQhpK3a1xbjA5u5qnG/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([38]);
            $model->cauthors()->sync([139,140,141]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Досліджуємо історію і суспільство. 5-6 класи (інтегрований курс) для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1CY5z3nE00cge1lXTWu9WkgQxy5QO7EDL/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([38]);
            $model->cauthors()->sync([142,143,144,145]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Україна і світ: вступ до історії та громадянської освіти. 5-6 класи (інтегрований курс)" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1izXMaY0mSl6UNyJZsYiGqZYvxm2wCk1v/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([39]);
            $model->cauthors()->sync([146,147,148,149,150]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Україна і світ: вступ до історії та громадянської освіти. 5-6 класи (інтегрований курс)" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1muymLosSummbNqMjk_tsM8pkFNutLF_s/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([39]);
            $model->cauthors()->sync([129,130,133,151,152]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Історія України. Всесвітня історія . 6 клас" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1rBG2jQY2v1mX0uTsJb4p8JD1F2S60lQn/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([21]);
            $model->cauthors()->sync([132,128,131,137,133]);
            $model->grades()->sync([5,6]);


        # Соціальна та здоров'язбережувальна


            $model = Cprogram::create([
                'name' => '"Етика. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1fi7TlsStZEcyT4-_rIuKANICafs4XyGE/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([16,17,18]);
            $model->cauthors()->sync([153,136,130,154,137,133]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Етика. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1rp6JE27MSfGIoJMg5gsDrb_5Ycnn-BPg/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([16,17,18]);
            $model->cauthors()->sync([142,143,155]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Вчимося жити разом. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1-uh_rv0ODRd8NI4I0yFd_qfkprziO26N/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([16,17,18]);
            $model->cauthors()->sync([156,157,158,159]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Культура добросусідства. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1-vSAd7D9KV3o7Dzy3oM3guPZ8tHrzJ-g/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([16,17,18]);
            $model->cauthors()->sync([160,147,161,162,118]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Здоров\'я, безпека та добробут. 5-6 класи (інтегрований курс)" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1mqsfWrSW1WW1qFNQqeq3xAXkearNQBHF/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([37]);
            $model->cauthors()->sync([156,157,158,159]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Здоров\'я, безпека та добробут. 5-6 класи (інтегрований курс)" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/17MwEgus-Ahsa_nbw9OaDIoZrM7wYHGZS/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([37]);
            $model->cauthors()->sync([163,164,69,165,166,167,168,169,170]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Здоров\'я, безпека та добробут. 5-6 класи (інтегрований курс)" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1kz6LoONostpqPjoNUSVW9yBV5mmGDGHA/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([37]);
            $model->cauthors()->sync([171,172]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Здоров\'я, безпека та добробут. 5-6 класи (інтегрований курс)" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1MZUNFq2p2SXB5TP_nLE8HeB86bj1n5s8/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([37]);
            $model->cauthors()->sync([173,174,175]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Здоров\'я, безпека та добробут. 5-6 класи (інтегрований курс)" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1sfs87ZCcZWTJOu63QznSvAX1LqS26aVv/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([37]);
            $model->cauthors()->sync([176,177]);
            $model->grades()->sync([5,6]);


        # Фізична культура


            $model = Cprogram::create([
                'name' => '"Фізична культура. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => '"Фізична культура. 5-6 класи" для закладів загальної середньої освіти',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([30]);
            $model->cauthors()->sync([178,179,180]);
            $model->grades()->sync([5,6]);


        # Природнича


            $model = Cprogram::create([
                'name' => '"Пізнаємо природу. 5-6 класи (інтегрований курс)" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1gkUtn5LuHCaxHrZm-5x-8ASCI_DXfPmf/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([9]);
            $model->cauthors()->sync([181]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Пізнаємо природу. 5-6 класи (інтегрований курс)" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1ZyHn0xenL-Samd4G4nsw2cyFr488aHZU/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([9]);
            $model->cauthors()->sync([182,183,184]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Пізнаємо природу. 5-6 класи (інтегрований курс)" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/16E0INMV6rPP5V11WXdR5hZixUgozH_lo/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([9]);
            $model->cauthors()->sync([185,186,187,188]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Природничі науки. 5-6 класи (інтегрований курс)" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1pJq_wshmZ95_nInpm8sUPXPOjxROdg_t/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([35]);
            $model->cauthors()->sync([189,190,191,192]);
            $model->grades()->sync([5,6]);


        # Математична


            $model = Cprogram::create([
                'name' => '"Математика. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1-_5YSGA120JWNL-4qJdQhliltEam5j7h/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([6]);
            $model->cauthors()->sync([193,194]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Математика. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1YMPwWKLNmdHTQ6wj4_5aUH0sPafkCBqX/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([6]);
            $model->cauthors()->sync([195,196,197,198,199]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Математика. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/174eWhQpn_qib08MSK_0GGucbM5AHZOHE/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([6]);
            $model->cauthors()->sync([200,201,202,203,204,205]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Математика. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1W8TXKiWm7gVS3xyLqQhX97yU9zGmrXXc/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([6]);
            $model->cauthors()->sync([206]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Математика. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1L9uwoxLYLij1-vN66n0bQ1NR702c4N37/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([6]);
            $model->cauthors()->sync([207,208,209,210]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Математика. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1ykOgcS2OiQbBxXAfxFoW-SxykuwZMlFm/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([6]);
            $model->cauthors()->sync([211,212]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Математика. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1jShpd5C5yxobtQwDINr9rXZtZreGyU7B/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([6]);
            $model->cauthors()->sync([213,214]);
            $model->grades()->sync([5,6]);


        # Інформатична


            $model = Cprogram::create([
                'name' => '"Інформатика. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/11eaTWGqRcI5SxsO35VFrTV3ipNaUu5X6/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([26]);
            $model->cauthors()->sync([215,216]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Інформатика. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1tmA53rTR7bsQLtduONUKceE18wkLFzRZ/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([26]);
            $model->cauthors()->sync([217,218,219,220]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Інформатика. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1ZKZUrVH6lGjvpKq_tJglAIDU-vcdETLY/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([26]);
            $model->cauthors()->sync([219,221]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Інформатика. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1Y1xKl0ZD2yrJO2bug5X85GZL80MTJfLx/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([26]);
            $model->cauthors()->sync([222,223,224]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Інформатика. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/11_xkaD5J_ozoPfFHKiGD2cvu5aMDrLqg/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([26]);
            $model->cauthors()->sync([213,225]);
            $model->grades()->sync([5,6]);


        # Технологічна


            $model = Cprogram::create([
                'name' => '"Технології. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1gpXUwsa1rQ0Zll1v65oXabHRqZLlk5G-/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([27]);
            $model->cauthors()->sync([226,227,228,229,230]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Технології. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1_n416iQ-TGvaEXA8Htpp-pY8BDcU5QAS/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([27]);
            $model->cauthors()->sync([231]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Технології. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1wIkKmEJMzVW_9Bv4Sgf1YJFTmSpJaOM7/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([27]);
            $model->cauthors()->sync([232,233,234,235]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Технології. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/13sqciH4Wt2ChYGn41rBpL24-W0akkYcV/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([27]);
            $model->cauthors()->sync([236,237,238,239]);
            $model->grades()->sync([5,6]);


        # Мистецька


            $model = Cprogram::create([
                'name' => '"Мистецтво. 5-6 класи (інтегрований курс)" для заклаів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/11rDG0XrahXJjmh2TFVu1agv6Ko0Wex17/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([41]);
            $model->cauthors()->sync([240,241]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Мистецтво. 5-6 класи (інтегрований курс)" для заклаів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1sQMkamUN1fNWNR3TPp5mPjAWFooeBjK_/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([41]);
            $model->cauthors()->sync([242]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Мистецтво. 5-6 класи (інтегрований курс)" для заклаів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1MuHajU0pMiPnv7lNWRvu8rrXoMYz6D7H/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([41]);
            $model->cauthors()->sync([243,244]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Мистецтво. 5-6 класи (інтегрований курс)" для заклаів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1Prhqk11XDJri4ZtXANe2Za59HUoyMhFo/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([41]);
            $model->cauthors()->sync([245,240,246,241,247,248,249,250]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Музичне мистецтво. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1J2LquI78MnybEAKfGwqOo6WqvjUZV5P_/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([29]);
            $model->cauthors()->sync([251,252,253,254,255,256]);
            $model->grades()->sync([5,6]);


        # Міжгалузеві курси

            $model = Cprogram::create([
                'name' => '"Драматургія і театр. 5-6 класи (міжгалузевий інтегрований курс)" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1B32UKcsnR2u8wdp_jUcRdrkN9z5185Ev/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([43]);
            $model->cauthors()->sync([62,249,245]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"Робототехніка. 5-6 класи" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/1bJkI1tn8Z5VHIQDi758Bazyg6HLVS8g_/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([42]);
            $model->cauthors()->sync([257,258]);
            $model->grades()->sync([5,6]);


            $model = Cprogram::create([
                'name' => '"STEM. 5-6 класи (міжгалузевий інтегрований курс)" для закладів загальної середньої освіти',
                'meta' => (object)[
                    'url' => 'https://drive.google.com/file/d/11on1K-_ALTZf2wTRXaM3exqGBpWnl-PC/view?usp=sharing',
                ],
            ]);
            $model->languagetypes()->sync([1,2]);
            $model->components()->sync([46]);
            $model->cauthors()->sync([259,260]);
            $model->grades()->sync([5,6]);

    }
}
