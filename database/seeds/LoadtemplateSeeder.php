<?php

use Illuminate\Database\Seeder;
use App\Models\Loadtemplate;

class LoadtemplateSeeder extends Seeder
{
    const DB_CONNECTION     = 'mysql';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection(self::DB_CONNECTION)->statement('SET FOREIGN_KEY_CHECKS=0;');
        Loadtemplate::truncate();
        DB::connection(self::DB_CONNECTION)->statement('SET FOREIGN_KEY_CHECKS=1;');

        $component = Loadtemplate::create([
            'name' => 'Загальний обсяг навчального навантаження для закладів із навчанням українською мовою',
            'languagetype_id' => 1,
            'meta' => (object)[
                'loads' => [
                    '5' => [
                        '1' => [
                            'recommended' => '11',
                            'minimum' => '10',
                            'maximum' => '13',
                        ],
                        '2' => [
                            'recommended' => '5',
                            'minimum' => '4',
                            'maximum' => '6',
                        ],
                        '3' => [
                            'recommended' => '2',
                            'minimum' => '1.5',
                            'maximum' => '3',
                        ],
                        '4' => [
                            'recommended' => '1.5',
                            'minimum' => '1',
                            'maximum' => '3',
                        ],
                        '5' => [
                            'recommended' => '1',
                            'minimum' => '1',
                            'maximum' => '2',
                        ],
                        '6' => [
                            'recommended' => '2',
                            'minimum' => '1',
                            'maximum' => '3',
                        ],
                        '7' => [
                            'recommended' => '1.5',
                            'minimum' => '1',
                            'maximum' => '2',
                        ],
                        '8' => [
                            'recommended' => '2',
                            'minimum' => '1',
                            'maximum' => '3',
                        ],
                        '9' => [
                            'recommended' => '3',
                            'minimum' => '3',
                            'maximum' => '3',
                        ],
                    ],
                    '6' => [
                        '1' => [
                            'recommended' => '11',
                            'minimum' => '10',
                            'maximum' => '13',
                        ],
                        '2' => [
                            'recommended' => '5',
                            'minimum' => '4',
                            'maximum' => '6',
                        ],
                        '3' => [
                            'recommended' => '4',
                            'minimum' => '2',
                            'maximum' => '5',
                        ],
                        '4' => [
                            'recommended' => '1.5',
                            'minimum' => '1',
                            'maximum' => '3',
                        ],
                        '5' => [
                            'recommended' => '2',
                            'minimum' => '1.5',
                            'maximum' => '3',
                        ],
                        '6' => [
                            'recommended' => '2',
                            'minimum' => '1',
                            'maximum' => '3',
                        ],
                        '7' => [
                            'recommended' => '1.5',
                            'minimum' => '1',
                            'maximum' => '2',
                        ],
                        '8' => [
                            'recommended' => '2',
                            'minimum' => '1',
                            'maximum' => '3',
                        ],
                        '9' => [
                            'recommended' => '3',
                            'minimum' => '3',
                            'maximum' => '3',
                        ],
                    ],
                    '7' => [
                        '1' => [
                            'recommended' => '10',
                            'minimum' => '9',
                            'maximum' => '12',
                        ],
                        '2' => [
                            'recommended' => '5',
                            'minimum' => '4',
                            'maximum' => '6',
                        ],
                        '3' => [
                            'recommended' => '7.5',
                            'minimum' => '7',
                            'maximum' => '9',
                        ],
                        '4' => [
                            'recommended' => '1.5',
                            'minimum' => '1',
                            'maximum' => '3',
                        ],
                        '5' => [
                            'recommended' => '2',
                            'minimum' => '1.5',
                            'maximum' => '3',
                        ],
                        '6' => [
                            'recommended' => '1',
                            'minimum' => '1',
                            'maximum' => '2',
                        ],
                        '7' => [
                            'recommended' => '2',
                            'minimum' => '1',
                            'maximum' => '2',
                        ],
                        '8' => [
                            'recommended' => '2',
                            'minimum' => '1',
                            'maximum' => '3',
                        ],
                        '9' => [
                            'recommended' => '3',
                            'minimum' => '3',
                            'maximum' => '3',
                        ],
                    ],
                    '8' => [
                        '1' => [
                            'recommended' => '10',
                            'minimum' => '8',
                            'maximum' => '12',
                        ],
                        '2' => [
                            'recommended' => '5',
                            'minimum' => '4',
                            'maximum' => '7',
                        ],
                        '3' => [
                            'recommended' => '8.5',
                            'minimum' => '8',
                            'maximum' => '10',
                        ],
                        '4' => [
                            'recommended' => '1.5',
                            'minimum' => '1',
                            'maximum' => '3',
                        ],
                        '5' => [
                            'recommended' => '3',
                            'minimum' => '2',
                            'maximum' => '3',
                        ],
                        '6' => [
                            'recommended' => '1',
                            'minimum' => '1',
                            'maximum' => '2',
                        ],
                        '7' => [
                            'recommended' => '2',
                            'minimum' => '1.5',
                            'maximum' => '3',
                        ],
                        '8' => [
                            'recommended' => '1',
                            'minimum' => '1',
                            'maximum' => '2',
                        ],
                        '9' => [
                            'recommended' => '3',
                            'minimum' => '3',
                            'maximum' => '3',
                        ],
                    ],
                    '9' => [
                        '1' => [
                            'recommended' => '10',
                            'minimum' => '8',
                            'maximum' => '12',
                        ],
                        '2' => [
                            'recommended' => '5',
                            'minimum' => '4',
                            'maximum' => '7',
                        ],
                        '3' => [
                            'recommended' => '9.5',
                            'minimum' => '8',
                            'maximum' => '11',
                        ],
                        '4' => [
                            'recommended' => '1.5',
                            'minimum' => '1',
                            'maximum' => '3',
                        ],
                        '5' => [
                            'recommended' => '3',
                            'minimum' => '2',
                            'maximum' => '3',
                        ],
                        '6' => [
                            'recommended' => '1',
                            'minimum' => '1',
                            'maximum' => '2',
                        ],
                        '7' => [
                            'recommended' => '1.5',
                            'minimum' => '1.5',
                            'maximum' => '3',
                        ],
                        '8' => [
                            'recommended' => '1',
                            'minimum' => '1',
                            'maximum' => '2',
                        ],
                        '9' => [
                            'recommended' => '3',
                            'minimum' => '3',
                            'maximum' => '3',
                        ],
                    ],
                ],
                'additional' => [
                    '5' => '2',
                    '6' => '2',
                    '7' => '1',
                    '8' => '1',
                    '9' => '0.5',
                ],
                'financed' => [
                    '5' => '31',
                    '6' => '34',
                    '7' => '35',
                    '8' => '36',
                    '9' => '36',
                ],
                'admissible' => [
                    '5' => '28',
                    '6' => '31',
                    '7' => '32',
                    '8' => '33',
                    '9' => '33',
                ],
            ]
        ]);
        $component->cperiods()->sync([1]);
        $component->cycles()->sync([3,4]);

        $component = Loadtemplate::create([
            'name' => 'Загальний обсяг навчального навантаження для класів з навчанням мовою корінного народу або національної меншини поряд з державною мовою чи з навчанням українською мовою та вивченням мови корінного народу або національної меншини',
            'languagetype_id' => 2,
            'meta' => (object)[
                'loads' => [
                    '5' => [
                        '1' => [
                            'recommended' => '11.5',
                            'minimum' => '11',
                            'maximum' => '13',
                        ],
                        '2' => [
                            'recommended' => '5',
                            'minimum' => '4',
                            'maximum' => '6',
                        ],
                        '3' => [
                            'recommended' => '2',
                            'minimum' => '1.5',
                            'maximum' => '3',
                        ],
                        '4' => [
                            'recommended' => '1',
                            'minimum' => '1',
                            'maximum' => '3',
                        ],
                        '5' => [
                            'recommended' => '1',
                            'minimum' => '1',
                            'maximum' => '3',
                        ],
                        '6' => [
                            'recommended' => '2',
                            'minimum' => '1',
                            'maximum' => '3',
                        ],
                        '7' => [
                            'recommended' => '1.5',
                            'minimum' => '1',
                            'maximum' => '3',
                        ],
                        '8' => [
                            'recommended' => '2',
                            'minimum' => '1',
                            'maximum' => '2',
                        ],
                        '9' => [
                            'recommended' => '3',
                            'minimum' => '3',
                            'maximum' => '3',
                        ],
                    ],
                    '6' => [
                        '1' => [
                            'recommended' => '11.5',
                            'minimum' => '11',
                            'maximum' => '13',
                        ],
                        '2' => [
                            'recommended' => '5',
                            'minimum' => '4',
                            'maximum' => '6',
                        ],
                        '3' => [
                            'recommended' => '4',
                            'minimum' => '2',
                            'maximum' => '5',
                        ],
                        '4' => [
                            'recommended' => '1',
                            'minimum' => '1',
                            'maximum' => '3',
                        ],
                        '5' => [
                            'recommended' => '2',
                            'minimum' => '1.5',
                            'maximum' => '3',
                        ],
                        '6' => [
                            'recommended' => '2',
                            'minimum' => '1',
                            'maximum' => '2',
                        ],
                        '7' => [
                            'recommended' => '1.5',
                            'minimum' => '1',
                            'maximum' => '3',
                        ],
                        '8' => [
                            'recommended' => '2',
                            'minimum' => '1',
                            'maximum' => '2',
                        ],
                        '9' => [
                            'recommended' => '3',
                            'minimum' => '3',
                            'maximum' => '3',
                        ],
                    ],
                    '7' => [
                        '1' => [
                            'recommended' => '10.5',
                            'minimum' => '10',
                            'maximum' => '12',
                        ],
                        '2' => [
                            'recommended' => '5',
                            'minimum' => '4',
                            'maximum' => '6',
                        ],
                        '3' => [
                            'recommended' => '7.5',
                            'minimum' => '7',
                            'maximum' => '9',
                        ],
                        '4' => [
                            'recommended' => '1',
                            'minimum' => '1',
                            'maximum' => '3',
                        ],
                        '5' => [
                            'recommended' => '2',
                            'minimum' => '1.5',
                            'maximum' => '3',
                        ],
                        '6' => [
                            'recommended' => '1',
                            'minimum' => '1',
                            'maximum' => '3',
                        ],
                        '7' => [
                            'recommended' => '2',
                            'minimum' => '1',
                            'maximum' => '2',
                        ],
                        '8' => [
                            'recommended' => '2',
                            'minimum' => '1',
                            'maximum' => '3',
                        ],
                        '9' => [
                            'recommended' => '3',
                            'minimum' => '3',
                            'maximum' => '3',
                        ],
                    ],
                    '8' => [
                        '1' => [
                            'recommended' => '10.5',
                            'minimum' => '8',
                            'maximum' => '12',
                        ],
                        '2' => [
                            'recommended' => '5',
                            'minimum' => '4',
                            'maximum' => '6',
                        ],
                        '3' => [
                            'recommended' => '8.5',
                            'minimum' => '8',
                            'maximum' => '10',
                        ],
                        '4' => [
                            'recommended' => '1',
                            'minimum' => '1',
                            'maximum' => '3',
                        ],
                        '5' => [
                            'recommended' => '3',
                            'minimum' => '2',
                            'maximum' => '3',
                        ],
                        '6' => [
                            'recommended' => '1',
                            'minimum' => '1',
                            'maximum' => '3',
                        ],
                        '7' => [
                            'recommended' => '2',
                            'minimum' => '1.5',
                            'maximum' => '2',
                        ],
                        '8' => [
                            'recommended' => '1',
                            'minimum' => '1',
                            'maximum' => '3',
                        ],
                        '9' => [
                            'recommended' => '3',
                            'minimum' => '3',
                            'maximum' => '3',
                        ],
                    ],
                    '9' => [
                        '1' => [
                            'recommended' => '10.5',
                            'minimum' => '8',
                            'maximum' => '12',
                        ],
                        '2' => [
                            'recommended' => '5',
                            'minimum' => '4',
                            'maximum' => '6',
                        ],
                        '3' => [
                            'recommended' => '9.5',
                            'minimum' => '8',
                            'maximum' => '10',
                        ],
                        '4' => [
                            'recommended' => '1',
                            'minimum' => '1',
                            'maximum' => '3',
                        ],
                        '5' => [
                            'recommended' => '3',
                            'minimum' => '2',
                            'maximum' => '3',
                        ],
                        '6' => [
                            'recommended' => '1',
                            'minimum' => '1',
                            'maximum' => '3',
                        ],
                        '7' => [
                            'recommended' => '1.5',
                            'minimum' => '1.5',
                            'maximum' => '2',
                        ],
                        '8' => [
                            'recommended' => '1',
                            'minimum' => '1',
                            'maximum' => '2',
                        ],
                        '9' => [
                            'recommended' => '3',
                            'minimum' => '3',
                            'maximum' => '3',
                        ],
                    ],
                ],
                'additional' => [
                    '5' => '2',
                    '6' => '2',
                    '7' => '1',
                    '8' => '1',
                    '9' => '0.5',
                ],
                'financed' => [
                    '5' => '31',
                    '6' => '34',
                    '7' => '35',
                    '8' => '36',
                    '9' => '36',
                ],
                'admissible' => [
                    '5' => '28',
                    '6' => '31',
                    '7' => '32',
                    '8' => '33',
                    '9' => '33',
                ],
            ]
        ]);
        $component->cperiods()->sync([1]);
        $component->cycles()->sync([3,4]);
    }
}
