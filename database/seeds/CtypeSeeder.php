<?php

use Illuminate\Database\Seeder;
use App\Models\Ctype;

class CtypeSeeder extends Seeder
{
    const DB_CONNECTION     = 'mysql';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection(self::DB_CONNECTION)->statement('SET FOREIGN_KEY_CHECKS=0;');
        Ctype::truncate();
        DB::connection(self::DB_CONNECTION)->statement('SET FOREIGN_KEY_CHECKS=1;');

        Ctype::create([
            'name' => 'Предмет',
        ]);
        Ctype::create([
            'name' => 'Інтегрований курс (внутрішньогалузевий)',
        ]);
        Ctype::create([
            'name' => 'Міжгалузевий курс',
        ]);

    }
}
