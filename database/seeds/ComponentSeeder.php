<?php

use Illuminate\Database\Seeder;
use App\Models\Component;

class ComponentSeeder extends Seeder
{
    const DB_CONNECTION     = 'mysql';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection(self::DB_CONNECTION)->statement('SET FOREIGN_KEY_CHECKS=0;');
        Component::truncate();
        DB::connection(self::DB_CONNECTION)->statement('SET FOREIGN_KEY_CHECKS=1;');

        # Предмети
            # 1 Мовно-літературна
                $component = Component::create([
                    'name' => 'Українська мова',
                    'ctype_id' => 1,
                ]);
                $component->languagetypes()->sync([1, 2]);
                $component->cbranches()->sync([1]);
                $component->grades()->sync([5,6,7,8,9]);
                $component = Component::create([
                    'name' => 'Українська література',
                    'ctype_id' => 1,
                ]);
                $component->languagetypes()->sync([1,2]);
                $component->cbranches()->sync([1]);
                $component->grades()->sync([5,6,7,8,9]);
                $component = Component::create([
                    'name' => 'Зарубіжна література',
                    'ctype_id' => 1,
                ]);
                $component->languagetypes()->sync([1,2]);
                $component->cbranches()->sync([1]);
                $component->grades()->sync([5,6,7,8,9]);
                $component = Component::create([
                    'name' => 'Іноземна мова',
                    'ctype_id' => 1,
                ]);
                $component->languagetypes()->sync([1,2]);
                $component->cbranches()->sync([1]);
                $component->grades()->sync([5,6,7,8,9]);
                $component = Component::create([
                    'name' => 'Друга іноземна мова',
                    'ctype_id' => 1,
                ]);
                $component->languagetypes()->sync([1,2]);
                $component->cbranches()->sync([1]);
                $component->grades()->sync([5,6,7,8,9]);


            # 2 Математична
                $component = Component::create([
                    'name' => 'Математика ',
                    'ctype_id' => 1,
                ]);
                $component->languagetypes()->sync([1,2]);
                $component->cbranches()->sync([2]);
                $component->grades()->sync([5,6]);
                $component = Component::create([
                    'name' => 'Алгебра ',
                    'ctype_id' => 1,
                ]);
                $component->languagetypes()->sync([1]);
                $component->cbranches()->sync([2]);
                $component->grades()->sync([7,8,9]);
                $component = Component::create([
                    'name' => 'Геометрія',
                    'ctype_id' => 1,
                ]);
                $component->languagetypes()->sync([1]);
                $component->cbranches()->sync([2]);
                $component->grades()->sync([7,8,9]);


            # 3 Природнича
                $component = Component::create([
                    'name' => 'Пізнаємо природу',
                    'ctype_id' => 1,
                ]);
                $component->languagetypes()->sync([1,2]);
                $component->cbranches()->sync([3]);
                $component->grades()->sync([5,6]);
                $component = Component::create([
                    'name' => 'Довкілля',
                    'ctype_id' => 1,
                ]);
                $component->languagetypes()->sync([1,2]);
                $component->cbranches()->sync([3]);
                $component->grades()->sync([5,6]);
                $component = Component::create([
                    'name' => 'Біологія',
                    'ctype_id' => 1,
                ]);
                $component->languagetypes()->sync([1]);
                $component->cbranches()->sync([3]);
                $component->grades()->sync([7,8,9]);
                $component = Component::create([
                    'name' => 'Географія',
                    'ctype_id' => 1,
                ]);
                $component->languagetypes()->sync([1,2]);
                $component->cbranches()->sync([3]);
                $component->grades()->sync([6,7,8,9]);
                $component = Component::create([
                    'name' => 'Фізика',
                    'ctype_id' => 1,
                ]);
                $component->languagetypes()->sync([1]);
                $component->cbranches()->sync([3]);
                $component->grades()->sync([7,8,9]);
                $component = Component::create([
                    'name' => 'Хімія',
                    'ctype_id' => 1,
                ]);
                $component->languagetypes()->sync([1]);
                $component->cbranches()->sync([3]);
                $component->grades()->sync([7,8,9]);


            # 4 Соціальна і здоров’язбережувальна
                $component = Component::create([
                    'name' => 'Здоров’я, безпека та добробут',
                    'ctype_id' => 1,
                ]);
                $component->languagetypes()->sync([1,2]);
                $component->cbranches()->sync([4]);
                $component->grades()->sync([5,6,7,8,9]);
                $component = Component::create([
                    'name' => 'Етика',
                    'ctype_id' => 1,
                ]);
                $component->languagetypes()->sync([1,2]);
                $component->cbranches()->sync([4]);
                $component->grades()->sync([5,6]);
                $component = Component::create([
                    'name' => 'Культура добросусідства',
                    'ctype_id' => 1,
                ]);
                $component->languagetypes()->sync([1,2]);
                $component->cbranches()->sync([4]);
                $component->grades()->sync([5,6]);
                $component = Component::create([
                    'name' => 'Курси морального спрямування',
                    'ctype_id' => 1,
                ]);
                $component->languagetypes()->sync([1,2]);
                $component->cbranches()->sync([4]);
                $component->grades()->sync([5,6]);
                $component = Component::create([
                    'name' => 'Підприємництво і фінансова грамотність',
                    'ctype_id' => 1,
                ]);
                $component->languagetypes()->sync([1]);
                $component->cbranches()->sync([4]);
                $component->grades()->sync([7,8,9]);


            # 5 Громадянська та історична
                $component = Component::create([
                    'name' => 'Вступ до історії України та громадянської освіти',
                    'ctype_id' => 1,
                ]);
                $component->languagetypes()->sync([1,2]);
                $component->cbranches()->sync([5]);
                $component->grades()->sync([5]);
                $component = Component::create([
                    'name' => 'Історія України. Всесвітня історія',
                    'ctype_id' => 1,
                ]);
                $component->languagetypes()->sync([1,2]);
                $component->cbranches()->sync([5]);
                $component->grades()->sync([6]);
                $component = Component::create([
                    'name' => 'Історія України',
                    'ctype_id' => 1,
                ]);
                $component->languagetypes()->sync([1]);
                $component->cbranches()->sync([5]);
                $component->grades()->sync([7,8,9]);
                $component = Component::create([
                    'name' => 'Всесвітня історія',
                    'ctype_id' => 1,
                ]);
                $component->languagetypes()->sync([1]);
                $component->cbranches()->sync([5]);
                $component->grades()->sync([7,8,9]);
                $component = Component::create([
                    'name' => 'Громадянська освіта',
                    'ctype_id' => 1,
                ]);
                $component->languagetypes()->sync([1]);
                $component->cbranches()->sync([5]);
                $component->grades()->sync([8]);
                $component = Component::create([
                    'name' => 'Правознавство',
                    'ctype_id' => 1,
                ]);
                $component->languagetypes()->sync([1]);
                $component->cbranches()->sync([5]);
                $component->grades()->sync([9]);


            # 6 Інформатична
                $component = Component::create([
                    'name' => 'Інформатика',
                    'ctype_id' => 1,
                ]);
                $component->languagetypes()->sync([1,2]);
                $component->cbranches()->sync([6]);
                $component->grades()->sync([5,6,7,8,9]);


            # 7 Технологічна
                $component = Component::create([
                    'name' => 'Технології',
                    'ctype_id' => 1,
                ]);
                $component->languagetypes()->sync([1,2]);
                $component->cbranches()->sync([7]);
                $component->grades()->sync([5,6,7,8,9]);


            # 8 Мистецька
                $component = Component::create([
                    'name' => 'Образотворче мистецтво',
                    'ctype_id' => 1,
                ]);
                $component->languagetypes()->sync([1,2]);
                $component->cbranches()->sync([8]);
                $component->grades()->sync([5,6,7,8,9]);
                $component = Component::create([
                    'name' => 'Музичне мистецтво',
                    'ctype_id' => 1,
                ]);
                $component->languagetypes()->sync([1,2]);
                $component->cbranches()->sync([8]);
                $component->grades()->sync([5,6,7,8,9]);


            # 9 Фізична культура
                $component = Component::create([
                    'name' => 'Фізична культура ',
                    'ctype_id' => 1,
                ]);
                $component->languagetypes()->sync([1,2]);
                $component->cbranches()->sync([9]);
                $component->grades()->sync([5,6,7,8,9]);


        # Внутрішньогалузеві курси
            # Мовно-літературна
                $component = Component::create([
                    'name' => 'Інтегрований мовно-літературний курс',
                    'ctype_id' => 2,
                ]);
                $component->languagetypes()->sync([1]);
                $component->cbranches()->sync([1]);
                $component->grades()->sync([5,6,7,8,9]);

                $component = Component::create([
                    'name' => 'Інтегрований курс літератур',
                    'ctype_id' => 2,
                ]);
                $component->languagetypes()->sync([1]);
                $component->cbranches()->sync([1]);
                $component->grades()->sync([5,6,7,8,9]);

                $component = Component::create([
                    'name' => 'Українська мова та література',
                    'ctype_id' => 2,
                ]);
                $component->languagetypes()->sync([1]);
                $component->cbranches()->sync([1]);
                $component->grades()->sync([5,6,7,8,9]);


            # Математична
                $component = Component::create([
                    'name' => 'Математика',
                    'ctype_id' => 2,
                ]);
                $component->languagetypes()->sync([1]);
                $component->cbranches()->sync([2]);
                $component->grades()->sync([7,8,9]);


            # Природнича
                $component = Component::create([
                    'name' => 'Природничі науки',
                    'ctype_id' => 2,
                ]);
                $component->languagetypes()->sync([1,2]);
                $component->cbranches()->sync([3]);
                $component->grades()->sync([5,6]);
                $component = Component::create([
                    'name' => 'Природничі науки',
                    'ctype_id' => 2,
                ]);
                $component->languagetypes()->sync([1]);
                $component->cbranches()->sync([3]);
                $component->grades()->sync([7,8,9]);


            # Соціальна і здоров’язбережувальна
                $component = Component::create([
                    'name' => 'Здоров’я, безпека та добробут',
                    'ctype_id' => 2,
                ]);
                $component->languagetypes()->sync([1,2]);
                $component->cbranches()->sync([4]);
                $component->grades()->sync([5,6]);


            # Громадянська та історична
                $component = Component::create([
                    'name' => 'Досліджуємо історію і суспільство',
                    'ctype_id' => 2,
                ]);
                $component->languagetypes()->sync([1,2]);
                $component->cbranches()->sync([5]);
                $component->grades()->sync([5,6]);
                $component = Component::create([
                    'name' => 'Україна і світ: вступ до історії України та громадянської освіти',
                    'ctype_id' => 2,
                ]);
                $component->languagetypes()->sync([1,2]);
                $component->cbranches()->sync([5]);
                $component->grades()->sync([5,6]);
                $component = Component::create([
                    'name' => 'Історія: Україна і світ',
                    'ctype_id' => 2,
                ]);
                $component->languagetypes()->sync([1]);
                $component->cbranches()->sync([5]);
                $component->grades()->sync([7,8,9]);


            # Мистецька
                $component = Component::create([
                    'name' => 'Мистецтво',
                    'ctype_id' => 2,
                ]);
                $component->languagetypes()->sync([1,2]);
                $component->cbranches()->sync([8]);
                $component->grades()->sync([5,6,7,8,9]);


        # Міжгалузеві курси
            $component = Component::create([
                'name' => 'Робототехніка',
                'ctype_id' => 3,
            ]);
                $component->languagetypes()->sync([1,2]);
            $component->cbranches()->sync([6,7]);
            $component->grades()->sync([5,6,7,8,9]);
            $component = Component::create([
                'name' => 'Драматургія і театр',
                'ctype_id' => 3,
            ]);
                $component->languagetypes()->sync([1,2]);
            $component->cbranches()->sync([1,8]);
            $component->grades()->sync([5,6,7,8,9]);
            $component = Component::create([
                'name' => 'Фізика та основи техніки',
                'ctype_id' => 3,
            ]);
                $component->languagetypes()->sync([1]);
            $component->cbranches()->sync([3,7]);
            $component->grades()->sync([7,8,9]);
            $component = Component::create([
                'name' => 'Економіка і право',
                'ctype_id' => 3,
            ]);
                $component->languagetypes()->sync([1]);
            $component->cbranches()->sync([4,5]);
            $component->grades()->sync([9]);
            $component = Component::create([
                'name' => 'STEM',
                'ctype_id' => 3,
            ]);
                $component->languagetypes()->sync([1,2]);
            $component->cbranches()->sync([2,3,6,7]);
            $component->grades()->sync([5,6,7,8,9]);


        # Компоненты исключительно нацменшинств
            $component = Component::create([ // 47
                'name' => 'Література корінного народу або національної меншини',
                'ctype_id' => 1,
            ]);
            $component->languagetypes()->sync([2]);
            $component->cbranches()->sync([1]);
            $component->grades()->sync([5,6]);

            $component = Component::create([ // 48
                'name' => 'Мова корінного народу або національної меншини',
                'ctype_id' => 1,
            ]);
            $component->languagetypes()->sync([2]);
            $component->cbranches()->sync([1]);
            $component->grades()->sync([5,6]);

            $component = Component::create([ // 49
                'name' => 'Інтегрований курс української мови і літератури',
                'ctype_id' => 2,
            ]);
            $component->languagetypes()->sync([2]);
            $component->cbranches()->sync([1]);
            $component->grades()->sync([5,6]);

            $component = Component::create([ // 50
                'name' => 'Інтегрований курс літератур (української та зарубіжної)',
                'ctype_id' => 2,
            ]);
            $component->languagetypes()->sync([2]);
            $component->cbranches()->sync([1]);
            $component->grades()->sync([5,6]);

            $component = Component::create([ // 51
                'name' => 'Інтегрований курс літератур (корінного народу або національної меншини та зарубіжної літератури)',
                'ctype_id' => 2,
            ]);
            $component->languagetypes()->sync([2]);
            $component->cbranches()->sync([1]);
            $component->grades()->sync([5,6]);

            $component = Component::create([ // 52
                'name' => 'Інтегрований мовно-літературний курс (мова та література корінного народу або національної меншини)',
                'ctype_id' => 2,
            ]);
            $component->languagetypes()->sync([2]);
            $component->cbranches()->sync([1]);
            $component->grades()->sync([5,6]);

        # Связи между компонентами
            $component = Component::find(9);
            $component->restricted()->sync([10,35]);
            $component = Component::find(10);
            $component->restricted()->sync([9,35]);

            $component = Component::find(16);
            $component->restricted()->sync([17,18,37]);
            $component = Component::find(17);
            $component->restricted()->sync([16,18,37]);
            $component = Component::find(18);
            $component->restricted()->sync([16,17,37]);

            $component = Component::find(1);
            $component->restricted()->sync([31,33,49,50]);
            $component = Component::find(2);
            $component->restricted()->sync([31,32,33,49]);
            $component = Component::find(3);
            $component->restricted()->sync([31,32,50,51]);

            $component = Component::find(31);
            $component->restricted()->sync([1,2,3]);
            $component = Component::find(32);
            $component->restricted()->sync([2,3]);
            $component = Component::find(33);
            $component->restricted()->sync([1,2]);

            $component = Component::find(7);
            $component->restricted()->sync([34]);
            $component = Component::find(8);
            $component->restricted()->sync([34]);
            $component = Component::find(34);
            $component->restricted()->sync([7,8]);

            $component = Component::find(12);
            $component->restricted()->sync([35,36]);
            $component = Component::find(35);
            $component->restricted()->sync([9,10,12]);
            $component = Component::find(36);
            $component->restricted()->sync([11,12,13,14]);

            $component = Component::find(15);
            $component->restricted()->sync([37]);
            $component = Component::find(37);
            $component->restricted()->sync([15,16,17,18]);


            $component = Component::find(20);
            $component->restricted()->sync([38,39]);
            $component = Component::find(21);
            $component->restricted()->sync([38,39]);
            $component = Component::find(38);
            $component->restricted()->sync([20,21]);
            $component = Component::find(39);
            $component->restricted()->sync([20,21]);

            $component = Component::find(22);
            $component->restricted()->sync([40]);
            $component = Component::find(23);
            $component->restricted()->sync([40]);
            $component = Component::find(40);
            $component->restricted()->sync([22,23]);

            $component = Component::find(28);
            $component->restricted()->sync([41]);
            $component = Component::find(29);
            $component->restricted()->sync([41]);
            $component = Component::find(41);
            $component->restricted()->sync([28,29]);

        # Связи между компонентами нацменшинств

            $component = Component::find(47);
            $component->restricted()->sync([51,52]);

            $component = Component::find(48);
            $component->restricted()->sync([52]);

            $component = Component::find(49);
            $component->restricted()->sync([1,2]);

            $component = Component::find(50);
            $component->restricted()->sync([1,3]);

            $component = Component::find(51);
            $component->restricted()->sync([3, 47]);

            $component = Component::find(52);
            $component->restricted()->sync([47, 48]);

    }
}
