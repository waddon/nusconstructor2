<?php

use Illuminate\Database\Seeder;
use App\Models\Cbranch;

class CbranchSeeder extends Seeder
{
    const DB_CONNECTION     = 'mysql';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection(self::DB_CONNECTION)->statement('SET FOREIGN_KEY_CHECKS=0;');
        Cbranch::truncate();
        DB::connection(self::DB_CONNECTION)->statement('SET FOREIGN_KEY_CHECKS=1;');

        Cbranch::create([
            'name' => 'Мовно-літературна',
            'marking' => 'МОВ',
        ]);
        Cbranch::create([
            'name' => 'Математична',
            'marking' => 'МАО',
        ]);
        Cbranch::create([
            'name' => 'Природнича',
            'marking' => 'ПРО',
        ]);
        Cbranch::create([
            'name' => 'Соціальна і здоров’язбережувальна',
            'marking' => 'СЗО',
        ]);
        Cbranch::create([
            'name' => 'Громадянська та історична',
            'marking' => 'ГІО',
        ]);
        Cbranch::create([
            'name' => 'Інформатична',
            'marking' => 'ІФО',
        ]);
        Cbranch::create([
            'name' => 'Технологічна',
            'marking' => 'ТЕО',
        ]);
        Cbranch::create([
            'name' => 'Мистецька',
            'marking' => 'МИО',
        ]);
        Cbranch::create([
            'name' => 'Фізична культура',
            'marking' => 'ФІО',
        ]);
    }
}
