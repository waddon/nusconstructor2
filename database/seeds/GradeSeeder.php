<?php

use Illuminate\Database\Seeder;
use App\Models\Grade;

class GradeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Grade::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Grade::create([
            'name' => '1 клас',
            'cycle_id' => 1,
        ]);
        Grade::create([
            'name' => '2 клас',
            'cycle_id' => 1,
        ]);
        Grade::create([
            'name' => '3 клас',
            'cycle_id' => 2,
        ]);
        Grade::create([
            'name' => '4 клас',
            'cycle_id' => 2,
        ]);
        Grade::create([
            'name' => '5 клас',
            'cycle_id' => 3,
        ]);
        Grade::create([
            'name' => '6 клас',
            'cycle_id' => 3,
        ]);

        Grade::create([
            'name' => '7 клас',
            'cycle_id' => 4,
        ]);
        Grade::create([
            'name' => '8 клас',
            'cycle_id' => 4,
        ]);
        Grade::create([
            'name' => '9 клас',
            'cycle_id' => 4,
        ]);
    }
}
