<?php

use Illuminate\Database\Seeder;
use App\Models\Languagetype;

class LanguagetypeSeeder extends Seeder
{
    const DB_CONNECTION     = 'mysql';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection(self::DB_CONNECTION)->statement('SET FOREIGN_KEY_CHECKS=0;');
        Languagetype::truncate();
        DB::connection(self::DB_CONNECTION)->statement('SET FOREIGN_KEY_CHECKS=1;');

        Languagetype::create([
            'name' => 'Українська мова',
        ]);
        Languagetype::create([
            'name' => 'Мови корінного народу або національної меншини',
        ]);
    }
}
