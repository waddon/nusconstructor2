<?php

use Illuminate\Database\Seeder;
use App\Models\Tload;
use App\Models\Team;
use App\User;

use App\Models\Loadtemplate;

class TloadSeeder extends Seeder
{
    const DB_CONNECTION     = 'mysql';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection(self::DB_CONNECTION)->statement('SET FOREIGN_KEY_CHECKS=0;');
        Tload::truncate();
        DB::connection(self::DB_CONNECTION)->statement('SET FOREIGN_KEY_CHECKS=1;');

        // $team = Team::find(6);
        // $user = User::find(6);

        // $loadtemplate = Loadtemplate::findOrFail(1);
        // $meta = $loadtemplate->getMetaByCycle(3);
        // $model = $team->tloads()->create([
        //     'name' => 'Загальний обсяг навчального навантаження для закладів із навчанням українською мовою',
        //     'cperiod_id' => 1,
        //     'loadtemplate_id' => 1,
        //     'cycle_id' => 3,
        //     'meta' => $meta,
        // ]);

        // $loadtemplate = Loadtemplate::findOrFail(1);
        // $meta = $loadtemplate->getMetaByCycle(4);
        // $model = $team->tloads()->create([
        //     'name' => 'Загальний обсяг навчального навантаження для закладів із навчанням українською мовою',
        //     'cperiod_id' => 1,
        //     'loadtemplate_id' => 1,
        //     'cycle_id' => 4,
        //     'meta' => $meta,
        // ]);

        // $loadtemplate = Loadtemplate::findOrFail(2);
        // $meta = $loadtemplate->getMetaByCycle(3);
        // $model = $team->tloads()->create([
        //     'name' => 'Загальний обсяг навчального навантаження для класів з навчанням мовою корінного народу або національної меншини поряд з державною мовою чи з навчанням українською мовою та вивченням мови корінного народу або національної меншини',
        //     'cperiod_id' => 1,
        //     'loadtemplate_id' => 2,
        //     'cycle_id' => 3,
        //     'meta' => $meta,
        // ]);


        // $loadtemplate = Loadtemplate::findOrFail(1);
        // $meta = $loadtemplate->getMetaByCycle(3);
        // $model = $user->tloads()->create([
        //     'name' => 'Загальний обсяг навчального навантаження для закладів із навчанням українською мовою',
        //     'cperiod_id' => 1,
        //     'loadtemplate_id' => 1,
        //     'cycle_id' => 3,
        //     'meta' => $meta,
        // ]);

    }
}
