<?php

use Illuminate\Database\Seeder;
use App\Models\Cauthor;


class CauthorSeeder extends Seeder
{
    const DB_CONNECTION     = 'mysql';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection(self::DB_CONNECTION)->statement('SET FOREIGN_KEY_CHECKS=0;');
        Cauthor::truncate();
        DB::connection(self::DB_CONNECTION)->statement('SET FOREIGN_KEY_CHECKS=1;');

        Cauthor::create([ 'name' => 'Голуб Н.', ]); // 1
        Cauthor::create([ 'name' => 'Горошкіна О.', ]); // 2

        Cauthor::create([ 'name' => 'Заболотний В.', ]); // 3
        Cauthor::create([ 'name' => 'Заболотний О.', ]); // 4
        Cauthor::create([ 'name' => 'Лавринчук В.', ]); // 5
        Cauthor::create([ 'name' => 'Плівачук К.', ]); // 6
        Cauthor::create([ 'name' => 'Попова Т.', ]); // 7

        Cauthor::create([ 'name' => 'Гнаткович Т.', ]); // 8
        Cauthor::create([ 'name' => 'Шумицька Г.', ]); // 9
        Cauthor::create([ 'name' => 'Борисова Є.', ]); // 10
        Cauthor::create([ 'name' => 'Лукач А.', ]); // 11

        Cauthor::create([ 'name' => 'Черничко С.', ]); // 12
        Cauthor::create([ 'name' => 'Певсе А.', ]); // 13
        Cauthor::create([ 'name' => 'Кордонець О.', ]); // 14
        Cauthor::create([ 'name' => 'Барань Є.', ]); // 15
        Cauthor::create([ 'name' => 'Чонка Т.', ]); // 16
        Cauthor::create([ 'name' => 'Павлович Ю.', ]); // 17
        Cauthor::create([ 'name' => 'Беца С.', ]); // 18
        Cauthor::create([ 'name' => 'Кантор С.', ]); // 19
        Cauthor::create([ 'name' => 'Кейзі-Бак С.', ]); // 20
        Cauthor::create([ 'name' => 'Копас І.', ]); // 21
        Cauthor::create([ 'name' => 'Павлович Ю.', ]); // 22
        Cauthor::create([ 'name' => 'Стець М.', ]); // 23
        Cauthor::create([ 'name' => 'Чейке О.', ]); // 24

        Cauthor::create([ 'name' => 'Свінтковська С.', ]); // 25
        Cauthor::create([ 'name' => 'Верготі Л.', ]); // 26
        Cauthor::create([ 'name' => 'Ткаченко С.', ]); // 27
        Cauthor::create([ 'name' => 'Цимбал Т.', ]); // 28

        Cauthor::create([ 'name' => 'Фонарюк Т.', ]); // 29
        Cauthor::create([ 'name' => 'Кульбабська О.', ]); // 30
        Cauthor::create([ 'name' => 'Філіп Ю.', ]); // 31
        Cauthor::create([ 'name' => 'Костіна-Кніжницька А.', ]); // 32
        Cauthor::create([ 'name' => 'Сиротюк О.', ]); // 33

        Cauthor::create([ 'name' => 'Архипова В.', ]); // 34
        Cauthor::create([ 'name' => 'Січкар С.', ]); // 35
        Cauthor::create([ 'name' => 'Шило С.', ]); // 36

        Cauthor::create([ 'name' => 'Чумарна М.', ]); // 37
        Cauthor::create([ 'name' => 'Пастушенко Н.', ]); // 38

        Cauthor::create([ 'name' => 'Яценко Т.', ]); // 39
        Cauthor::create([ 'name' => 'Качак Т.', ]); // 40
        Cauthor::create([ 'name' => 'Кизилова В.', ]); // 41
        Cauthor::create([ 'name' => 'Пахаренко В.', ]); // 42
        Cauthor::create([ 'name' => 'Дячок С.', ]); // 43
        Cauthor::create([ 'name' => 'Овдійчук Л.', ]); // 44
        Cauthor::create([ 'name' => 'Слижук О.', ]); // 45
        Cauthor::create([ 'name' => 'Макаренко В.', ]); // 46
        Cauthor::create([ 'name' => 'Тригуб І.', ]); // 47

        Cauthor::create([ 'name' => 'Ніколенко О.', ]); // 48
        Cauthor::create([ 'name' => 'Ісаєва О. ', ]); // 49
        Cauthor::create([ 'name' => 'Клименко Ж.', ]); // 50
        Cauthor::create([ 'name' => 'Мацевко-Бекерська Л.', ]); // 51
        Cauthor::create([ 'name' => 'Юлдашева Л.', ]); // 52
        Cauthor::create([ 'name' => 'Рудніцька Н.', ]); // 53
        Cauthor::create([ 'name' => 'Туряниця В.', ]); // 54
        Cauthor::create([ 'name' => 'Тіхоненко С.', ]); // 55
        Cauthor::create([ 'name' => 'Вітко М.', ]); // 56
        Cauthor::create([ 'name' => 'Джагонбекова Т.', ]); // 57

        Cauthor::create([ 'name' => 'Волощук Є.', ]); // 58

        Cauthor::create([ 'name' => 'Богданець-Білоскаленко Н.', ]); // 59
        Cauthor::create([ 'name' => 'Снєгірьова В.', ]); // 60
        Cauthor::create([ 'name' => 'Фідкевич О.', ]); // 61

        Cauthor::create([ 'name' => 'Старагіна І.', ]); // 62
        Cauthor::create([ 'name' => 'Новосьолова В.', ]); // 63
        Cauthor::create([ 'name' => 'Терещенко В.', ]); // 64
        Cauthor::create([ 'name' => 'Романенко Ю.', ]); // 65
        Cauthor::create([ 'name' => 'Блажко М.', ]); // 66
        Cauthor::create([ 'name' => 'Ткач П.', ]); // 67
        Cauthor::create([ 'name' => 'Панченков А.', ]); // 68
        Cauthor::create([ 'name' => 'Волошенюк О.', ]); // 69

        Cauthor::create([ 'name' => 'Мілков А.', ]); // 70
        Cauthor::create([ 'name' => 'Ніколова Х.', ]); // 71
        Cauthor::create([ 'name' => 'Проданова О.', ]); // 72

        Cauthor::create([ 'name' => 'Кіор І.', ]); // 73

        Cauthor::create([ 'name' => 'Бакуліна Н.', ]); // 74
        Cauthor::create([ 'name' => 'Щербак О.', ]); // 75

        Cauthor::create([ 'name' => 'Іванова М.', ]); // 76

        Cauthor::create([ 'name' => 'Мацькович М.', ]); // 77
        Cauthor::create([ 'name' => 'Калуські Томаш-Аркадіуш', ]); // 78
        Cauthor::create([ 'name' => 'Калуська Р.', ]); // 79

        Cauthor::create([ 'name' => 'Войцева О.', ]); // 80
        Cauthor::create([ 'name' => 'Бучацька Т.', ]); // 81

        Cauthor::create([ 'name' => 'Самонова О.', ]); // 82
        Cauthor::create([ 'name' => 'Крюченкова О.', ]); // 83

        Cauthor::create([ 'name' => 'Говорнян Л.', ]); // 84
        Cauthor::create([ 'name' => 'Зазулинська Т.', ]); // 85
        Cauthor::create([ 'name' => 'Бику А.', ]); // 86
        Cauthor::create([ 'name' => 'Микайлу М.', ]); // 87

        Cauthor::create([ 'name' => 'Карайван Н.', ]); // 88
        Cauthor::create([ 'name' => 'Галупа Н.', ]); // 89
        Cauthor::create([ 'name' => 'Лунгу Н.', ]); // 90

        Cauthor::create([ 'name' => 'Богадиця М.', ]); // 91
        Cauthor::create([ 'name' => 'Воєвутко Н.', ]); // 92
        Cauthor::create([ 'name' => 'Добра О.', ]); // 93
        Cauthor::create([ 'name' => 'Кіор Т.', ]); // 94
        Cauthor::create([ 'name' => 'Косюк Л.', ]); // 95
        Cauthor::create([ 'name' => 'Никишова В.', ]); // 96
        Cauthor::create([ 'name' => 'Сабадаш Т.', ]); // 97
        Cauthor::create([ 'name' => 'Сніговська О.', ]); // 98
        Cauthor::create([ 'name' => 'Чайка Л.', ]); // 99

        Cauthor::create([ 'name' => 'Герзанич М.', ]); // 100
        Cauthor::create([ 'name' => 'Шабаш Н.', ]); // 101

        Cauthor::create([ 'name' => 'Браун Є.', ]); // 102
        Cauthor::create([ 'name' => 'Бардош Н.', ]); // 103
        Cauthor::create([ 'name' => 'Зикань Х.', ]); // 104
        Cauthor::create([ 'name' => 'Ковач П.', ]); // 105
        Cauthor::create([ 'name' => 'Пердук І.', ]); // 106
        Cauthor::create([ 'name' => 'Гнатик-Рішко М.', ]); // 107

        Cauthor::create([ 'name' => 'Мамутова М.', ]); // 108
        Cauthor::create([ 'name' => 'Сеїт-Джеліль А.', ]); // 109

        Cauthor::create([ 'name' => 'Кіраль К.', ]); // 110

        Cauthor::create([ 'name' => 'Лебедь Р.', ]); // 111

        Cauthor::create([ 'name' => 'Колесникова Д.', ]); // 112
        Cauthor::create([ 'name' => 'Мирон Л.', ]); // 113
        Cauthor::create([ 'name' => 'Криган Л.', ]); // 114

        Cauthor::create([ 'name' => 'Зимомря І.', ]); // 115
        Cauthor::create([ 'name' => 'Мойсюк В.', ]); // 116
        Cauthor::create([ 'name' => 'Тріфан М.', ]); // 117
        Cauthor::create([ 'name' => 'Унгурян І.', ]); // 118
        Cauthor::create([ 'name' => 'Яковчук М.', ]); // 119

        Cauthor::create([ 'name' => 'Редько В.', ]); // 120
        Cauthor::create([ 'name' => 'Шаленко О.', ]); // 121
        Cauthor::create([ 'name' => 'Сотникова С.', ]); // 122
        Cauthor::create([ 'name' => 'Коваленко О.', ]); // 123
        Cauthor::create([ 'name' => 'Коропецька І.', ]); // 124
        Cauthor::create([ 'name' => 'Якоб О.', ]); // 125
        Cauthor::create([ 'name' => 'Самойлюкевич І.', ]); // 126

        Cauthor::create([ 'name' => 'Глинюк Л.', ]); // 127

        Cauthor::create([ 'name' => 'Бурлака О.', ]); // 128
        Cauthor::create([ 'name' => 'Власова Н.', ]); // 129
        Cauthor::create([ 'name' => 'Желіба О.', ]); // 130
        Cauthor::create([ 'name' => 'Майорський В.', ]); // 131
        Cauthor::create([ 'name' => 'Піскарьова І.', ]); // 132
        Cauthor::create([ 'name' => 'Щупак І.', ]); // 133

        Cauthor::create([ 'name' => 'Гісем О.', ]); // 134
        Cauthor::create([ 'name' => 'Мартинюк О.', ]); // 135

        Cauthor::create([ 'name' => 'Бакка Т.', ]); // 136
        Cauthor::create([ 'name' => 'Мелещенко Т.', ]); // 137

        Cauthor::create([ 'name' => 'Мокрогуз О.', ]); // 138

        Cauthor::create([ 'name' => 'Васильків І.', ]); // 139
        Cauthor::create([ 'name' => 'Димій І.', ]); // 140
        Cauthor::create([ 'name' => 'Шеремета Р.', ]); // 141

        Cauthor::create([ 'name' => 'Пометун О', ]); // 142
        Cauthor::create([ 'name' => 'Ремех Т.', ]); // 143
        Cauthor::create([ 'name' => 'Малієнко Ю.', ]); // 144
        Cauthor::create([ 'name' => 'Мороз П.', ]); // 145

        Cauthor::create([ 'name' => 'Кафтан М.', ]); // 146
        Cauthor::create([ 'name' => 'Козорог О.', ]); // 147
        Cauthor::create([ 'name' => 'Костюк І.', ]); // 148
        Cauthor::create([ 'name' => 'Мудрий М.', ]); // 149
        Cauthor::create([ 'name' => 'Селіваненко В.', ]); // 150

        Cauthor::create([ 'name' => 'Кронгауз В.', ]); // 151
        Cauthor::create([ 'name' => 'Секиринський Д.', ]); // 152

        Cauthor::create([ 'name' => 'Ашортіа Є.', ]); // 153
        Cauthor::create([ 'name' => 'Козіна Л.', ]); // 154

        Cauthor::create([ 'name' => 'Кришмарел В.', ]); // 155

        Cauthor::create([ 'name' => 'Воронцова Т.', ]); // 156
        Cauthor::create([ 'name' => 'Пономаренко В.', ]); // 157
        Cauthor::create([ 'name' => 'Лаврентьєва І.', ]); // 158
        Cauthor::create([ 'name' => 'Хомич О.', ]); // 159

        Cauthor::create([ 'name' => 'Араджіоні М.', ]); // 160
        Cauthor::create([ 'name' => 'Лебідь Н.', ]); // 161
        Cauthor::create([ 'name' => 'Потапова В.', ]); // 162

        Cauthor::create([ 'name' => 'Шиян О.', ]); // 163
        Cauthor::create([ 'name' => 'Дяків В.', ]); // 164
        Cauthor::create([ 'name' => 'Гриньова М.', ]); // 165
        Cauthor::create([ 'name' => 'Козак О.', ]); // 166
        Cauthor::create([ 'name' => 'Овчарук О.', ]); // 167
        Cauthor::create([ 'name' => 'Седоченко А.', ]); // 168
        Cauthor::create([ 'name' => 'Сорока І.', ]); // 169
        Cauthor::create([ 'name' => 'Страшко С.', ]); // 170

        Cauthor::create([ 'name' => 'Хитра З.', ]); // 171
        Cauthor::create([ 'name' => 'Романенко О.', ]); // 172

        Cauthor::create([ 'name' => 'Василенко С.', ]); // 173
        Cauthor::create([ 'name' => 'Коваль Я.', ]); // 174
        Cauthor::create([ 'name' => 'Колотій Л.', ]); // 175

        Cauthor::create([ 'name' => 'Гущина Н.', ]); // 176
        Cauthor::create([ 'name' => 'Василашко І.', ]); // 177

        Cauthor::create([ 'name' => 'Педан О.', ]); // 178
        Cauthor::create([ 'name' => 'Коломоєць Г.', ]); // 179
        Cauthor::create([ 'name' => 'Боляк А.', ]); // 180

        Cauthor::create([ 'name' => 'Коршевнюк Т.', ]); // 181

        Cauthor::create([ 'name' => 'Біда Д.', ]); // 182
        Cauthor::create([ 'name' => 'Гільберг Т', ]); // 183
        Cauthor::create([ 'name' => 'Колісник Я.', ]); // 184

        Cauthor::create([ 'name' => 'Шаламов Р.', ]); // 185
        Cauthor::create([ 'name' => 'Каліберда М.', ]); // 186
        Cauthor::create([ 'name' => 'Григорович О.', ]); // 187
        Cauthor::create([ 'name' => 'Фіцайло С.', ]); // 188

        Cauthor::create([ 'name' => 'Білик Ж.', ]); // 189
        Cauthor::create([ 'name' => 'Засєкіна Т.', ]); // 190
        Cauthor::create([ 'name' => 'Лашевська Г.', ]); // 191
        Cauthor::create([ 'name' => 'Яценко В.', ]); // 192

        Cauthor::create([ 'name' => 'Бурда М.', ]); // 193
        Cauthor::create([ 'name' => 'Васильєва Д.', ]); // 194

        Cauthor::create([ 'name' => 'Василишин М.', ]); // 195
        Cauthor::create([ 'name' => 'Миляник А.', ]); // 196
        Cauthor::create([ 'name' => 'Працьовитий М.', ]); // 197
        Cauthor::create([ 'name' => 'Простакова Ю.', ]); // 198
        Cauthor::create([ 'name' => 'Школьний О.', ]); // 199

        Cauthor::create([ 'name' => 'Мерзляк А.', ]); // 200
        Cauthor::create([ 'name' => 'Номіровський Д.', ]); // 201
        Cauthor::create([ 'name' => 'Пихтар М.', ]); // 202
        Cauthor::create([ 'name' => 'Рубльов Б.', ]); // 203
        Cauthor::create([ 'name' => 'Семенов В.', ]); // 204
        Cauthor::create([ 'name' => 'Якір М.', ]); // 205

        Cauthor::create([ 'name' => 'Істер О.', ]); // 206

        Cauthor::create([ 'name' => 'Беденко М.', ]); // 207
        Cauthor::create([ 'name' => 'Клочко І.', ]); // 208
        Cauthor::create([ 'name' => 'Кордиш Т.', ]); // 209
        Cauthor::create([ 'name' => 'Тадеєв В.', ]); // 210

        Cauthor::create([ 'name' => 'Скворцова С.', ]); // 211
        Cauthor::create([ 'name' => 'Тарасенкова Н.', ]); // 212

        Cauthor::create([ 'name' => 'Радченко С.', ]); // 213
        Cauthor::create([ 'name' => 'Зайцева К.', ]); // 214

        Cauthor::create([ 'name' => 'Морзе Н.', ]); // 215
        Cauthor::create([ 'name' => 'Барна О.', ]); // 216

        Cauthor::create([ 'name' => 'Ривкінд Й.', ]); // 217
        Cauthor::create([ 'name' => 'Лисенко Т.', ]); // 218
        Cauthor::create([ 'name' => 'Чернікова Л.', ]); // 219
        Cauthor::create([ 'name' => 'Шакотько В.', ]); // 220

        Cauthor::create([ 'name' => 'Пасічник О.', ]); // 221

        Cauthor::create([ 'name' => 'Завадський І.', ]); // 222
        Cauthor::create([ 'name' => 'Коршунова О.', ]); // 223
        Cauthor::create([ 'name' => 'Лапінський В.', ]); // 224

        Cauthor::create([ 'name' => 'Боровцова Є.', ]); // 225

        Cauthor::create([ 'name' => 'Ходзицька І.', ]); // 226
        Cauthor::create([ 'name' => 'Горобець О.', ]); // 227
        Cauthor::create([ 'name' => 'Медвідь О.', ]); // 228
        Cauthor::create([ 'name' => 'Пасічна Т.', ]); // 229
        Cauthor::create([ 'name' => 'Приходько Ю.', ]); // 230

        Cauthor::create([ 'name' => 'Туташинський В.', ]); // 231

        Cauthor::create([ 'name' => 'Кільдеров Д.', ]); // 232
        Cauthor::create([ 'name' => 'Мачача Т.', ]); // 233
        Cauthor::create([ 'name' => 'Юрженко В.', ]); // 234
        Cauthor::create([ 'name' => 'Луп\'як Д.', ]); // 235

        Cauthor::create([ 'name' => 'Терещук А.', ]); // 236
        Cauthor::create([ 'name' => 'Абрамова О.', ]); // 237
        Cauthor::create([ 'name' => 'Гащак В.', ]); // 238
        Cauthor::create([ 'name' => 'Павич Н.', ]); // 239

        Cauthor::create([ 'name' => 'Комаровська О.', ]); // 240
        Cauthor::create([ 'name' => 'Лємешева Н.', ]); // 241

        Cauthor::create([ 'name' => 'Кондратова Л.', ]); // 242

        Cauthor::create([ 'name' => 'Масол Л.', ]); // 243
        Cauthor::create([ 'name' => 'Просіна О.', ]); // 244

        Cauthor::create([ 'name' => 'Івасюк О.', ]); // 245
        Cauthor::create([ 'name' => 'Кізілова Г.', ]); // 246
        Cauthor::create([ 'name' => 'Лобова О.', ]); // 247
        Cauthor::create([ 'name' => 'Назар Л.', ]); // 248
        Cauthor::create([ 'name' => 'Чужинова І.', ]); // 249
        Cauthor::create([ 'name' => 'Шулько О.', ]); // 250

        Cauthor::create([ 'name' => 'Ержейбет Фрінг', ]); // 251
        Cauthor::create([ 'name' => 'Діана Антал', ]); // 252
        Cauthor::create([ 'name' => 'Людвиг Гейдер', ]); // 253
        Cauthor::create([ 'name' => 'Юліанна Кіш', ]); // 254
        Cauthor::create([ 'name' => 'Лілія Корнейчук', ]); // 255
        Cauthor::create([ 'name' => 'Єлизавета Ференц', ]); // 256

        Cauthor::create([ 'name' => 'Сокол І.', ]); // 257
        Cauthor::create([ 'name' => 'Ченцов О.', ]); // 258

        Cauthor::create([ 'name' => 'Бутурліна О.', ]); // 259
        Cauthor::create([ 'name' => 'Артем\'єва О.', ]); // 260

    }
}
