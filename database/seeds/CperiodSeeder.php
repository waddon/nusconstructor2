<?php

use Illuminate\Database\Seeder;
use App\Models\Cperiod;

class CperiodSeeder extends Seeder
{
    const DB_CONNECTION     = 'mysql';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection(self::DB_CONNECTION)->statement('SET FOREIGN_KEY_CHECKS=0;');
        Cperiod::truncate();
        DB::connection(self::DB_CONNECTION)->statement('SET FOREIGN_KEY_CHECKS=1;');

        Cperiod::create([ 'name' => '2021 - 2022 навчальний рік', ]);
    }
}
